/**
 * @api {get} /limit Get Master data Limmit
 * @apiVersion 1.0.0
 * @apiName Master Data Limit
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {uuid} uuid  uuid
 * @apiParam {String} sales_org_id  sales_org_id
 * @apiParam {String} sales_org_name  sales_org_name
 * @apiParam {float} min_qty  min_qty
 * @apiParam {float} max_qty  max_qty
 * @apiParam {Integer} duration_date  duration_date
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid
 * @apiSuccess {String} data.sales_org_id  sales_org_id
 * @apiSuccess {String} data.sales_org_name  sales_org_name
 * @apiSuccess {float} data.min_qty  min_qty
 * @apiSuccess {float} data.max_qty  max_qty
 * @apiSuccess {Integer} data.duration_date  duration_date
 * @apiSuccess {String} data.sales_office_name
 * @apiSuccess {string} data.product_name
 * @apiSuccess {String} data.incoterm
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */

/**
 * @api {get} /limit/:uuid Detail Data
 * @apiVersion 1.0.0
 * @apiName Detail Data
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid
 * @apiSuccess {String} data.sales_org_id  sales_org_id
 * @apiSuccess {String} data.sales_org_name  sales_org_name
 * @apiSuccess {float} data.min_qty  min_qty
 * @apiSuccess {float} data.max_qty  max_qty
 * @apiSuccess {Integer} data.duration_date  duration_date
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */

 /**
 * @api {put} /limit/:uuid Update Data
 * @apiVersion 1.0.0
 * @apiName Update Data
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_org_id  sales_org_id
 * @apiParam {float} min_qty  min_qty
 * @apiParam {float} max_qty  max_qty
 * @apiParam {Integer} duration_date  duration_date
 * @apiParam {Integer} product_id product Id
 * @apiParam {String} sales_office_id sales Office Id
 * @apiParam {String} incoterm incoterm
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid
 * @apiSuccess {String} data.sales_org_id  sales_org_id
 * @apiSuccess {String} data.sales_org_name  sales_org_name
 * @apiSuccess {float} data.min_qty  min_qty
 * @apiSuccess {float} data.max_qty  max_qty
 * @apiSuccess {Integer} data.duration_date  duration_date
 * @apiSuccess {String} data.sales_office_name
 * @apiSuccess {string} data.product_name
 * @apiSuccess {String} data.incoterm
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */

 /**
 * @api {post} /limit Create Limit
 * @apiVersion 1.0.0
 * @apiName Create Limit
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_org_id  sales_org_id
 * @apiParam {float} min_qty  min_qty
 * @apiParam {float} max_qty  max_qty
 * @apiParam {Integer} duration_date  duration_date
 * @apiParam {Integer} product_id product Id
 * @apiParam {String} sales_office_id sales Office Id
 * @apiParam {String} incoterm incoterm
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid
 * @apiSuccess {String} data.sales_org_id  sales_org_id
 * @apiSuccess {String} data.sales_org_name  sales_org_name
 * @apiSuccess {float} data.min_qty  min_qty
 * @apiSuccess {float} data.max_qty  max_qty
 * @apiSuccess {Integer} data.duration_date  duration_date
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */

 /**
 * @api {delete} /limit/:uuid Delete Limit
 * @apiVersion 1.0.0
 * @apiName Delete Limit
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid  uuid
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid
 * @apiSuccess {String} data.sales_org_id  sales_org_id
 * @apiSuccess {String} data.sales_org_name  sales_org_name
 * @apiSuccess {float} data.min_qty  min_qty
 * @apiSuccess {float} data.max_qty  max_qty
 * @apiSuccess {Integer} data.duration_date  duration_date
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */


 /**
 * @api {post} /limit/upload Upload Limit
 * @apiVersion 1.0.0
 * @apiName Upload Limit
 * @apiGroup Limit
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {file} file  the File Limit
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {array[]} data.success  Success Response
 * @apiSuccess {array[]} data.success.line  Succes Respnse Line
 * @apiSuccess {array[]} data.error  error Response
 * @apiSuccess {Array[]} data.error.line Error Response Line
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */