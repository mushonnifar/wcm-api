/**
 * @api {post} /spjb/upload Upload Excel SPJB
 * @apiVersion 1.0.0
 * @apiName Upload Excel SPJB
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Sukses Input Data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /spjb Master header
 * @apiVersion 1.0.0
 * @apiName Master Header
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer}  id  ID master Header
 * @apiParam {String}  uuid UUID master header
 * @apiParam {Integer}  year  Year
 * @apiParam {String}  no_doc  No Document
 * @apiParam {Integer}  sales_org_id  Produsen Id
 * @apiParam {String}  sales_org_name Produsen Name
 * @apiParam {Integer}  customer_id  Distributor Id
 * @apiParam {String}  customer_name Distributor Name
 * @apiParam {String}  status Status
 * @apiParam {String}  status_name  Status name
 * @apiParam {Timestamp}  created_at  tanggal Buat
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id  ID master Header
 * @apiSuccess {String} data.uuid UUID master header
 * @apiSuccess {Integer} data.year  Year
 * @apiSuccess {String} data.no_doc  No Document SPJB
 * @apiSuccess {Integer} data.sales_office_id  Provinsi ID
 * @apiSuccess {String} data.sales_office_name  Provinsi Name
 * @apiSuccess {Integer} data.sales_group_id  kabupatan ID
 * @apiSuccess {String} data.sales_group_name  kabupatan Name
 * @apiSuccess {Integer} data.sales_unit_id  Kecamatan ID
 * @apiSuccess {String} data.sales_unit_name  Kecamatan Name
 * @apiSuccess {Integer} data.sales_org_id  Produsen Id
 * @apiSuccess {String} data.sales_org_name Produsen Name
 * @apiSuccess {String} data.status Status
 * @apiSuccess {String} data.status_name  Status name
 * @apiSuccess {Timestamp} data.created_at  tanggal Buat
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /spjb/detail/{uuid} Hirarki SPJB
 * @apiVersion 1.0.0
 * @apiName Hirarki SPJB
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.hirarki Hirarkir Array
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /spjb/item/{uuid} Master Item By UUID HEADER
 * @apiVersion 1.0.0
 * @apiName Master Item By UUID HEADER
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {String} no_doc   No Documen SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {String} sales_office_name  Nama Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten
 * @apiParam {String} sales_group_name    Nama Kabupaten
 * @apiParam {Integer} sales_unit_id id Kecamatan
 * @apiParam {String} sales_unit_name   Nama Kecamatan
 * @apiParam {Integer} sales_org_id   id Produsen
 * @apiParam {String} sales_org_name  nama Produsen
 * @apiParam {Integer} customer_id    id Distributo
 * @apiParam {String} customer_name    Nama Distributor
 * @apiParam {Integer} product_id    id Product
 * @apiParam {String} product_name    Nama Product
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 * @apiParam {Integer} year    Tahun
 * @apiParam {String} status    "y"
 * @apiParam {String} status_name   "Active"
 * @apiParam {String} created_at  Tanggal Dibuat
 *
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.contract_id    id contract no doc SPJB
 * @apiSuccess {String} data.no_doc   No Documen SPJB
 * @apiSuccess {Integer} data.sales_office_id   id Provinsi
 * @apiSuccess {String} data.sales_office_name  Nama Provinsi
 * @apiSuccess {Integer} data.sales_group_id    id Kabupaten
 * @apiSuccess {String} data.sales_group_name    Nama Kabupaten
 * @apiSuccess {Integer} data.sales_unit_id id Kecamatan
 * @apiSuccess {String} data.sales_unit_name   Nama Kecamatan
 * @apiSuccess {Integer} data.sales_org_id   id Produsen
 * @apiSuccess {String} data.sales_org_name  nama Produsen
 * @apiSuccess {Integer} data.customer_id    id Distributo
 * @apiSuccess {String} data.customer_name    Nama Distributor
 * @apiSuccess {Integer} data.product_id    id Product
 * @apiSuccess {String} data.product_name    Nama Product
 * @apiSuccess {Integer} data.month    Bulan
 * @apiSuccess {Integer} data.initial_qty    Jumlah Qty
 * @apiSuccess {Integer} data.year    Tahun
 * @apiSuccess {String} data.status    "y"
 * @apiSuccess {String} data.status_name   "Active"
 * @apiSuccess {String} data.created_at  Tanggal Dibuat
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

  /**
 * @api {get} /spjb/item/detail/{uuid} GET Detail Contruct item
 * @apiVersion 1.0.0
 * @apiName GET Detail Contruct item
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.contract_id    id contract no doc SPJB
 * @apiSuccess {String} data.no_doc   No Documen SPJB
 * @apiSuccess {Integer} data.sales_office_id   id Provinsi
 * @apiSuccess {String} data.sales_office_name  Nama Provinsi
 * @apiSuccess {Integer} data.sales_group_id    id Kabupaten
 * @apiSuccess {String} data.sales_group_name    Nama Kabupaten
 * @apiSuccess {Integer} data.sales_unit_id id Kecamatan
 * @apiSuccess {String} data.sales_unit_name   Nama Kecamatan
 * @apiSuccess {Integer} data.sales_org_id   id Produsen
 * @apiSuccess {String} data.sales_org_name  nama Produsen
 * @apiSuccess {Integer} data.customer_id    id Distributo
 * @apiSuccess {String} data.customer_name    Nama Distributor
 * @apiSuccess {Integer} data.product_id    id Product
 * @apiSuccess {String} data.product_name    Nama Product
 * @apiSuccess {Integer} data.month    Bulan
 * @apiSuccess {Integer} data.initial_qty    Jumlah Qty
 * @apiSuccess {Integer} data.year    Tahun
 * @apiSuccess {String} data.status    "y"
 * @apiSuccess {String} data.status_name   "Active"
 * @apiSuccess {String} data.created_at  Tanggal Dibuat
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /spjb/item Create SPJB ITEM
 * @apiVersion 1.0.0
 * @apiName Create SPJB ITEM
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten
 * @apiParam {Integer} sales_unit_id id Kecamatan
 * @apiParam {Integer} product_id    id Product
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 * @apiParam {Integer} year    Tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {put} /spjb/item/{uuid} UPDATE SPJB ITEM
 * @apiVersion 1.0.0
 * @apiName UPDATE SPJB ITEM
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten
 * @apiParam {Integer} sales_unit_id id Kecamatan
 * @apiParam {Integer} product_id    id Product
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 * @apiParam {Integer} year    Tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /spjb/item/update/bulk UPDATE Bulk Status SPJB ITEM
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB ITEM
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request-Example (data):
 * [ { "id":"7B822CC3-E5D8-33A7-88BD-C512F5CFB351", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /spjb/group Find Seles Group By SPjb
 * @apiVersion 1.0.0
 * @apiName Find Seles Group By SPjb
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {uuid} uuid Contact Header (tbl wcm_contract)
 * @apiParam {integer} sales_office_id Provinsi ID
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Sukses Input Data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /spjb/export/ export spjb asal item
 * @apiVersion 1.0.0
 * @apiName export SPJB
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {get} /spjb/download export spjb asal header
 * @apiVersion 1.0.0
 * @apiName export spjb asal header
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /spjb/statusbulk UPDATE Bulk Status SPJB HEADER WITH ITEM
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB HEADER WITH ITEM
 * @apiGroup Spjb
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request-Example (data):
 * [ { "uuid":"7B822CC3-E5D8-33A7-88BD-C512F5CFB351", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */