/**
 * @api {get} /gudang Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Gudang
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id_customer Untuk Sub menu di Distributor
 * @apiParam {String} sub_distributor Untuk Sub menu di Distributor tanpa selection region
 * @apiParam {String} customer_id id customer / kode
 * @apiParam {String} sales_area_id id sales area
 * @apiParam {String} term_of_payment term of payment
 * @apiParam {String} top_dp top dp
 * @apiParam {String} tax_classification tax classification
 * @apiParam {String} pph22 pph22
 * @apiParam {String} top_up_uom top up uom
 * @apiParam {uuid} customer_uuid uuid customer / distributor
 * @apiParam {String} customer_name nama customer / distributor
 * @apiParam {String} owner nama pemilik
 * @apiParam {String} address alamat
 * @apiParam {Numeric} tlp_no nomor telepon
 * @apiParam {Numeric} fax_no nomor fax
 * @apiParam {String} sales_unit_name nama sales unit
 * @apiParam {String} sales_group_name nama sales group
 * @apiParam {String} sales_office_name nama sales office
 * @apiParam {Char} status status
 * @apiParam {Date} created_at waktu create data (dd-mm-yyyy), multiple date gunakan separator ";"
 * @apiParam {Date} updated_at waktu update data (dd-mm-yyyy), multiple date gunakan separator ";"
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales area
 * @apiSuccess {String} data.uuid uuid sales area
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_uuid uuid customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.owner nama pemilik
 * @apiSuccess {String} data.sales_area_id id sales area
 * @apiSuccess {String} data.term_of_payment term of payment
 * @apiSuccess {String} data.top_dp top dp
 * @apiSuccess {String} data.tax_classification tax classification
 * @apiSuccess {String} data.pph22 pph22
 * @apiSuccess {String} data.top_up_uom top up uom 
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {Numeric} data.tlp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /gudang/id/:uuid Get Gudang By ID
 * @apiVersion 1.0.0
 * @apiName Get Gudang By ID
 * @apiGroup Gudang
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid customer sales area
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales area
 * @apiSuccess {String} data.uuid uuid sales area
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.sales_area_id id sales area
 * @apiSuccess {String} data.term_of_payment term of payment
 * @apiSuccess {String} data.top_dp top dp
 * @apiSuccess {String} data.tax_classification tax classification
 * @apiSuccess {String} data.pph22 pph22
 * @apiSuccess {String} data.top_up_uom top up uom
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.owner nama pemilik
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {Numeric} data.tlp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /gudang/shipto Get Gudang Ship To Party
 * @apiVersion 1.0.0
 * @apiName Get Gudang Ship To Party
 * @apiGroup Gudang
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} customer_id id customer
 * @apiParam {String} sales_org_id id sales org
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_id nama customer
 * @apiSuccess {String} data.partner_type tipe partner
 * @apiSuccess {String} data.partner_type_desc deskripsi tipe partner
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
