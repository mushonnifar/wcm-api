/**
 * @api {get} /log-host-to-host Get Log Host To Host
 * @apiVersion 1.0.0
 * @apiName Get Log Order
 * @apiGroup Log Host To Host
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} order_id order_id
 * @apiParam {String} timestamp_req timestamp_req
 * @apiParam {String} timestamp_respon timestamp_respon
 * @apiParam {String} pengirim_req pengirim_req
 * @apiParam {String} ip_address_req ip_address_req
 * @apiParam {String} http_status http_status
 * @apiParam {String} ip_address_respon ip_address_respon
 * @apiParam {String} pengirim_respon pengirim_respon
 * @apiParam {String} request_type request_type [CHECK_LIMIT, PAYMENT, INQUIRY, SEND_INVOICE]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id log order
 * @apiSuccess {Integer} data.order_id id order
 * @apiSuccess {Timestamp} data.timestamp_req timestamp_req
 * @apiSuccess {String} data.ip_address_req ip_address_req
 * @apiSuccess {String} data.pengirim_req pengirim_req
 * @apiSuccess {Json} data.isi_req isi_req
 * @apiSuccess {String} data.pengirim_req pengirim_req
 * @apiSuccess {String} data.http_status http_status
 * @apiSuccess {String} data.ip_address_respon ip_address_respon
 * @apiSuccess {String} data.pengirim_respon pengirim_respon
 * @apiSuccess {Json} data.isi_respon isi_respon
 * @apiSuccess {String} data.request_type request_type
 * @apiSuccess {Timestamp} data.timestamp_respon timestamp_respon
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
