 /**
  * @api {get} /monitoringdo Master Data
  * @apiVersion 1.0.0
  * @apiName Master Data
  * @apiGroup Monitoring DO
  *
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  *
  * @apiParam {String} number  number
  * @apiParam {Integer} sales_org_id  sales_org_id
  * @apiParam {String} sales_org_name  sales_org_name
  * @apiParam {Integer} customer_id  customer_id
  * @apiParam {String} customer_name  customer_name
  * @apiParam {Timestamp} order_date  order_date
  * @apiParam {number} quantity_qty  quantity_qty
  * @apiParam {String} code_so  code_so
  * @apiParam {String} no_doc  no_doc
  * @apiParam {Integer} delivery_id  delivery_id
  * @apiParam {Integer} product_id  product_id
  * @apiParam {String} product_name  product_name
  * @apiParam {number} delivery_qty  delivery_qty
  * @apiParam {Timestamp} delivery_date  delivery_date
  * @apiParam {String} created_by  created_by
  * @apiParam {Timestamp} created_at  created_at
  *
  * @apiSuccess {Integer} status 1
  * @apiSuccess {String} status_txt success
  * @apiSuccess {String} message pesan sukses
  * @apiSuccess {Array[]} data array data
  * @apiSuccess {String} data.number  number
  * @apiSuccess {Integer} data.sales_org_id  sales_org_id
  * @apiSuccess {String} data.sales_org_name  sales_org_name
  * @apiSuccess {Integer} data.customer_id  customer_id
  * @apiSuccess {String} data.customer_name  customer_name
  * @apiSuccess {Timestamp} data.order_date  order_date
  * @apiSuccess {number} data.quantity_qty  quantity_qty
  * @apiSuccess {String} data.code_so  code_so
  * @apiSuccess {String} data.no_doc  no_doc
  * @apiSuccess {Integer} data.delivery_id  delivery_id
  * @apiSuccess {Integer} data.product_id  product_id
  * @apiSuccess {String} data.product_name  product_name
  * @apiSuccess {number} data.delivery_qty  delivery_qty
  * @apiSuccess {Timestamp} data.delivery_date  delivery_date
  * @apiSuccess {String} data.created_by  created_by
  * @apiSuccess {Timestamp} data.created_at  created_at
  *
  * @apiError {Integer} status 0
  * @apiError {String} status_txt errors
  * @apiError {String} message pesan kesalahan
  */
 /**
  * @api {post} /monitoringdo Create Monitoring Do
  * @apiVersion 1.0.0
  * @apiName Create Monitoring DO
  * @apiGroup Monitoring DO
  *
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * @apiParamExample {json} Request Example:
  * [
  *   {
  *       "number": "ABCD00004",
  *       "kode_so": 99999,
  *       "delivery_date": "2019-01-01",
  *       "product_id": "P000",
  *       "delivery_qty" : 10
  *   },
  * ]
  *
  * @apiSuccess {Integer} status 1
  * @apiSuccess {String} status_txt success
  * @apiSuccess {String} message pesan sukses
  * @apiSuccess {Array[]} data array data
  * @apiSuccess {Array[]} data.inserted array data
  * @apiSuccess {String} data.inserted.number  number
  * @apiSuccess {Integer} data.inserted.id  id
  * @apiSuccess {Integer} data.inserted.order_id  order id
  * @apiSuccess {Timestamp} data.inserted.delivery_date  delivery_date
  * @apiSuccess {String} data.inserted.created_by  created_by
  * @apiSuccess {Timestamp} data.inserted.created_at  created_at
  * @apiSuccess {Array[]} data.failed array data
  * @apiSuccess {Integer} data.failed.number  number
  * @apiSuccess {Integer} data.failed.kode_so  kode_so
  * @apiSuccess {Timestamp} data.failed.delivery_date  delivery_date
  *
  * @apiError {Integer} status 0
  * @apiError {String} status_txt errors
  * @apiError {String} message pesan kesalahan
  *
  *
  */

/**
  * @api {post} /monitoringdo/reverse Reverse Monitoring Do
  * @apiVersion 1.0.0
  * @apiName Reverse Monitoring DO
  * @apiGroup Monitoring DO
  *
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * @apiParam {String} kode_so the so number
  * @apiParam {String} number_do the number delivery order
  * @apiSuccess {Integer} status 1
  * @apiSuccess {String} status_txt success
  * @apiSuccess {String} message pesan sukses
  * @apiSuccess {Array[]} data array data
  * @apiSuccess {Array[]} data.count count deleted
  *
  * @apiError {Integer} status 0
  * @apiError {String} status_txt errors
  * @apiError {String} message pesan kesalahan
  *
  *
  */
