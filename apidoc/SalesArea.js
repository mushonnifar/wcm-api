/**
 * @api {get} /salesarea Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Sales Area
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {uuid} customer_uuid uuid customer / distributor
 * @apiParam {String} customer_id id customer
 * @apiParam {String} customer_name nama customer
 * @apiParam {String} sales_group_id id sales group
 * @apiParam {String} sales_group_name nama sales group
 * @apiParam {String} distrib_channel_id id distribution channel
 * @apiParam {String} distrib_channel_name nama distribution channel
 * @apiParam {String} sales_division_id id sales division
 * @apiParam {String} sales_division_name nama sales division
 * @apiParam {String} term_of_payment term of payment
 * @apiParam {String} top_dp top dp
 * @apiParam {String} tax_classification tax classification
 * @apiParam {String} pph22 pph22
 * @apiParam {String} top_dp_uom top up uom
 * @apiParam {String} payment_method payment method name
 * @apiParam {Date} created_at waktu create data (dd-mm-yyyy), multiple date gunakan separator ";"
 * @apiParam {Date} updated_at waktu update data (dd-mm-yyyy), multiple date gunakan separator ";"
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales area
 * @apiSuccess {String} data.uuid uuid sales area
 * @apiSuccess {uuid} data.customer_uuid uuid customer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.distrib_channel_id id distribution channel
 * @apiSuccess {String} data.distrib_channel_name nama distribution channel
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.sales_division_name nama sales division
 * @apiSuccess {String} data.term_of_payment term of payment
 * @apiSuccess {String} data.top_dp top dp
 * @apiSuccess {String} data.tax_classification tax classification
 * @apiSuccess {String} data.pph22 pph22
 * @apiSuccess {String} data.top_dp_uom top up uom
 * @apiSuccess {String} data.payment_method payment method
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /salesarea/id/:uuid Get Sales Area By ID
 * @apiVersion 1.0.0
 * @apiName Get Sales Area By ID
 * @apiGroup Sales Area
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid sales area
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales area
 * @apiSuccess {String} data.uuid uuid sales area
 * @apiSuccess {uuid} data.customer_uuid uuid customer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.distrib_channel_id id distribution channel
 * @apiSuccess {String} data.distrib_channel_name nama distribution channel
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.sales_division_name nama sales division
 * @apiSuccess {String} data.term_of_payment term of payment
 * @apiSuccess {String} data.top_dp top dp
 * @apiSuccess {String} data.tax_classification tax classification
 * @apiSuccess {String} data.pph22 pph22
 * @apiSuccess {String} data.top_dp_uom top up uom
 * @apiSuccess {String} data.payment_method payment method
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /salesarea/customer Get Customer Sales Area
 * @apiVersion 1.0.0
 * @apiName Get Customer Sales Area
 * @apiGroup Sales Area
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} customer_id id customer
 * @apiParam {String} sales_org_id id sales org
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id customer sales area
 * @apiSuccess {String} data.uuid uuid customer sales area
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.distrib_channel_id id distribution channel
 * @apiSuccess {String} data.distrib_channel_name nama distribution channel
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.sales_division_name nama sales division
 * @apiSuccess {String} data.term_of_payment term of payment
 * @apiSuccess {String} data.top_dp top dp
 * @apiSuccess {String} data.tax_classification tax classification
 * @apiSuccess {String} data.pph22 pph22
 * @apiSuccess {String} data.top_dp_uom top up uom
 * @apiSuccess {String} data.payment_method payment method
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
