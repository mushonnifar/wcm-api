
 /**
 * @api {get} /spjb/critem/{uuid} Master header
 * @apiVersion 1.0.0
 * @apiName Detail CR SPJB
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer}  uuid  uuid master Header
 * @apiParam {String} sales_org_id      sales_org_id
 * @apiParam {String} sales_org_name    sales_org_name
 * @apiParam {String} number            number
 * @apiParam {String} contract_type     contract_type
 * @apiParam {Integer} sales_office_id   sales_office_id
 * @apiParam {String} sales_office_name sales_office_name
 * @apiParam {Integer} sales_group_id    sales_group_id
 * @apiParam {String} sales_group_name  sales_group_name
 * @apiParam {String} product_id        product_id
 * @apiParam {String} product_name      product_name
 * @apiParam {Integer} sales_unit_id      sales_unit_id
 * @apiParam {String} sales_unit_name      sales_unit_name
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} uuid uuid
 * @apiSuccess {String} sales_org_id sales_org_id
 * @apiSuccess {String} sales_org_name sales_org_name
 * @apiSuccess {String} number number
 * @apiSuccess {String} contract_type contract_type
 * @apiSuccess {Integer} year year
 * @apiSuccess {Integer} sales_office_id sales_office_id
 * @apiSuccess {String} sales_office_name sales_office_name
 * @apiSuccess {Integer} sales_group_id sales_group_id
 * @apiSuccess {String} sales_group_name sales_group_name
 * @apiSuccess {Integer} product_id product_id
 * @apiSuccess {String} product_name product_name
 * @apiSuccess {Integer} sales_unit_id      sales_unit_id
 * @apiSuccess {String} sales_unit_name      sales_unit_name
 * @apiSuccess {Float} 1 Month 1
 * @apiSuccess {char} Status1 Status Bulan 1
 * @apiSuccess {Float} 2 Month 2
 * @apiSuccess {char} Status2 Status Bulan 2
 * @apiSuccess {Float} 3 Month 3
 * @apiSuccess {char} Status3 Status Bulan 3
 * @apiSuccess {Float} 4 Month 4
 * @apiSuccess {char} Status4 Status Bulan 4
 * @apiSuccess {Float} 5 Month 5
 * @apiSuccess {char} Status5 Status Bulan 5
 * @apiSuccess {Float} 6 Month 6
 * @apiSuccess {char} Status6 Status Bulan 6
 * @apiSuccess {Float} 7 Month 7
 * @apiSuccess {char} Status7 Status Bulan 7
 * @apiSuccess {Float} 8 Month 8
 * @apiSuccess {char} Status8 Status Bulan 8
 * @apiSuccess {Float} 9 Month 9
 * @apiSuccess {char} Status9 Status Bulan 9
 * @apiSuccess {Float} 10 Month 10
 * @apiSuccess {char} Status10 Status Bulan 10
 * @apiSuccess {Float} 11 Month 11
 * @apiSuccess {char} Status11 Status Bulan 11
 * @apiSuccess {Float} 12 Month 12
 * @apiSuccess {char} Status12 Status Bulan 12
 * @apiSuccess {Float} TOTAL TOTAL ALL MONTH 

 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {put} /spjb/asal/critem/{uuid} UPDATE AND CREATE SPJB ASAL ITEM
 * @apiVersion 1.0.0
 * @apiName UPDATE AND CREATE SPJB ASAL ITEM
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten
 * @apiParam {Integer} sales_unit_id id Kecamatan
 * @apiParam {Integer} product_id    id Product
 * @apiParam {Integer} year    Tahun
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /spjb/operational/critem/{uuid} UPDATE SPJB OPERATIONAL ITEM
 * @apiVersion 1.0.0
 * @apiName UPDATE SPJB OPERATIONAL ITEM
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten
 * @apiParam {Integer} sales_unit_id id Kecamatan
 * @apiParam {Integer} product_id    id Product
 * @apiParam {Integer} year    Tahun
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {PUT} /spjb/crstatus/operational UPDATE Bulk Status SPJB OPR ITEM CR
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB OPR ITEM CR
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 * [
	{
		"contract_id":"214",
		"product_id":"P01",
		"sales_office_id":"0035",
		"sales_group_id":"3519",
		"sales_unit_id":"351903",
		"month":"01",
		"year":"2019",
		"status": "y"
	},
	{
		"contract_id":"214",
		"product_id":"P02",
		"sales_office_id":"0035",
		"sales_group_id":"3519",
		"sales_unit_id":"351903",
		"month":"01",
		"year":"2019",
		"status": "y"
	}
]
 *  
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {PUT} /spjb/crstatus/asal UPDATE Bulk Status SPJB ASAL ITEM CR
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB ASAL ITEM CR
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 * [
	{
		"contract_id":"214",
		"product_id":"P01",
		"sales_office_id":"0035",
		"sales_group_id":"3519",
		"sales_unit_id":"351903",
		"month":"01",
		"year":"2019",
		"status": "y"
	},
	{
		"contract_id":"214",
		"product_id":"P02",
		"sales_office_id":"0035",
		"sales_group_id":"3519",
		"sales_unit_id":"351903",
		"month":"01",
		"year":"2019",
		"status": "y"
	}
]
 *  
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



  /**
 * @api {PUT} /spjb/crqty/operational UPDATE Bulk Status SPJB OPERATIONAL ITEM CR
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB OPERATIONAL ITEM CR
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 * [
	 {
        "contract_id": 509,
        "sales_group_id": 3517,
        "sales_unit_id": 351703,
        "sales_office_id": "0035",
        "product_id": "P01",
        "month": 1,
        "initial_qty": 505,
        "year": 2020
    },
    {
        "contract_id": 509,
        "sales_group_id": 3517,
        "sales_unit_id": 351703,
        "sales_office_id": "0035",
        "product_id": "P01",
        "month": 2,
        "initial_qty": 505,
        "year": 2020
    }
]
 *  
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {PUT} /spjb/crqty/asal UPDATE Bulk Status SPJB ASAL ITEM CR
 * @apiVersion 1.0.0
 * @apiName UPDATE Bulk Status SPJB ASAL ITEM CR
 * @apiGroup CR SPJB
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 * [
	 {
        "contract_id": 509,
        "sales_group_id": 3517,
        "sales_unit_id": 351703,
        "sales_office_id": "0035",
        "product_id": "P01",
        "month": 1,
        "initial_qty": 505,
        "year": 2020
    },
    {
        "contract_id": 509,
        "sales_group_id": 3517,
        "sales_unit_id": 351703,
        "sales_office_id": "0035",
        "product_id": "P01",
        "month": 2,
        "initial_qty": 505,
        "year": 2020
    }
]
 *  
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */