/**
 * @api {post} /permission Create Permission Route
 * @apiVersion 1.0.0
 * @apiName Create Permission Route
 * @apiGroup Permission
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} guard_name nama guard (api/web)
 * @apiParam {Integer} route_id id route
 * @apiParam {Integer} action_id id action
 * @apiParam {String} method nama method. ex: GET,POST,PUT,DELETE. etc...
 * @apiParam {String} url link/url route
 * @apiParam {String} path nama controller dan fungsi ex: UserController@index, etc...
 * @apiParam {String} middleware midleware yang digunakan pada route tertentu. ex: jwt.auth,jwt.refresh etc...
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id permission
 * @apiSuccess {String} data.name nama guard
 * @apiSuccess {String} data.guard_name nama guard
 * @apiSuccess {String} data.name nama guard
 * @apiSuccess {String} data.created_by pembuat
 * @apiSuccess {String} data.updated_by pengubah
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Array[]} data.has_routes array data detail permission (data baru yang berhasil dibuat)
 * @apiSuccess {Integer} data.has_routes.id id detail permission
 * @apiSuccess {Integer} data.has_routes.permission_id id permission (parent)
 * @apiSuccess {String} data.has_routes.method nama method. ex: GET,POST,PUT,DELETE. etc...
 * @apiSuccess {String} data.has_routes.url link/url route
 * @apiSuccess {String} data.has_routes.path nama controller dan fungsi ex: UserController@index, etc...
 * @apiSuccess {String} data.has_routes.middleware midleware yang digunakan pada route tertentu. ex: jwt.auth,jwt.refresh etc...
 * @apiSuccess {String} data.has_routes.status status
 * @apiSuccess {String} data.has_routes.created_by pembuat
 * @apiSuccess {String} data.has_routes.updated_by pengubah
 * @apiSuccess {Timestamp} data.has_routes.created_at waktu create data
 * @apiSuccess {Timestamp} data.has_routes.updated_at waktu update data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /permission/:id_detail_permission  Update Permission Route
 * @apiVersion 1.0.0
 * @apiName Update Permission Route
 * @apiGroup Permission
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} guard_name nama guard (api/web)
 * @apiParam {Integer} route_id id route
 * @apiParam {Integer} action_id id action
 * @apiParam {String} method nama method. ex: GET,POST,PUT,DELETE. etc...
 * @apiParam {String} url link/url route
 * @apiParam {String} path nama controller dan fungsi ex: UserController@index, etc...
 * @apiParam {String} middleware midleware yang digunakan pada route tertentu. ex: jwt.auth,jwt.refresh etc...
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id permission
 * @apiSuccess {String} data.name nama guard
 * @apiSuccess {String} data.guard_name nama guard
 * @apiSuccess {String} data.name nama guard
 * @apiSuccess {String} data.created_by pembuat
 * @apiSuccess {String} data.updated_by pengubah
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Array[]} data.has_routes array data detail permission (data baru yang berhasil dibuat)
 * @apiSuccess {Integer} data.has_routes.id id detail permission
 * @apiSuccess {Integer} data.has_routes.permission_id id permission (parent)
 * @apiSuccess {String} data.has_routes.method nama method. ex: GET,POST,PUT,DELETE. etc...
 * @apiSuccess {String} data.has_routes.url link/url route
 * @apiSuccess {String} data.has_routes.path nama controller dan fungsi ex: UserController@index, etc...
 * @apiSuccess {String} data.has_routes.middleware midleware yang digunakan pada route tertentu. ex: jwt.auth,jwt.refresh etc...
 * @apiSuccess {String} data.has_routes.status status
 * @apiSuccess {String} data.has_routes.created_by pembuat
 * @apiSuccess {String} data.has_routes.updated_by pengubah
 * @apiSuccess {Timestamp} data.has_routes.created_at waktu create data
 * @apiSuccess {Timestamp} data.has_routes.updated_at waktu update data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
 
/**
 * @api {delete} /permission/:id_detail_permission Delete Permission
 * @apiVersion 1.0.0
 * @apiName Delete Permission
 * @apiGroup Permission
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} id id detail permission
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {String} data true
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */