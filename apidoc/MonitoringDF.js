/**
 * @api {get} /monitoringdf Get Data Monitoring DF
 * @apiVersion 1.0.0
 * @apiName Get Data Monitoring DF
 * @apiGroup Monitoring DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} no_penebusan nomor penebusan
 * @apiParam {String} kode_referensi kode referensi
 * @apiParam {String} produsen produsen
 * @apiParam {String} distributor distributor
 * @apiParam {String} bank bank
 * @apiParam {String} tgl_pelunasan tanggal pelunasan
 * @apiParam {String} tgl_rencana tanggal rencana pelunasan df
 * @apiParam {String} tgl_order tanggal_order
 * @apiParam {String} data.status_order nama status order(Approve = y; Unapprove = s(submit); Suspend = p; Unsuspend = y(approve); PAID = l; DPPAID = u; CLOSE = x; REVIEWED = r; COMPLETE = c)
 * @apiParam {String} kode_so kode sales order
 * @apiParam {String} provinsi provinsi
 * @apiParam {String} kabupaten kabupaten
 * @apiParam {String} total_price total harga
 * @apiParam {String} tipe_pembayaran tipe pembayaran
 * @apiParam {String} disbursement_status dikripsi status disbursement
 * @apiParam {String} status_sent nama status sent (Success = 1; Failed = 0)
 * @apiParam {String} created_at tanggal pembuatan data
 * @apiParam {String} updated_at tanggal update data
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor penebusan
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {String} data.bank_name nama bank
 * @apiSuccess {String} data.billing_fulldate tanggal pelunasan
 * @apiSuccess {String} data.df_due_date tanggal rencana pelunasan df
 * @apiSuccess {String} data.order_date tanggal order
 * @apiSuccess {String} data.status status order
 * @apiSuccess {String} data.error_message_send_order pesan gagal pengiriman Host to Host
 * @apiSuccess {String} data.so_number nomor sales_order
 * @apiSuccess {String} data.sales_office_id id provinsi
 * @apiSuccess {String} data.sales_office_name nama provinsi
 * @apiSuccess {String} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.sales_group_name nama kabupaten
 * @apiSuccess {String} data.payment_method tipe pembayaran
 * @apiSuccess {String} data.total_price total harga
 * @apiSuccess {String} data.is_sent status pengiriman order
 * @apiSuccess {String} data.disbursement_status disbursement_status
 * @apiSuccess {String} data.disbursement_status_name disbursement_status_name
 * @apiSuccess {String} data.error_message_disbursement error message send
 * @apiSuccess {String} data.send_order_count jumlah percobaan send
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */