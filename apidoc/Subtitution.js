/**
 * @api {get} /subtitution/cust_id/{uuid} Get All Data By Customer ID
 * @apiVersion 1.0.0
 * @apiName Get All Data by Customer ID
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Uuid} uuid
 * @apiSuccess {Integer} product_id
 * @apiSuccess {String} name_product
 * @apiSuccess {Integer} sales_org_id
 * @apiSuccess {String} name_sales_org
 * @apiSuccess {Integer} plant_id
 * @apiSuccess {String} name_plane
 * @apiSuccess {Integer} material_no
 * @apiSuccess {String} material_name
 * @apiSuccess {Integer} material_no_subtitution
 * @apiSuccess {String} material_name_subtitusi
 * @apiSuccess {String} uom
 * @apiSuccess {String} reason
 * @apiSuccess {Date} valid_from
 * @apiSuccess {Date} valid_to
 * @apiSuccess {String} default
 * @apiSuccess {String} status
 * @apiSuccess {Timestamp} updated_at
 * @apiSuccess {Timestamp} created_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /subtitution Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Uuid} uuid
 * @apiSuccess {Integer} product_id
 * @apiSuccess {String} name_product
 * @apiSuccess {Integer} sales_org_id
 * @apiSuccess {String} name_sales_org
 * @apiSuccess {Integer} plant_id
 * @apiSuccess {String} name_plane
 * @apiSuccess {Integer} material_no
 * @apiSuccess {String} material_name
 * @apiSuccess {Integer} material_no_subtitution
 * @apiSuccess {String} material_name_subtitusi
 * @apiSuccess {String} uom
 * @apiSuccess {String} reason
 * @apiSuccess {Date} valid_from
 * @apiSuccess {Date} valid_to
 * @apiSuccess {String} default
 * @apiSuccess {String} status
 * @apiSuccess {Timestamp} updated_at
 * @apiSuccess {Timestamp} created_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /subtitution/id/:uuid Get All Data by UUID
 * @apiVersion 1.0.0
 * @apiName Get Data By UUID
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Uuid} uuid
 * @apiSuccess {Integer} product_id
 * @apiSuccess {String} name_product
 * @apiSuccess {Integer} sales_org_id
 * @apiSuccess {String} name_sales_org
 * @apiSuccess {Integer} plant_id
 * @apiSuccess {String} name_plane
 * @apiSuccess {Integer} material_no
 * @apiSuccess {String} material_name
 * @apiSuccess {Integer} material_no_subtitution
 * @apiSuccess {String} material_name_subtitusi
 * @apiSuccess {String} uom
 * @apiSuccess {String} reason
 * @apiSuccess {Date} valid_from
 * @apiSuccess {Date} valid_to
 * @apiSuccess {String} default
 * @apiSuccess {String} status
 * @apiSuccess {Timestamp} updated_at
 * @apiSuccess {Timestamp} created_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {post} /subtitution/  Create Data
 * @apiVersion 1.0.0
 * @apiName Create Data
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam {Integer} product_id
 * @apiParam {Integer} sales_org_id
 * @apiParam {Integer} plant_id
 * @apiParam {Integer} material_no
 * @apiParam {Integer} material_no_subtitution
 * @apiParam {String} uom
 * @apiParam {String} reason
 * @apiParam {Date} valid_from
 * @apiParam {Date} valid_to
 * 
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} product_id
 * @apiSuccess {Integer} sales_org_id
 * @apiSuccess {Integer} plant_id
 * @apiSuccess {Integer} material_no
 * @apiSuccess {Integer} material_no_subtitution
 * @apiSuccess {String} uom
 * @apiSuccess {String} reason
 * @apiSuccess {Date} valid_from
 * @apiSuccess {Date} valid_to
 * @apiSuccess {String} default
 * @apiSuccess {String} status
 * @apiSuccess {Timestamp} updated_at
 * @apiSuccess {Timestamp} created_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /subtitution/:uuid  update Data
 * @apiVersion 1.0.0
 * @apiName Update Data
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam {Integer} material_no_subtitution
 * @apiParam {String} uom
 * @apiParam {String} reason
 * 
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} product_id
 * @apiSuccess {Integer} sales_org_id
 * @apiSuccess {Integer} plant_id
 * @apiSuccess {Integer} material_no
 * @apiSuccess {Integer} material_no_subtitution
 * @apiSuccess {String} uom
 * @apiSuccess {String} reason
 * @apiSuccess {Date} valid_from
 * @apiSuccess {Date} valid_to
 * @apiSuccess {String} default
 * @apiSuccess {String} status
 * @apiSuccess {Timestamp} updated_at
 * @apiSuccess {Timestamp} created_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /subtitution/bulk Update Multiple Status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update - multiple status
 * @apiGroup Master Subtitution
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [
	{
		"id":"65FBF6F9-939F-4C45-81DC-A3B7AE0A8A20",
		"status":"n"
	},
		{
		"id":"6A0F448E-BFE5-4D53-AAF0-372825824229",
		"status":"n"
	}
]
 * @apiParam {String} data.status status penyimpanan (y: active, n:inactive)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data  
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */