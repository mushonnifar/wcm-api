/**
* @api {get} /reportf5 Master Report F5
* @apiVersion 1.0.0
* @apiName Master Report F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {uuid} uuid  uuid
* @apiParam {Integer} sales_org_id  sales_org_id
* @apiParam {String} sales_org_name  sales_org_name
* @apiParam {String} number  number
* @apiParam {Integer} customer_id  customer_id
* @apiParam {String} customer_name  customer_name
* @apiParam {Integer} month  month
* @apiParam {Integer} year  year
* @apiParam {Timestemp} submited_date  submited_date
* @apiParam {Integer} sales_office_id  sales_office_id
* @apiParam {String} sales_office_name  sales_office_name
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {String} status  status
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} uuid  uuid
* @apiSuccess {Integer} sales_org_id  sales_org_id
* @apiSuccess {String} sales_org_name  sales_org_name
* @apiSuccess {String} number  number
* @apiSuccess {Integer} customer_id  customer_id
* @apiSuccess {String} customer_name  customer_name
* @apiSuccess {Integer} month  month
* @apiSuccess {Integer} year  year
* @apiSuccess {Timestemp} submited_date  Distribution Date
* @apiSuccess {Integer} sales_office_id  sales_office_id
* @apiSuccess {String} sales_office_name  sales_office_name
* @apiSuccess {Integer} sales_group_id  sales_group_id
* @apiSuccess {String} sales_group_name  sales_group_name
* @apiSuccess {String} status  status
* @apiSuccess {String} status_name  status_name
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {post} /reportf5 Create Report F5
* @apiVersion 1.0.0
* @apiName Create Report F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*

* @apiParam {Integer} sales_org_id  sales_org_id
* @apiParam {Integer} sales_office_id sales_office_id
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {Integer} customer_id  customer_id
* @apiParam {Integer} month  month (Required)
* @apiParam {Integer} year  year (Required)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.sales_org_id  sales_org_id
* @apiSuccess {Integer} data.customer_id  customer_id
* @apiSuccess {Integer} data.sales_group_id  sales_group_id
* @apiSuccess {Integer} data.year  year
* @apiSuccess {Integer} data.month  month
* @apiSuccess {String} data.number  number
* @apiSuccess {Timestamp} data.updated_at  updated_at
* @apiSuccess {Timestamp} data.created_at  created_at
* @apiSuccess {Integer} data.id  id
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /detailreportf5/{uuid} Hirarki Report F5
* @apiVersion 1.0.0
* @apiName Hirarki Report F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.header  sales_org_id
* @apiSuccess {Integer} data.data  sales_org_id
* @apiSuccess {Integer} data.total  sales_org_id
* @apiSuccess {Integer} data.produks  sales_org_id
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /detailreportf5/sonumber/{uuid} So Number Report F5
* @apiVersion 1.0.0
* @apiName So Number Report F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /detailreportf5/pkpnumber/{uuid} No PKP Report F5
* @apiVersion 1.0.0
* @apiName No PKP Report F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/


/**
* @api {post}  /detailreportf5  Create Report F5 Detail
* @apiVersion 1.0.0
* @apiName Create Report F5 Detail
* @apiGroup report f5


* @apiParamExample {json} Request-Example (data):
* [       
       {
            "f6": {
                "uuid": "306001F7-10DD-4BE6-8C9C-73D269E3EABA",
                "number": "F60000000001",
                "report_f5_id": "1",
                "sales_org_id": "B000",
                "customer_id": "1000000000",
                "sales_group_id": "1114",
                "year": "2019",
                "month": "01",
                "status": "d"
            },
            "f5items": [
                {
                    "uuid": "4F5F513B-8500-4562-ABCF-F3E7EBD76572",
                    "product_id": "P01",
                    "report_f5_id": "1",
                    "stok_awal": 0,
                    "penebusan": 2,
                    "penyaluran": 4,
                    "stok_akhir": -2,
                    "status": "d",
                    "number_f5": "1"
                },
                {
                    "uuid": "51984CF6-1DD5-48D0-975E-37E9000DA256",
                    "product_id": "P02",
                    "report_f5_id": "1",
                    "stok_awal": 0,
                    "penebusan": 0,
                    "penyaluran": 5,
                    "stok_akhir": -5,
                    "number_f5": "1",
                    "status": "d"
                },
                {
                    "uuid": "6D1BC381-F7B5-46C9-8F19-62D5E08D6BB2",
                    "product_id": "P03",
                    "report_f5_id": "1",
                    "stok_awal": 0,
                    "penebusan": 0,
                    "penyaluran": 6,
                    "stok_akhir": 10,
                    "status": "d",
                    "number_f5": "1"
                },
                {
                    "uuid": "F8CC93CE-76CD-44FF-82D1-973C1B055E6B",
                    "product_id": "P04",
                    "report_f5_id": "1",
                    "stok_awal": 0,
                    "penebusan": 0,
                    "penyaluran": 0,
                    "stok_akhir": 0,
                    "status": "d",
                    "number_f5": "1"
                },
                {
                    "uuid": "586CEDA3-2BF0-44AF-84C5-E94DE363BB99",
                    "product_id": "P05",
                    "report_f5_id": "1",
                    "stok_awal": 0,
                    "penebusan": 0,
                    "penyaluran": 0,
                    "stok_akhir": 0,
                    "status": "d",
                    "number_f5": "1"
                }
            ]
        
        }
]
*
* 
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/


/**
* @api {get}  /reportf5/bast/{uuid} BAST F5
* @apiVersion 1.0.0
* @apiName BAST F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {file} data file array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /reportf5/download/{uuid} DOWNLOAD F5
* @apiVersion 1.0.0
* @apiName DOWNLOAD F5
* @apiGroup report f5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {file} data file array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/