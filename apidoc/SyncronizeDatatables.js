/**
 * @api {get} /sycnronize/:params get datatable syncronize
 * @apiVersion 1.0.0
 * @apiName Syncronize
 * @apiGroup Syncronize
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} params [Customer, material_list, sales_area, sales_division, sales_distribution, plant,regional]
 * 
 * @apiSuccessExample {json} Customer:
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 3339,
 *    "recordsFiltered": 3339,
 *    "data": [
 *        {
 *            "sales_office_id": 12345,
 *            "sales_office_name": 12345,
 *            "sales_group_id": 12345,
 *            "sales_group_name": 12345,
 *            "sales_unit_id": 12345,
 *            "sales_unit_name": 12345,
 *            "id": "1000000000",
 *            "full_name": "abced",
 *            "owner": "abcde",
 *            "address": 12345,
 *            "tlp_no": "08xxxxx",
 *            "fax_no": "0xxxxx",
 *            "status": "Active"
 *        },
 *    ]
 * }
 *
 * @apiSuccessExample {json} Material List:
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *        {
 *          "sales_org_id": "A000",
 *          "sales_org_name": "PT Pupuk Indonesia",
 *          "plant_code": "000",
 *			"plant_name": "Name of Plant",
 *          "name": "PT Pupuk Indonesia",
 *          "product_name": "UREA",
 *          "material_no": "1000000",
 *          "unit": "Unit Name",
 *          "mat_desc": "Material List Deskribtion" 
 *        },
 *    ]
 * }
 *
 * @apiSuccessExample {json} Sales Area:
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *        {
 *           "sales_org_id": "A000",
 *           "sales_org_name": "PT Pupuk Indonesia",
 *          "distrib_channel_id": "20",
 *           "distrib_channel_name": "Dom-Non Intercompany",
 *           "sales_division_id ": "00",
 *          "sales_division_name": "Com. Div Subsidi"
 *      },
 *    ]
 * }
 * 
 * @apiSuccessExample {json} Sales Division:
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *       {
 *          "id": "00",
 *          "name": "Sales Division"
 *       },
 *    ]
 * }
 * 
 * @apiSuccessExample {json} Sales Distribution:
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *       {
 *          "id": "00",
 *          "name": "Sales Distribution"
 *       },
 *    ]
 * }
 * 
 * @apiSuccessExample {json} Plant
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *       {
 *          "code": "ABC00000",
 *          "name": "Gudang 1",
 *          "address": "jalan palangkaraya",
 *          "sales_group_id": "1101",
 *          "sales_group_name": "Kab. Aceh Selatan",
 *          "sales_office_id": "0011",
 *          "sales_office_name": "ACEH",
 *          "sales_org_id": "C000",
 *          "sales_org_name": "PT Pupuk Kujang",
 *       },
 *    ]
 * }
 *
 * @apiSuccessExample {json} Regional
 *     HTTP/1.1 200 OK
 * {
 *    "draw": 0,
 *    "recordsTotal": 999,
 *    "recordsFiltered": 999,
 *    "data": [
 *       {
 *            "sales_office_id": "0011",
 *           "sales_office_name": "ACEH",
 *            "sales_group_id": "1101",
 *           "sales_group_name": "Kab. Aceh Selatan",
 *           "sales_unit_id": "110102",
 *          "sales_unit_name": "Kluet Utara"
 *       },
 *    ]
 * }
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {post} /syncronizes/plant/updatesbulk Update relation status - multiple (raw)
 * @apiVersion 1.0.0
 * @apiName Syncronize
 * @apiGroup Syncronize
 *
 * @apiParamExample {json} Request-Example (data):
 * [ { "id":"7B822CC3-E5D8-33A7-88BD-C512F5CFB351", "status":"d" } ]
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */
