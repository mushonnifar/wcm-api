
/**
 * @api {get} /bankdf/cust_id/:uuid Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Bank DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid customer
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id bankdf
 * @apiSuccess {String} data.customer_uuid uuid customer / distributor
 * @apiSuccess {String} data.customer_name nama customer / distributor
 * @apiSuccess {String} data.produsen_name nama produsen
 * @apiSuccess {String} data.name Nama Bank
 * @apiSuccess {Timestamp} data.start_date valid start
 * @apiSuccess {Timestamp} data.end_date valid end
 * @apiSuccess {string} data.status Active/Inactive
 * @apiSuccess {uuid} data.uuid uuid bankdf
 * @apiSuccess {string} data.status_name status_name bankdf
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



/**
 * @api {get} /bankdf/id/:uuid Get Data BY UUID
 * @apiVersion 1.0.0
 * @apiName Get Data By UUID
 * @apiGroup Bank DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer}  data.id  id
 * @apiSuccess {String}  data.uuid  uuid
 * @apiSuccess {Integer}  data.customer_id  customer_id
 * @apiSuccess {String}  data.customer_name  customer_name
 * @apiSuccess {Integer}  data.produsen_id  produsen_id
 * @apiSuccess {String}  data.produsen_name  produsen_name
 * @apiSuccess {Integer}  data.bank_id  bank_id
 * @apiSuccess {String}  data.bank_name  bank_name
 * @apiSuccess {Timestamp}  data.start_date  start_date
 * @apiSuccess {Timestamp}  data.end_date  end_date
 * @apiSuccess {String}  data.status  status
 * @apiSuccess {String}  data.status_name  status_name
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



/**
 * @api {post} /bankdf Create Data
 * @apiVersion 1.0.0
 * @apiName Create Data
 * @apiGroup Bank DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {uuid} customer_uuid   uuid customer
 * @apiParam {Integer} customer_id     id customer
 * @apiParam {Integer} sales_org_id    id sales org
 * @apiParam {Integer} bank_id    id bank
 * @apiParam {Timestamp} start_date      start date
 * @apiParam {Timestamp} end_date    end date
 * @apiParam {String} status      status (y/n)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id     id bankdf
 * @apiSuccess {String} data.uuid    uuid bank df
 * @apiSuccess {uuid} data.customer_uuid     uuid customer
 * @apiSuccess {String} data.customer_id     id customer
 * @apiSuccess {String} data.sales_org_id    id sales org
 * @apiSuccess {Integer} data.bank_id    id bank
 * @apiSuccess {String} data.start_date      start date
 * @apiSuccess {String} data.end_date    end date
 * @apiSuccess {String} data.status      status
 * @apiSuccess {Timestamp} data.created_by  created_by
 * @apiSuccess {Timestamp} data.updated_by  updated_by
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {put} /bankdf/:uuid Update Data
 * @apiVersion 1.0.0
 * @apiName Update Data
 * @apiGroup Bank DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} data.customer_id     id customer
 * @apiParam {Integer} data.sales_org_id    id sales org
 * @apiParam {Integer} data.bank_id    id bank
 * @apiParam {Timestamp} data.start_date      start date
 * @apiParam {Timestamp} data.end_date    end date
 * @apiParam {String} data.status      status (y/n)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer}  data.id  id
 * @apiSuccess {String}  data.uuid  uuid
 * @apiSuccess {Integer}  data.customer_id  customer_id
 * @apiSuccess {String}  data.customer_name  customer_name
 * @apiSuccess {Integer}  data.produsen_id  produsen_id
 * @apiSuccess {String}  data.produsen_name  produsen_name
 * @apiSuccess {Integer}  data.bank_id  bank_id
 * @apiSuccess {String}  data.bank_name  bank_name
 * @apiSuccess {Timestamp}  data.start_date  start_date
 * @apiSuccess {Timestamp}  data.end_date  end_date
 * @apiSuccess {String}  data.status  status
 * @apiSuccess {String}  data.status_name  status_name
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {PUT} /bankdf/update/bulk update multiple (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Bulk
 * @apiGroup Bank DF
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "uuid":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "uuid":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data request
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
