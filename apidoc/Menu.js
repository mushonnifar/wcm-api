/**
 * @api {get} /sidebar Sidebar Menu
 * @apiVersion 1.0.0
 * @apiName Sidebar Menu
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data array bertingkar (parent child)
 * @apiSuccess {String} data.id nama user
 * @apiSuccess {String} data.name username user
 * @apiSuccess {String} data.url email user
 * @apiSuccess {String} data.order_no id user
 * @apiSuccess {String} data.icon id user
 * @apiSuccess {String} data.parent id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /menu Create Menu
 * @apiVersion 1.0.0
 * @apiName Create Menu
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name_ID nama menu ID
 * @apiParam {String} name_EN nama menu EN
 * @apiParam {String} url link/url 
 * @apiParam {Integer} order_no urutan menu
 * @apiParam {String} icon icon menu
 * @apiParam {String} parent_id parent menu 1 tingkat
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /menu Get All Menu
 * @apiVersion 1.0.0
 * @apiName Get All Menu
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id menu
 * @apiSuccess {String} data.name_ID nama menu ID
 * @apiSuccess {String} data.name_EN nama menu EN
 * @apiSuccess {String} data.url link/url menu
 * @apiSuccess {String} data.order_no urutan menu
 * @apiSuccess {String} data.icon icon menu
 * @apiSuccess {String} data.parent_name_ID parent 1 tingkat ID
 * @apiSuccess {String} data.parent_name_EN parent 1 tingkat EN
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /menu/id/:id Get Menu by ID
 * @apiVersion 1.0.0
 * @apiName Get Menu by ID
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} id id menu (param untuk URL)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id menu
 * @apiSuccess {String} data.name_ID nama menu ID
 * @apiSuccess {String} data.name_EN nama menu EN
 * @apiSuccess {String} data.url link/url menu
 * @apiSuccess {String} data.order_no urutan menu
 * @apiSuccess {String} data.icon icon menu
 * @apiSuccess {String} data.parent_name_ID parent 1 tingkat ID
 * @apiSuccess {String} data.parent_name_EN parent 1 tingkat EN
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by Pengedit data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 
 /**
 * @api {put} /menu/:id Update Menu
 * @apiVersion 1.0.0
 * @apiName Update Menu
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id id menu (param untuk URL)
 * @apiParam {String} name_ID nama menu ID
 * @apiParam {String} name_EN nama menu EN
 * @apiParam {String} url link/url 
 * @apiParam {Integer} order_no urutan menu
 * @apiParam {String} icon icon menu
 * @apiParam {String} parent_id parent menu 1 tingkat
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id menu
 * @apiSuccess {String} data.name_ID nama menu ID
 * @apiSuccess {String} data.name_EN nama menu EN
 * @apiSuccess {String} data.url link/url menu
 * @apiSuccess {String} data.order_no urutan menu
 * @apiSuccess {String} data.icon icon menu
 * @apiSuccess {String} data.parent_name_ID parent 1 tingkat ID
 * @apiSuccess {String} data.parent_name_EN parent 1 tingkat EN
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by Pengedit data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

  /**
 * @api {delete} /menu/{id} Delete Menu
 * @apiVersion 1.0.0
 * @apiName Delete Menu
 * @apiGroup Menu
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id id menu (param untuk URL)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id menu
 * @apiSuccess {String} data.name_ID nama menu ID
 * @apiSuccess {String} data.name_EN nama menu EN
 * @apiSuccess {String} data.url link/url menu
 * @apiSuccess {String} data.order_no urutan menu
 * @apiSuccess {String} data.icon icon menu
 * @apiSuccess {String} data.parent_name_ID parent 1 tingkat ID
 * @apiSuccess {String} data.parent_name_EN parent 1 tingkat EN
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by Pengedit data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
