 /**
 * @api {post} /rdkk/update/bulk Update Multiple Status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update - multiple status
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [
	{
		"id":"65FBF6F9-939F-4C45-81DC-A3B7AE0A8A20",
		"status":"n"
	},
		{
		"id":"6A0F448E-BFE5-4D53-AAF0-372825824229",
		"status":"n"
	}
]
 * @apiParam {String} data.status status penyimpanan (y: active, n:inactive)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data 
 * @apiSuccess {String} data.id id rdkk header
 * @apiSuccess {String} data.uuid uuid rdkk item
 * @apiSuccess {String} data.number nomor doc rdkk
 * @apiSuccess {String} data.planting_period_id id planting period
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.retail_id id pengecer
 * @apiSuccess {String} data.farmer_group_id id kelompok tani
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_unit id kecamatan
 * @apiSuccess {String} data.planting_area luas tanam
 * @apiSuccess {Date} data.planting_from_date tanggal mulai valid data
 * @apiSuccess {Date} data.planting_thru_date tanggal akhir valid data
 * @apiSuccess {String} data.commodity id komoditas (example value: 1;2;6;)
 * @apiSuccess {String} data.status status data
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /rdkk Get Data Rdkk List
 * @apiVersion 1.0.0
 * @apiName Get Data Rdkk List
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} number nomor rdkk
 * @apiParam {String} customer nama customer name
 * @apiParam {String} sales_org nama sales org name
 * @apiParam {String} farmer_group nama farmer group name
 * @apiParam {String} retail nama retail name
 * @apiParam {String} sales_office nama sales_office name
 * @apiParam {String} sales_group nama sales_group name
 * @apiParam {String} sales_unit nama sales_unit name
 * @apiParam {String} land_area luas lahan
 * @apiParam {String} farmer_period_name deskripsi periode tanam  
 * @apiParam {String} P01 alokasi untuk produk 1
 * @apiParam {String} P02 alokasi untuk produk 2
 * @apiParam {String} P03 alokasi untuk produk 3
 * @apiParam {String} P04 alokasi untuk produk 4
 * @apiParam {String} P05 alokasi untuk produk 5
 * @apiParam {String} status status
 * @apiParam {String} created_at tanggal pembuatan data
 * @apiParam {String} updated_at tanggal update data
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id rdkk
 * @apiSuccess {String} data.uuid uuid rdkk
 * @apiSuccess {String} data.number nomor rdkk
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.land_area luas lahan
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.farmer_group_id id farmer group atau kelompok tani
 * @apiSuccess {String} data.farmer_group_name nama kelompok tani
 * @apiSuccess {String} data.retail_id id retail
 * @apiSuccess {String} data.retail_name nama retail
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.farmer_period_id id farmer period atau id periode tanam
 * @apiSuccess {String} data.farmer_period_name nama farmer period  
 * @apiSuccess {String} data.P01 alokasi untuk produk 1
 * @apiSuccess {String} data.P02 alokasi untuk produk 2
 * @apiSuccess {String} data.P03 alokasi untuk produk 3
 * @apiSuccess {String} data.P04 alokasi untuk produk 4
 * @apiSuccess {String} data.P05 alokasi untuk produk 5 
 * @apiSuccess {String} data.P01_approved_qty alokasi untuk produk 1
 * @apiSuccess {String} data.P02_approved_qty alokasi untuk produk 2
 * @apiSuccess {String} data.P03_approved_qty alokasi untuk produk 3
 * @apiSuccess {String} data.P04_approved_qty alokasi untuk produk 4
 * @apiSuccess {String} data.P05_approved_qty alokasi untuk produk 5 
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data 
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {get} /rdkk/:uuid Get Data Rdkk Single
 * @apiVersion 1.0.0
 * @apiName Get Data Rdkk Single
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id rdkk
 * @apiSuccess {String} data.uuid uuid rdkk
 * @apiSuccess {String} data.number nomor rdkk 
 * @apiSuccess {String} data.planting_period_id id planting period
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.retail_id id retail
 * @apiSuccess {String} data.farmer_group_id id farmer group atau kelompok tani
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.plating_area luas tanam
 * @apiSuccess {String} data.plating_from_data tangal mulai valid data
 * @apiSuccess {String} data.plating_thru_data tangal akhir valid data
 * @apiSuccess {String} data.commodity komoditas
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {String} data.product array data product
 * @apiSuccess {String} data.product.id id rdkk item
 * @apiSuccess {String} data.product.uuid uuid rdkk item
 * @apiSuccess {String} data.product.product_id id produk
 * @apiSuccess {String} data.product.required_qty qty rdkk item
 * @apiSuccess {String} data.product.status status
 * @apiSuccess {String} data.product.created_by id user create data
 * @apiSuccess {String} data.product.updated_by id user update data
 * @apiSuccess {Timestamp} data.product.updated_at waktu update data
 * @apiSuccess {Timestamp} data.product.created_at waktu create data 
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /rdkk Create RDKK
 * @apiVersion 1.0.0
 * @apiName Create RDKK
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *  
 * @apiParam {String} planting_period_id id planting period
 * @apiParam {String} customer_id id customer
 * @apiParam {String} retail_id id retail
 * @apiParam {String} farmer_group_id id farmer group
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {String} planting_area planting area
 * @apiParam {String} planting_from_date tanggal mulai valid data
 * @apiParam {String} planting_thru_date tanggal akhir valid data
 * @apiParam {String} commodity id komoditas value 1;2;3;4;5 (default wajib diisi 0 jika tidak isi)
 * @apiParam {String} rdkk_item id product value P01;P02;P03;P04;P05
 * @apiParam {String} rdkk_item_qty 100;200;300;400;500
 * @apiParam {String} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.number nomor rdkk
 * @apiSuccess {String} data.planting_period_id id planting period
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.retail_id id retail
 * @apiSuccess {String} data.farmer_group_id id farmer group
 * @apiSuccess {String} data.planting_area planting area
 * @apiSuccess {String} data.planting_form_date tanggal mulai valid data
 * @apiSuccess {String} data.planting_thru_date tanggal akhir valid data
 * @apiSuccess {String} data.commodity id komoditas
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data 
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data 
 * @apiSuccess {String} data.id id rdkk item
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /rdkk/product/list Get Data Produk
 * @apiVersion 1.0.0
 * @apiName Get Data Produk
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id prduk
 * @apiSuccess {String} data.name nama produk 
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
/**
 * @api {get} /rdkk/planting/list Get Data Periode Tanam
 * @apiVersion 1.0.0
 * @apiName Get Data Periode Tanam
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id periode tanam
 * @apiSuccess {String} data.desc deskripsi periode tanam
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /rdkk/:uuid Update RDKK
 * @apiVersion 1.0.0
 * @apiName Update RDKK
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} planting_area luas tanam
 * @apiParam {String} commodity komoditas
 * @apiParam {String} rdkk_item_id id rdkk product format separator id;id;id;id;id
 * @apiParam {String} rdkk_item_qty qty dari rdkk item format separator qty;qty;qty;qty;qty
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id rdkk
 * @apiSuccess {String} data.uuid uuid rdkk
 * @apiSuccess {String} data.number nomor rdkk
 * @apiSuccess {String} data.planting_period_id id planting period atau periode tanam
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.retail_id id retail
 * @apiSuccess {String} data.farmer_group_id id farmer group
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.planting_area planting area
 * @apiSuccess {String} data.planting_form_date tanggal mulai valid data
 * @apiSuccess {String} data.planting_thru_date tanggal akhir valid data
 * @apiSuccess {String} data.commodity id komoditas
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data 
 * @apiSuccess {String} data.updated_by pengubah data  
 * @apiSuccess {Timestamp} data.created_at waktu create data  
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * 
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /rdkk/upload Upload Excel RDKK
 * @apiVersion 1.0.0
 * @apiName Upload Excel RDKK
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.nomor_rdkk nomor rdkk, jika di excel tidak ada maka sistem otomatik menggenerated nomor rdkk
 * @apiSuccess {String} data.kode_distributo kode distributo
 * @apiSuccess {String} data.distributor nama distributor
 * @apiSuccess {String} data.produsen kode produsen
 * @apiSuccess {String} data.kode_prov kode provinsi
 * @apiSuccess {String} data.deskripsi_prov nama provinsi
 * @apiSuccess {String} data.kode_kotakab kode kabupaten kota
 * @apiSuccess {String} data.deskripsi_kotakab nama kabupaten kota
 * @apiSuccess {String} data.kode_kecamatan kode kecamatan
 * @apiSuccess {String} data.deskripsi_kecamatan nama kecamatan
 * @apiSuccess {String} data.kode_pengecer kode retail atau pengecer
 * @apiSuccess {String} data.pengecer nama retail
 * @apiSuccess {String} data.kode_kelompok_tani kode kelompok tani
 * @apiSuccess {String} data.kelompok_tani nama kelompok tani
 * @apiSuccess {String} data.jumlah_petani jumlah petani
 * @apiSuccess {String} data.luas_lahan luas lahan
 * @apiSuccess {String} data.luas_tanam luas tanam
 * @apiSuccess {String} data.periode_tanam kode periode tanam atau planting period
 * @apiSuccess {String} data.deskripsi_periode_tanam deskripsi periode tanam
 * @apiSuccess {String} data.komoditas komoditas
 * @apiSuccess {String} data.kuantum_kebutuhan_urea alokasi kebutuhan produk urea
 * @apiSuccess {String} data.kuantum_kebutuhan_npk alokasi kebutuhan produk npk
 * @apiSuccess {String} data.kuantum_kebutuhan_organik alokasi kebutuhan organik
 * @apiSuccess {String} data.kuantum_kebutuhan_za alokasi kebutuhan za
 * @apiSuccess {String} data.kuantum_kebutuhan_sp36 alokasi kebutuhan sp36
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {get} /rdkk/approval/list Get Data Approval Rdkk List
 * @apiVersion 1.0.0
 * @apiName Get Data Rdkk List
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} number nomor rdkk
 * @apiParam {String} customer nama customer name
 * @apiParam {String} sales_org nama sales org name
 * @apiParam {String} farmer_group nama farmer group name
 * @apiParam {String} retail nama retail name
 * @apiParam {String} sales_office nama sales_office name
 * @apiParam {String} sales_group nama sales_group name
 * @apiParam {String} sales_unit nama sales_unit name
 * @apiParam {String} land_area luas lahan
 * @apiParam {String} farmer_period_name deskripsi periode tanam  
 * @apiParam {String} P01 alokasi untuk produk 1
 * @apiParam {String} P02 alokasi untuk produk 2
 * @apiParam {String} P03 alokasi untuk produk 3
 * @apiParam {String} P04 alokasi untuk produk 4
 * @apiParam {String} P05 alokasi untuk produk 5
 * @apiParam {String} status status
 * @apiParam {String} created_at tanggal pembuatan data
 * @apiParam {String} updated_at tanggal update data
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id rdkk
 * @apiSuccess {String} data.uuid uuid rdkk
 * @apiSuccess {String} data.number nomor rdkk
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.land_area luas lahan
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.farmer_group_id id farmer group atau kelompok tani
 * @apiSuccess {String} data.farmer_group_name nama kelompok tani
 * @apiSuccess {String} data.retail_id id retail
 * @apiSuccess {String} data.retail_name nama retail
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.farmer_period_id id farmer period atau id periode tanam
 * @apiSuccess {String} data.farmer_period_name nama farmer period  
 * @apiSuccess {String} data.P01 alokasi untuk produk 1
 * @apiSuccess {String} data.P02 alokasi untuk produk 2
 * @apiSuccess {String} data.P03 alokasi untuk produk 3
 * @apiSuccess {String} data.P04 alokasi untuk produk 4
 * @apiSuccess {String} data.P05 alokasi untuk produk 5 
 * @apiSuccess {String} data.P01_approved_qty alokasi untuk produk 1
 * @apiSuccess {String} data.P02_approved_qty alokasi untuk produk 2
 * @apiSuccess {String} data.P03_approved_qty alokasi untuk produk 3
 * @apiSuccess {String} data.P04_approved_qty alokasi untuk produk 4
 * @apiSuccess {String} data.P05_approved_qty alokasi untuk produk 5 
 * @apiSuccess {String} data.P01_approved_qty permission approved
 * @apiSuccess {String} data.P02_approved_qty permission approved
 * @apiSuccess {String} data.P03_approved_qty permission approved
 * @apiSuccess {String} data.P04_approved_qty permission approved
 * @apiSuccess {String} data.P05_approved_qty permission approved
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data 
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



 /**
 * @api {put} /rdkk/approval/approved approved qty 
 * @apiVersion 1.0.0
 * @apiName approved qty 
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample
 * 
 * [
*     {
*      "rdkk_id": 15,
*      "product_id": "P03",
*      "approved_qty": 99
*     },
*     {
*      "rdkk_id": 15,
*      "product_id": "P02",
*      "approved_qty": 92
*     },
*     {
*      "rdkk_id": 15,
*      "product_id": "P01",
*      "approved_qty": 91
*     },
*     {
*      "rdkk_id": 15,
*      "product_id": "P04",
*      "approved_qty": 94
*}
*]
 * 
 * @apiSuccess {array} success
 * @apiSuccess {array} errors
 */


 /**
 * @api {put} /rdkk/approval/change-status Change status Rdkk Item
 * @apiVersion 1.0.0
 * @apiName Change status Rdkk Item
 * @apiGroup RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample
 * 
 * [
*     {
*      "rdkk_id": 15,
*      "product_id": "P03",
*      "status": "y|n"
*     }
*]
 * 
 * @apiSuccess {array} success
 * @apiSuccess {array} errors
 */
