/**
 * @api {get} /deliverymethod Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Delivery Method
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id delivery method
 * @apiSuccess {String} data.uuid uuid delivery method
 * @apiSuccess {String} data.name nama delivery method
 * @apiSuccess {String} data.ident_name nama ident
 * @apiSuccess {String} data.desc description
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /deliverymethod/id/:uuid Get Delivery Method By ID
 * @apiVersion 1.0.0
 * @apiName Get Delivery Method By ID
 * @apiGroup Delivery Method
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid delivery method
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id delivery method
 * @apiSuccess {String} data.uuid uuid delivery method
 * @apiSuccess {String} data.name nama delivery method
 * @apiSuccess {String} data.ident_name nama ident
 * @apiSuccess {String} data.desc description
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /deliverymethod/kabupaten/:sales_group_id Get Delivery Method By Sales Group
 * @apiVersion 1.0.0
 * @apiName Get Delivery Method By Sales Group
 * @apiGroup Delivery Method
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_group_id id sales group
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id delivery method
 * @apiSuccess {String} data.name nama delivery method
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
