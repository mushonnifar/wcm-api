/**
 * @api {get} /salesorg/ get All Sales Organisation
 * @apiVersion 1.0.0
 * @apiName Get All Sales Org
 * @apiGroup Configuration
 *
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "draw": 0,
 *      "recordsTotal": 1,
 *      "recordsFiltered": 1,
 *      "data": [
 *          {
 *              "id": "1",
 *              "name": "The Name"
 *              "status": "false"
 *          }
 *      ],
 * }
 *
 */
/**
 * @api {put} /salesorg/:id update status sales Organisation
 * @apiVersion 1.0.0
 * @apiName Update Sales Org
 * @apiGroup Configuration
 *
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.id  id
 * @apiSuccess {String} data.name  name
 * @apiSuccess {Integer} data.status  status

 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /region/salesorg/provinsi/assg
 * @apiVersion 1.0.0
 * @apiName Sales Org Region Province Assign
 * @apiGroup configuration
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {sales_org_id} sales_org_id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


  /**
 * @api {get} /region/provinsi/salesgroup/assg
 * @apiVersion 1.0.0
 * @apiName Sales Org Region Kabupaten Assign
 * @apiGroup configuration
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {sales_office_id} sales_office_id sales_office_id (Multiple seperate by (;))
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */