/**
 * @api {get} /logorder Get Log Order
 * @apiVersion 1.0.0
 * @apiName Get Log Order
 * @apiGroup Log Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id log order
 * @apiSuccess {String} data.uuid uuid log order
 * @apiSuccess {Integer} data.order_id id order
 * @apiSuccess {String} data.message pesan log
 * @apiSuccess {String} data.status status log
 * @apiSuccess {String} data.order_number nomor order
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /logorder/:uuid Resend Order
 * @apiVersion 1.0.0
 * @apiName Resend Order
 * @apiGroup Log Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid log order
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */