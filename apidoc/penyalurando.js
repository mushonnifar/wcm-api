/**
* @api {get} /supplydo Penyaluran DO
* @apiVersion 1.0.0
* @apiName Penyaluran DO
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {Integer}  id  ID master Header
* @apiParam {Integer} sales_org_id  sales_org_id
* @apiParam {String} sales_org_name  sales_org_name
* @apiParam {String} so_number  so_number
* @apiParam {Number} number  number
* @apiParam {Integer} customer_id  customer_id
* @apiParam {String} customer_name  customer_name
* @apiParam {Number} so_qty  so_qty
* @apiParam {Number} do_qty  do_qty
* @apiParam {Number} totals  totals
* @apiParam {Integer} delivery_method_id  delivery_method_id
* @apiParam {String} delivery_method_name  delivery_method_name
* @apiParam {Integer} sales_office_id  sales_office_id
* @apiParam {String} sales_office_name  sales_office_name
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {String} number_order  number_order
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.id id
* @apiSuccess {uuid} data.uuid uuid
* @apiSuccess {Integer} data.qty_submit qty_submit
* @apiSuccess {String} data.status_submit status_submit
* @apiSuccess {Integer} data.qty_draft qty_draft
* @apiSuccess {String} data.status_draft status_draft
* @apiSuccess {Integer} data.sales_org_id  sales_org_id
* @apiSuccess {String} data.sales_org_name  sales_org_name
* @apiSuccess {String} data.so_number  so_number
* @apiSuccess {Number} data.number  number
* @apiSuccess {Integer} data.customer_id  customer_id
* @apiSuccess {String} data.customer_name  customer_name
* @apiSuccess {Number} data.so_qty  so_qty
* @apiSuccess {Number} data.do_qty  do_qty
* @apiSuccess {Number} data.totals  totals
* @apiSuccess {Number} data.sisa_qty_do  sisa_qty_do
* @apiSuccess {Integer} data.delivery_method_id  delivery_method_id
* @apiSuccess {String} data.delivery_method_name  delivery_method_name
* @apiSuccess {Integer} data.sales_office_id  sales_office_id
* @apiSuccess {String} data.sales_office_name  sales_office_name
* @apiSuccess {Integer} data.sales_group_id  sales_group_id
* @apiSuccess {String} data.sales_group_name  sales_group_name
* @apiSuccess {String} data.number_order  number_order
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /supplydo/{uuid} Detail Distrip report In Order UUID
* @apiVersion 1.0.0
* @apiName Detail Distrip report In Order UUID
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {Integer} id   id Distrip report
* @apiParam {uuid} uuid   uuid Distrip report
* @apiParam {Integer} report_f5_id   report_f5_id
* @apiParam {String} number   number Distrip report (NO PKP)
* @apiParam {String} so_number   so_number
* @apiParam {Date} order_date   order_date
* @apiParam {Integer} customer_id   customer_id
* @apiParam {String} customer_name customer_name
* @apiParam {Integer} sales_group_id   sales_group_id
* @apiParam {String} sales_group_name sales_group_name
* @apiParam {Integer} sales_org_id   sales_org_id
* @apiParam {String} sales_org_name sales_org_name
* @apiParam {Integer} delivery_method_id   delivery_method_id
* @apiParam {String} inconterm_name inconterm_name
* @apiParam {String} status   status
* @apiParam {String} status_name  status_name
* @apiParam {Integer} month   month
* @apiParam {Integer} year   year
* @apiParam {String} f5_number   f5_number (No Laporan)
* @apiParam {String} f5_uuid   f5_uuid (UUID F5)
* @apiParam {Date} distribution_date   distribution_date
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.id   id Distrip report
* @apiSuccess {uuid} data.uuid   uuid Distrip report
* @apiSuccess {Integer} data.report_f5_id   report_f5_id
* @apiSuccess {String} data.number   number Distrip report (NO PKP)
* @apiSuccess {String} data.so_number   so_number
* @apiSuccess {Date} data.order_date   order_date
* @apiSuccess {Integer} data.customer_id   customer_id
* @apiSuccess {String} data.customer_name customer_name
* @apiSuccess {Integer} data.sales_group_id   sales_group_id
* @apiSuccess {String} data.sales_group_name sales_group_name
* @apiSuccess {Integer} data.sales_org_id   sales_org_id
* @apiSuccess {String} data.sales_org_name sales_org_name
* @apiSuccess {Integer} data.delivery_method_id   delivery_method_id
* @apiSuccess {String} data.inconterm_name inconterm_name
* @apiSuccess {String} data.status   status
* @apiSuccess {String} data.status_name  status_name
* @apiSuccess {Integer} data.month   month
* @apiSuccess {Integer} data.year   year
* @apiSuccess {String} data.f5_number   f5_number (No Laporan)
* @apiSuccess {String} data.f5_uuid   f5_uuid (No Laporan)
* @apiSuccess {Date} data.distribution_date   distribution_date
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /supplydo/entry/{number_penebusan} Get Entry Atribut Modal Add
* @apiVersion 1.0.0
* @apiName Get Entry Atribut Modal Add
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.sales_org_id   sales_org_id
* @apiSuccess {String} data.sales_org_name   sales_org_name
* @apiSuccess {Integer} data.customer_id   customer_id
* @apiSuccess {String} data.customer_name   customer_name
* @apiSuccess {Integer} data.sales_office_id   sales_office_id
* @apiSuccess {String} data.sales_office_name   sales_office_name
* @apiSuccess {Integer} data.sales_group_id   sales_group_id
* @apiSuccess {String} data.sales_group_name   sales_group_name
* @apiSuccess {Integer} data.delivery_method_id   id Inconterm
* @apiSuccess {String} data.delivery_method_name   name Inconterm
* @apiSuccess {String} data.so_number   so_number
* @apiSuccess {Integer} data.order_id   order_id
* @apiSuccess {Date} data.order_date   order_date
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {post} /supplydo Create Distribution DO
* @apiVersion 1.0.0
* @apiName Create Distribution DO
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {Integer} sales_org_id  sales_org_id
* @apiParam {Integer} customer_id  customer_id
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {Integer} so_number  so_number
* @apiParam {Integer} delivery_method_id  delivery_method_id
* @apiParam {Date} order_date  order_date
* @apiParam {Date} distribution_date  data penyaluran
* @apiParam {Integer} order_id  order_id
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.id id
* @apiSuccess {Integer} data.uuid uuid
* @apiSuccess {Integer} data.report_f5_id report_f5_id
* @apiSuccess {Number} data.number number
* @apiSuccess {Integer} data.customer_id customer_id
* @apiSuccess {Integer} data.order_id order_id
* @apiSuccess {Number} data.so_number so_number
* @apiSuccess {Integer} data.month month
* @apiSuccess {Integer} data.year year
* @apiSuccess {Integer} data.sales_org_id sales_org_id
* @apiSuccess {Integer} data.initial_stock_id initial_stock_id
* @apiSuccess {Integer} data.sales_group_id sales_group_id
* @apiSuccess {Date} data.distribution_date distribution_date
* @apiSuccess {Date} data.distrib_resportable_type distrib_resportable_type
* @apiSuccess {Char} data.status status
* @apiSuccess {uuid} data.created_by created_by
* @apiSuccess {uuid} data.updated_by updated_by
* @apiSuccess {Timestamp} data.created_at created_at
* @apiSuccess {Timestamp} data.updated_at updated_at
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /supplydo/detail/{uuid} Hirarki
* @apiVersion 1.0.0
* @apiName Hirarki
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Array[]} data.header array data
* @apiSuccess {String} data.header.number  number
* @apiSuccess {String} data.header.so_number  so_number
* @apiSuccess {Timestamp} data.header.order_date  order_date
* @apiSuccess {Integer} data.header.customer_id  customer_id
* @apiSuccess {String} data.header.customer_name  customer_name
* @apiSuccess {Integer} data.header.sales_org_id  sales_org_id
* @apiSuccess {String} data.header.sales_org_name  sales_org_name
* @apiSuccess {Integer} data.header.inconterm_id  inconterm_id
* @apiSuccess {String} data.header.inconterm_name  inconterm_name
* @apiSuccess {Timestamp} data.header.distribution_date  distribution_date
* @apiSuccess {String} data.header.status  status
* @apiSuccess {String} data.header.status_name  status_name
* @apiSuccess {Timestamp} data.header.periode periode
* @apiSuccess {Array[]} data.hiraki array data
* @apiSuccess {Array[]} data.hiraki.datas array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
 * @api {post} /supplydo/detail Update And Insert Bulk - multiple (raw)
 * @apiVersion 1.0.0
 * @apiName Update And Insert Bulk
 * @apiGroup supply do
 * 
 * @apiParamExample {json} Request-Example (data):
 * [	{"distrib_report_id": "1","report_f5_id": "1","retail_id": "3","uuid": "86984989-E4AD-44B5-BB22-2AD7A4B7BB57","qty":10,"product_id": "P01","status":"d"},{"distrib_report_id": "1","report_f5_id": "1","retail_id": "3","qty": 100,"uuid": "","product_id": "P02","status":"d"} ]
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data 
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */


/**
* @api {delete} /supplydo/{uuid}  Delete DO
* @apiVersion 1.0.0
* @apiName Delete DO
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /supplydo/download/{uuid}  BAST Download
* @apiVersion 1.0.0
* @apiName BAST Download
* @apiGroup supply do
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {file} data.file file file
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/