/**
 * @api {post} /pricing Create Pricing
 * @apiVersion 1.0.0
 * @apiName Create Pricing
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_org_id id sales org / produsen
 * @apiParam {String} sales_office_id id sales office / provinsi
 * @apiParam {String} sales_group_id id sales group / kabupaten
 * @apiParam {String} sales_unit_id id sales unit / kecamatan
 * @apiParam {String} product_id id produk / material
 * @apiParam {Integer} delivery_method_id id delivery method
 * @apiParam {String} sales_division_id id sales division
 * @apiParam {String} distrib_channel_id id distribution channel
 * @apiParam {String} condition_type tipe kondisi
 * @apiParam {String} calculate_type tipe kalkulasi
 * @apiParam {String} amount amount
 * @apiParam {String} uom uom
 * @apiParam {String} per_uom id per uom
 * @apiParam {Date} valid_from tanggal valid awal
 * @apiParam {Date} valid_to tanggal valid akhir
 * @apiParam {Integer} level level
 * @apiParam {String} product_desc deskripsi produk
 * @apiParam {String} current current
 * @apiParam {String} sap_tax_classification sap tax classification
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /pricing Get All Pricing
 * @apiVersion 1.0.0
 * @apiName Get All Pricing
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_org_name nama sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.sales_unit_name nama sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {String} data.product_name nama produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.delivery_method_name nama delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.sales_division_name nama sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.distrib_channel_name nama distrib channel
 * @apiSuccess {Date} data.valid_from valid from
 * @apiSuccess {Date} data.valid_to valid to
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /pricing/id/:uuid Get Pricing By ID
 * @apiVersion 1.0.0
 * @apiName Get Pricing By ID
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} uuid uuid pricing
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_org_name nama sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.sales_unit_name nama sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {String} data.product_name nama produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.delivery_method_name nama delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.sales_division_name nama sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.distrib_channel_name nama distrib channel
 * @apiSuccess {Date} data.valid_from valid from
 * @apiSuccess {Date} data.valid_to valid to
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /pricing/:uuid Update Pricing
 * @apiVersion 1.0.0
 * @apiName Update Pricing
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid pricing
 * @apiParam {String} amount amount
 * @apiParam {Date} valid_from valid from
 * @apiParam {Date} valid_to valid to
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /pricing/:uuid Delete Pricing
 * @apiVersion 1.0.0
 * @apiName Delete Pricing
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid pricing
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /pricing/update/bulk update multiple (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Multiple Data
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "uuid":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "uuid":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /pricing/upload Upload Excel Pricing
 * @apiVersion 1.0.0
 * @apiName Upload Excel Pricing
 * @apiGroup Pricing
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 * @apiParam {String} level level key combination
 * @apiParam {String} condition_type tipe kondisi
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pricing
 * @apiSuccess {String} data.uuid uuid pricing
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {String} data.distrib_channel_id id distrib channel
 * @apiSuccess {String} data.condition_type tipe kondisi
 * @apiSuccess {String} data.calculate_type tipe kalkulasi
 * @apiSuccess {String} data.amount amount
 * @apiSuccess {String} data.uom uom
 * @apiSuccess {String} data.per_uom id per uom
 * @apiSuccess {Date} data.valid_from tanggal valid awal
 * @apiSuccess {Date} data.valid_to tanggal valid akhir
 * @apiSuccess {Integer} data.level level
 * @apiSuccess {String} data.product_desc deskripsi produk
 * @apiSuccess {String} data.current current
 * @apiSuccess {String} data.sap_tax_classification sap tax classification
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */