/**
* @api {get} /reportf6 Master Report F6
* @apiVersion 1.0.0
* @apiName Master Report F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
* @apiParam {uuid} uuid  uuid
* @apiParam {String} number  number
* @apiParam {Integer} sales_org_id  sales_org_id
* @apiParam {String} sales_org_name  sales_org_name
* @apiParam {Integer} customer_id  customer_id
* @apiParam {String} customer_name  customer_name
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {Integer} month  month
* @apiParam {Integer} year  year
* @apiParam {Timestemp} submited_date  submited_date
* @apiParam {String} status  status
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {Integer} sales_office_id  sales_office_id
* @apiParam {String} sales_office_name  sales_office_name
* @apiParam {String} status_name  status_name
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {String} data.number  number
* @apiSuccess {Integer} data.sales_org_id  sales_org_id
* @apiSuccess {String} data.sales_org_name  sales_org_name
* @apiSuccess {Integer} data.customer_id  customer_id
* @apiSuccess {String} data.customer_name  customer_name
* @apiSuccess {Integer} data.sales_group_id  sales_group_id
* @apiSuccess {Integer} data.month  month
* @apiSuccess {Integer} data.year  year
* @apiSuccess {Timestemp} data.submited_date  submited_date
* @apiSuccess {String} data.status  status
* @apiSuccess {String} data.status_name  status_name
* @apiSuccess {Integer} sales_group_id  sales_group_id
* @apiSuccess {String} sales_group_name  sales_group_name
* @apiSuccess {Integer} sales_office_id  sales_office_id
* @apiSuccess {String} sales_office_name  sales_office_name
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /reportf6/sonumber/{uuid} So Number Report F6
* @apiVersion 1.0.0
* @apiName So Number Report F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /reportf6/detail/{uuid} So Hirarki Detail Report F6
* @apiVersion 1.0.0
* @apiName So Hirarki Detail Report F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Array[]} data.hirarki array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {post}  /reportf6  Create Report F6 Detail
* @apiVersion 1.0.0
* @apiName Create Report F6 Detail
* @apiGroup report f6


* @apiParamExample {json} Request-Example (data):
* [
       {
        "report_f6_id":1,
        "status":"d",
        "data":[
                        {
                            "name":"Suci Farida",
                            "data":[
                                {
                                    "product_id":"P01",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"2",
                                    "uuid":"BFEABEDE-C006-45D4-8BF3-AD71967E961E"
                                },
                                {
                                    "product_id":"P02",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"2",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P03",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"2",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P04",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"2",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P05",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"2",
                                    "uuid":""
                                }
                            ]
                        },
                        {
                            "name":"Nasim Dodo Megantara",
                            "data":[
                                {
                                    "product_id":"P01",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"3",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P02",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"3",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P03",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"3",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P04",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"3",
                                    "uuid":""
                                },
                                {
                                    "product_id":"P05",
                                    "stok_awal":0,
                                    "penebusan":0,
                                    "penyaluran":0,
                                    "stok_akhir":0,
                                    "retail_id":"3",
                                    "uuid":""
                                }
                            ]
                        }
        ]
    }
]
*
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get}  /reportf6/download/{uuid} DOWNLOAD F6
* @apiVersion 1.0.0
* @apiName DOWNLOAD F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {file} data file PDF
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/


/**
* @api {get}  /reportf6/bast/{uuid} DOWNLOAD BAST F6
* @apiVersion 1.0.0
* @apiName DOWNLOAD BAST F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {file} data file PDF
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/


/**
* @api {get}  /reportf6/rekap/{uuid}/{file_tipe} DOWNLOAD BAST F6
* @apiVersion 1.0.0
* @apiName DOWNLOAD BAST F6
* @apiGroup report f6
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {String} uuid uuid report f6
* @apiParam {String} file_tipe (xls, xlsx, pdf)
*
* @apiParam {String} sales_org_name  sales_org_name
* @apiParam {String} customer_name  customer_name
* @apiParam {Integer} month  month
* @apiParam {Integer} year  year
* @apiParam {String} status  status
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {String} sales_office_name  sales_office_name
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {file} data file PDF
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/
