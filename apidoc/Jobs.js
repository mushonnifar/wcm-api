/**
 * @api {get} /job/payment-due-date Check Payment Due Date
 * @apiVersion 1.0.0
 * @apiName Check Payment Due Date
 * @apiGroup Jobs Scheduler
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */