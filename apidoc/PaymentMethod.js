/**
 * @api {get} /paymentmethod?customer_id=&sales_org_id= Get Payment Method
 * @apiVersion 1.0.0
 * @apiName Get Payment Method
 * @apiGroup Payment
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} customer_id id customer
 * @apiParam {String} sales_org_id id sales org
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id payment method
 * @apiSuccess {String} data.uuid uuid payment method
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.ident_name kode payment method
 * @apiSuccess {String} data.name nama payment method
 * @apiSuccess {String} data.desc deskripsi
 * @apiSuccess {String} data.system system
 * @apiSuccess {String} data.sap_code kode sap
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
