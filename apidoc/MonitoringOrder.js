/**
 * @api {get} /monitoringorder Get Data Monitoring order
 * @apiVersion 1.0.0
 * @apiName Get Data Monitoring Order
 * @apiGroup Monitoring Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} number nomor order
 * @apiParam {String} refrence_code kode referensi
 * @apiParam {String} distributor_name nama distributor
 * @apiParam {String} sales_org_name farmer group name
 * @apiParam {String} good_redemption_due_date good_redemption_due_date
 * @apiParam {String} booking_code kode booking
 * @apiParam {String} so_number kode sales order
 * @apiParam {String} payment_due_date batas akhir pembayaran
 * @apiParam {String} payment_method tipe pembayaran
 * @apiParam {String} billing_date tanggal pelunasan
 * @apiParam {String} due_date_plan tanggal rencana pelunasan
 * @apiParam {String} order_date tanggal order
 * @apiParam {String} status status (Approve = y; Unapprove = s(submit); Suspend = p; Unsuspend = y(approve); PAID = l; DPPAID = u; CLOSE = x; REVIEWED = r; COMPLETE = c)
 * @apiParam {String} incoterm incoterm
 * @apiParam {String} sales_office_name nama provinsi
 * @apiParam {String} sales_group_name nama kabupaten
 * @apiParam {String} payment_method tipe pembayaran
 * @apiParam {String} total_price total harga
 * @apiParam {String} upfront_payment uang muka
 * @apiParam {String} pickup_date tanggal pengambilan
 * @apiParam {String} created_by nama user pembuat
 * @apiParam {String} created_at tanggal pembuatan data
 * @apiParam {String} updated_at tanggal update data
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {String} data.good_redemption_due_date good_redemption_due_date
 * @apiSuccess {String} data.booking_code kode booking
 * @apiSuccess {String} data.payment_due_date batas akhir pembayaran
 * @apiSuccess {String} data.billing_date tanggal pembayaran
 * @apiSuccess {String} data.df_due_date batas akhir pembayaran df
 * @apiSuccess {String} data.df_due_date_plan tanggal rencana
 * @apiSuccess {String} data.order_date tanggal order
 * @apiSuccess {String} data.so_number nomor so
 * @apiSuccess {String} data.total_price total harga setelah pajak
 * @apiSuccess {String} data.ppn ppn yang harus dibayarkan
 * @apiSuccess {String} data.total_price_before_ppn total harga sebelum pajak
 * @apiSuccess {String} data.upfront_payment uang muka
 * @apiSuccess {String} data.pickup_due_date tanggal batas akhir pengambilan
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.contract_id id kontrak
 * @apiSuccess {String} data.contract_number nomor kontrak
 * @apiSuccess {String} data.sales_office_id id provinsi
 * @apiSuccess {String} data.sales_office_name nama provinsi
 * @apiSuccess {String} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.sales_group_name nama kabupaten
 * @apiSuccess {String} data.incoterm_id id jenis angkut
 * @apiSuccess {String} data.incoterm nama jenis angkut
 * @apiSuccess {String} data.status nama status (Approve = y; Unapprove = s(submit); Suspend = p; Unsuspend = y(approve); PAID = l; DPPAID = u; CLOSE = x; REVIEWED = r; COMPLETE = c)
 * @apiSuccess {String} data.delivery_qty total kuantitas DO
 * @apiSuccess {String} data.sisa_tebus total sisa SO
 * @apiSuccess {String} data.created_by nama user
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
* @api {get} /monitoringorder/downloadbooking/:uuid Download Kode Booking
* @apiVersion 1.0.0
* @apiName Download Kode Booking
* @apiGroup Monitoring Order
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {File} file file download kode booking
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/
/**
 * @api {get} /monitoringorder/downloadso/:uuid Download Sales Order
 * @apiVersion 1.0.0
 * @apiName Download Sales Order
 * @apiGroup Monitoring Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {File} file file download sales order
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /monitoringorder/unapproved Unupproved Monitoring Order
 * @apiVersion 1.0.0
 * @apiName Un Approved Monitoring Order
 * @apiGroup Monitoring Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParamExample Request as JSON Array
 * [
 *      {"uuid","aaa-bbb-ccc-ddd"},
 * ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data of UUID
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */