
 /**
 * @api {get} /changeRequest/{type}/detail/{uuid} Get Change Request Permentan, Pergub
 * @apiVersion 1.0.0
 * @apiName GET CHANGE REQUEST PERMENTAN, PERGUB
 * @apiGroup CR CONTRACT GOVERMENT
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} type [permentan, pergub]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /changeRequest/{type}/detail/{uuid} Update Change Request Permentan, Pergub
 * @apiVersion 1.0.0
 * @apiName UPDATE CHANGE REQUEST PERMENTAN, PERGUB
 * @apiGroup CR CONTRACT GOVERMENT
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} type [permentan, pergub]
 * @apiParam {Integer} contract_id    id contract no doc SPJB
 * @apiParam {Integer} sales_office_id   id Provinsi
 * @apiParam {Integer} sales_group_id    id Kabupaten if type = pergub
 * @apiParam {Integer} product_id    id Product
 * @apiParam {Integer} year    Tahun
 * @apiParam {Integer} month    Bulan
 * @apiParam {Integer} initial_qty    Jumlah Qty
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {put} /changeRequest/{type}/detail/{uuid}/status Update Status Change Request Permentan, Pergub
 * @apiVersion 1.0.0
 * @apiName UPDATE STATUS CHANGE REQUEST PERMENTAN, PERGUB
 * @apiGroup CR CONTRACT GOVERMENT
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request as Array Of Json
 *[
 *  {"sales_office_id":"0011","product_id":"A01","month":1, "year":2019,"status": "n|y"}
 *]
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.msg Pesan Sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
