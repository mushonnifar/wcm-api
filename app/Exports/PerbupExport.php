<?php

namespace App\Exports;

use App\models\ContractGoverment;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PerbupExport implements FromCollection, WithHeadings
{
    private $perbub;

    public function __construct(ContractGoverment $perbub)
    {
        $this->perbub = $perbub;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
      $items = $this->perbub->items()
        ->leftjoin('wcm_sales_group as z','z.id','=','wcm_contract_gov_item.sales_group_id')
        ->orderBy(DB::raw('CONVERT(INT,wcm_contract_gov_item.sales_office_id)'), 'asc')
        ->orderBy(DB::raw('CONVERT(INT,wcm_contract_gov_item.sales_group_id)'), 'asc')
        ->orderBy('wcm_contract_gov_item.product_id', 'asc')
        ->orderBy(DB::raw('CONVERT(INT,wcm_contract_gov_item.month)'), 'asc')
        ->select('wcm_contract_gov_item.*','z.district_code')
        ->get();

      return $items->map(function ($item) {
          return [
              $item->contractgoverment->sales_org_id,
              $item->contractgoverment->number,
              (int) $item->sales_office_id,
              (int) $item->district_code,
              (int) $item->sales_unit_id,
              $item->year,
              (int) $item->month,
              $item->product_id,
              $item->initial_qty,
          ];
      });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            "Sales Organization",
            "No Doc",
            "Kode Provinsi",
            "Kode Kabupaten",
            "Kode Kecamatan",
            "Tahun",
            "Bulan",
            "Kode Jenis Produk",
            "Amount Alokasi",
        ];
    }
}
