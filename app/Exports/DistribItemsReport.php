<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DistribItemsReport implements FromCollection, WithHeadings, WithColumnFormatting, ShouldAutoSize
{
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
        return $this->model->map(function ($item){
            return [
                $item->produsen_name,
                $item->number,
                $item->customer_id,
                $item->customer_name,
                $item->distrib_type,
                $item->order_number,
                $item->so_number,
                $item->number_pkp,
                $item->sales_office_id,
                $item->sales_office_name,
                $item->sales_group_id,
                $item->sales_group_name,
                $item->sales_unit_id,
                $item->sales_unit_name,
                $item->retail_code,
                $item->retail_name,
                $item->distribution_date,
                $item->product_name,
                $item->qty,
                $item->status_f5_f6,
                $item->status_name,
            ];
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            "Nama Produsen",
            "Nomor F5/F6",
            "Kode Distributor",
            "Nama Distributor",
            "Tipe Penyaluran",
            "Nomor Order",
            "Nomor SO",
            "Nomor Penyaluran",
            "Kode Provinsi",
            "Nama Provinsi",
            "Kode Kabupaten",
            "Nama Kabupaten",
            "Kode Kecamatan",
            "Nama Kecamatan",
            "Kode Retail",
            "Nama Retail",
            "Tanggal Penyaluran",
            "Produk",
            "QTY",
            "status F5/F6",
            "Status"
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
        ];
    }
}
