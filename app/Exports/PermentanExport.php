<?php

namespace App\Exports;

use App\models\ContractGoverment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PermentanExport implements FromCollection, WithHeadings
{
    private $contract;

    public function __construct(ContractGoverment $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $items = $this->contract->items;

        return $items->map(function ($item) {
            return [
                $item->contractgoverment->sales_org_id,
                $item->contractgoverment->number,
                $item->sales_office_id,
                $item->year,
                (int) $item->month,
                $item->product_id,
                $item->initial_qty,
            ];
        });

    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
        return [
            "Sales Organization",
            "No Doc",
            "Kode Provinsi",
            "Tahun",
            "Bulan",
            "Kode Jenis Produk",
            "Amount Alokasi",
        ];
    }
}
