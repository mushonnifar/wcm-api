<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportSOExport implements FromCollection, WithHeadings
{

    /**
     * @return Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            NomorSalesOrder
            SOItem
            SalesOrganization
            DistributionChannel
            Division
            SalesOffice
            DeskripsiSalesOffice
            SalesGroup
            DeskripsiSalesGroup
            SOLegacy
            KecamatanSO
            KecamatanSODesc
            ProvinsiDistributor
            KabupatenDistributor
            Distributor
            NamaDistributor
            EndUser / Pengecer
            ProvinsiTujuan
            DeskripsiProvinsiTujuan
            NegaraTujuan
            DeskripsiNegaraTujuan
            NomorKontrak
            TanggalMulaiKontrak
            TanggalBerakhirKontrak
            TanggalSODibuat
            TanggalDokumen
            TanggalSOReleased
            PaymentTerm
            PaymentMethod
            NomorMaterial
            DeskripsiMaterial
            MaterialGroup
            AlokasiAsal
            AlokasiOperasional
            QuantitySO
            UnitofMeasure
            MataUang
            HargaJual(exc . PPN)
            PPN
            Total
            Harga / Ton(InclPPn)
            HargaTotal(InclPPn)
            NomorDO
            TanggalPGI
            PlantSO
            GudangSO
            GudangSODeskripsi
            KodeGudang
            GudangPengambilan
            QuantityDO
            QuantitySO-do
            StatusSO
            Remarks
            B / LNumber
            NomorSPE
            SisaSPE
            PGIqty
            TotalHargaTonasePGI
            OutstandingSO
            SOType
            SOTypeDescription
            ProvinsiGudang
            KabupatenGudang
            PaymentTerm
            PaymentMethod
            BatasAkhirPengambilan
            NOPO
            TanggalPO
            SOCreatedBy
            Ext . FinancialDoc . No
            OpeningDate
            LatestShipmentDate
            ExpiryDate
            OpeningBankKey
            Description
            Sektor
            NoBilling
            BillingDate
            Incoterm1
            Incoterm2
            BillingQuantity
            BillingNet
            BillingTax
            PODStatus
            PODStatusDesc
            FinanceDoc . Number
            PortofDischarge
            PortofLoading,

        ]
    }
}
