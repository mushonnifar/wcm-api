<?php

namespace App\Exports;

use App\Models\ContractItem;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class SpjbOprExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        return [

            'Sales Organization',
            'No Doc',
            'Kode Distributor',
            'Diskripsi Distributor',
            'Kode Provinsi',
            'Diskripsi Provinsi',
            'Kode Kabupaten',
            'Diskripsi Kabupaten',
            'Kode Kecamatan',
            'Diskripsi Kecamatan',
            'Tahun',
            'Bulan',
            'Kode Jenis Produk',
            'Diskripsi Jenis Produk',
            'Amount Alokasi',
            'SO',
            'SO Approve',
            'Sisa SO',
        ];
    }

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function collection()
    {
        return DB::table('wcm_contract_item AS tb1')
            ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
            ->leftjoin('wcm_customer AS tb3', 'tb2.customer_id', '=', 'tb3.id')
            ->leftjoin('wcm_sales_office as z', 'z.id', '=', 'tb1.sales_office_id')
            ->leftjoin('wcm_sales_group as y', 'y.id', '=', 'tb1.sales_group_id')
            ->leftjoin('wcm_sales_unit as x', 'x.id', '=', 'tb1.sales_unit_id')
            ->leftjoin('wcm_product as w', 'w.id', '=', 'tb1.product_id')
            ->leftjoin(DB::raw("(SELECT a.contract_id, b.product_id, MONTH(a.order_date) as order_dates,c.sales_unit_id, ISNULL(SUM(b.qty),0) as qty FROM [dbo].[wcm_orders] a
                LEFT JOIN wcm_order_item b ON a.id = b.order_id
                LEFT JOIN wcm_retail c ON b.retail_id = c.id
                where a.status IN ('r','i','u','c','k')
                GROUP BY a.contract_id, b.product_id,MONTH(a.order_date), c.sales_unit_id
                ) as so"), function ($q) {
                $q->on('so.contract_id', '=', 'tb1.contract_id')
                    ->on('so.sales_unit_id', 'tb1.sales_unit_id')
                    ->on('so.order_dates', DB::raw('CONVERT(INT,tb1.month)'))
                    ->on('so.product_id', 'tb1.product_id');
            })
            ->leftjoin(DB::raw("(SELECT a.contract_id, b.product_id, MONTH(a.order_date) as order_dates,c.sales_unit_id, ISNULL(SUM(b.qty),0) as qty FROM [dbo].[wcm_orders] a
                LEFT JOIN wcm_order_item b ON a.id = b.order_id
                LEFT JOIN wcm_retail c ON b.retail_id = c.id
                where a.status IN ('y','s')
                GROUP BY a.contract_id, b.product_id,MONTH(a.order_date), c.sales_unit_id
                ) as soApprove"), function ($q) {
                $q->on('soApprove.contract_id', '=', 'tb1.contract_id')
                    ->on('soApprove.sales_unit_id', 'tb1.sales_unit_id')
                    ->on('soApprove.order_dates', DB::raw('CONVERT(INT,tb1.month)'))
                    ->on('soApprove.product_id', 'tb1.product_id');
            })
            ->select(
                'tb2.sales_org_id',
                'tb2.number',
                'tb2.customer_id',
                'tb3.full_name',
                'tb1.sales_office_id',
                'z.name as sales_office_name',
                'y.district_code',
                'y.name as sales_group_name',
                'tb1.sales_unit_id',
                'x.name as sales_unit_name',
                'tb1.year',
                'tb1.month',
                'tb1.product_id',
                'w.name as product_name',
                'tb1.initial_qty',
                DB::raw('ISNULL(so.qty,0) as so'),
                DB::raw('ISNULL(soApprove.qty,0) as so_approve'),
                DB::raw('ISNULL(tb1.initial_qty,0) - (ISNULL(so.qty,0)+ISNULL(soApprove.qty,0)) as sisa')
            )
            ->where('tb2.uuid', $this->uuid)->get();
    }
}
