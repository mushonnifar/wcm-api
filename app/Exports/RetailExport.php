<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RetailExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    public function __construct(Collection $items)
    {
        $this->items = $items;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->items->map(function($item){
            return [
                $item->code,
                $item->name,
                $item->owner,
                $item->address,
                $item->sales_office_id,
                $item->sales_office_name,
                $item->sales_group_id,
                $item->sales_group_name,
                $item->sales_unit_id,
                $item->sales_unit_name,
                $item->village,
                $item->tlp_no,
                $item->fax_no,
                $item->hp_no,
                $item->email,
                $item->npwp_no,
                $item->npwp_register,
                $item->siup_no,
                $item->valid_date_siup,
                $item->situ_no,
                $item->valid_date_situ,
                $item->tdp_no,
                $item->valid_date_tdp,
                $item->recomd_letter,
                $item->recomd_letter_date,
                $item->status,
            ];
        });
    }

    public function headings(): array
    {
        return [
            "Kode Pengecer",
            "Nama Pengecer",
            "Nama Pemilik Perusahaan",
            "Alamat Pengecer",
            "Kode Provinsi",
            "Nama Provinsi",
            "Kode Kota/Kabupaten",
            "Nama Kota/Kabupaten",
            "Kode Kecamatan",
            "Nama Kecamatan",
            "Desa",
            "No Telpon",
            "Nomor Fax",
            "Nomor Handphone",
            "Email",
            "NPWP",
            "Masa Berlaku NPWP",
            "SIUP",
            "Masa Berlaku SIUP",
            "SITU",
            "Masa Berlaku SITU",
            "TDP",
            "Masa Berlaku TDP",
            "Surat Rekomendasi Dinas",
            "Masa Berlaku ",
            "Status"
        ];
    }

    
}
