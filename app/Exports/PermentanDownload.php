<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PermentanDownload implements FromCollection,WithHeadings, ShouldAutoSize
{
	public function __construct(Collection $items)
	{
		$this->items = $items;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->items;
    }

    public function headings():array{
    	return [
    		"Number",
    		"Name",
    		"Tahun",
    		"Tanggal",
    		"Status",
    	];
    }
}
