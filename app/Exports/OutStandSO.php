<?php

namespace App\Exports;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Sheet;

class OutStandSO implements FromCollection,WithHeadings, ShouldAutoSize
{
    public function __construct(Collection $items,$columns=[])
	{
		$this->items=$items;
		$this->columns=$columns;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->items->map(function($item){
            return [
            $item->sales_org_id,
            $item->distrib_channel_id,
            $item->sales_division_id,
            $item->sales_office_id,
            $item->sales_office_name,
            $item->sales_group_id,
            $item->sales_group_name,
            $item->customer_name,
            $item->sales_unit_id,
            $item->sales_unit_name,
            '-',
            '-',
            $item->customer_id,
            $item->customer_name,$item->customer_owner,
            $item->retail_name,
            '-','-','-',
            $item->contract_number,
            '-','-',
            $item->so_number,
            $item->order_date,
            $item->order_date,
            '-',
            $item->payment_term,
            $item->payment_method,
            $item->material_no,
            $item->mat_desc,
            $item->product_name,
            $item->asal_qty,
            $item->oprasional_qty,
            $item->qty,
            $item->unit,
            $item->currency,
            intval($item->total_price_before_ppn)-intval($item->qty),
            $item->ppn,
            $item->total_price_before_ppn,
            '-',
            $item->total_price,
            $item->delivery_number,
            $item->delivery_date,
            $item->plant_code,
            '-',
            $item->plant_name,
            $item->plant_code,
            $item->plant_name,
            $item->delivery_qty,
            intval($item->qty)-intval($item->delivery_qty),
            $item->status_name,
            '-',
            '-',
            '-',
            '-',
            $item->delivery_qty,
            intval($item->qty)-intval($item->delivery_qty),
            $item->so_type,
            $item->so_type_desc,
            $item->plant_sales_office_name,
            $item->plant_sales_group_name,
            $item->term_of_payment,
            $item->payment_method_code,
            $item->good_redemption_due_date,
            $item->order_number,
            $item->order_date,
            $item->created_name,
            '-',
            '-',
            '-',
            '-',
            '-',
            '-',
            $item->sector,
            $item->sap_billing_dp_doc,
            $item->billing_date,
            $item->delivery_method_name,
            $item->sales_group_name,
            '-',
            '-',
            '-',
            '-',
            '-',
            '-',
            '-',
            '-',       
            ];
        });
    }

    public function headings(): array
    {
        if ($this->columns) return $this->columns;

        $firstRow = $this->items->first();

        if ($firstRow instanceof Arrayable || \is_object($firstRow)) {
            return array_keys(Sheet::mapArraybleRow($firstRow));
        }

        return $this->items->collapse()->keys()->all();
    }
}
