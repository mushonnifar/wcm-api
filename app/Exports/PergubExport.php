<?php

namespace App\Exports;

use App\models\ContractGoverment;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PergubExport implements FromCollection, WithHeadings
{
    private $pergub;

    public function __construct(ContractGoverment $pergub)
    {
        $this->pergub = $pergub;
    }

    /**
     * @return Collection
     */
    public function collection()
    {

        $items = $this->pergub->items()
            ->leftjoin('wcm_sales_group as z','z.id','=','sales_group_id')
            ->orderBy('sales_office_id', 'asc')
            ->orderBy('sales_group_id', 'asc')
            ->orderBy('product_id', 'asc')
            ->orderBy(DB::raw('CONVERT(INT,month)'), 'asc')
            ->select('wcm_contract_gov_item.*','z.district_code')
            ->get();

        return $items->map(function ($item) {
            return [
                $item->contractgoverment->sales_org_id,
                $item->contractgoverment->number,
                (int) $item->sales_office_id,
                (int) $item->district_code,
                $item->year,
                (int) $item->month,
                $item->product_id,
                $item->initial_qty,
            ];
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            "Sales Organization",
            "No Doc",
            "Kode Provinsi",
            "Kode Kabupaten",
            "Tahun",
            "Bulan",
            "Kode Jenis Produk",
            "Amount Alokasi",
        ];
    }
}
