<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Imports\RdkkImport;
use App\Models\MatrixRdkk;
use App\Models\PlantingPeriod;
use App\Models\Product;
use App\Models\Rdkk;
use App\Models\RdkkItem;
use App\Models\Retail;
use App\Models\SalesOrgUserAssg;
use App\Traits\ValidationTemplateImport;
use Carbon\Carbon;
use Excel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yajra\DataTables\DataTables;

class RdkkController extends Controller
{
    use ValidationTemplateImport;
    public function index(Request $request, $exported = false)
    {
        \LogActivity::addToLog('get all rdkk group');
        $where = array();
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $header = Rdkk::getAll($where);

        $columns = [
            'tb1.number' => 'number',
            'tb9.full_name' => 'customer_name',
            'tb1.customer_id' => 'customer_id',
            'tb5.name' => 'sales_org_name',
            'tb11.name' => 'farmer_group_name',
            'tb10.code' => 'retail_code',
            'tb10.name' => 'retail_name',
            'tb1.status' => 'status',
            'tb8.name' => 'sales_office_name',
            'tb8.id' => 'sales_office_id',
            'tb7.name' => 'sales_group_name',
            'tb7.id' => 'sales_group_id',
            'tb6.name' => 'sales_unit_name',
            'tb6.id' => 'sales_unit_id',
            'tb1.land_area' => 'land_area',
            'tb3.desc' => 'farmer_period_name',
            'tb2.P01' => 'P01',
            'tb2.P02' => 'P02',
            'tb2.P03' => 'P03',
            'tb2.P04' => 'P04',
            'tb2.P05' => 'P05',
            'tb2.P51' => 'P51',
            'tb2.P54' => 'P54',
            'tb1.indeks_pertanaman' => 'indeks_pertanaman',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
            'tb1.reference_rdkk' => 'reference_rdkk',
            'tb1.planting_area' => 'planting_area',

            // number
            // kode distributor
            // reference dkk

        ];
        if ($exported) {
            $header->select(
                'tb1.number',
                'tb1.customer_id',
                'tb9.full_name as customer_name',
                'tb10.code as retail_code',
                'tb10.name as retail_name',
                DB::raw("
                (CASE
                    WHEN tb1.status = 'y' THEN 'Active'
                    WHEN tb1.status = 'n' THEN 'Inactive'
                    WHEN tb1.status = 'p' THEN 'Suspend'
                    WHEN tb1.status = 'd' THEN 'Draft'
                    WHEN tb1.status = 's' THEN 'Submited'
                    WHEN tb1.status = 'z' THEN 'Partial Active'
                    ELSE '-'
                END) as status_name"),
                'tb8.name as sales_office_name',
                'tb7.name as sales_group_name',
                'tb6.name as sales_unit_name',
                'tb3.desc as farmer_period_name',
                'tb1.land_area',
                'tb1.indeks_pertanaman',
                'tb1.planting_area',
                'tb2.P01',
                'tb2.P02',
                'tb2.P03',
                'tb2.P04',
                'tb2.P05',
                'tb2.P51',
                'tb2.P54'
            );
            $products = Product::pluck('id')->unique();
            $products->each(function ($item) use (&$header) {
                $header->addSelect(
                    DB::raw("(".
                        DB::table("wcm_rdkk_item")
                        ->select(DB::raw("
                        (CASE
                            WHEN status = 'y' THEN 'Active'
                            WHEN status = 'n' THEN 'Inactive'
                            WHEN status = 'p' THEN 'Suspend'
                            WHEN status = 'd' THEN 'Draft'
                            WHEN status = 's' THEN 'Submited'
                            WHEN status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"))
                        ->whereRaw("rdkk_id = tb1.id")
                        ->whereRaw("product_id='{$item}'")
                        ->toSql()
                    .") as {$item}_status")
                )
                ->addSelect(
                    DB::raw("(" .
                        DB::table("wcm_rdkk_item")
                            ->select(DB::raw("ISNULL(sum(approved_qty),0)"))
                            ->whereRaw("rdkk_id = tb1.id")
                            ->whereRaw("product_id='{$item}'")
                            ->toSql()
                        . ") as {$item}_approved_qty")
                );
            });
            $header->addSelect("tb1.reference_rdkk");
        }

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $header->whereIn("tb7.id", $filters["sales_group_id"]);
                }
            }
        }

        $model = DataTables::of($header)
            ->filter(function ($header) use ($request, $columns) {
                $this->filterColumn($columns, $request, $header);
            })
            ->order(function ($header) use ($request, $columns, $exported) {
                !$exported && $this->orderColumn($columns, $request, $header);
            });
        if ($exported) {
            return $model->getFilteredQuery()->get();
        }

        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
    public function product()
    {
        \LogActivity::addToLog('get produk rdkk');
        $data = Product::select('id', 'name')->orderBy('id', 'asc')->get();
        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
    public function planting()
    {
        \LogActivity::addToLog('get produk rdkk');
        $data = PlantingPeriod::select('id', 'desc')->where('status', 'y')->get();
        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data = $this->index($request, true);

        $columns = [[" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "Kuantum Pengajuan", " ", " ", " ", " ", "Kuantum Persetujuan", " ", " ", " "], ["No Rdkk", "Kode Distributor", "Nama Distributor", "Kode Pengecer", "Nama Pengecer", "Status", "Provinsi", "Kabupaten", "Kecamatan", "Luas Lahan", "Indeks Pertanaman", "Luas Tanam", "Periode Tanam", "UREA", "NPK", "Organik", "ZA", "SP-36", "UREA", "NPK", "Organik", "ZA", "SP-36", "ORGANIK CAIR", "NPK KAKAO","Refrensi RDKK"]];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:A2')->setCellValue('A1', 'No RDKK')->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('B1:B2')->setCellValue('B1', 'Kode Distributor')->getStyle('B1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('C1:C2')->setCellValue('C1', 'Nama Distributor')->getStyle('C1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('D1:D2')->setCellValue('D1', 'Kode Pengecer')->getStyle('D1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('E1:E2')->setCellValue('E1', 'Nama Pengecer')->getStyle('E1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('F1:F2')->setCellValue('F1', 'Status')->getStyle('F1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('G1:G2')->setCellValue('G1', 'Provinsi')->getStyle('G1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('H1:H2')->setCellValue('H1', 'Kabupaten')->getStyle('H1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('I1:I2')->setCellValue('I1', 'Kecamatan')->getStyle('I1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('J1:J2')->setCellValue('J1', 'Luas Lahan')->getStyle('J1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('K1:K2')->setCellValue('K1', 'Indeks Pertanaman')->getStyle('K1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('L1:L2')->setCellValue('L1', 'Luas Tanam')->getStyle('L1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('M1:M2')->setCellValue('M1', 'Periode Tanam')->getStyle('M1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('N1:P1')->setCellValue('N1', 'UREA')->getStyle('N1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('Q1:S1')->setCellValue('Q1', 'NPK')->getStyle('Q1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('T1:V1')->setCellValue('T1', 'ORGANIK')->getStyle('T1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('W1:Y1')->setCellValue('W1', 'ZA')->getStyle('W1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('Z1:AB1')->setCellValue('Z1', 'SP-36')->getStyle('Z1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('AC1:AE1')->setCellValue('AC1', 'ORGANIK CAIR')->getStyle('AC1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('AF1:AH1')->setCellValue('AF1', 'NPK KAKAO')->getStyle('AF1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('AI1:AI2')->setCellValue('AI1', 'Referensi RDKK')->getStyle('AI1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $sheet->setCellValue('N2', 'Pengajuan')->getStyle('N2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('O2', 'Persetujuan')->getStyle('O2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('P2', 'Status')->getStyle('P2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $sheet->setCellValue('Q2', 'Pengajuan')->getStyle('Q2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('R2', 'Persetujuan')->getStyle('R2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('S2', 'Status')->getStyle('S2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('T2', 'Pengajuan')->getStyle('T2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('U2', 'Persetujuan')->getStyle('U2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('V2', 'Status')->getStyle('V2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('W2', 'Pengajuan')->getStyle('W2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('X2', 'Persetujuan')->getStyle('X2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('Y2', 'Status')->getStyle('Y2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('Z2', 'Pengajuan')->getStyle('Z2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('AA2', 'Persetujuan')->getStyle('AA2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $sheet->setCellValue('AB2', 'Status')->getStyle('AB2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('AC2', 'Pengajuan')->getStyle('AC2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('AD2', 'Persetujuan')->getStyle('AD2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $sheet->setCellValue('AE2', 'Status')->getStyle('AE2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('AF2', 'Pengajuan')->getStyle('AF2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->setCellValue('AG2', 'Persetujuan')->getStyle('AG2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $sheet->setCellValue('AH2', 'Status')->getStyle('AH2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        // $sheet->getStyle('A1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $start = 3;
        foreach ($data as $key => $value) {
            # code...
            $sheet->setCellValue('A' . $start, $value->number);
            $sheet->setCellValue('B' . $start, $value->customer_id);
            $sheet->setCellValue('C' . $start, $value->customer_name);
            $sheet->setCellValue('D' . $start, $value->retail_code);
            $sheet->setCellValue('E' . $start, $value->retail_name);
            $sheet->setCellValue('F' . $start, $value->status_name);
            $sheet->setCellValue('G' . $start, $value->sales_office_name);
            $sheet->setCellValue('H' . $start, $value->sales_group_name);
            $sheet->setCellValue('I' . $start, $value->sales_unit_name);
            $sheet->setCellValue('J' . $start, $value->land_area);
            $sheet->setCellValue('K' . $start, $value->indeks_pertanaman);
            $sheet->setCellValue('L' . $start, $value->planting_area);
            $sheet->setCellValue('M' . $start, $value->farmer_period_name);
            $sheet->setCellValue('N' . $start, $value->P01);
            $sheet->setCellValue('O' . $start, $value->P01_approved_qty);
            $sheet->setCellValue('P' . $start, $value->P01_status);
            $sheet->setCellValue('Q' . $start, $value->P02);
            $sheet->setCellValue('R' . $start, $value->P02_approved_qty);
            $sheet->setCellValue('S' . $start, $value->P02_status);
            $sheet->setCellValue('T' . $start, $value->P03);
            $sheet->setCellValue('U' . $start, $value->P03_approved_qty);
            $sheet->setCellValue('V' . $start, $value->P03_status);
            $sheet->setCellValue('W' . $start, $value->P04);
            $sheet->setCellValue('X' . $start, $value->P04_approved_qty);
            $sheet->setCellValue('Y' . $start, $value->P04_status);
            $sheet->setCellValue('Z' . $start, $value->P05);
            $sheet->setCellValue('AA' . $start, $value->P05_approved_qty);
            $sheet->setCellValue('AB' . $start, $value->P05_status);

            $sheet->setCellValue('AC' . $start, $value->P51);
            $sheet->setCellValue('AD' . $start, $value->P51_approved_qty);
            $sheet->setCellValue('AE' . $start, $value->P51_status);

            $sheet->setCellValue('AF' . $start, $value->P54);
            $sheet->setCellValue('AG' . $start, $value->P54_approved_qty);
            $sheet->setCellValue('AH' . $start, $value->P54_status);
            $sheet->setCellValue('AI' . $start, $value->reference_rdkk);

            $start += 1;
        }
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);
        $sheet->getColumnDimension('R')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);
        $sheet->getColumnDimension('T')->setAutoSize(true);
        $sheet->getColumnDimension('U')->setAutoSize(true);
        $sheet->getColumnDimension('V')->setAutoSize(true);
        $sheet->getColumnDimension('W')->setAutoSize(true);
        $sheet->getColumnDimension('X')->setAutoSize(true);
        $sheet->getColumnDimension('Y')->setAutoSize(true);
        $sheet->getColumnDimension('Z')->setAutoSize(true);
        $sheet->getColumnDimension('AA')->setAutoSize(true);
        $sheet->getColumnDimension('AB')->setAutoSize(true);

        $sheet->getColumnDimension('AC')->setAutoSize(true);
        $sheet->getColumnDimension('AD')->setAutoSize(true);
        $sheet->getColumnDimension('AE')->setAutoSize(true);

        $sheet->getColumnDimension('AF')->setAutoSize(true);
        $sheet->getColumnDimension('AG')->setAutoSize(true);
        $sheet->getColumnDimension('AH')->setAutoSize(true);

        $sheet->getColumnDimension('AI')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);

        $writer->save("download_rdkk.xlsx");
        header("Content-Type: application/vnd.ms-excel");
        return redirect(url('/download_rdkk.xlsx'));

        return Excel::download((new Download($data, $columns)), "Download Rdkk.xls");
    }

    public function detail($uuid = null)
    {
        \LogActivity::addToLog('get single data rdkk');
        is_uuid($uuid);
        $id = $this->findDataUuid(Rdkk::class, $uuid);
        $where['tb1.uuid'] = $uuid;
        $data = DB::table('wcm_rdkks as tb1')
        ->join('wcm_planting_period AS tb3', 'tb1.planting_period_id', '=', 'tb3.id')
        ->join('wcm_sales_unit AS tb6', 'tb1.sales_unit_id', '=', 'tb6.id')
        ->join('wcm_sales_group AS tb7', 'tb6.sales_group_id', '=', 'tb7.id')
        ->join('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
        ->join('wcm_customer AS tb4', 'tb1.customer_id', '=', 'tb4.id')
        ->join('wcm_retail AS tb10', 'tb1.retail_id', '=', 'tb10.id')
        ->select('tb1.id', 'tb1.planting_area', 'tb1.uuid', 'tb1.number', 'tb1.status', 'tb1.commodity', DB::raw("
                (CASE
                    WHEN tb1.status = 'y' THEN 'Active'
                    WHEN tb1.status = 'n' THEN 'Inactive'
                    WHEN tb1.status = 'p' THEN 'Suspend'
                    WHEN tb1.status = 'd' THEN 'Draft'
                    WHEN tb1.status = 's' THEN 'Submited'
                    WHEN tb1.status = 'z' THEN 'Partial Active'
                    ELSE '-'
                END) as status_name"), 'tb4.id as customer_id', 'tb4.full_name as customer_name',
                'tb1.retail_id', 'tb10.code as retail_code', 'tb10.name as retail_name',
                'tb8.id as sales_office_id', 'tb8.name as sales_office_name',
                'tb7.id as sales_group_id', 'tb7.name as sales_group_name', 'tb6.id as sales_unit_id', 'tb6.name as sales_unit_name',
                'tb3.id as farmer_period_id', 'tb3.desc as farmer_period_name','tb1.created_by', 'tb1.updated_by', DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at"),
                "tb1.reference_rdkk","tb1.indeks_pertanaman","tb1.land_area"
            )
        ->where($where)->get();
        $product = Rdkk::find($id->id)->Item()->orderBy('id', 'asc')->get();
        $data["can_approve"] = (!$this->isDistributor);
        $data[0]->planting_area = $id->planting_area;
        $data[0]->land_area = $id->land_area;

        $data['product'] = $product;
        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create rdkk');

        $attributes = $request->all();

        $this->validate($attributes, Rdkk::RuleCreate());
        // $product                      = explode(';', @$attributes['rdkk_item_id']);
        // $qty                          = explode(';', @$attributes['rdkk_item_qty']);
        // $id = DB::table('wcm_rdkks')->latest()->first();
        // ($id) ? $gagal = explode('RDKK', $id->number): $gagal[1]=0;
        // $value = (int) $gagal[1] + 1;
        // $number = str_pad($value, 8, '0', STR_PAD_LEFT);
        // $header['number'] = 'RDKK' . $number;
        $header['planting_period_id'] = $attributes['planting_period_id'];
        $header['customer_id'] = $attributes['customer_id'];
        $header['retail_id'] = $attributes['retail_id'];
        $header['planting_area'] = $attributes['planting_area'];
        $header['planting_from_date'] = @$attributes['planting_from_date'];
        $header['planting_thru_date'] = @$attributes['planting_thru_date'];
        $header['sales_unit_id'] = $attributes['sales_unit_id'];
        $header['commodity'] = ""; //$attributes['commodity'];
        $header['status'] = $attributes['status'];
        $header['created_by'] = Auth::user()->id;
        $header['land_area'] = $attributes['land_area'];
        $header['indeks_pertanaman'] = $attributes['indeks_pertanaman'];

        $arrproduct = Product::select('id')->get()->pluck('id');

        DB::beginTransaction();
        // get Reference RDKK
        $referenceRDKK = Rdkk::where(
            Arr::only($header,
                [
                    "customer_id", "retail_id", "planting_period_id", "sales_unit_id",
                ]
            )
        )->get()->last();
        $referenceRDKKNumber = $referenceRDKK ? $referenceRDKK->number : null;
        $header["reference_rdkk"] = trim($referenceRDKKNumber);

        $rdkkheader = Rdkk::create($header);
        $attributes['type'] = 'required';

        if ($rdkkheader) {
            $item = [];
            if ($attributes['type'] == 'approved') {

                $arrRequired = explode(';', $attributes['rdkk_item_required_id']);
                $arrApproved = explode(';', $attributes['rdkk_item_approved_id']);
                $arr = collect(array_merge($arrRequired, $arrApproved))->unique()->toArray();
                $required_qty = explode(';', $attributes['rdkk_item_required_qty']);
                $approvedqty = explode(';', $attributes['rdkk_item_approved_qty']);

                $fnArr = [];
                foreach ($arrproduct as $key => $v) {
                    // return response()->json(in_array($v, $arr),500,[],JSON_PRETTY_PRINT);
                    $item['rdkk_id'] = $rdkkheader->id;
                    $item['product_id'] = $v;
                    $item['approved_by'] = (!empty($arrApproved) && in_array($v, $arrApproved)) ? Auth::user()->id : null;
                    $item['required_qty'] = (!empty($arrRequired) && in_array($v, $arrRequired)) ? @$required_qty[array_search($v, $arrRequired)] : '';
                    $item['approved_qty'] = (!empty($arrApproved) && in_array($v, $arrApproved)) ? @$approvedqty[array_search($v, $arrApproved)] : '';
                    $item['status'] = $attributes['status'];
                    $item['created_by'] = $header['created_by'];
                    $this->validate($item, RdkkItem::RuleCreate());
                    $rdkkitem = RdkkItem::create($item);
                    array_push($fnArr, $item);
                    $item = [];
                }

                if ($attributes['status'] = 's') {
                    $sum = collect($fnArr)->pluck('required_qty')->sum();
                    if ($sum == 0) {
                        DB::rollback();
                        $response = responseFail('required quantity harus di isi');
                        $response['errors'] = ['required_qty' => ['required qty tidak boleh kosong']];
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }

            } else {
                $product = explode(';', $attributes['rdkk_item_required_id']);
                $qty = explode(';', $attributes['rdkk_item_required_qty']);
                $fnArr = [];
                foreach ($arrproduct as $key => $v) {
                    $item['rdkk_id'] = $rdkkheader->id;
                    $item['product_id'] = $v;
                    $item['required_qty'] = in_array($v, $product) ? $qty[array_search($v, $product)] : '';
                    $item['status'] = $attributes['status'];
                    $item['created_by'] = $header['created_by'];
                    $this->validate($item, RdkkItem::RuleCreate());
                    $rdkkitem = RdkkItem::create($item);
                    array_push($fnArr, $item);
                    $item = [];
                }
                if ($attributes['status'] = 's') {
                    $sum = collect($fnArr)->pluck('required_qty')->sum();
                    if ($sum == 0) {
                        DB::rollback();
                        $response = responseFail('required quantity harus di isi');
                        $response['errors'] = ['required_qty' => ['required qty tidak boleh kosong']];
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'));
                $response['data'] = $rdkkheader;
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }
    public function update(Request $request, $uuid)
    {
        \LogActivity::addToLog('update rdkk');
        is_uuid($uuid);

        $attributes = $request->all();
        $attributes['type'] = 'required';

        $header['planting_area'] = $attributes['planting_area'];
        $header['commodity'] = ""; //$attributes['commodity'];
        $header['status'] = $attributes['status'];
        $header['reference_rdkk'] = $attributes['reference_rdkk'];
        $header['type'] = $attributes['type'];
        $header['indeks_pertanaman'] = $attributes['indeks_pertanaman'];
        $header['land_area'] = $attributes['land_area'];

        $this->validate($header, Rdkk::RuleUpdate());

        $condition['customer_id'] = $attributes['customer_id'];
        $condition['retail_id'] = $attributes['retail_id'];
        $condition['sales_unit_id'] = $attributes['sales_unit_id'];
        $condition['planting_area'] = $attributes['planting_area'];
        $condition['planting_period_id'] = $attributes['planting_period_id'];
        $condition['status'] = $attributes['status'];

        $cekDuplicate = Rdkk::where($condition)->where('uuid', '!=', $uuid)->first();

        if ($cekDuplicate) {
            $response = responseFail(trans('messages.create-fail'));
            $response['messages'] = trans('messages.double-data');
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        $model = $this->findDataUuid(Rdkk::class, $uuid);
        $model->update(
            [
                'updated_by' => Auth::user()->id,
                'indeks_pertanaman' => $attributes['indeks_pertanaman'],
                'customer_id' => $attributes['customer_id'],
                'planting_period_id' => $attributes['planting_period_id'],
                'customer_id' => $attributes['customer_id'],
                'retail_id' => $attributes['retail_id'],
                'planting_area' => $attributes['planting_area'],
                'land_area' => $attributes['land_area'],
                'sales_unit_id' => $attributes['sales_unit_id'],
                'status' => $attributes['status'],
            ]
        );
        $header['updated_by'] = Auth::user()->id;
        $header['status'] = $attributes['status'];

        if ($attributes['type'] == 'approved') {

            $arrRequired = explode(';', $attributes['rdkk_item_required_id']);
            $arrApproved = explode(';', $attributes['rdkk_item_approved_id']);

            $requiredstatus = explode(';', $attributes['rdkk_item_required_status']);
            $approvedstatus = explode(';', $attributes['rdkk_item_approved_status']);

            $arr = collect(array_merge($arrRequired, $arrApproved))->unique();
            $required_qty = explode(';', $attributes['rdkk_item_required_qty']);
            $approvedqty = explode(';', $attributes['rdkk_item_approved_qty']);

            $fnArr = [];
            foreach ($arr as $key => $v) {
                if ($requiredstatus[$key] == "d") {
                    $item['required_qty'] = (in_array($v, $arrRequired)) ? @$required_qty[array_search($v, $arrRequired)] : '';
                    $item['status'] = $attributes['status'];
                    $item['updated_by'] = $header['updated_by'];

                    $this->validate($item, RdkkItem::RuleUpdate());

                    $rdkkitem = RdkkItem::where('rdkk_id', $model->id)->where('product_id', $v)->first();
                    $rdkkitem->update($item);
                    array_push($fnArr, $item);
                    $item = [];
                }
            }

            if ($attributes['status'] = 's' && !empty($fnArr)) {
                $sum = collect($fnArr)->pluck('required_qty')->sum();
                if ($sum == 0) {
                    DB::rollback();
                    $response = responseFail('required quantity harus di isi');
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            }

        } else {
            $product = explode(';', $attributes['rdkk_item_required_id']);
            $qty = explode(';', $attributes['rdkk_item_required_qty']);
            $status = explode(';', $attributes['rdkk_item_required_status']);

            $fnArr = [];

            foreach ($product as $key => $v) {
                if ($status[$key] == "d") {
                    $item['required_qty'] = $qty[$key];
                    $item['status'] = $attributes['status'];
                    $item['updated_by'] = $header['updated_by'];

                    $this->validate($item, RdkkItem::RuleUpdate());

                    $rdkkitem = RdkkItem::where('rdkk_id', $model->id)->where('product_id', $v)->first();
                    if ($rdkkitem) {
                        $rdkkitem->update($item);
                    } else {
                        $item['product_id'] = $v;
                        $item['rdkk_id'] = $model->id;
                        $redkkitem = RdkkItem::create($item);
                    }
                    array_push($fnArr, $item);
                    $item = [];
                }
            }

            if ($attributes['status'] = 's' && !empty($fnArr)) {
                $sum = collect($fnArr)->pluck('required_qty')->sum();
                if ($sum == 0) {
                    DB::rollback();
                    $response = responseFail('required quantity harus di isi');
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            }
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk rdkk header');

        $attributes = $request->all();

        DB::beginTransaction();

        $products = Product::select('id')->get()->pluck('id');
        $uuids = collect($attributes)->pluck('id');
        $status = Rdkk::whereIn('uuid', $uuids)->pluck('status')->unique()->count();

        if ($status != 1) {
            DB::rollback();
            $response = responseFail(['status' => 'Status yang dipilih lebih dari 1 (Satu)']);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $sales_org_assg = SalesOrgUserAssg::where('user_id', Auth::user()->id)->get()->pluck('sales_org_id');

        if ($sales_org_assg->contains(null)) {
            $sales_group_matrix = MatrixRdkk::get()->pluck('sales_group_id')->unique();
        } else {
            $sales_group_matrix = MatrixRdkk::whereIn('sales_org_id', $sales_org_assg)->get()->pluck('sales_group_id')->unique();
        }

        $statusChange = false;
        foreach ($attributes as $i => $iv) {
            is_uuid($iv['id']);
            $iv['updated_by'] = Auth::user()->id;
            $model = $this->findDataUuid(rdkk::class, $iv['id']);
            $dataItem = RdkkItem::where('rdkk_id', $model->id);
            $data = $this->approvalList($request->replace(['number' => $model->number]), true);
            if ($sales_group_matrix->contains($data[0]->sales_group_id) == true && !$this->isDistributor) {
                foreach ($products as $key => $value) {
                    # code...
                    $approved = $value . '_can_approve';
                    $status = $value . '_status';
                    $item = $model->item()->where('product_id', $value)->first();
                    if (!$item) continue;
                    if ($data[0]->$approved == 'y') {
                        if ($this->cekStatus($item->status, $iv['status'])) {
                            if ($iv['status'] == 'y') {
                                $item->update(['status' => 'y', 'approved_date' => date('Y-m-d H:i:s'), 'approved_qty' => $item->required_qty, 'approved_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
                            } else {
                                $item->update(['status' => $iv['status'], 'updated_by' => Auth::user()->id]);
                            }
                            $statusChange = true;
                        }
                    }
                }
            }

            if($this->isDistributor)
            {
                foreach ($products as $key => $value) {
                    # code...
                    $approved = $value . '_can_approve';
                    $status = $value . '_status';
                    $item = $model->item()->where('product_id', $value)->first();
                    if (!$item) continue;
                    if ($this->cekStatus($item->status, $iv['status'])) {
                        $item->update(['status' => $iv['status'], 'updated_by' => Auth::user()->id]);
                        $statusChange = true;
                    }
                }
                
            }
            $model->touchStatus();

            // return $data;

            // if($iv['status']=='s')
            // {
            //     $dataItem->where('status','d');
            // }else if($iv['status']=='d'){
            //     $dataItem->where('status','s');
            // }

            // if($iv['status']=='y' || $iv['status']=='n')
            // {
            //     $data = $this->approvalList($request->replace(['number'=>$model->number]),true);
            //     // return $data;
            //     if($sales_group_matrix->contains($data[0]->sales_group_id)==true){
            //         foreach ($products as $key => $value) {
            //             # code...
            //             $approved=$value.'_can_approve';
            //             $status=$value.'_status';
            //             $item = $model->item()->where('product_id', $value)->first();
            //             if($data[0]->$approved == 'y' && ($data[0]->$status == 's' || $data[0]->$status == 'y')  ){
            //                 if($iv['status']=='y'){
            //                     $item->update(['status'=>'y','approved_date'=>date('Y-m-d H:i:s'),'approved_qty'=>$item->required_qty,'approved_by'=>Auth::user()->id,'updated_by'=>Auth::user()->id]);
            //                 }else{
            //                     $item->update(['status'=>'n','updated_by'=>Auth::user()->id]);
            //                 }
            //             }else if($data[0]->$approved == 'y' && $data[0]->$status == 'n' && $iv['status']=='y' ) {
            //                 $item->update(['status'=>'y','updated_by'=>Auth::user()->id]);
            //             }
            //         }
            //     }
            // }else{
            //     $dataItem->update(['status' => $iv['status'],'updated_by'=>Auth::user()->id]);
            // }

        }

        if ($statusChange === false) {
            DB::rollback();
            $response = responseFail(['status' => 'Periksa Kembali Status RDKK, Tidak ada data yang diUpdate.']);
            $response['errors'] = ['Tidak ada data yang diUpdate'];
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique' => trans('messages.unique'),
            'email' => trans('messages.email'),
            'numeric' => trans('messages.numeric'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => $newMessages,
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getImport(Request $request)
    {
        \LogActivity::addToLog('get import rdkk');
        $attributes = $request->all();
        $this->validateImport($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $dataExcel = Excel::toArray(new RdkkImport, $file)[0];
            $product = Product::orderBy('id', 'asc')->get();
            $user = Auth::user()->id;

            if (!$this->compareTemplate("rdkk", array_keys($dataExcel[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }
            DB::beginTransaction();

            $uploadedData = [];
            foreach ($dataExcel as $key => $value) {
                // if (!@$value['nomor_rdkk']) {
                //     $id = Rdkk::get()->last();
                //     $gagal = explode('RDKK', $id->number);
                //     $number = (int) $gagal[1] + 1;
                //     $data['number'] = sprintf('RDKK%06s', $number);
                // } else {
                //     $data['number'] = $value['nomor_rdkk'];
                //     $this->validate($data, Rdkk::RuleRdkk());
                // }
                // $id = DB::table('wcm_rdkks')->latest()->first();
                // ($id) ? $gagal = explode('RDKK', $id->number): $gagal[1]=0;
                // $valueNumber = (int) $gagal[1] + 1;
                // $number= str_pad($valueNumber, 8, '0', STR_PAD_LEFT);
                // $data['number'] = 'RDKK' . $number;
                $data['customer_id'] = $value['kode_distributor'];
                $data['indeks_pertanaman'] = $value['indeks_pertanaman'];
                $data['land_area'] = $value['luas_lahan'];
                $data["reference_rdkk"]="";

                $plantingPeriod = PlantingPeriod::where("desc", $value["periode_tanam"])->first();
                $data['planting_period_id'] = $plantingPeriod ? $plantingPeriod->id : 0;
                $retail = Retail::where("code", $value['kode_pengecer'])->first();
                $data['sales_unit_id'] = $retail ? $retail->sales_unit_id : 0;
                $data['retail_id'] = $retail ? $retail->id : 0;
                $data['status'] = 's';
                $data['created_by'] = $user;
                $qty['required_qty'][0] = str_replace(',', '.', $value['kuantum_kebutuhan_urea_kg']);
                $qty['required_qty'][1] = str_replace(',', '.', $value['kuantum_kebutuhan_npk_kg']);
                $qty['required_qty'][2] = str_replace(',', '.', $value['kuantum_kebutuhan_organik_kg']);
                $qty['required_qty'][3] = str_replace(',', '.', $value['kuantum_kebutuhan_za_kg']);
                $qty['required_qty'][4] = str_replace(',', '.', $value['kuantum_kebutuhan_sp36_kg']);
                $qty['required_qty'][5] = (int) str_replace(',', '.', $value['kuantum_kebutuhan_organik_cair_liter']);
                $qty['required_qty'][6] = (int) str_replace(',', '.', $value['kuantum_kebutuhan_npk_kakao_kg']);

                $qty['required_qty'][0] = ($qty['required_qty'][0] == "" || $qty['required_qty'][0] == null  ) ? 0 : $qty['required_qty'][0] ;
                $qty['required_qty'][1] = ($qty['required_qty'][1] == "" || $qty['required_qty'][1] == null  ) ? 0 : $qty['required_qty'][1] ;
                $qty['required_qty'][2] = ($qty['required_qty'][2] == "" || $qty['required_qty'][2] == null  ) ? 0 : $qty['required_qty'][2] ;
                $qty['required_qty'][3] = ($qty['required_qty'][3] == "" || $qty['required_qty'][3] == null  ) ? 0 : $qty['required_qty'][3] ;
                $qty['required_qty'][4] = ($qty['required_qty'][4] == "" || $qty['required_qty'][4] == null  ) ? 0 : $qty['required_qty'][4] ;
                $data['required_qty'] = $qty['required_qty'];
                $data['customer_retail_relation']= [
                    'customer_id' => $value['kode_distributor'],
                    'retail_id'=> $data['retail_id']
                ];
                $data['retail_in_sales_unit_spjb'] = [
                    'customer_id' => $value['kode_distributor'],
                    'year' => $value['periode_tanam'],
                    'month' => date('m'),
                    'sales_unit_id' => $data['sales_unit_id'],
                ];

                $this->validateImportXls($data, Rdkk::RuleImport(), ($key+2));
                
                $data['planting_area'] = $value['luas_lahan'] * $value['indeks_pertanaman'];
                unset($data['required_qty']);unset($data['retail_in_sales_unit_spjb']);
                unset($data['customer_retail_relation']);

                // produsen, distributor, kelompok tani, pengecer, kecamatan, periode
                // Tidak boleh sama
                $rdkk = Rdkk::where(Arr::only($data, ['customer_id', 'retail_id', 'sales_unit_id', 'planting_period_id']));

                if ($rdkk->exists()) {
                    $rdkk = $rdkk->orderBy('number', 'desc')->first();
                    $data["reference_rdkk"] = trim($rdkk->number);

                }
                // else {
                //     $header = Rdkk::create($data);
                //     foreach ($product as $p => $v) {
                //         $item['rdkk_id']      = $header->id;
                //         $item['product_id']   = $v->id;
                //         $item['required_qty'] = (float) $qty['required_qty'][$p];
                //         $item['status']       = 's';
                //         $item['created_by']   = $user;
                //         $this->validate($item, RdkkItem::RuleCreate());
                //         RdkkItem::create($item);
                //     }

                //     $uploadedData['success'][] = $header;

                // }
                $header = Rdkk::create($data);
                foreach ($product as $p => $v) {
                    $item['rdkk_id'] = $header->id;
                    $item['product_id'] = $v->id;
                    $item['required_qty'] = (float) $qty['required_qty'][$p];
                    $item['status'] = 's';
                    $item['created_by'] = $user;
                    $this->validate($item, RdkkItem::RuleCreate());
                    RdkkItem::create($item);
                }

                $uploadedData['success'][] = $header;
            }
            try {
                $response = responseSuccess(trans('messages.create-success'));

                if (@$uploadedData['error']) {
                    $response = responseFail(trans('messages.create-fail'));
                    $response['data'] = $uploadedData['error'];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                DB::commit();
                $response['data'] = $uploadedData;
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($uuid)
    {
        \LogActivity::addToLog('delete permentan');
        is_uuid($uuid);
        $model = $this->findDataUuid(Rdkk::class, $uuid);
        DB::beginTransaction();
        try {
            $model->delete($uuid);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            array_push($data, $datas[$value]);

        }
        return $data;
    }

    public function approvalList(Request $request, $exported = false)
    {
        \LogActivity::addToLog('get all rdkk group');
        $where = array();

        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $header = Rdkk::getAll($where);

        $user = $request->user();
        $filters = $user->filterRegional;
//
        // return $filters;
        if (count($filters) > 0) {
            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $header->whereIn("tb7.id", $filters["sales_group_id"]);
            }
        }

        $columns = [
            'tb1.number' => 'number',
            'tb9.full_name' => 'customer_name',
            'tb5.name' => 'sales_org_name',
            'tb11.name' => 'farmer_group_name',
            'tb10.name' => 'retail_name',
            'tb1.status' => 'status',
            'tb8.name' => 'sales_office_name',
            'tb7.name' => 'sales_group_name',
            'tb6.name' => 'sales_unit_name',
            'tb8.id' => 'sales_office_id',
            'tb7.id' => 'sales_group_id',
            'tb6.id' => 'sales_unit_id',
            'tb2.P01' => 'P01',
            'tb2.P02' => 'P02',
            'tb2.P03' => 'P03',
            'tb2.P04' => 'P04',
            'tb2.P05' => 'P05',
            'tb2.P51' => 'P51',
            'tb2.P54' => 'P54',

        ];

        $products = Product::pluck('id')->unique();

        $model = DataTables::of($header)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($header) use ($request, $columns) {
                $this->orderColumn($columns, $request, $header);
            });
        if ($exported) {
            return $model->getFilteredQuery()->get();
        }

        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function doApproved(Request $request)
    {
        $params = $request->all();
        $response = array();
        $row = 1;
        $user = $request->user();
        $filters = $user->filterRegional;

        // if (!$this->isAdmin) {
        //     return response()->json(responseFail("You dont Have Permission"), 401);
        // }

        foreach ($params as $param) {
            if (!is_array($param)) {
                continue;
            }

            $validator = Validator::make($param, [
                "rdkk_id" => "required|exists:wcm_rdkks,id",
                "product_id" => "required|exists:wcm_product,id",
                "approved_qty" => "required|min:0",
            ]);
            if ($validator->fails()) {
                $response["errors"][$row] = responseFail(
                    $validator->errors()
                );
            }

            try {
                $rdkk = Rdkk::findOrFail($param["rdkk_id"]);
                $matrix = MatrixRdkk::where("product_id", $param["product_id"]);

                if (@$filters["sales_org_id"]) {
                    $matrix->whereIn("sales_org_id", @$filters["sales_org_id"]);
                }

                if (@$filters["sales_group_id"]) {
                    $matrix->whereIn("sales_group_id", @$filters["sales_group_id"]);
                }

                $matrix->firstOrFail();

                $item = $rdkk->item()
                    ->where("product_id", $param["product_id"])
                    ->firstOrFail();
                $item->approved_qty = $param["approved_qty"];
                $item->approved_by = $request->user()->id;
                $item->status = 'y';
                $item->approved_date = Carbon::now();
                $item->save();
                $rdkk->touchStatus();
                $response["success"][$row] = $item;
            } catch (ModelNotFoundException $e) {
                $response["errors"][$row] = responseFail(
                    trans("messages.approved-not-permitted")
                );
            }

            $row++;
        }

        return response()->json($response);
    }

    public function toggleItemStatus(Request $request)
    {
        $params = $request->input();
        $validator = Validator::make($params, [
            "*.rdkk_id" => "required",
            "*.status" => "in:d,y,n",
            "*.product_id" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 402);
        }

        $product = collect($params)->pluck('product_id');
        $rdkk_id = collect($params)->pluck('rdkk_id');

        $hasil = RdkkItem::where(function ($query) use ($params) {
            foreach ($params as $value) {
                # code...
                $query->orwhere(function ($query) use ($value) {
                    $query->where('product_id', $value['product_id'])->where('rdkk_id', $value['rdkk_id']);
                });
            }
        })->pluck('status')->unique()->count();

        if ($hasil > 1) {
            $response = responseFail(trans('messages.error-multiple-status-selected'));
            $response['messages']['status'] = trans('messages.error-multiple-status-selected');
            return response()->json($response, 402);
        }

        DB::beginTransaction();
        $statusChange = false;
        $response = array();
        foreach ($params as $param) {

            $rdkkitem = RdkkItem::where(Arr::except($param, "status"))->first();
            if (!$rdkkitem) {
                $response[$param["rdkk_id"]]["error"] = "Item Not Found";
                continue;
            }
            if ($param["status"] == 'd') {
                if ($rdkkitem->status != 'n' && $rdkkitem->status != 's') {
                    $response = responseFail(trans('messages.error-approval-rdkk-status-before-must-n'));
                    $response['messages']['status'] = trans('messages.error-approval-rdkk-status-before-must-n');
                    return response()->json($response, 402);
                }
            }
            if ($this->cekStatus($rdkkitem->status, $param['status'])) {
                if ($param['status'] == 'y') {
                    $rdkkitem->update(['status' => 'y', 'approved_date' => date('Y-m-d H:i:s'), 'approved_by' => Auth::user()->id, 'updated_by' => Auth::user()->id]);
                } else {
                    $rdkkitem->update(['status' => $param['status'], 'updated_by' => Auth::user()->id]);
                }
                $statusChange = true;
            }
            $response[$param["rdkk_id"]]["success"] = true;
        }

        foreach ($rdkk_id as $rdkk) {
            # code...
            $headerrdkk = Rdkk::where('id', $rdkk)->first();
            $headerrdkk->touchStatus();
        }

        if ($statusChange === false) {
            $response = responseFail(trans('Periksa Kembali Status RDKK, Tidak ada data yang diUpdate.'));
            $response['messages']['status'] = trans('Periksa Kembali Status RDKK, Tidak ada data yang diUpdate.');
            return response()->json($response, 402);
        }

        DB::commit();
        return response()->json($response);
    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'errors'    => 'Data tidak valid di baris ke : ' . $line,
                'message'     => $newMessages,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}
