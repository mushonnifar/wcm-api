<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ForgetPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Mail;

class ForgetPasswordController extends Controller
{
    //
	public $to_mail;
	public function __construct()
	{
		$this->to_mail="";
	}

	public function index (Request $request)
	{
		\LogActivity::addToLog('Lupa Password | Forget Password Send Email');

		$attributes = $request->all();

		$this->validate($attributes, ForgetPassword::ruleSendEmail());
		
		$valid_now = Carbon::now()->toArray();
		$valid_to = Carbon::now()->addHour(2)->toArray();
		
		
		$model=User::where('email',$attributes['email'])->first()->toArray();
		
		$this->to_mail=$model['email'];
		DB::beginTransaction();

		$cekDouble=ForgetPassword::where('user_id',$model['id'])
		->where('status','y')
		->whereRaw('GETDATE() <= link_valid_to_date')
		->first();        
		if($cekDouble)
		{
			DB::rollback();
			$response = responseFail(trans('messages.create-fail'));
			$response['messages']='Permintaan Perubahan Password Anda Sedang Aktif';
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}

		try {
			$attributes=['user_id'=>$model['id'],'link_create_date'=>$valid_now['formatted'],'link_valid_to_date'=>$valid_to['formatted']];
			$models=ForgetPassword::create($attributes);
			
			/*Find the row of user*/
			$model = ForgetPassword::where($attributes)->first();
			$datauser=User::where('id',$model->user_id)->first();
			$link=URL::to('/#/new-password/'.$model->uuid);

            Mail::send('forgetpassword', ['data'=>$model,'users'=>$datauser, 'valid_now'=>$valid_now['formatted'],'valid_to'=>$valid_to['formatted'],'link'=>$link] , function ($message) {
    			$message->subject("SUBJECT PASSWORD WCM PIHC");
	            $message->from('support@wcm-pupukindonesia.com', 'WCM PI SUPPORT');
	            $message->to($this->to_mail);
	        });       	 

			DB::commit();
			
			$response = responseSuccess(trans('messages.create-success'),$model);
			return response()->json($response, 201, [], JSON_PRETTY_PRINT);

		}catch (Exception $e){
			DB::rollback();
			$response = responseFail(trans('messages.create-fail'));
			$response['errors'] = $ex->getMessage();
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}


		
	}

	public function dateactive($uuid)
	{
		\LogActivity::addToLog('Cek Link Active Or Not');
		is_uuid($uuid);

		$cekDouble=ForgetPassword::where('uuid',$uuid)
								->whereRaw('GETDATE() <= link_valid_to_date')
								->where('status','y')
								->first();        
		if($cekDouble)
		{
			$response = responseSuccess('Reset Password Active');
			$response['messages']='Permintaan Perubahan Password Anda Sedang Aktif';
			return response()->json($response, 200, [], JSON_PRETTY_PRINT);
		}else{
			$response = responseFail('Reset Password Not Active');
			$response['messages']='Permintaan Perubahan Password Anda Sedang Tidak Aktif';
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}
	}

	public function store(Request $request,$uuid)
	{
		# code...
		\LogActivity::addToLog('Store New Password');

		is_uuid($uuid);
		$attributes=$request->all();
		// validate
		$this->validate($attributes,ForgetPassword::forgetpassword());

		// Cek Different password
		if($attributes['password'] != $attributes['re-password'])
		{
			$response["status"]= 0;
			$response["status_txt"]= "errors";
			$response["message"]= [array('password'=> 'Password Tidak Sama'),array('re-password'=>'Password Tidak Sama')];
			$response['messages']='Password Yang anda Inputkan Tidak Sama';
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}

		// Cek Active Url
		$cekDouble=ForgetPassword::where('uuid',$uuid)
		->where('status','y')
		->whereRaw('GETDATE() <= link_valid_to_date')
		->first();        
		if(!$cekDouble)
		{
			$response = responseFail(trans('messages.create-fail'));
			$response['messages']='Link Expired Atau Password Sudah Anda Ubah';
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}
		
		DB::beginTransaction();
		try {

			$dataforget['status']='n';
			$data['password']=Hash::make($attributes['password']);

			$model = ForgetPassword::where('uuid', $uuid)->first();
			$user = $this->findData(User::class, $model->user_id);
			// return response()->json($model);
			$user->update($data);
			$forget=DB::table('user_forget_password')->where('uuid',$model->uuid)->update($dataforget);

			DB::commit();
			$dataresponse=[
				'updated'=>date('d-m-Y H:i:s'),
				'user'=>$user->name,
			];
			$response = responseSuccess(trans('messages.update-success'),$dataresponse);
			return response()->json($response, 201, [], JSON_PRETTY_PRINT);

		} catch (Exception $ex) {
			DB::rollback();
			$response = responseFail(trans('messages.create-fail'));
			$response['errors'] = $ex->getMessage();
			return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}

	}


}
