<?php

namespace App\Http\Controllers\F5;

use App\Http\Controllers\Controller;
use App\Models\ContractItem;
use App\Models\ReportF5;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class JobCreateF5Controller extends Controller
{

    //

    public function create()
    {
        $months             = date('m');
        $year               = date('Y');
        $where['tb1.year']  = $year;
        $where['tb1.month'] = intval($months);
        $datasSpjb          = json_decode(ContractItem::getItemSPJBDownload($where)
                ->select('tb2.sales_org_id', 'tb2.customer_id', 'tb1.sales_group_id', 'tb1.year', 'tb1.month')
                ->groupby('tb2.sales_org_id', 'tb2.customer_id', 'tb1.sales_group_id', 'tb1.year', 'tb1.month')
                ->get(), true);
        
        $idData = [];
        DB::beginTransaction();
        try {

            foreach ($datasSpjb as $key) {
                # code...
                $f5check = ReportF5::where($key)->first();
                if (!$f5check) {
                    $attributes['sales_org_id']   = $key['sales_org_id'];
                    $attributes['sales_group_id'] = $key['sales_group_id'];
                    $attributes['customer_id']    = $key['customer_id'];
                    $attributes['sales_group_id'] = $key['sales_group_id'];
                    $attributes['month']          = intval($key['month']);
                    $attributes['year']           = $key['year'];
                    $attributes['status']         = 'd';
                    // $attributes['number']         = $this->genUniqF5();
                    $data                         = ReportF5::insert($attributes);
                    array_push($idData, $data['id']);
                }
            }

            DB::commit();

            Log::info('Job Create ReportF5', [
                'status' => 'success',
                'data'   => implode($idData, ','),
            ]);

            $response = responseSuccess(trans('messages.create-success'), $idData);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (Exception $ex) {
            DB::rollback();
            Log::info('Job Create ReportF5', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    private function genUniqF5()
    {
        $maxNumber = DB::table('wcm_report_f5')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "F5" . $number;
    }

    private function checkspjb($attributes)
    {
        $where['tb2.sales_org_id']    = $attributes['sales_org_id'];
        $where['tb2.customer_id']     = $attributes['customer_id'];
        $where['tb1.sales_office_id'] = $attributes['sales_office_id'];
        $where['tb1.sales_group_id']  = $attributes['sales_group_id'];
        $where['tb1.year']            = $attributes['year'];
        $where['tb1.month']           = $attributes['month'];
        $where['tb2.contract_type']   = 'operational';
        return ContractItem::getItemSPJBDownload($where)->first();
    }

}
