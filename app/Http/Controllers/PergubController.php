<?php

namespace App\Http\Controllers;

use App\Exports\PergubExport;
use App\Exports\Download;
use App\Imports\GubernurGov;
use App\Models\ContractGoverment;
use App\Models\ContractGovItem;
use App\Models\SalesGroup;
use App\Rules\SalesGroupRule;
use App\Traits\TraitContractGoverment;
use App\Rules\SalesOrgAssgRules;
use App\Rules\SalesOrgAlokasiOneAktive;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PergubController extends Controller
{
    //
    // use TraitContractGoverment;
    public function index(Request $request, $exported=false)
    {
        # code...
        \LogActivity::addToLog('get all Pergub');

        $columns = [
            'tb1.uuid'         => 'uuid',
            'tb1.number'       => 'pergub',
            'tb1.year'         => 'year',
            'tb1.created_at'   => 'created_at',
            'tb5.id'           => 'provinsi_id',
            'tb5.name'         => 'provinsi_name',
            'tb1.sales_org_id' => 'produsen_id',
            'tb3.name'         => 'produsen_name',
            'tb1.status'       => 'status',
        ];

        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }

        $query = ContractGoverment::getHeaderPergub($where,$exported);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }
        }

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }

        $model=$model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($uuid)
    {
        \LogActivity::addToLog('Get Pergub Item by uuid pergub nomer');

        is_uuid($uuid);
        // $attributes=$request->only('year');
        $this->validate(['uuid'=>$uuid],['uuid'=>"required|exists:wcm_contract_goverment,uuid"]);

        // Condition
        // $where['tb1.year'] = $attributes['year'];
        $where['tb2.uuid'] = $uuid;

        $model = ContractGovItem::getItemPergub($where)->get();


        // return $model->where('sales_group_id','3519');
        $datas = json_decode($model);

        $provinces = array_unique(array_column($datas, 'sales_office_id'));

        $uniq_produk = array_unique(array_column($datas, 'product_id'));

        $totals = [];

        foreach ($uniq_produk as $key => $value) {
            # code...
            $totals[$value]['id']    = $value;
            $totals[$value]['name']  = $datas[$key]->product_name;
            $totals[$value]['total'] = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $totals[$value]['month'][$j] = 0;
            }
        }

        $print = [];
        // Initial Total Di Provinsi

        // Provinces
        foreach ($provinces as $iprov => $prov) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_office_id'), $prov);
            $prov_id      = $datas[$iprov]->sales_office_id;
            $prov_name    = $datas[$iprov]->provinsi;
            $globProv     = $datas[$iprov];

            // Print Array Provinsi
            $provArr = array('id' => $prov_id, 'name' => $prov_name, 'kabupaten' => array());

            $keyItemsKab = array_keys(array_column($datas, 'sales_office_id'), $datas[$keyItemsProv[0]]->sales_office_id);

            // Make Arr Kabupaten
            $kabupatensData = $this->makeArr($datas, $keyItemsKab);
            $kabupatens     = array_unique(array_column($kabupatensData, 'sales_group_id'));

            // Provinces
            foreach ($kabupatens as $ikab => $vkab) {
                # code...
                $keyItemsKab = array_keys(array_column($kabupatensData, 'sales_group_id'), $vkab);
                $kab_id      = $kabupatensData[$ikab]->sales_group_id;
                $kab_name    = $kabupatensData[$ikab]->kabupaten;

                // Print Array Kabupaten
                $kabArr = array('id' => $kab_id, 'name' => $kab_name, 'product' => array());

                // Make Arr Kabupaten
                $products = $this->makeArr($kabupatensData, $keyItemsKab);
              
                // return response()->json($products);exit;
                $keyItemsProduct = array_unique(array_column($products, 'product_id'));

                foreach ($keyItemsProduct as $iprod => $vprod) {
                    # code...
                    $keyItemsProd = array_keys(array_column($products, 'product_id'), $vprod);

                    $product = $this->makeArr($products, $keyItemsProd);

                    // Print Product Kabupaten Punya
                    $prodArr = array('id' => $products[$iprod]->product_id, 'name' => $products[$iprod]->product_name, 'month' => array(), 'status_qty' => array());

                    $totalMonth = 0;

                    for ($i = 1; $i <= 12; $i++) {
                        # code...
                        $monKeyItems          = array_keys(array_column($product, 'month'), $i);
                        ($monKeyItems) ? $qty = is_null($product[$monKeyItems[0]]->initial_qty) ? 0 : $product[$monKeyItems[0]]->initial_qty : $qty = 0;

                        ($monKeyItems) ? $status = is_null($product[$monKeyItems[0]]->status) ? "-" : $product[$monKeyItems[0]]->status : $status = "";
                        array_push($prodArr['month'], $qty);
                        array_push($prodArr['status_qty'], $status);

                        $totals[$products[$iprod]->product_id]['month'][$i] += $qty;
                        $totals[$products[$iprod]->product_id]['total'] += $qty;
                        $totalMonth += $qty;
                    }

                    $count_status=collect($prodArr['status_qty'])->filter()->unique();//->all();
                    // if($kabArr['id']==3521 && $vprod=="P01"){
                    //     return $count_status->first();
                    // }
                    (count($count_status) > 1 ) ? $status_name="Partial Active" : $status_name = ($count_status->first() == 'n') ? 'InActive' : 'Active';

                    $prodArr['total']  = $totalMonth;
                    $prodArr['year']   = $product[0]->year;
                    $prodArr['date']   = $product[0]->created_at;
                    $prodArr['status'] = $status_name;
                    // $totals[$products[$iprod]->product_id]['sum']+=$totals[$products[$iprod]->product_id]['month'][$i];

                    array_push($kabArr['product'], $prodArr);

                }
                // Make Arr Detail Product
                array_push($provArr['kabupaten'], $kabArr);

            }
            // Push Arr Prov To Print
            array_push($print, $provArr);

        }

        // make Total Global Provinsi
        $newTotals = [];
        foreach ($totals as $key => $value) {
            # code...
            array_push($newTotals, $value);
        }

        $prints = array('provinsi' => $print, 
            'totals' => $newTotals, 
            'pergub_id' => @$datas[0]->contract_gov_id, 
            'pergub_no' => @$datas[0]->pergubno,
            'year'=> @$datas[0]->year,
            'sales_org_id'=>$datas[0]->sales_org_id,
            'sales_org_name'=>$datas[0]->sales_org_name);

        $response = responseSuccess(trans('messages.read-success'), $prints);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function showitem($uuid)
    {
        \LogActivity::addToLog('Get Item Data');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model             = ContractGovItem::getItemPergub($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addTolog('Update  Pergub Item');

        // Cek Pasti Atributs;
        $ceks = $request->only(['contract_gov_id', 'sales_office_id', 'sales_group_id', 'month', 'year', 'product_id']);

        // Cek Duplicate
        $cekOnItem = $this->cekDuplikasiItemUpdate(ContractGovItem::class, $ceks, $uuid);

        // Request All Atributs
        $attributes               = $request->all();
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        // Validate Atrributs
        $this->validate($attributes, ContractGovItem::pergupRuleCreateItems());

        DB::beginTransaction();
        if ($cekOnItem) {
            DB::rollback();
            $error = [
                "contract_gov_id" => [trans('messages.duplicate')],
                "sales_office_id" => [trans('messages.duplicate')],
                "sales_group_id"  => [trans('messages.duplicate')],
                "month"           => [trans('messages.duplicate')],
                "year"            => [trans('messages.duplicate')],
                "product_id"      => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $model = $this->findDataUuid(ContractGovItem::class, $uuid);

        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'));
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store (Request $request)
    {
        \LogActivity::addToLog('create pergub item');

        $attributes = $request->only(['contract_gov_id', 'sales_office_id', 'sales_group_id', 'year', 'product_id']);

        $this->validate($attributes, [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'year'            => 'required|numeric',
            'initial_qty'     => 'numeric|min:0',
        ]);

        $contract = ContractGoverment::where('id',$attributes['contract_gov_id'])->where('contract_type','pergub')->first();

        if(!$contract)
        {
            $response = responseFail(['Data'=> ['Data Tidak Valid di Pergub']] );
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $attrib =[
            'sales_org_id' => $contract->sales_org_id ,
            'rayonisasi' => [
                            'sales_org_id'  => $contract->sales_org_id,
                            'kode_provinsi' => $attributes['sales_office_id'],
                        ],
            'sales_group_id' => $attributes['sales_group_id'],

        ];
        $this->validate($attrib, ContractGovItem::rayonisasi_sales_org()+['sales_group_id' => new SalesGroupRule(@$attributes['sales_office_id']),]);
       
        $monthExists = ContractGovItem::where('contract_gov_id',$attributes['contract_gov_id'])
                        ->where('sales_group_id', $attributes['sales_group_id'])
                        ->where('sales_office_id',$attributes['sales_office_id'])
                        ->where('product_id',$attributes['product_id'])
                        ->where('year',$attributes['year'])->exists();
        if ($monthExists) {
            DB::rollback();
            $response         = responseFail(['status' => ["Produk yang dipilih telah tersedia, silahkan update manual"]]);
            $response['data'] = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }


        $attributes['status'] = 'y';
        $attributes['created_by'] = Auth::user()->id;
        $attributes['created_at'] = now();
        $arr = [];
        for ($i=1; $i <=12 ; $i++) { 
            # code...
            $attributes['month']=$i;
            $attributes['initial_qty']=0;

            $this->checkpergubOneStatusActive(['contract_gov_id'=>$contract->id,'sales_group_id'=> $attributes['sales_group_id'],'contract_type'=> 'permentan','month'=> $i,'year'=> $attributes['year'],'product_id'=> $attributes['product_id'],'sales_office_id'=> $attributes['sales_office_id'],'sales_org_id'=> $contract->sales_org_id,]);
            
            array_push($arr, $attributes);
        }
        DB::beginTransaction();
        try {
            $pergub = ContractGovItem::insert($arr);
            $contract->touchStatus();
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function detailpergubuuid(Request $request, $uuid)
    {
        \LogActivity::addToLog('Detail Pergub Table');
        is_uuid($uuid);

        $where['tb2.uuid'] = $uuid;

        $columns = [
            'tb1.id'              => 'id',
            'tb1.uuid'            => 'uuid',
            'tb1.contract_gov_id' => 'contract_gov_id',
            'tb2.number'          => 'pergubno',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb3.name '           => 'provinsi',
            'tb1.sales_group_id'  => 'sales_group_id',
            'tb4.name'            => 'kabupaten',
            'tb1.product_id'      => 'product_id',
            'tb5.name'            => 'product_name',
            'tb1.month'           => 'month',
            'tb1.initial_qty'     => 'initial_qty',
            'tb1.year'            => 'year',
            'tb1.status'          => 'status',
            'tb1.created_at'      => 'created_at',
        ];

        $query = ContractGovItem::getItemPergub($where);

        $model = DataTables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function updatesbulk(Request $request)
    {

        \LogActivity::addToLog('update Status Activated bulk pergub');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);

                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(ContractGovItem::class, $iv['id']);
                unset($iv['id']);
                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function getImport(Request $request)
    {
        \LogActivity::addToLog('Upload File Peraturan Gub');
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $user = $request->user();
        $filters = $user->filterRegional;

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new GubernurGov, $file)[0];

            $cekKeys = array_keys(@$data[0]);
            
            if($cekKeys[0]!="sales_organization" or $cekKeys[1]!="no_doc" or $cekKeys[2]!="kode_provinsi" or $cekKeys[3]!="kode_kabupaten" or $cekKeys[4]!="tahun"  or $cekKeys[5]!="bulan" or $cekKeys[6]!="kode_jenis_produk" or $cekKeys[7]!="amount_alokasi"){
                $response           = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template'=>['Format template Tidak Sesuai'],'file'=>['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
            $warning=[];
            foreach ($data as $key => $value) {
                # code...

                $headerData = [
                    'number'        => $value['no_doc'],
                    'contract_type' => 'pergub',
                    'sales_org_id'  => $value['sales_organization'],
                    'year'          => $value['tahun'],
                ];
                /*Find Header kecuali status n*/

                $header = ContractGoverment::where($headerData)
                    ->where('status', '!=', 'n')
                    ->first();
                if (!$header) {
                    $headerData += [
                        'rayonisasi'      => [
                            'sales_org_id'  => $value['sales_organization'],
                            'kode_provinsi' => $this->cekSalesOfficeCode($value['kode_provinsi']),
                        ],
                        'sales_office_id' => $this->cekSalesOfficeCode($value['kode_provinsi']),
                        'sales_group_id'  => $this->cekGroupCode($value['kode_kabupaten']),
                    ];

                    $rules = ContractGoverment::pergubRuleCreate();

                    $rules = $rules + [
                        'sales_group_id' => new SalesGroupRule(@$headerData['sales_office_id']),
                    ];

                    $this->validateImportXls($headerData, $rules, intval($key) + 2);

                    // $uniqueHeader = [
                    //     'year' => $value['tahun'],
                    //     'contract_type' => 'pergub',
                    //     'status' => 'y',
                    // ];
    
                    // $uniqueItems = [
                    //     "product_id" => $value['kode_jenis_produk'],
                    //     'sales_group_id' => $this->cekGroupCode($value['kode_kabupaten']),
                    // ];
                    
                    // $existsOrg = ContractGoverment::where($uniqueHeader)
                    //         ->whereHas("items", function($q) use($uniqueItems) {
                    //             $q->where($uniqueItems);
                    //         })->pluck('sales_org_id');
                    // if (!$existsOrg->isEmpty() && !$existsOrg->contains($value["sales_organization"])) {
                    //     return response()->json(
                    //         responseFail(["status"=>["Wilayah {$uniqueItems['sales_group_id']} Produk {$uniqueItems['product_id']} Sudah tersedia oleh " . $existsOrg->implode(",")]]),
                    //         400
                    //     )->throwResponse();
                    // } else {
                    //     Arr::forget($headerData, ['rayonisasi', 'sales_office_id', 'sales_group_id']);
                    //     $header = ContractGoverment::create($headerData);
                    // }

                    Arr::forget($headerData, ['rayonisasi', 'sales_office_id', 'sales_group_id']);
                    $headerData['created_by']=Auth::user()->id;
                    $header = ContractGoverment::create($headerData);

                }

                $detItem = [
                    'contract_gov_id' => $header->id,
                    'product_id'      => $value['kode_jenis_produk'],
                    'sales_office_id' => $this->cekSalesOfficeCode($value['kode_provinsi']),
                    'sales_group_id'  => $this->cekGroupCode($value['kode_kabupaten']),
                    'month'           => intval($value['bulan']),
                    'year'            => $value['tahun'],
                ];

                // cek item Duplicate
                $cekOnItem = $this->cekDuplikasiItem(ContractGovItem::class, $detItem);

                if ($cekOnItem) {
                    DB::rollback();
                    $response           = responseFail(['data_rangkap'=> ['Data rangkap pada baris ke-' . ($key + 2)]]);
                    $response['errors'] = 'Data rangkap pada baris ke-' . ($key + 2);
                    $response['data']   = $value;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                } else {
                    $detItem['initial_qty'] = $value['amount_alokasi'];
                    // $detItem['sales_org_rule']  = [
                    //     'sales_office_id' => $this->cekSalesOfficeCode($value['kode_provinsi']),
                    //     'sales_group_id'  => $this->cekGroupCode($value['kode_kabupaten']),
                    //     'month' => intval($value['bulan']),
                    //     'year' => $value['tahun'],
                    //     'contract_type' => 'pergub',
                    //     'product_id'      => $value['kode_jenis_produk'],
                    // ];
                    $this->validateImportXls($detItem, ContractGovItem::pergupRuleCreateItems($detItem), intval($key) + 2);

                    $attr['sales_org_id']= $value['sales_organization'];
                    $attr['pergub']  = [
                        'contract_type'=> 'pergub',
                        'sales_org_id' => $header->sales_org_id,
                        'year'         => $header->year,
                        'product_id'   => $detItem['product_id'],
                        'month' => intval($detItem['month']),
                        'sales_office_id' => $detItem['sales_office_id'],
                        'sales_group_id' => $detItem['sales_group_id'],
                    ];
                    $this->validateImportXls($attr, ['sales_org_id' => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],'pergub'=> new SalesOrgAlokasiOneAktive], intval($key) + 2);
                    
                    // unset($detItem['sales_org_rule']);
                    $detItem['created_by'] = Auth::user()->id;
                    $detItem['created_at'] = now();
                    
                    $headerItem = ContractGovItem::insert($detItem);

                    // warning message
                    $sumpermentan=$this->sumqty('permentan',$value);
                    $sumpergub=$this->sumqty('pergub',$value);
                    if($sumpermentan<$sumpergub)
                    {
                        array_push($warning,['Jumlah SK Prov '.$this->cekSalesOfficeCode($value['kode_provinsi']).' Sales Org '.$value['sales_organization'].', Bulan '.$value['bulan'].' melebihi jumlah permentan']);
                    }
                    // end warning
                }
                
            }
            
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'));
                $response['warning'] = $warning;
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateStatus(Request $request)
    {
        \LogActivity::addToLog('Update Status Header Pergub');
        $attributes = $request->all();

        $this->validate($attributes,["status" => "in:y,n","uuid" => "required|array"]);

        DB::beginTransaction();

        $contracts = ContractGoverment::whereIn("uuid", $attributes['uuid'])->get();
        $status = $contracts->pluck("status")->unique();

        if(count($status)>1){
            DB::rollback();
            $response           = responseFail(['status'=>['Status yang dipilih lebih dari 1 ']]);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        $arr=[];
        foreach ($attributes['uuid'] as $key => $value) {
            # code...
            $contract = ContractGoverment::where('uuid',$value)->where('contract_type','pergub')->first();
            if(!$contract){
                DB::rollback();
                $response           = responseFail(['data'=> ['Data tidak valid']]);
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $items = $contract->items()->get();
            
            foreach ($items as $key => $item) {
                # code...
                $attr['pergub']  = [
                    'contract_type'=> 'pergub',
                    'sales_org_id' => $contract->sales_org_id,
                    'year'         => $contract->year,
                    'product_id'   => $item['product_id'],
                    'month' => $item['month'],
                    'sales_office_id' => $item['sales_office_id'],
                    'sales_group_id' => $item['sales_group_id'],
                ];

                $govitem=$contract->items()->where(['contract_gov_id'=>$item['contract_gov_id'],'month'=>$item['month'],'year'=>$item['year'],'sales_office_id'=> $item['sales_office_id'],'product_id'=>$item['product_id'],'sales_group_id'=>$item['sales_group_id']])->first();
                
                if($attributes['status']=='y' and $govitem->status=='n'){
                    $this->validate($attr,ContractGovItem::headerSalesOrgPergubStatus());
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }else{
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }
                array_push($arr,$attr);
            }

            $contract->touchStatus();
        }
        

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'),$arr);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No SK Dinas Prov","Provinsi","Produsen","Tahun","Tanggal","Status"];
        return Excel::download((new Download($data,$columns)), "PergubDownload.xls");
    }

    private function cekDuplikasiHeader($model, $attrib)
    {
        $attrib['contract_type'] = 'pergub';

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    private function cekDuplikasiItem($model, $attrib)
    {

        $query = $model::where($attrib);//->where('status', '!=', 'n');

        $check = $query->first();

        return $check;
    }

    private function cekDuplikasiItemUpdate($model, $attrib, $uuid)
    {

        $query = $model::where($attrib)
            ->where('uuid', '!=', $uuid);

        $check = $query->first();

        return $check;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'errors'    => 'Data tidak valid di baris ke : ' . $line,
                'message'     => $newMessages,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    // tidak boleh ada 2 no pergub pada provinsi, produsen, yg sma dalam satu tahun
    private function cekNumberProdesenInYear($value)
    {

        $contract = ContractGoverment::where('contract_type', 'pergub')
            ->where('sales_org_id', $value['sales_org_id'])
            ->where('year', $value['year'])
            ->pluck('id');

        $item = ContractGovItem::whereIn('contract_gov_id', $contract)
            ->where('sales_office_id', $value['sales_office_id'])
            ->where('sales_group_id', $value['sales_group_id'])
            ->first();

        if ($item) {
            return false;
        }

        return true;
    }

    private function cekGroupCode($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length < 4) {
            $kabupaten = SalesGroup::where('district_code', $sales_group_id)->first();
            return @$kabupaten->id;
        }
        return $sales_group_id;
    }

    private function cekSalesOfficeCode($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }
    /**
     * Download Excell
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function export(Request $request)
    {
        $this->validate($request->all(), ['uuid' => 'required']);
        try {

            $uuid     = $request->get('uuid');
            $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return Excel::download(
            new PergubExport($contract),
            str_slug("{$contract->number}") . ".xls"
        );
    }

    private function sumqty($type,$value)
    {
        $sumpermentan = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', $type)
            ->where('tb1.month', intval($value['bulan']))
            ->where('tb2.year', $value['tahun'])
            ->where('tb1.product_id', $value['kode_jenis_produk'])
            ->where('tb1.status', 'y')
            ->where('tb2.sales_org_id', $value['sales_organization'])
            ->where('tb1.sales_office_id',$this->cekSalesOfficeCode($value['kode_provinsi']))->sum('tb1.initial_qty');
        return $sumpermentan;
    }

    private function checkpergubOneStatusActive($attr)
    {
        $query = DB::table('wcm_contract_gov_item AS tb1')
                ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
                ->where('tb2.contract_type', $attr['contract_type'])
                ->where('tb1.month', intval($attr['month']))
                ->where('tb2.year', $attr['year'])
                ->where('tb2.id','!=',$attr['contract_gov_id'])
                ->where('tb1.product_id', $attr['product_id'])
                ->where('tb1.sales_office_id',$attr['sales_office_id'])
                ->where('tb2.sales_org_id',$attr['sales_org_id'])
                ->where('tb1.sales_group_id',$attr['sales_group_id'])
                ->where('tb1.status','y')->exists();
        if($query)
        {
            DB::rollback();
            $response           = responseFail(['status'=>["Produsen ".$attr["sales_org_id"].", di Kabupaten: ".$attr['sales_group_id']." di tahun ".$attr["year"].", bulan ".$attr['month']." sudah ada yang aktif."]]);
            $response['data'] = $attr;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }
    }
}
