<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaymentMethod;

class PaymentMethodController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get payment method');

        $attrib = $request->only('sales_org_id', 'customer_id');

        $model = PaymentMethod::where(["sales_org_id" => $attrib['sales_org_id'], "customer_id" => $attrib['customer_id']])->get();

        $response = responseSuccess(trans('messages.read-success'), $model);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
