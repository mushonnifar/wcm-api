<?php

namespace App\Http\Controllers;

use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\ReportF5;
use App\Models\ProductLimit;
use App\Rules\CustomerExistsRule;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PenyaluranInitialStockF6Controller extends Controller
{

    private $type = "InitialStockF6";

    public function index($uuid = null)
    {
        \LogActivity::addToLog('get all Penyeluran initial stock F6');
        $where['tb1.distrib_resportable_type'] = $this->type;

        if ($uuid) {
            $where['tb5.uuid'] = $uuid;
        }

        $query   = DistribReports::getInitF5($where);
        $columns = [
            'tb1.number'       => 'number',
            'tb1.report_f5_id' => 'report_f5_id',
            'tb2.number'       => 'report_f5_number',
            'tb1.customer_id'  => 'customer_id',
            'tb3.full_name'    => 'customer_name',
            'tb1.month'        => 'month',
            'tb1.year'         => 'year',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb4.name'         => 'sales_org_name',
            'tb1.status'       => 'status',
            'tb1.created_by'   => 'created_by',
            'tb1.updated_by'   => 'updated_by',
            'tb1.created_at'   => 'created_at',
            'tb1.updated_at'   => 'updated_at',
        ];

        $model = \DataTables::of($query)
            ->filter(function ($query) use ($columns) {
                $this->filterColumn($columns, request(), $query);
            })
            ->order(function ($query) use ($columns) {
                $this->orderColumn($columns, request(), $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200);
    }

    public function create(Request $request)
    {
        \LogActivity::addToLog('Saving Penyeluran InitialStockF6');

        $validator_rules = [
            "initial_stock_id"  => "required|exists:view_initial_stock_f6,id",
            "sales_org_id"      => "required|exists:wcm_sales_org,id",
            "customer_id"       => ["required", new CustomerExistsRule],
            "sales_group_id"    => "required|digits:4|numeric|exists:wcm_sales_group,id",
            "distribution_date" => "required|date|date_format:Y-m-d", #|after_or_equal:-1Months"
        ];

        $params = $request->only([
            'initial_stock_id',
            'customer_id',
            'sales_group_id',
            'distribution_date',
            'sales_org_id',
        ]);

        $this->validate($params, $validator_rules);

        try {

            $reportF5 = $this->getReportF5(collect($params));

            $params["report_f5_id"]             = $reportF5->id;
            $params["status"]                   = "d"; // Draft
            $params["number"]                   = $this->generateCode(DistribReports::class, 'number', 'PKP', 10);
            $params["distrib_resportable_type"] = $this->type;
            $params["month"]                    = intval($reportF5->month);
            $params["year"]                     = $reportF5->year;
            $params['created_by']               = $request->user()->id;
            $params['created_at']               = date('Y-m-d H:i:s');
            
            $distribReport = DistribReports::insert($params);
        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return responseSuccess(trans("messages.create-success"), DistribReports::where(['number'=>$params['number']])->first());
    }

    public function createItem(Request $request)
    {
        \LogActivity::addToLog('Create Item Distribution / Penyaluran F6');

        $rules = DistribReportItems::ruleCreate();

        $result = [];

        try {
            $params = collect($request->all());

            /**
             * get Only First Array for get distrib report
             * and update this status
             *
             */
            $header = collect($params->first());

            $distrib = DistribReports::where("id", $header->get("distrib_report_id"))
                ->where("distrib_resportable_type", $this->type)
                ->first();
            /**
             * throw if distrib report is no exists
             */
            if (is_null($distrib)) {
                throw new \Exception("DistribReports id: {$header->get('distrib_report_id')} " . trans("messages.read-fail"), 1);
            }

            $distrib->status = $header->get("status");
            $distrib->save();

            $params->each(function ($req, $key) use ($rules, $request, &$result, &$distrib) {
                $this->validate($req, $rules);

                $params = collect($req)->only([
                    "distrib_report_id", "report_f5_id",
                    "retail_id", "qty", "status", "product_id",
                ]);

                $params->put("created_by", $request->user()->id);
                $params->put("updated_by", $request->user()->id);

                if ($item = $distrib->distribItems()
                    ->updateOrCreate(
                        $params->only(["retail_id", "product_id"])->toArray(), $params->except(["retail_id", "product_id"])->toArray()
                    )
                ) {

                    $result[] = $item;
                }

                return true;
            });
        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return responseSuccess(trans("messages.create-success"), $result);
    }

    public function getItemPenyaluran($uuid)
    {
        \LogActivity::addToLog('getItemPenyaluran F6');
        //$initialStock = InitialStocks::where("uuid", $uuid)->first();

        $distribReport = DistribReports::where("uuid", $uuid)->select("*", \DB::raw("CONVERT ( VARCHAR,distribution_date, 105 ) as distribution_date_format"))->first();

        try {
            if (is_null($distribReport)) {
                throw new \Exception("DistribReports Not Exists");
            }

            $headers      = $this->getMainHeader($distribReport);
            $distribItems = $distribReport->distrib_report_items_f6;
            $units        = $distribReport->retails_penyaluran_f6;
        } catch (\Exception $e) {
            $response = responseFail($e->getMessage());

            return response()->json($response, 500);
        }

        return responseSuccess(
            trans("messages.read-success"), ["header" => $headers, "data" => ["header" => $distribItems, "data" => $units]]
        );
    }

    private function getMainHeader($arr)
    {
        $array = [
            'id'                => $arr->id,
            'uuid'              => $arr->uuid,
            'number'            => $arr->number,
            'report_f5_id'      => $arr->report_f5_id,
            'sales_org_id'      => $arr->sales_org_id,
            'sales_org_name'    => $arr->salesorg->name,
            'customer_id'       => $arr->customer_id,
            'customer_name'     => $arr->customer->full_name,
            'sales_group_id'    => $arr->sales_group_id,
            'sales_group_name'  => $arr->salesGroup->name,
            'month'             => $arr->month,
            'year'              => $arr->year,
            'distribution_date' => $arr->distribution_date_format,
            'status'            => $arr->status,
            'status_name'       => $arr->status_name,
            "so_number"         => $arr->so_number,
            "products_limit"    => ProductLimit::get(),
        ];

        return $array;
    }

    private function getReportF5(Collection $params)
    {
        try {
            $date  = \Carbon\carbon::parse($params->get("distribution_date"));
            $day   = sprintf("%02d", $date->month);
            $month = sprintf("%02d", $date->month);
            $year  = $date->year;
        } catch (\Exception $e) {
            throw $e;
        }

        $report = ReportF5::where([
            'sales_org_id'   => $params->get("sales_org_id"),
            'customer_id'    => $params->get("customer_id"),
            'sales_group_id' => $params->get("sales_group_id"),
            'month'          => (int) $month,
            'year'           => $year,
        ])->first();

        if ($report) {
            return $report;
        }

        throw new \Exception("Report F5 - " . trans("messages.read-fail"));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            // Because Only Unsubmit & draf
            "status" =>'required|in:d,s',
        ]);

        try {
            $distrib = DistribReports::findOrFail($id);

            if (
                !in_array($distrib->status, ['s','d'] ) ||
                $distrib->distrib_resportable_type != $this->type
            ) {
                throw new \Exception("Invalid status");
            }

            $distrib->update(['status'=>$request->get('status')]);

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()
            ->json(
                responseSuccess(
                    trans("messages.update-success"),
                    $distrib
                )
            );
    }
    public function delete($id)
    {
        try {
            $distrib = DistribReports::findOrFail($id);

            if ($distrib->status !== 'd' || $distrib->distrib_resportable_type != $this->type) {
                throw new \Exception("Invalid status");
            }

            $distrib->delete();

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()
            ->json(
                responseSuccess(
                    trans("messages.deleted-success"),
                    $distrib
                )
            );
    }

}
