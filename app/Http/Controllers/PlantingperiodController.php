<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\PlantingPeriod;
use Maatwebsite\Excel\Facades\Excel;

class PlantingperiodController extends Controller
{
    //tes

    public function index (Request $request, $exported=false)
    {
    	\LogActivity::addToLog('get Plating Periode');

    	$query = PlantingPeriod::getAll();
        $columns = [
          'code'			=> 'code', 
          'desc'			=> 'desc', 
          'status'		=> 'status',
	        'from_date'	=> 'from_date', 
	        'thru_date'	=> 'thru_date', 
	        'create_at'	=> 'create_at', 
	        'update_at'	=> 'update_at',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if ($exported) return $model->getFilteredQuery();
        
        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }


    public function show ($uuid)
    {
    	\LogActivity::addToLog('Get Planting Period by uuid');

      is_uuid($uuid);     
      $model = $this->findDataUuid(PlantingPeriod::class, $uuid);

      $response = responseSuccess(trans('messages.read-success'), $model);
      return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function bulk (Request $request)
    {
    	\LogActivity::addToLog('update bulk Planting Period');

      $attributes = $request->all();
      DB::beginTransaction();
      try {
          $model = [];
          foreach ($attributes as $i => $iv) {
              is_uuid($iv['id']);
              
              $model = $this->findDataUuid(PlantingPeriod::class, $iv['id']);
              unset($iv['id']);
              $iv['updated_by'] = Auth::user()->id;

              $model->update($iv);
          }
          DB::commit();
          $response = responseSuccess(trans('messages.update-success'), $attributes);
          return response()->json($response, 200, [], JSON_PRETTY_PRINT);
      } catch (\Exception $ex) {
          DB::rollback();
          $response = responseFail(trans('messages.update-fail'));
          $response['errors'] = $ex->getMessage();
          return response()->json($response, 500, [], JSON_PRETTY_PRINT);
      }
    }


    public function store (Request $request)
    {
    	\LogActivity::addToLog('Creata Planting Period');

    	$attributes=$request->all();
    	$this->validate($attributes, PlantingPeriod::RuleCreate());
    	DB::beginTransaction();
      try {
          //code...
      		$attributes['created_by']=Auth::user()->id;
          $model=PlantingPeriod::create($attributes);
          DB::commit();
          $response = responseSuccess(trans('messages.create-success'), $model);
          return response()->json($response, 200, [], JSON_PRETTY_PRINT);
      } catch (\Exception $ex) {
          //Exc $ex;
          DB::rollback();
          $response = responseFail(trans('messages.create-fail'));
          $response['message'] = $ex->getMessage();
          return response()->json($response, 500, [], JSON_PRETTY_PRINT);
      }
    }

    public function destroy($uuid)
    {
    	# code...
    	\LogActivity::addToLog('Delete Soft Planting Period');
    	$model = $this->findDataUuid(PlantingPeriod::class, $uuid);
      
      DB::beginTransaction();
      try {          
          $model->delete();
          DB::commit();
          $response = responseSuccess(trans('messages.delete-success'), $model);
          return response()->json($response, 200, [], JSON_PRETTY_PRINT);
      } catch (\Exception $ex) {
          DB::rollback();
          $response = responseFail(trans('messages.delete-fail'));
          $response['errors'] = $ex->getMessage();
          return response()->json($response, 500, [], JSON_PRETTY_PRINT);
      }
    }


    public function update (Request $request, $uuid)
    {
    	\LogActivity::addToLog('Update Planting Period');

    	$attributes=$request->all();
    	$this->validate($attributes, PlantingPeriod::RuleUpdate());

    	$duplicates=$this->cekDuplicate($attributes['code'],$attributes['desc'],$uuid);

    	if($duplicates)
    	{
    		$response = responseFail($duplicates);        
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    	}

    	$model = $this->findDataUuid(PlantingPeriod::class, $uuid);
    	DB::beginTransaction();
      try {
          $attributes['updated_by'] = Auth::user()->id;
          $model->update($attributes);
          DB::commit();
          $response = responseSuccess(trans('messages.update-success'), $model);
          return response()->json($response, 200, [], JSON_PRETTY_PRINT);
      } catch (\Exception $ex) {
          DB::rollback();
          $response = responseFail(trans('messages.update-fail'));
          $response['errors'] = $ex->getMessage();
          return response()->json($response, 500, [], JSON_PRETTY_PRINT);
      }

    	


    }


    private function cekDuplicate($code, $desc, $uuid)
    {
    		$get=PlantingPeriod::whereRaw("( code = '".$code."' or [desc] = '".$desc."') ")
    												->whereRaw("uuid != '".$uuid."' ")
    												->first();
    		if($get)
    		{
    			if ($get->desc==$desc) {
    				# code...
    				$error = [
                "desc" => ['Duplicate Data'],
                ];
    			}else{
    				# code...
    				$error = [
                "code" => ['Duplicate Data'],
                ];
    			}

    			return $error;
    		}
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $model = $this->index($request, true);
        $data = $model->select(PlantingPeriod::getExportedColumns());

        return Excel::download(new Download($data->get()), "plant-periode.xls");
    }
}
