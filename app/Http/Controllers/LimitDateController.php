<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Helpers\LogActivity;
use App\Imports\LimitDateImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\LimitDate;
use App\Rules\LimiDateImportRule;
use Exception;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class LimitDateController extends Controller
{
    //
    public function index (Request $request, $exported=false)
    {
    	\LogActivity::addToLog('get master data Limit due date ');

        $query = LimitDate::getAll();
        $users = $request->user();
        $filters = $users->filterRegional;
        if (@$filters["sales_org_id"]) {
            $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
        }

        if (@$filters["sales_group_id"]) {
            $officeId = collect($filters["sales_group_id"])->map(function($groupID){
                return str_pad(substr($groupID, 0,2), 4, "0", STR_PAD_LEFT);
            });
            $query->whereIn("tb1.sales_office_id", $officeId);
        }
        
        $columns = [
            'tb1.uuid'=> 'uuid',
            'tb1.sales_org_id'=> 'sales_org_id',
            'tb1.min_qty'=> 'min_qty',
            'tb1.max_qty'=> 'max_qty',
            'tb1.duration_date'=> 'duration_date',
            'tb2.name'=> 'sales_org_name',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb1.incoterm' => 'incoterm',
            'tb4.name' => 'product_name',
        ];
        $model = Datatables::of($query) 
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });

        if ($exported) return $model->getFilteredQuery();
        
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($uuid)
    {
    	\LogActivity::addToLog('show detail limit due date');
    	is_uuid($uuid);

    	$where['tb1.uuid']=$uuid;
    	$model=LimitDate::getAll($where)->get();
    	$response = responseSuccess(trans('messages.read-success'), $model);
      return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request,$uuid)
    {
    	\LogActivity::addToLog('update data limit due date');

    	$attributes=$request->all();
    	$this->validate($attributes,LimitDate::ruleUpdate($uuid));
      /*
      * Cek Nilai Min Lebih Besar Max
      */
      if($attributes['min_qty'] > $attributes['max_qty'])
      {
        $response=responseFail(trans('messages.update-fail'));
        $response['messages']='Minimum Qty Melebihi Nilai Maximum Qty';
        return response()->json($response,500,[],JSON_PRETTY_PRINT);
      }

    	$model = $this->findDataUuid(LimitDate::class, $uuid);

    	DB::beginTransaction();
    	try {

    		$attributes['updated_by'] = Auth::user()->id;
    		$model->update($attributes)	;
    		DB::commit();
        $response = responseSuccess(trans('messages.update-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    	} catch (Exception $e) {

    		DB::rollback();
        $response = responseFail(trans('messages.update-fail'));
        $response['message'] = $e->getMessage();
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);

    	}
    }

    public function store(Request $request)
    {
    	$attributes=$request->all();
    	$this->validate($attributes,LimitDate::ruleCreate());
    	$attributes['created_by'] = Auth::user()->id;

      /*
      * Cek Nilai Min harus  Lebih dari Max
      */
      if($attributes['min_qty'] > $attributes['max_qty'])
      {
        $response=responseFail(trans('messages.create-fail'));
        $response['messages']='Minimum Qty Melebihi Nilai Maximum Qty';
        return response()->json($response,500,[],JSON_PRETTY_PRINT);
      }

    	DB::beginTransaction();
    	try {

    		$model=LimitDate::create($attributes);
    		DB::commit();
    		$response=responseSuccess(trans('messages.create-success'),$model);
    		return response()->json($response,200,[],JSON_PRETTY_PRINT);
    		
    	} catch (Exception $e) {

    		DB::rollback();
    		$response=responseFail(trans('messages.create-fail'));
    		$response['message']=$e->getMessage();
    		return response()->json($response,500,[],JSON_PRETTY_PRINT);
    	}

    }

    public function destroy($uuid)
    {
    	\LogActivity::addToLog('Delete Limit Due Date');

    	$model = $this->findDataUuid(LimitDate::class, $uuid);

    	DB::beginTransaction();
    	try {

    		$model->delete();
    		DB::commit();
    		$response = responseSuccess(trans('messages.delete-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    	} catch (Exception $e) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $e->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);

    	}
    }

    public function upload(Request $request) {
        LogActivity::addToLog("Upload Limit Date");
        $this->validate($request->all(), [
            "file" => "required|mimes:xls,xlsx"
        ]);
        DB::beginTransaction();
        $response = [];$error=false;
        if ($request->has("file")) {
            $uploadData = Excel::toArray(new LimitDateImport(), $request->file("file"))[0];
            $formData = $this->prepareUpload($uploadData);
            $line = 2;
            foreach($formData as $row) {
                $row["sales_office_id"] = sprintf("%04s",$row["sales_office_id"]);
                $validation = Validator::make($row, [
                    "sales_office_id" => "required|exists:wcm_sales_office,id",
                    "incoterm" => "required",
                    "min_qty" => "min:0",
                    "max_qty" => "min:0",
                    "duration_date" => "min:1",
                    "product_id" => "nullable|exists:wcm_product,id",
                    "sales_org_id" => [
                        "required", new LimiDateImportRule($row),
                    ]
                ]);

                if ($validation->fails()) {
                    $response["errors"][$line] = $validation->errors();
                    $resp = responseFail('import data gagal. baris : '.$line);
                    $resp['errors'] = $validation->errors();
                    return response()->json($resp, 500, [], JSON_PRETTY_PRINT);
            
                } else {
                    try {
                        $row['created_by'] = Auth::user()->id;
                        LimitDate::create($row);
                        $response["success"][$line] = $row;
                    } catch(\Exception $e) {
                        $response["errors"][$line]= $e->getMessage();
                    }
                }
                $line++;
            }
        }

        try {
            DB::commit();
            $resp = responseSuccess(trans('messages.create-success'), $response);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (Exception $e) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $e->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        // return $response;
    }

    private function prepareUpload(array $data) {
        $return = [];
        $headers = collect($data[0])->map(function($item){
            $fixHeader = [
                "company_code" => "sales_org_id",
                "kode_provinsi" => "sales_office_id",
                "incoterm" => "incoterm",
                "minimum_order" => "min_qty",
                "maksimum_order" => "max_qty",
                "produk" => "product_id",
                "jumlah_hari" => "duration_date"
            ];

            return Arr::get($fixHeader, Str::snake($item));
        });

        if ($headers) {
            $return = collect($data)->slice(1)->map(function($item) use ($headers){
                return collect($headers)->combine($item);
            });

        } 

        return $return->toArray();
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $model = $this->index($request, true);
        $data = $model->select(LimitDate::getExportedColumns());
        
        return Excel::download(new Download($data->get()), "limit-date.xls");
    }
}
