<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\ReportF6;
use PDF;

class ReportF6Controller extends Controller
{
    //
    public function index (Request $request)
    {
        \LogActivity::addToLog('Report Master F6 Datatable');
        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        $query = ReportF6::MasterReportF6($where);
        
        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }


        $columns = [
            'tb1.uuid' => 'uuid',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb2.name' => 'sales_org_name',
            'tb1.number' => 'number',
            'tb1.customer_id' => 'customer_id',
            'tb3.full_name' => 'customer_name',
            'tb1.month' => 'month',
            'tb1.year' => 'year',
            'tb1.submited_date' => 'submited_date',
            'tb1.status' => 'status',
            'tb1.sales_group_id'=> 'sales_group_id',
            'tb4.name'=>'sales_group_name',
            'tb5.id'=>'sales_office_id',
            'tb5.name'=>'sales_office_name',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                })
                ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'submited_date' || $value === 'year') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } elseif($value=="month"){
                if(strpos($request->get($value), ';') !== false){
                    $listMonth = explode(';', $request->get($value));
                    $query->whereIn($key, $listMonth);
                }else{
                    $query->where($key, '=', "{$request->get($value)}");
                }
            }else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }
}
