<?php

namespace App\Http\Controllers;

use App\Models\MaterialList;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MaterialListController extends Controller
{
    //

    public function MaterialList(Request $request)
    {
        \LogActivity::addToLog('get material list');

        $query   = MaterialList::getAll();
        $columns = [
            'tb1.product_id'   => 'product_id',
            'tb2.name'         => 'product_name',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb3.name'         => 'sales_org_name',
            'tb1.id'           => 'mat_id',
            'tb1.mat_desc'     => 'mat_desv',
            'tb1.plant_id'     => 'plant_id',
            'tb4.plant_name'   => 'plant_name',
            'tb1.status'       => 'status',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function MaterialListProduct(Request $request)
    {
        \LogActivity::addToLog('get distinct material list');

        $query   = MaterialList::DistinctProduct();
        $columns = [
            'tb1.sales_org_id' => 'sales_org_id',
            'tb1.plant_id'     => 'plant_id',
            'tb1.status'       => 'status',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Show the List Materials.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     JSON
     */
    public function showList(Request $request)
    {
        $params = $request->only(["sales_org_id", "plant_id", "product_id"]);

        try {
            $data = MaterialList::getAll($params)->get();
        } catch (\Exception $e) {
            return response()
                ->json(responseFail($e->getMessage()), 500);
        }

        return responseSuccess(trans("messages.read-success"), $data);
    }

    /**
     * Show the Material products.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     JSON
     */
    public function showProduct(Request $request)
    {
        $params = $request->only(["sales_org_id", "plant_id"]);

        try {
            $data = MaterialList::DistinctProduct($params)->get();
        } catch (\Exception $e) {
            return response()
                ->json(responseFail($e->getMessage()), 500);
        }

        return responseSuccess(trans("messages.read-success"), $data);
    }

}
