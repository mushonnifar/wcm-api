<?php

namespace App\Http\Controllers;

use App\Exports\ExportSPJBAsalHeader;
use App\Exports\SpjbAsalExport;
use App\Imports\SpjbImport;
use App\Models\Contract;
use App\Models\ContractGovItem;
use App\Models\ContractItem;
use App\Models\SalesGroup;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class SpjbController extends Controller
{
    //
    public function index(Request $request, $exported = false)
    {
        \LogActivity::addToLog('Show Data Master Data table Header');

        $columns = [
            'tb1.id'          => 'id',
            'tb1.uuid'        => 'uuid',
            'tb1.year'        => 'year',
            'tb1.number'      => 'no_doc',
            'tb1.customer_id' => 'customer_id',
            'tb7.full_name'   => 'customer_name',
            'tb1.id'          => 'sales_org_id',
            'tb3.name'        => 'sales_org_name',
            'tb1.status'      => 'status',
            'tb1.created_at'  => 'created_at',
            'tb1.updated_at'  => 'updated_at',
        ];

        $where['tb1.contract_type'] = 'asal';
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $query = Contract::MasterContrract($where);
        // return $where;

        $user    = $request->user();
        $filters = $user->filterRegional;

        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0 && $this->isDistributor === false) {
                $query->whereIn("tb8.sales_group_id", $filters["sales_group_id"]);
            }
        }

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
                // ($request->get('draw') == "1") ? $query->orderBy('tb1.created_at', 'DESC') : $this->orderColumn($columns, $request, $query);
            });
        if ($exported) {
            return $model->getFilteredQuery()->get();
        }

        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function hirarki($uuid)
    {
        \LogActivity::addToLog('Get Hirarki Spjb ');

        is_uuid($uuid);

        $where['tb8.uuid']          = $uuid;
        $where['tb8.contract_type'] = 'asal';

        $model = Contractitem::getItemSPJB($where)->get();
        $datas = json_decode($model);

        $provinces = array_unique(array_column($datas, 'sales_office_id'));

        $uniq_produk_global = array_unique(array_column($datas, 'product_id'));

        $totals = [];

        foreach ($uniq_produk_global as $key => $value) {
            # code...
            $totals[$value]['id']    = $value;
            $totals[$value]['name']  = $datas[$key]->product_name;
            $totals[$value]['total'] = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $totals[$value]['month'][$j] = 0;
            }
        }

        // Print Provinsi Arr
        $printArr = array('province' => array(), 'total' => array());

        foreach ($provinces as $iprov => $vprov) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_office_id'), $vprov);
            $datasprovArr = $this->makeArr($datas, $keyItemsProv);

            // Seacrh Kabupatens In provinsi
            $kabupatens = array_unique(array_column($datasprovArr, 'sales_group_id'));

            $nameProvArr = array('id' => $datas[$iprov]->sales_office_id, 'name' => $datas[$iprov]->sales_office_name, 'kabupaten' => array(), 'total' => array());

            // Total In Provinsi
            $produksProv = array_unique(array_column($datasprovArr, 'product_id'));
            $sumprovinsi = $this->gettotal($produksProv, $datasprovArr);

            // Mencari Jumlah Kabupaten
            foreach ($kabupatens as $ikab => $vkab) {
                # code...
                $keyItemsKab = array_keys(array_column($datasprovArr, 'sales_group_id'), $vkab);
                $dataskabArr = $this->makeArr($datasprovArr, $keyItemsKab);

                $kacamatans = array_unique(array_column($dataskabArr, 'sales_unit_id'));

                $printKabArr = array('id' => $datasprovArr[$ikab]->sales_group_id, 'name' => $datasprovArr[$ikab]->sales_group_name, 'kecamatan' => array(), 'total' => array());

                $produksKab   = array_unique(array_column($dataskabArr, 'product_id'));
                $sumkabupaten = $this->gettotal($produksKab, $dataskabArr);
                // Cari Kecamatan di kabupaten

                foreach ($kacamatans as $ikec => $vkec) {
                    # code...
                    $keyItemsKec = array_keys(array_column($dataskabArr, 'sales_unit_id'), $vkec);
                    $dataskecArr = $this->makeArr($dataskabArr, $keyItemsKec);

                    // Cari produk Yang Dipunyai kecamatan
                    $produks = array_unique(array_column($dataskecArr, 'product_id'));

                    // creatae
                    $printKecArr = array('id' => $dataskabArr[$ikec]->sales_unit_id, 'name' => $dataskabArr[$ikec]->sales_unit_name, 'data' => array(), 'total' => array());

                    $sumkecamatan = $this->gettotal($produks, $dataskecArr);

                    foreach ($produks as $iprod => $vprod) {
                        # code...
                        $keyItemsProd = array_keys(array_column($dataskecArr, 'product_id'), $vprod);
                        $dataspodArr  = $this->makeArr($dataskecArr, $keyItemsProd);

                        // Print Product Kabupaten Punya
                        $prodArr = array('id' => $dataskecArr[$iprod]->product_id, 'name' => $dataskecArr[$iprod]->product_name, 'month' => array(), 'status' => array(), 'total' => 0);

                        for ($i = 1; $i <= 12; $i++) {
                            # code...
                            $monKeyItems             = array_keys(array_column($dataspodArr, 'month'), $i);
                            ($monKeyItems) ? $qty    = is_null($dataspodArr[$monKeyItems[0]]->initial_qty) ? 0 : $dataspodArr[$monKeyItems[0]]->initial_qty : $qty    = 0;
                            ($monKeyItems) ? $status = is_null($dataspodArr[$monKeyItems[0]]->status) ? "-" : $dataspodArr[$monKeyItems[0]]->status : $status = "";
                            // array_push($prodArr['month'], ['qty'=>$qty,'status'=>$status]);
                            array_push($prodArr['month'], $qty);
                            array_push($prodArr['status'], $status);
                            $prodArr['total'] += $qty;
                            $sumkecamatan[$vprod]['month'][$i] += $qty;
                            $sumkabupaten[$vprod]['month'][$i] += $qty;
                            $sumprovinsi[$vprod]['month'][$i] += $qty;
                            $totals[$vprod]['month'][$i] += $qty;

                            $sumkecamatan[$vprod]['total'] += $qty;
                            $sumkabupaten[$vprod]['total'] += $qty;
                            $sumprovinsi[$vprod]['total'] += $qty;
                            $totals[$vprod]['total'] += $qty;
                        }

                        array_push($printKecArr['data'], $prodArr);

                    }

                    // Mengeluarkan perhitungan Total kecamatan
                    $sumkecamatan         = $this->pushtotal($sumkecamatan);
                    $printKecArr['total'] = $sumkecamatan;

                    array_push($printKabArr['kecamatan'], $printKecArr);

                }

                $sumkabupaten         = $this->pushtotal($sumkabupaten);
                $printKabArr['total'] = $sumkabupaten;

                array_push($nameProvArr['kabupaten'], $printKabArr);

            }

            $sumprovinsi          = $this->pushtotal($sumprovinsi);
            $nameProvArr['total'] = $sumprovinsi;

            array_push($printArr['province'], $nameProvArr);

        }
        // Mengeluarkan perhitungan Total kecamatan
        $sumprovinsi                = $this->pushtotal($totals);
        $printArr['total']          = $sumprovinsi;
        $printArr['no_spjb']        = $datas[0]->no_doc;
        $printArr['no_spjb']        = $datas[0]->no_doc;
        $printArr['sales_org_id']   = $datas[0]->sales_org_id;
        $printArr['sales_org_name'] = $datas[0]->sales_org_name;
        $printArr['customer_id']    = $datas[0]->customer_id;
        $printArr['customer_name']  = $datas[0]->customer_name;
        $printArr['year']           = $datas[0]->year;

        // array_push($printKabArr['data'],$printKecArr);

        $response = responseDatatableSuccess(trans('messages.read-success'), $printArr);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function getImport(Request $request)
    {
        \LogActivity::addToLog('Import Excel SPJB');

        $attributes = $request->all();

        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $user    = $request->user();
        $filters = $user->filterRegional;

        DB::beginTransaction();
        if ($request->hasFile('file')) {
            $file  = $request->file('file');
            $datas = Excel::toArray(new SpjbImport, $file)[0];
            // return $datas;
            // Cek template
            $cekKeys = array_keys(@$datas[0]);
            if ($cekKeys[0] != "sales_organization" or $cekKeys[1] != "no_doc" or $cekKeys[2] != "kode_distributor" or $cekKeys[3] != "kode_provinsi" or $cekKeys[4] != "kode_kabupaten" or $cekKeys[5] != "kode_kecamatan" or $cekKeys[6] != "tahun" or $cekKeys[7] != "bulan" or $cekKeys[8] != "kode_jenis_produk" or $cekKeys[9] != "amount_alokasi") {
                $response            = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template' => ['Format template Tidak Sesuai'], 'file' => ['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $no_docs = array_unique(array_column($datas, 'no_doc'));
            // return $no_docs;

            /*Loop Uniq No DOcs*/
            $arr=[];
            foreach ($no_docs as $key => $value) {
                $kode_provinsi  = $this->cekSalesOfficeCode($datas[$key]['kode_provinsi']);
                $kode_kabupaten = $this->cekGroupCode($datas[$key]['kode_kabupaten']);

                $dataValidate = [
                    'number'                => $datas[$key]['no_doc'],
                    'sales_org_id'          => $datas[$key]['sales_organization'],
                    'customer_id'           => $datas[$key]['kode_distributor'],
                    'year'                  => $datas[$key]['tahun'],
                    'rayonisasi'            => [
                        'sales_org_id'    => $datas[$key]['sales_organization'],
                        'sales_office_id' => $kode_provinsi,
                        'sales_group_id'  => $kode_kabupaten,
                        'sales_unit_id'   => $datas[$key]['kode_kecamatan'],
                    ],
                    'distributor_salesunit' => $datas[$key],
                ];
                /* Cek Header Exit Or Not*/
                $cekHeader = $dataValidate;
                unset($cekHeader['rayonisasi']);
                unset($cekHeader['distributor_salesunit']);

                $header = $this->getHeaderExist($cekHeader += ['contract_type' => 'asal']);

                if (!$header) {
                    $insertHeader = [
                        [
                            'number'        => $datas[$key]['no_doc'],
                            'contract_type' => 'operational',
                            'sales_org_id'  => $datas[$key]['sales_organization'],
                            'customer_id'   => $datas[$key]['kode_distributor'],
                            'year'          => $datas[$key]['tahun'],
                            'created_by'    => Auth::user()->id,
                            'created_at'    => now(),
                        ], [
                            'number'        => $datas[$key]['no_doc'],
                            'contract_type' => 'asal',
                            'sales_org_id'  => $datas[$key]['sales_organization'],
                            'customer_id'   => $datas[$key]['kode_distributor'],
                            'year'          => $datas[$key]['tahun'],
                            'created_by'    => Auth::user()->id,
                            'created_at'    => now(),
                        ],
                    ];

                    $this->validateImportXls($insertHeader[0], [
                        'number' => 'required|unique:wcm_contract',
                        'customer_id' => 'required|exists:wcm_customer,id',
                        'sales_org_id'          => 'required|exists:wcm_sales_org,id',
                    ], intval($key) + 2);

                    Contract::insert($insertHeader);
                }
            }
            
            /*END*/
            /*END Loop Uniq No DOcs*/
            /* Loop Datas From Excel Upload */
            $datasInsert = [];
            foreach ($datas as $key => $val) {
                // if($key==250) dd($val);
                # code...
                /*Month Tidak Sesuai*/
               
                if (intval($val['bulan']) < 1 || intval($val['bulan']) > 12) {
                    DB::rollback();
                    $response           = responseFail(trans('messages.update-fail'));
                    $response['errors'] = 'Hitungan Bulan Tidak Sesuai. baris ke-' . (intval($key) + 2);
                    $response['data']   = $val;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $val['kode_provinsi']  = $this->cekSalesOfficeCode($val['kode_provinsi']);
                $val['kode_kabupaten'] = $this->cekGroupCode($val['kode_kabupaten']);

                $getHeader = [
                    'rayonisasi'            => [
                        'sales_org_id'    => $val['sales_organization'],
                        'sales_office_id' => $val['kode_provinsi'],
                        'sales_group_id'  => $val['kode_kabupaten'],
                        'sales_unit_id'   => $val['kode_kecamatan'],
                    ],
                    'distributor_salesunit' => $val,
                    'data_header'           => [
                        'number'        => $val['no_doc'],
                        'sales_org_id'  => $val['sales_organization'],
                        'customer_id'   => $val['kode_distributor'],
                        'year'          => $val['tahun'],
                        'contract_type' => 'asal',
                    ],
                    'initial_qty'   => $val['amount_alokasi'],
                    'sales_org_id'  => $val['sales_organization'],
                    'customer_id'  => $val['kode_distributor'],
                ];

                $this->validateImportXls($getHeader, Contract::spjbUploadDetail(), intval($key) + 2);

                $findData = [
                    'tb2.sales_org_id' => $val['sales_organization'],
                    'product_id'       => $val['kode_jenis_produk'],
                    'sales_office_id'  => $val['kode_provinsi'],
                    'sales_group_id'   => $val['kode_kabupaten'],
                    'sales_unit_id'    => $val['kode_kecamatan'],
                    'month'            => intval($val['bulan']),
                    'tb1.year'         => $val['tahun'],
                    'tb1.status'       => 'y',
                ];

                // Data On Perbup
                $query = ContractGovItem::cekDataForSpjb($findData)->first();

                // Data Availabe On perbup
                if (!$query) {
                    DB::rollback();
                    $response           = responseFail(trans('messages.create-fail'));
                    $response['errors'] = 'Data di Perbup tidak tersedia. baris ke-' . (intval($key) + 2);
                    $response['data']   = $val;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                /*
                 * GEt HEader SPJB asal dan Operational
                 */
                $getHeader['data_header']['contract_type'] = 'asal';
                $headerAsal                                = $this->getHeaderExist($getHeader['data_header']);

                $getHeader['data_header']['contract_type'] = 'operational';
                $headerOperational                         = $this->getHeaderExist($getHeader['data_header']);
                /*End*/

                if (count($filters) > 0) {
                    if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                        if (!in_array($val['sales_organization'], $filters["sales_org_id"])) {
                            DB::rollback();
                            $response           = responseFail(trans('messages.create-fail'));
                            $response['errors'] = 'Produsen Tidak Sesuai User Management Pada baris : ' . (intval($key) + 2);
                            $response['data']   = $val;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                }

                $detItem = [
                    'contract_id'          => $headerAsal->id,
                    'product_id'           => $val['kode_jenis_produk'],
                    'sales_office_id'      => $val['kode_provinsi'],
                    'sales_group_id'       => $val['kode_kabupaten'],
                    'sales_unit_id'        => $val['kode_kecamatan'],
                    'month'                => intval($val['bulan']),
                    'year'                 => $val['tahun'],
                ];
                $detItemOps = [
                    'contract_id'          => $headerOperational->id,
                    'product_id'           => $val['kode_jenis_produk'],
                    'sales_office_id'      => $val['kode_provinsi'],
                    'sales_group_id'       => $val['kode_kabupaten'],
                    'sales_unit_id'        => $val['kode_kecamatan'],
                    'month'                => intval($val['bulan']),
                    'year'                 => $val['tahun'],
                ];
                $itemAsal = $this->getUploadItemExist($detItem);
                $itemOpr  = $this->getUploadItemExist($detItemOps);

                //  Cek Kondisi untuk Update data atau Inser Data SPJB
                if ($itemAsal) {
                    if ($itemAsal['status'] == "n") {
                        DB::rollback();
                        $response           = responseFail(trans('messages.update-fail'));
                        $response['errors'] = 'Data SPJB InActive pada baris ke-' . ($key + 2);
                        $response['data']   = $val;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }

                    $spjbUpdate['initial_qty'] = $val['amount_alokasi'];
                    $spjbUpdate['updated_by']  = Auth::user()->id;
                    $itemAsal->update($spjbUpdate);

                } else {
                    $detItem['initial_qty'] = $val['amount_alokasi'];
                    $detItem['created_by']  = Auth::user()->id;

                    // membatasi 1 area aktive
                    $checkspjbitem    = $this->checkOneActiveArea($detItem,'asal');
                    if($checkspjbitem)
                    {
                        DB::rollback();
                        $response           = responseFail("Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}");
                        $response['errors'] = ['Gagal Update' => "Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}"];
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                    $detItem['created_at'] = now();
                    array_push($datasInsert, $detItem);
                }
                // cek spjb opr exit or not, not exixst then create
                if (!$itemOpr) {
                    $detItemOps['initial_qty'] = $val['amount_alokasi'];
                    $detItemOps['created_by']  = Auth::user()->id;
                    $detItemOps['created_at']  = now();
                    array_push($datasInsert, $detItemOps);
                }

                if (count($datasInsert) != 0) {
                    $DetailInsertItem = ContractItem::insert($datasInsert);
                    $datasInsert      = [];
                }
            }

            // Try To Commit
            try {
                DB::commit();
                $sukses   = [array('msg' => 'Sukses Input Data')];
                $response = responseSuccess(trans('messages.create-success'), $sukses);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function detailitem($uuid)
    {
        # code...
        \LogActivity::addToLog('GET Detail Item By UUID');
        is_uuid($uuid);

        $where['tb1.uuid'] = $uuid;
        $model             = Contractitem::getItemSPJB($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function masteritems(Request $request, $uuid)
    {
        # code...
        \LogActivity::addToLog('Material Item SPJB Controller');
        is_uuid($uuid);
        $where['tb8.uuid'] = $uuid;

        $columns = [
            'tb1.contract_id'     => 'contract_id',
            'tb8.number'          => 'no_doc',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb5.name'            => 'sales_office_name',
            'tb1.sales_group_id'  => 'sales_group_id',
            'tb4.name'            => 'sales_group_name',
            'tb1.sales_unit_id'   => 'sales_unit_id',
            'tb6.name'            => 'sales_unit_name',
            'tb1.sales_org_id'    => 'sales_org_id',
            'tb3.name'            => 'sales_org_name',
            'tb8.customer_id'     => 'customer_id',
            'tb7.full_name'       => 'customer_name',
            'tb1.product_id'      => 'product_id',
            'tb2.name'            => 'product_name',
            'tb1.month'           => 'month',
            'tb1.initial_qty'     => 'initial_qty',
            'tb1.year'            => 'year',
            'tb1.status'          => 'status',
            'tb1.created_at'      => 'created_at',
        ];

        $query = Contractitem::getItemSPJB($where);
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        # code...
        \LogActivity::addToLog('Entry Baru Contract Item');
        // Cek Pasti Atributs;
        $ceks = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year']);

        // Find Contract Header Operasional By Header asal Data
        $header['id']    = $ceks['contract_id'];
        $headerDetail    = $this->getHeaderDetail($header);
        $headerDuplicate = $this->getDuplicateHeader($header);

        $checkspjbitem = $this->checkOneActiveArea($ceks, 'asal');

        if ($checkspjbitem) {
            DB::rollback();
            $response           = responseFail(['Gagal Update' => "Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}"]);
            $response['errors'] = ['Gagal Update' => "Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}"];
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        // Cek Duplicate
        $cekOnItem = $this->cekDuplikasiItem(Contractitem::class, $ceks);

        $attributes               = $request->all();
        $attributes['created_by'] = Auth::user()->id;

        // Duplicate Data Atribut
        $duplicate_attributes                = $attributes;
        $duplicate_attributes['contract_id'] = $headerDuplicate;
        $duplicate_attributes['created_by']  = Auth::user()->id;

        $this->validate($attributes, Contractitem::spjbContractCreateItems());

        DB::beginTransaction();

        $findData = [
            'tb2.sales_org_id' => $headerDetail->sales_org_id,
            'product_id'       => $attributes['product_id'],
            'sales_office_id'  => $attributes['sales_office_id'],
            'sales_group_id'   => $attributes['sales_group_id'],
            'sales_unit_id'    => $attributes['sales_unit_id'],
            'month'            => intval($attributes['month']),
            'tb1.year'         => $attributes['year'],
        ];

        // Data On Perbup
        $query = ContractGovItem::cekDataForSpjb($findData)->first();

        // Data Availabe On perbup
        if (!$query) {
            DB::rollback();
            $response           = responseFail(['data perbup' => ['Data tidak valid di Perbub']]);
            $response['errors'] = 'Data tidak valid di Perbub';
            $response['data']   = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        // Cek Duplicate Data
        if ($cekOnItem) {
            DB::rollback();
            $error = [
                "sales_group_id" => [trans('messages.duplicate')],
                "sales_unit_id"  => [trans('messages.duplicate')],
                "month"          => [trans('messages.duplicate')],
                "year"           => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        try {
            $attributes['month']           = intval($attributes['month']);
            $duplicate_attributes['month'] = intval($attributes['month']);
            Contractitem::insert($attributes);
            Contractitem::insert($duplicate_attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'));
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function update(Request $request, $uuid)
    {
        # code...
        \LogActivity::addToLog('Update SPJB Controller');
        is_uuid($uuid);

        $model = $this->findDataUuid(Contractitem::class, $uuid);

        if ($model->status == "n") {
            DB::rollback();
            $errors             = ['status' => "Status Item SPJB Asal Tidak Active"];
            $response           = responseFail($errors);
            $response['errors'] = 'Status Item SPJB Asal Tidak Active';
            $response['data']   = $model;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $modelAsal = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
        $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

        // Data Untuk mencari Item yang Kembar
        $findItem["contract_id"]     = $modelOpr->id;
        $findItem["product_id"]      = $model["product_id"];
        $findItem["sales_office_id"] = $model["sales_office_id"];
        $findItem["sales_group_id"]  = $model["sales_group_id"];
        $findItem["sales_unit_id"]   = $model["sales_unit_id"];
        $findItem["month"]           = $model["month"];
        $findItem["year"]            = $model["year"];

        // $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);

        /*SUM Calc SPJB OPR TAHUN*/
        $qtyspjbOprYear          = $this->spjbOprYear($findItem);
        $findItem["contract_id"] = $modelAsal->id;
        $qtyspjbAsalYear         = $this->spjbOprYear($findItem) - $model->initial_qty;

        $qtyOld = $model["initial_qty"]; // Get Qty Didatabase

        // Cek Pasti Atributs;
        $ceks = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year']);
        // Cek Duplicate
        $cekOnItem = $this->cekDuplikasiItemUpdate(Contractitem::class, $ceks, $uuid);

        // Request All Atributs
        $attributes               = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year', 'initial_qty']);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        // Validate Atrributs
        $this->validate($attributes, Contractitem::spjbContractCreateItems());

        $findData = [
            'tb2.sales_org_id'    => $modelAsal->sales_org_id,
            'tb1.product_id'      => $attributes['product_id'],
            'tb1.sales_office_id' => $attributes['sales_office_id'],
            'tb1.sales_group_id'  => $attributes['sales_group_id'],
            'tb1.sales_unit_id'   => $attributes['sales_unit_id'],
            'tb1.month'           => intval($attributes['month']),
            'tb1.year'            => $attributes['year'],
            'tb2.contract_type'   => 'perbup',
            'tb1.status'          => 'y',
        ];

        DB::beginTransaction();
        // Data On Perbup
        $query = ContractGovItem::cekDataForSpjb($findData)->first();

        // Data Availabe On perbup
        if ($query) {

            // Prepare For Update Qty On Perbub
            $model_perbub = $this->findDataUuid(ContractGovitem::class, $query->uuid);

            if ($qtyOld < $attributes['initial_qty']) {
                // Case Old Qty Kurang Dari
                $selisih = $model_perbub->initial_qty - ($attributes['initial_qty'] - $qtyOld);
            } elseif ($qtyOld == $attributes['initial_qty']) {
                // Case Old Qty Sama dengan Data baru
                $selisih = $model_perbub->initial_qty;
            } else {
                // Case Qty Lebih Dari
                $selisih = $model_perbub->initial_qty + ($qtyOld - $attributes['initial_qty']);
            }

            // Condition On Alokasion QTY,
            if ($selisih >= 0) {
                $UpdateSpjbData['initial_qty'] = $selisih;
                $model_perbub->update($UpdateSpjbData);

            } else {
                // Rollback Where Qty Melebihi Kuota dari Perbup
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = 'QTY Over Limit ( Perbub Qty Available  Only ' . $query->initial_qty . ')';
                $response['data']   = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            // Not Macth In Perbup then  Rollback With  Message item
        } else {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = 'Data tidak valid di perbup ';
            $response['data']   = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        if (($qtyspjbAsalYear + $attributes['initial_qty']) < $qtyspjbOprYear) {
            DB::rollback();
            $msg = 'Total SPJB Asal ' . ($qtyspjbAsalYear + $attributes['initial_qty']) . ' lebih kecil dari Total SPJB Operational Pada tahun ' . $model['year'] . ' Produk : ' . $model['product_id'] . ' = ' . $qtyspjbOprYear . ' untuk Kecamatan ' . $request['sales_unit_name'];

            $response           = responseFail(['initial_qty' => ["qty kurang dari SPJB Operational : " . $qtyspjbOprYear]]);
            $response['errors'] = $msg;
            $response['data']   = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        if ($cekOnItem) {
            DB::rollback();
            $error = [
                "sales_group_id" => [trans('messages.duplicate')],
                "sales_unit_id"  => [trans('messages.duplicate')],
                "month"          => [trans('messages.duplicate')],
                "year"           => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        try {
            unset($attributes['contract_id']);
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function updates(Request $request)
    {
        # code...
        \LogActivity::addToLog('Update Bulk Status SPJB');
        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);

                $iv['updated_by'] = Auth::user()->id;

                $model     = $this->findDataUuid(Contractitem::class, $iv['id']);
                $modelAsal = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
                $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

                // Data Untuk mencari Item yang Kembar
                $findItem["contract_id"]     = $modelOpr->id;
                $findItem["product_id"]      = $model["product_id"];
                $findItem["sales_office_id"] = $model["sales_office_id"];
                $findItem["sales_group_id"]  = $model["sales_group_id"];
                $findItem["sales_unit_id"]   = $model["sales_unit_id"];
                $findItem["month"]           = $model["month"];
                $findItem["year"]            = $model["year"];

                $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);
                unset($iv['id']);
                $model->update($iv);
                $modelDuplicate->update($iv);
            }

            /* Update Header ke z */
            $num         = $this->getuniqStatusForHeader(Contractitem::class, $model->contract_id);
            $modelheader = Contract::where('id', $model->contract_id)->first();
            if ($num == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            }

            $numOpr         = $this->getuniqStatusForHeader(Contractitem::class, $modelDuplicate->contract_id);
            $modelheaderOpr = Contract::where('id', $modelDuplicate->contract_id)->first();
            if ($numOpr == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheaderOpr->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheaderOpr->update($updateHeader);
            }
            /*End*/
            /*Cek Partisial Active Status*/
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        return Excel::download((new ExportSPJBAsalHeader($data)), "Download SPJB Asal.xls");
    }

    public function statusBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk SPJB Header');
        $attributes = $request->all();

        $validator = Validator::make($attributes, [
            "*.uuid"   => "required",
            "*.status" => "in:d,y,n,s",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $collect = collect($attributes)->pluck('uuid');
        // check status active inactive
        $data = Contract::whereIn('uuid', $collect)->WhereIn('status', ['y', 'n'])->pluck('status')->unique()->count();

        if ($data > 1) {
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = ['status' => [trans('messages.status-only-one')]];
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['uuid']);
                $iv['updated_by'] = Auth::user()->id;
                $iv['status']     = $iv['status'];
                $model            = $this->findDataUuid(Contract::class, $iv['uuid']);
                $spjbitem         = Contractitem::where('contract_id', $model->id)->get();
                $checkspjbitem    = $this->checkOneItemActive($spjbitem, $iv['status'], 'asal');
                if ($checkspjbitem) {
                    DB::rollback();
                    $response           = responseFail("Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}");
                    $response['errors'] = ['Gagal Update' => "Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}"];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
                $item = Contractitem::where('contract_id', $model->id)->update($iv);
                $model->update($iv);

                /* Update SPJB OPR */
                $spjbOpr = Contract::where('number', $model->number)
                ->where('customer_id', $model->customer_id)
                ->where('sales_org_id', $model->sales_org_id)
                ->where('contract_type', 'operational')
                ->where('year', $model->year)
                ->first();
                $itemOpr = Contractitem::where('contract_id', $spjbOpr->id)->update($iv);
                $spjbOpr->update($iv);

            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function getGroupByUUIDSPJB(Request $request)
    {
        \LogActivity::addToLog('GET Detail Item By UUID SPJB ASAL');
        $attributes = $request->all();
        $this->validate($attributes, Contract::getgroupbyspjb());
        is_uuid($attributes['uuid']);

        $where['tb8.uuid'] = $attributes['uuid'];
        $datas             = ContractItem::getItemSPJB($where)->get();
        $datas             = json_decode($datas);

        $keyItemsProv = array_keys(array_column($datas, 'sales_office_id'), $attributes['sales_office_id']);
        $datasprovArr = $this->makeArr($datas, $keyItemsProv);

        $kabupatens = array_unique(array_column($datasprovArr, 'sales_group_id'));

        $print = [];
        foreach ($kabupatens as $key => $val) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_group_id'), $val);
            array_push($print, array('sales_group_id' => $datas[$keyItemsProv[0]]->sales_group_id, 'sales_group_name' => $datas[$keyItemsProv[0]]->sales_group_name));

        }

        $response = responseSuccess(trans('messages.read-success'), $print);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

//
    // Atribut For Ceker For data in header
    //

    public function cekDuplikasiHeaderOperational($model, $attrib)
    {
        $attrib['contract_type'] = 'operational';

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    public function cekDuplikasiHeader($model, $attrib)
    {
        $attrib['contract_type'] = 'asal';

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }
    public function cekDuplikasiItem($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    public function getDataItem($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    public function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    public function gettotal($produks, $datasArr)
    {
        # code...
        foreach ($produks as $key => $value) {
            # code...
            $sumkecamatan[$value]['id']    = $value;
            $sumkecamatan[$value]['name']  = $datasArr[$key]->product_name;
            $sumkecamatan[$value]['total'] = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $sumkecamatan[$value]['month'][$j] = 0;
            }
        }

        return $sumkecamatan;
    }

    public function pushtotal($totals)
    {
        $newTotals = [];
        foreach ($totals as $key => $value) {
            # code...
            array_push($newTotals, $value);
        }
        return $newTotals;
    }

    public function cekDuplikasiItemUpdate($model, $attrib, $uuid)
    {

        $query = $model::where($attrib)
            ->where('uuid', '!=', $uuid);
        // ->where('contract_id', '!=', $duplicateUuid);

        $check = $query->first();

        return $check;
    }

    public function getDuplicateHeader($where)
    {
        # code...
        $query = Contract::where($where);
        $check = $query->first();

        // $find=
        $find["number"]        = $check["number"];
        $find["customer_id"]   = $check["customer_id"];
        $find["sales_org_id"]  = $check["sales_org_id"];
        $find["contract_type"] = 'operational';
        $find["year"]          = $check["year"];

        $query = Contract::where($find);
        $check = $query->first();

        return $check['id'];
    }

    public function getHeaderDetail($where)
    {
        # code...
        $query = Contract::where($where);
        $check = $query->first();

        return $check;
    }

    public function getDuplicateItem($notId, $where)
    {
        # code...
        $query = Contractitem::where($where)
            ->where('id', '!=', $notId);
        $check = $query->first();

        return $check['uuid'];
    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
                'errors'     => 'Unvalidate Data in Line ' . $line,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function cekGroupCode($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length < 4) {
            $kabupaten = SalesGroup::where('district_code', $sales_group_id)->first();
            return @$kabupaten->id;
        }
        return $sales_group_id;
    }

    public function cekSalesOfficeCode($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }

/**
 * Export SPJB To Excell
 *
 * @param      \Illuminate\Http\Request  $request  The request
 *
 * @return     Binary or \Illumintae\Http\Response
 */
    public function export(Request $request)
    {
        \LogActivity::addToLog('EXPORT SPJB ASAL');
        $this->validate($request->all(), ['uuid' => 'required']);

        try {

            $uuid     = $request->get('uuid');
            $contract = Contract::where("uuid", $uuid)->firstOrFail();

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return Excel::download(
            new SpjbAsalExport($contract),
            str_slug("{$contract->number}") . ".xls"
        );
    }

    public function getHeaderExist($value)
    {
        $query = Contract::where($value);
        $check = $query->first();
        return $check;
    }

    public function getUploadItemExist($value)
    {
        $query = ContractItem::where($value);
        $check = $query->first();
        return $check;
    }

    public function getuniqStatusForHeader($model, $idheader)
    {
        $allStatus = $model::select('status')
            ->distinct()
            ->where('contract_id', $idheader)
            ->get()
            ->toArray();
        return count($allStatus);
    }

    private function spjbOprYear($data)
    {
        $year = $data['month'];unset($data['month']);

        $sum = DB::table('wcm_contract_item')
            ->whereIn('month', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            ->where($data)
            ->select(DB::raw('SUM(initial_qty) as qty'))
            ->get();
        return (is_null($sum[0]->qty)) ? 0 : $sum[0]->qty;

    }

    private function checkOneItemActive($data, $status, $type)
    {
        foreach ($data as $key => $value) {
            # code...
            if ($status == 'y' && $value['status'] == 'n') {
                $checkitem = $this->checkOneActiveArea($value, $type);
                if ($checkitem) {
                    return $checkitem;
                }
            }
        }

        return false;
    }

    private function checkOneActiveArea($where, $type)
    {

        $dataitem = Contractitem::getItemSPJB(['tb1.month' => intval($where['month']), 'tb1.product_id' => $where['product_id'],
            'tb1.sales_group_id'                               => $where['sales_group_id'],
            'tb1.sales_office_id'                              => $where['sales_office_id'],
            'tb1.sales_unit_id'                                => $where['sales_unit_id'], 'tb1.year'       => $where['year'], 'tb1.status' => 'y', 'tb8.contract_type' => $type,
        ])->first();
        return $dataitem;
    }

}
