<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Yajra\DataTables\DataTables;
use App\Rules\OnlyOneRules;

class RoleController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all user');
        $query=Role::leftJoin('wcm_sales_org as tb2', 'roles.sales_org_id', '=', 'tb2.id')
                        ->select('roles.*','tb2.name as sales_org_name');

        

        $user = $request->user();
        $filters = $user->filterRegional;

        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                if(count($filters["sales_org_id"])!=5){
                    $query->whereIn("sales_org_id", $filters["sales_org_id"]);
                }
            }
        }
    
        if (!$request->user()->isAdminBaru  && $request->has('except') && $request->has('filter') ) {
            $query->where("roles.id", "!=", 2);
            $query->whereNotIn("roles.id",[1,2,3,4]);
        }

        if(!$request->user()->isAdminBaru){
            if($request->has('filter')) $query->orWhereIn("roles.id",[3,4]);

            if($request->has('except'))
            {
                $query->whereNotIn('roles.name',explode(';',  strtoupper($request->get('except'))));
            }
        }

        $columns = ['roles.name' => 'name' ,'tb2.name' =>'sales_org_name'];

        $user = Datatables::of($query)
                ->filter(function ($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);                    
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                })->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $user->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request) {

        \LogActivity::addToLog('create role');

        $attributes = $request->only(['name','sales_org_id']);
        $attributes['guard_name'] = 'api';

        $user = $request->user();
        $filters = $user->filterRegional;

        $rules = [
            'name' => 'required|unique:roles',
            // 'sales_org_id' => 'exists:wcm_sales_org,id'
        ];
        

        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                if(count($filters["sales_org_id"])!=5){
                $rules['sales_org_id']='exists:wcm_sales_org,id|in:'.implode($filters["sales_org_id"], ",").'';
                }
            }
        }
        
        // if ($this->isAdminAnper) {
        //     $user = $request->user();
        //     $filters = $user->filterRegional;

        //     if (count($filters) > 0) {
        //         if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
        //             $attributes['sales_org_id']=implode($filters["sales_org_id"],'');
        //         }
        //     }
        // }

        $this->validate($attributes, $rules);

        DB::beginTransaction();
        try {
            $model = Role::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($id) {
        \LogActivity::addToLog('get role by id');

        $rules = [
            'id' => 'required|exists:roles,id',
            'sales_org_id' => 'exists:wcm_sales_org,id'
        ];

        $attributes['id']=$id;

        $this->validate($attributes, $rules);

        $data = Role::leftJoin('wcm_sales_org as tb2', 'roles.sales_org_id', '=', 'tb2.id')
                        ->select('roles.*','tb2.name as sales_org_name')
                        ->where('roles.id',$id)
                        ->first();
        // $data = $this->findData(Role::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id) {

        \LogActivity::addToLog('update role');

        $rules = [
            'name' => ["required", new OnlyOneRules()],
            'sales_org_id' => 'exists:wcm_sales_org,id'
        ];

        $attributes = $request->only(['name','sales_org_id']);

        $attributes['name'] = array("find"=>'id',"uuid"=>$id, 'name'=>$attributes['name'], "tables" => "roles"); 

        $user = $request->user();
        $filters = $user->filterRegional;

        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $rules['sales_org_id']='exists:wcm_sales_org,id|in:'.implode($filters["sales_org_id"], ",").'';
            }
        }

        $this->validate($attributes, $rules);

        $attributes['name']=$attributes['name']['name'];

        $data = $this->findData(Role::class, $id);

        DB::beginTransaction();
        try {
            $data->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $data);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id) {
        \LogActivity::addToLog('delete role');

        $data = $this->findData(Role::class, $id);
        // return $data;

        DB::beginTransaction();

        $check = DB::table('model_has_roles')
                // ->leftJoin('model_has_roles' ,'model_has_roles.id', '=' ,'roles.id')
                ->leftJoin('users', 'users.id' ,'=', 'model_has_roles.model_id')
                ->where('users.status','y')
                ->where('model_has_roles.role_id',$id)
                ->exists();

        if($check){
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail').' User pada role ini masih ada yang berstatus Aktif');
            // $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        
        try {
            $data->delete();
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $data);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function setPermissions(Request $request, $model) {
        $attributes = $request->all();
        $rules = [
            'role_id' => 'required|numeric',
            'permissions' => 'required'
        ];
        $this->validate($attributes, $rules);
        switch ($model) {
            case 'givePermissionTo':
                $msg = 'give permission to role';
                break;
            case 'syncPermissions':
                $msg = 'sync permission to role';
                break;
            default:
                $msg = 'revoke permission to role';
        }

        \LogActivity::addToLog($msg);

        $role = $this->findData(Role::class, $attributes['role_id']);
        $data = $role->$model(json_decode($attributes['permissions'], true));
        $response = responseSuccess(trans('messages.update-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function setRoles(Request $request, $model) {
        $attributes = $request->all();
        $rules = [
            'user_id' => 'required',
            'roles' => 'required'
        ];

        $this->validate($attributes, $rules);

        switch ($model) {
            case 'assignRole':
                $msg = 'assign role';
                break;
            case 'syncRoles':
                $msg = 'sync role';
                break;
            default:
                $msg = 'remove role';
        }

        \LogActivity::addToLog($msg);

        $user = $this->findData(User::class, $attributes['user_id']);
        $data = $user->$model(json_decode($attributes['roles'], true));
        $response = responseSuccess(trans('messages.create-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
