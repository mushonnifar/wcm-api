<?php

namespace App\Http\Controllers;

use App\Exports\RetailExport;
use App\Imports\RetailImport;
use App\Libraries\ExcelUtils;
use App\Models\CustomerRetailerAssg;
use App\Models\Retail;
use App\Rules\RetailAdministratifSalesUnitRule;
use App\Traits\ValidationTemplateImport;
use Excel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Constraint\Attribute;
use Validator;
use Yajra\DataTables\DataTables;

// use Illuminate\Foundation\Validation\ValidatesRequests;

class RetailController extends Controller
{
    use ValidationTemplateImport;
    public function index(Request $request, $cust_uuid = null, $exported = false)
    {
        \LogActivity::addToLog('get all Retail');

        // dd($request = new Request(['kabupaten'=> 1602,
        // 'type'=> 'relasi',
        // 'customer_id' => '1000001414',
        // 'sales_org_id'=> 'F000']));

        $except = $request->get('except');
        isset($cust_uuid) ? is_uuid($cust_uuid) : '';

        $where = array();
        $where['sub_district_default'] = 0;

        if ($cust_uuid) {
            $where['f.uuid'] = $cust_uuid;
            $where['a.status'] = 'y';
        }

        /* cek For Master Retail with Param type=retail */
        if ($request['type'] == 'retail') {
            $where['type'] = 'retail';
        }
        if ($except) {
            $arr['sales_org_uuid'] = $request->get('sales_org_uuid');
            $arr['customer_uuid'] = $request->get('customer_uuid');
            $query = Retail::getData($where, $except, $arr);
        } else {
            $query = Retail::getData($where);
        }

        if (is_null($cust_uuid) && !isset($request["type"])) {
            $query = Retail::masterPengecer($where);
            // if ($this->isDistributor || $request->get('form') == "rdkkform") {
            //     $query->leftJoin('wcm_customer_retailer_assg as e', function ($q) {
            //         $q->on('a.id', '=', 'e.retail_id')
            //           ->where('e.customer_id', '=' , $this->customerId );
            //     });
            // }
        }

        if ($request->get('form') == "rdkkform") {

            $query->leftJoin('wcm_customer_retailer_assg as e', function ($q) {
                $q->on('a.id', '=', 'e.retail_id');
                if ($this->isDistributor) {
                    $q->where('e.customer_id', '=', $this->customerId);
                }
            });

            $query->where('e.status', 'y');
            $query->select('a.id', 'a.code', 'a.name')->distinct();
        }

        $user = $request->user();
        $filters = @$user->filterRegional;

        if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
            $query->whereIn("c.id", $filters["sales_group_id"]);
        }

        $columns = [
            'e.sales_org_id' => 'sales_org_id',
            'g.name' => 'sales_org_name',
            'e.customer_id' => 'customer_id',
            'f.full_name' => 'customer_name',
            'a.code' => 'code',
            'a.uuid' => 'uuid',
            'a.name' => 'name',
            'a.email' => 'email',
            'a.owner' => 'owner',
            'a.address' => 'address',
            'a.village' => 'village',
            'a.tlp_no' => 'tlp_no',
            'a.hp_no' => 'hp_no',
            'a.fax_no' => 'fax_no',
            'a.latitude' => 'latitude',
            'a.longitude' => 'longitude',
            'a.npwp_no' => 'npwp_no',
            'a.npwp_register' => 'npwp_register',
            'a.siup_no' => 'siup_no',
            'a.valid_date_siup' => 'valid_date_siup',
            'a.situ_no' => 'situ_no',
            'a.valid_date_situ' => 'valid_date_situ',
            'a.tdp_no' => 'tdp_no',
            'a.valid_date_tdp' => 'valid_date_tdp',
            'a.recomd_letter' => 'recomd_letter',
            'a.recomd_letter_date' => 'recomd_letter_date',
            'a.old_number' => 'old_number',
            'b.id' => 'sales_unit_id',
            'b.name' => 'sales_unit_name',
            'c.id' => 'sales_group_id',
            'c.name' => 'sales_group_name',
            'd.id' => 'sales_office_id',
            'd.name' => 'sales_office_name',
            'a.status' => 'status',
            'a.created_by' => 'created_by',
            'a.updated_by' => 'updated_by',
            'a.created_at' => 'created_at',
            'a.updated_at' => 'updated_at',
            'a.retail_administratif' => 'retail_administratif',
        ];

        $data = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns, $except) {
                if ($except) {
                    $this->filterColumnExcept($columns, $request, $query);
                } else {
                    $this->filterColumn($columns, $request, $query);
                }
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });

        if ($exported) {
            return $data->getFilteredQuery()->get();
        }

        $data = $data->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $data->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value) && $request->get($value, false)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {
            if ($value === 'created_at' || $value === 'updated_at' || $value === 'npwp_register' || $value === 'valid_date_siup' || $value === 'valid_date_situ' || $value === 'valid_date_tdp' || $value === 'recomd_letter_date') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } else if ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } elseif ($value === 'retail_administratif') {
                $val = (int) $request->get($value);
                if ($val < 1) {
                    $query->where(function ($q){
                        $q->where("retail_administratif", "!=", 1)
                            ->orWhereRaw("retail_administratif is null");
                    });
                } else {
                    $query->where("retail_administratif", 1);
                }
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function filterColumnExcept($columns, $request, $query)
    {
        // $query->where(function ($query) use ($request) {
        //     $query->where('g.id', '<>', $request->get('sales_org_uuid'))
        //             ->orWhereNull('e.sales_org_id');

        // });
        // $query->where(function ($query) use ($request) {
        //     $query->where('f.id', '<>', $request->get('customer_uuid'))
        //           ->orWhereNull('e.customer_id');
        // });

        $salesUnitID = $request->get('sales_unit_id');
        $customerID = $request->get('customer_uuid');
        $salesOrgID = $request->get("sales_org_uuid");

        if ($salesUnitID) {
            $query->where('b.id', '=', $request->get('sales_unit_id'));
        }

        /* Mengambil data sesuai spjb apabila field kecamtan tidak diisi */
        if ($salesUnitID == "") {
            $spjb = DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id' => $customerID, 'tb2.sales_org_id' => $salesOrgID, 'tb2.contract_type' => 'asal', 'tb1.status' => 'y', 'tb1.year' => date('Y')])
                ->select('tb1.sales_unit_id')
                ->distinct()->get()->pluck('sales_unit_id');
            $query->whereIn('b.id', $spjb);
        }
        /* End Mengambil data sesuai spjb apabila field kecamtan tidak diisi */

        $arr = compact('salesUnitID', 'customerID', 'salesOrgID');
        // $query->whereNull('e.id');
        $query->whereNotIn('a.id', function ($s) use ($arr) {
            $s->select('r.id')->from('wcm_retail as r');
            $s->leftJoin('wcm_customer_retailer_assg as cra', function ($q) use ($arr) {
                $q->on('r.id', '=', 'cra.retail_id');
                // ->where('cra.status', '=', 'y');
            });
            if (@$arr["salesUnitID"]) {
                $s->where('r.sales_unit_id', $arr["salesUnitID"]);
            }
            $s->Where('cra.customer_id', '=', $arr["customerID"]);
            $s->Where('cra.sales_org_id', '=', $arr["salesOrgID"]);
        });

        $query->where('a.status', 'y');

        foreach ($columns as $key => $value) {
            if ($request->has($value) && $request->get($value, false)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create retail');

        $attributes = $request->all();
        $attributes['name'] = [
            'name' => $request['name'],
            'sales_unit_id' => $request['sales_unit_id'],
        ];
        $attributes['owner'] = [
            'sales_unit_id' => $request['sales_unit_id'],
            'owner' => $request['owner'],
        ];
        $this->validate($attributes, Retail::ruleCreate());

        $attributes['name'] = $request['name'];
        $attributes['owner'] = $request['owner'];

        // Cek For Profil Distributor
        // if (array_key_exists('customer_uuid', $attributes)) {
        //     $ceksalesgroup = $this->cekRetailSalesGroup($attributes['customer_uuid'], $attributes['sales_unit_id']);
        //     if ($ceksalesgroup) {
        //         $error = [
        //             "sales_office_id" => [trans('messages.retailer-sales-office')],
        //         ];
        //         $response = responseFail($error);
        //         return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        //     }
        // }

        $attributes['code'] = $this->generateCode(Retail::notAdministratif(), 'code', 'RT', 10);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;
        $attributes['sub_district_default'] = 0;
        $attributes["npwp_register"] = parseDate($attributes["npwp_register"]);
        $attributes["valid_date_siup"] = parseDate($attributes["valid_date_siup"]);
        $attributes["valid_date_tdp"] = parseDate($attributes["valid_date_tdp"]);
        $attributes["valid_date_situ"] = parseDate($attributes["valid_date_situ"]);
        $attributes["retail_administratif"] = 0;

        DB::beginTransaction();
        try {
            $retail = Retail::create($attributes);

            // if ($this->isDistributor) {
            //     //Distributor
            //     $custReAssg = new CustomerRetailerAssg([
            //         'customer_id'  => $this->customerId,
            //         'sales_org_id' => $this->salesOrgId,
            //         'created_by'   => Auth::user()->id,
            //         'updated_by'   => Auth::user()->id,
            //     ]);
            //     $retail->customerRetailerAssg()->save($custReAssg);
            // }

            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $retail);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function store_retail_administratif(Request $request)
    {
        \LogActivity::addToLog('create retail administratif');

        $attributes = $request->all();
        $rules = Retail::ruleAdministratif();
        $rules["sales_unit_id"][] = new RetailAdministratifSalesUnitRule(@$attributes["id"]);
        $this->validate($attributes, $rules);

        $attributes['name'] = $request['name'];
        $attributes['owner'] = $request['owner'];
        $attributes['parent_id'] = $request['id'];
        $attributes['code'] = $this->generateCode(Retail::administratif(), 'code', 'RTA', 9);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['sub_district_default'] = 0;
        $attributes['retail_administratif'] = 1;
        $attributes["npwp_register"] = parseDate($attributes["npwp_register"]);
        $attributes["valid_date_siup"] = parseDate($attributes["valid_date_siup"]);
        $attributes["valid_date_tdp"] = parseDate($attributes["valid_date_tdp"]);
        $attributes["valid_date_situ"] = parseDate($attributes["valid_date_situ"]);

        $attributes = Arr::except($attributes, ["created_at", "updated_at"]);
        DB::beginTransaction();
        try {
            $parent = Retail::find($attributes["parent_id"]);

            if ($parent->retail_administratif == 1) {
                throw new Exception("Can't Create Retail Administratif From Retail Admisitratif");
            }

            if ($parent->sales_unit_id == $attributes['sales_unit_id']) {
                throw new Exception(trans('messages.retail-admin-sales-unit-unique'));
            }

            $retail = Retail::create($attributes);

            if ($this->isDistributor) {
                //Distributor
                $custReAssg = new CustomerRetailerAssg([
                    'customer_id' => $this->customerId,
                    'sales_org_id' => $this->salesOrgId,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);
            }

            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $retail);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function update_retail_administratif($uuid, Request $request)
    {
        \LogActivity::addToLog('update retail administratif');
        is_uuid($uuid);

        $attributes = $request->except(['retail_administratif', 'code']);

        $this->validate($attributes, Retail::ruleAdministratifupdate());

        $attributes['name'] = $request['name'];
        $attributes['owner'] = $request['owner'];
        $attributes['updated_by'] = Auth::user()->id;
        $attributes["npwp_register"] = parseDate($attributes["npwp_register"]);
        $attributes["valid_date_siup"] = parseDate($attributes["valid_date_siup"]);
        $attributes["valid_date_tdp"] = parseDate($attributes["valid_date_tdp"]);
        $attributes["valid_date_situ"] = parseDate($attributes["valid_date_situ"]);

        DB::beginTransaction();
        try {
            $model = $this->findDataUuid(Retail::class, $uuid);
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {
        \LogActivity::addToLog('get retail by id');
        is_uuid($uuid);

        $where['a.uuid'] = $uuid;
        $model = Retail::getData($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show_retail_administratif($uuid)
    {
        \LogActivity::addToLog('Show Retail administratif');
        is_uuid($uuid);

        $model = $this->findDataUuid(Retail::class, $uuid);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addToLog('update retail');
        is_uuid($uuid);

        $attributes = $request->all();
        $attributes['name'] = [
            'name' => $request['name'],
            'sales_unit_id' => $request['sales_unit_id'],
            'uuid' => $uuid,
        ];
        $this->validate($attributes, Retail::ruleUpdate());
        $attributes['name'] = $request['name'];
        $attributes["npwp_register"] = parseDate($attributes["npwp_register"]);
        $attributes["valid_date_siup"] = parseDate($attributes["valid_date_siup"]);
        $attributes["valid_date_tdp"] = parseDate($attributes["valid_date_tdp"]);
        $attributes["valid_date_situ"] = parseDate($attributes["valid_date_situ"]);

        // Cek For Profil Distributor
        // if (array_key_exists('customer_uuid', $attributes) && $attributes['customer_id']) {
        //     $ceksalesgroup = $this->cekRetailSalesGroup($attributes['customer_uuid'], $attributes['sales_unit_id']);
        //     if ($ceksalesgroup) {
        //         $error = [
        //             "address" => ['Alamat tidak cocok dengan Distributor'],
        //             "sales_unit_id" => ['Alamat tidak cocok dengan Distributor'],
        //         ];
        //         $response = responseFail($error);
        //         return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        //     }
        // }

        $model = $this->findDataUuid(Retail::class, $uuid);

        if ($model->email != $attributes['email']) {
            $data = Retail::where('email', $attributes['email'])->first();
            if ($data) {
                $response = responseFail(array('email' => array('Email ' . trans('messages.data-used'))));
                return response()->json($response, 400, [], JSON_PRETTY_PRINT);
            }
        } elseif ($model->npwp_no != $attributes['npwp_no']) {
            $data = Retail::where('npwp_no', $attributes['npwp_no'])->first();
            if ($data) {
                $response = responseFail(array('npwp_no' => array('Npwp_no ' . trans('messages.data-used'))));
                return response()->json($response, 400, [], JSON_PRETTY_PRINT);
            }
        } elseif ($model->siup_no != $attributes['siup_no']) {
            $data = Retail::where('siup_no', $attributes['siup_no'])->first();
            if ($data) {
                $response = responseFail(array('siup_no' => array('Siup_no ' . trans('messages.data-used'))));
                return response()->json($response, 400, [], JSON_PRETTY_PRINT);
            }
        }

        DB::beginTransaction();
        try {
            $attributes['updated_by'] = Auth::user()->id;
            Arr::forget($attributes, ['updated_at', 'created_at']);
            $model->update($attributes);

            $parent = Retail::find($model->id);
            $childs = Arr::except(
                $parent->toArray(),
                ["id", "code", "parent_id", "uuid", "sales_unit_id", "retail_administratif"]
            );

            Retail::where("parent_id", $model->id)
                ->update($childs);

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk retail');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(Retail::class, $iv['id']);

                /*Condition Error When Status Retion Not Draft*/
                $cekStatusAssg = $this->cekRetailAssgNotDraft($model->id);
                if ($cekStatusAssg) {
                    DB::rollback();
                    $response = responseFail('Retail Assegment Status tidak Draft');
                    $response['data'] = $model;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                // /* Update Condistion When Inactive Then Only Status D and S */
                // if ($model->status == 'n' and ($iv['status'] != 's' or $iv['status'] != 'd')) {
                //     DB::rollback();
                //     $response         = responseFail('Inactive Hanya diperbolehkan status Draft atau Submit');
                //     $response['data'] = $model;
                //     return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                // }
                //

                $this->validateStatusChange($model->uuid, $model->status, $iv['status']);
                unset($iv['id']);
                $iv['updated_by'] = Auth::user()->id;
                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($uuid)
    {
        \LogActivity::addToLog('delete retail');
        is_uuid($uuid);

        $model = $this->findDataUuid(Retail::class, $uuid);
        $attributes = ['status' => 'n'];

        DB::beginTransaction();
        try {
            $attributes['updated_by'] = Auth::user()->id;

            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique' => trans('messages.unique'),
            'email' => trans('messages.email'),
            'numeric' => trans('messages.numeric'),
            'exists' => trans('messages.exists'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => $validator->errors()->messages(),
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getImport(Request $request)
    {

        $attributes = $request->all();
        $this->validate($attributes, Retail::ruleFileImport());

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new RetailImport, $file)[0];

            if (!$this->compareTemplate("retail", array_keys($data[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }

            $newData = [];
            foreach ($data as $i => $iv) {
                $iv['id'] = strval($iv['distributor']);
                $iv['npwp_no'] = $iv['npwp'];
                $iv['siup_no'] = $iv['siup'];
                $iv['name'] = [
                    'name' => $iv['nama_pengecer'],
                    'sales_unit_id' => strval($iv['kode_kecamatan']),
                ];
                $iv['owner'] = $iv['nama_pemilik_perusahaan'];
                $iv['address'] = $iv['alamat_pengecer'];
                $iv['status'] = '';
                if ($iv['status_pengecer'] === 'Active') {
                    $iv['status'] = 'y';
                } else if ($iv['status_pengecer'] === 'Inactive') {
                    $iv['status'] = 'n';
                } else if ($iv['status_pengecer'] === 'Suspend') {
                    $iv['status'] = 'p';
                } else if ($iv['status_pengecer'] === 'Draft') {
                    $iv['status'] = 'd';
                } else {
                    $iv['status'] = 's';
                }

                // Add Unique Validation name and Owner
                $iv['name_and_owner'] = [
                    'name' => $iv['nama_pengecer'],
                    'owner' => $iv['nama_pemilik_perusahaan'],
                    'sales_unit_id' => strval($iv['kode_kecamatan']),
                ];

                //retail & customer sales office validation
                $iv['sales_unit_id'] = [
                    'kecamatan_id' => strval($iv['kode_kecamatan']),
                    'customer_id' => strval($iv['distributor']),
                ];

                $iv['sales_unit_id_spjb'] = [
                    'customer_id' => strval($iv['distributor']),
                    'year' => date('Y'),
                    'month' => date('m'),
                    'sales_unit_id' => strval($iv['kode_kecamatan']),
                ];
                $valida = Retail::ruleImport();
                $valida['no_faks_kantor'] = "max:12";
                $valida['no_telepon_kantor'] = "max:20";
                $valida['address'] = "max:191";
                $error = $this->validateImport($iv, $valida);
                unset($iv['sales_unit_id_spjb']);
                // $error = $this->validateImport($spjb,[new RetailSalesUnitExistSPJB]);
                unset($iv['name_and_owner']);
                $iv['name'] = $iv['nama_pengecer'];
                $iv['sales_unit_id'] = strval($iv['kode_kecamatan']);

                $customer_name = '';
                $sales_org_name = '';
                $newData[$i]['customer_id'] = $iv['distributor'];
                $newData[$i]['sales_org_id'] = $iv['sales_org'];
                $newData[$i]['id'] = $iv['kode_pengecer'];
                $newData[$i]['name'] = $iv['name'];
                $newData[$i]['owner'] = $iv['owner'];
                $newData[$i]['address'] = $iv['address'];
                $newData[$i]['sales_unit_id'] = $iv['sales_unit_id'];
                $newData[$i]['sales_unit_name'] = $iv['kecamatan'];
                $newData[$i]['village'] = $iv['desa'];
                $newData[$i]['tlp_no'] = $iv['no_telepon_kantor'];
                $newData[$i]['fax_no'] = $iv['no_faks_kantor'];
                $newData[$i]['hp_no'] = $iv["no_handphone"];
                $newData[$i]['email'] = $iv['email'];
                $newData[$i]['npwp_no'] = $iv['npwp_no'];
                $newData[$i]['npwp_register'] = ExcelUtils::ExcelToDate($iv['masa_berlaku_npwp']);
                $newData[$i]['siup_no'] = $iv['siup_no'];
                $newData[$i]['valid_date_siup'] = ExcelUtils::ExcelToDate($iv['masa_berlaku_siup']);
                $newData[$i]['situ_no'] = $iv['situ'];
                $newData[$i]['valid_date_situ'] = ExcelUtils::ExcelToDate($iv['masa_berlaku_situ']);
                $newData[$i]['tdp_no'] = $iv['tdp'];
                $newData[$i]['valid_date_tdp'] = ExcelUtils::ExcelToDate($iv['masa_berlaku_tdp']);
                $newData[$i]['recomd_letter'] = $iv['surat_rekomendasi_dinas'];
                $newData[$i]['recomd_letter_date'] = ExcelUtils::ExcelToDate($iv['masa_berlaku_surat_rekomendasi_dinas']);
                $newData[$i]['status'] = $iv['status'];

                $newData[$i]['validate'] = $error;
            }

            $response = responseSuccess(trans('messages.show-import-success'), $newData);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        $response = responseFail(trans('messages.show-import-fail'));
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    }

    public function saveJsonImport(Request $request)
    {
        DB::beginTransaction();
        $flag = true;
        $attributes = $request->all();
        $newData = [];
        $retail_assign = array();
        $valMax = $this->getMaxCode(Retail::notAdministratif());
        foreach ($attributes as $i => $iv) {
            $arrRetail = $attributes[$i];

            $kecamatan = $iv['sales_unit_id'];
            $iv['sales_unit_id'] = [
                'kecamatan_id' => strval($kecamatan),
                'customer_id' => strval($iv['customer_id']),
            ];

            $iv['sales_unit_id_spjb'] = [
                'customer_id' => strval($iv['customer_id']),
                'year' => date('Y'),
                'month' => date('m'),
                'sales_unit_id' => strval($kecamatan),
            ];
            // Add Unique Validation name and Owner
            $iv['name_and_owner'] = [
                'name' => $iv['name'],
                'owner' => $iv['owner'],
                'sales_unit_id' => strval($kecamatan),
            ];
            $iv['name'] = [
                'name' => $iv['name'],
                'sales_unit_id' => strval($kecamatan),
            ];

            $valida = Retail::ruleCreateUpload();
            $valida['fax_no'] = "max:12";
            $valida['tlp_no'] = "max:20";
            $valida['address'] = "max:191";
            $error = $this->validateImport($iv, $valida);

            unset($iv['sales_unit_id_spjb']);
            unset($iv['name_and_owner']);
            // $error += $this->validateImport($spjb,[new RetailSalesUnitExistSPJB]);

            $iv['sales_unit_id'] = $kecamatan;
            $iv['name'] =  $attributes[$i]['name'];

            if ($error['status'] == 0) {
                $flag = false;
                $attributes[$i]['validate'] = $error; //
                $arrRetail['validate'] = $error; //
            } else {
                $attributes[$i]['status'] = 's';
                $retail_assign[$i]['customer_id'] = $attributes[$i]['customer_id'];
                $retail_assign[$i]['sales_org_id'] = $attributes[$i]['sales_org_id'];
                $retail_assign[$i]['status'] = 'd';
                $retail_assign[$i]['created_by'] = Auth::user()->id;
                $retail_assign[$i]['updated_by'] = Auth::user()->id;
                $retail_assign[$i]['created_at'] = date('Y-m-d H:m:s');
                $retail_assign[$i]['updated_at'] = date('Y-m-d H:m:s');
                unset($attributes[$i]['validate']);
                unset($attributes[$i]['customer_id']);
                unset($attributes[$i]['sales_org_id']);
                unset($attributes[$i]['sales_unit_name']);
                unset($attributes[$i]['id']);
                $attributes[$i]['created_by'] = Auth::user()->id;
                $attributes[$i]['updated_by'] = Auth::user()->id;
                $attributes[$i]['created_at'] = date('Y-m-d H:m:s');
                $attributes[$i]['updated_at'] = date('Y-m-d H:m:s');
                $attributes[$i]['code'] = $this->generateCodeBulk($valMax, 'RT', 10);
                $attributes[$i]['sub_district_default'] = 0;
                $attributes[$i]['retail_administratif'] = 0;
                $valMax++;
                $retail_id = Retail::insertGetId($attributes[$i]);
                $retail_assign[$i]['retail_id'] = $retail_id;
            }
            $newData[] = $arrRetail;
        }

        if ($flag) {
            CustomerRetailerAssg::insert($retail_assign);
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $newData);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['data'] = $newData;
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    public function saveImport(Request $request)
    {
        $attributes = $request->all();
        $this->validate($attributes, Retail::ruleFileImport());

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new RetailImport, $file)[0];

            $newData = [];
            $flag = true;
            $sheet_no = 2;

            DB::beginTransaction();
            foreach ($data as $i => $iv) {

                $iv['npwp_no'] = $iv['npwp'];
                $iv['siup_no'] = $iv['siup'];
                $iv['name'] = [
                    'name' => $iv['nama_pengecer'],
                    'sales_unit_id' => $iv['kode_kecamatan'],
                ];
                $iv['owner'] = $iv['nama_pemilik_perusahaan'];
                $iv['address'] = $iv['alamat_pengecer'];
                $iv['sales_unit_id'] = $iv['kode_kecamatan'];
                $iv['status'] = '';
                if ($iv['status_pengecer'] === 'Active') {
                    $iv['status'] = 'y';
                } else if ($iv['status_pengecer'] === 'Inactive') {
                    $iv['status'] = 'n';
                } else if ($iv['status_pengecer'] === 'Suspend') {
                    $iv['status'] = 'p';
                } else if ($iv['status_pengecer'] === 'Draft') {
                    $iv['status'] = 'd';
                } else {
                    $iv['status'] = 's';
                }
                // Add Unique Validation name and Owner
                $iv['name_and_owner'] = [
                    'name' => $iv['name'],
                    'owner' => $iv['owner'],
                ];

                //retail & customer sales office validation
                $iv['sales_unit_id'] = [
                    'kecamatan_id' => $iv['kode_kecamatan'],
                    'customer_id' => $iv['distributor'],
                ];

                $iv['sales_unit_id_spjb'] = [
                    'customer_id' => $iv['customer_id'],
                    'year' => date('Y'),
                    'month' => date('m'),
                    'sales_unit_id' => $iv['sales_unit_id'],
                ];

                $validate = $this->validateImport($iv, Retail::ruleImport());
                $iv['name'] = $iv['nama_pengecer'];
                $iv['sales_unit_id'] = $iv['kode_kecamatan'];
                $validate['sheet_no'] = $sheet_no;
                $newDataChild[$i]['customer_id'] = $iv['distributor'];
                $newDataChild[$i]['sales_org_id'] = $iv['sales_org'];
                $newDataChild[$i]['created_by'] = Auth::user()->id;
                $newData[$i]['name'] = $iv['name'];
                $newData[$i]['owner'] = $iv['owner'];
                $newData[$i]['address'] = $iv['address'];
                $newData[$i]['sales_unit_id'] = $iv['sales_unit_id'];
                $newData[$i]['village'] = $iv['desa'];
                $newData[$i]['tlp_no'] = $iv['no_telepon_kantor'];
                $newData[$i]['fax_no'] = $iv['no_faks_kantor'];
                $newData[$i]["hp_no"] = $iv["no_handphone"];
                $newData[$i]['email'] = $iv['email'];
                $newData[$i]['npwp_no'] = $iv['npwp_no'];
                $newData[$i]['npwp_register'] = $iv['masa_berlaku_npwp'];
                $newData[$i]['siup_no'] = $iv['siup_no'];
                $newData[$i]['valid_date_siup'] = $iv['masa_berlaku_siup'];
                $newData[$i]['situ_no'] = $iv['situ'];
                $newData[$i]['valid_date_situ'] = $iv['masa_berlaku_situ'];
                $newData[$i]['tdp_no'] = $iv['tdp'];
                $newData[$i]['valid_date_tdp'] = $iv['masa_berlaku_tdp'];
                $newData[$i]['recomd_letter'] = $iv['surat_rekomendasi_dinas'];
                $newData[$i]['recomd_letter_date'] = $iv['masa_berlaku_surat_rekomendasi_dinas'];
                $newData[$i]['status'] = $iv['status'];
                $newData[$i]['sub_district_default'] = 0;

                $newData[$i]['created_by'] = Auth::user()->id;

                $newData[$i]['code'] = $this->generateCode(Retail::class, 'code', 'RT', 10);

                $newData[$i]['validate'] = $validate;

                if ($validate['status'] == 0) {
                    $flag = false;
                } else {
                    unset($newData[$i]['validate']);
                    $retail_id = Retail::insertGetId($newData[$i]);
                    $newDataChild[$i]['retail_id'] = $retail_id;
                    CustomerRetailerAssg::insert($newDataChild[$i]);
                }

                $sheet_no++;
            }

            if ($flag) {
                DB::commit();

                $response = responseSuccess(trans('messages.import-success'), true);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } else {
                DB::rollback();

                $response = responseFail(trans('messages.import-fail'));
                $response['data'] = $newData;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        }
    }

    private function cekRetailSalesGroup($customer_uuid, $sales_unit_id)
    {
        $customer = DB::table('wcm_customer as a')
            ->leftjoin('wcm_address as b', function ($q) {
                $q->on('b.customer_id', '=', 'a.id')->where('b.address_type', 'FORMAL');
            })
            ->where('a.uuid', $customer_uuid)
            ->first();
        $salesgroups = DB::table('wcm_sales_unit as a')
            ->leftjoin('wcm_sales_group as b', 'a.sales_group_id', '=', 'b.id')
            ->where('a.id', $sales_unit_id)
            ->first();

        if ($customer->sales_office_id != $salesgroups->sales_office_id) {
            return true;
        }
    }

    private function cekRetailAssgNotDraft($retail_id)
    {
        return CustomerRetailerAssg::where('retail_id', $retail_id)
            ->where('status', '!=', 'd')->exists();
    }

    public function export(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data = $this->index($request, null, true);
        return Excel::download((new RetailExport($data)), "retail_download.xls");
    }
}
