<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Retail;
use App\Models\SalesOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CompanyController extends Controller
{

    public function Distributor(Request $request)
    {
        \LogActivity::addToLog('get company distributor');

        $where = [];
        if ($this->isAdminAnper) {
            $where['tb2.sales_org_id'] = $this->salesOrgId;
        } elseif ($this->isDistributor) {
            $where['tb1.id'] = $this->customerId;
        }

        // Get attributtes for user management Area
        $user = $request->user();
        $filters = $user->filterRegional;        
                    
        if ($this->isAdmin) {
            if ($request['sales_org_id']) {
                $query   = Customer::getAll($where);
                $columns = [
                    'tb1.id'              => 'id',
                    'tb1.uuid'            => 'uuid',
                    'tb1.full_name'       => 'name',
                    'tb1.code_cust_group' => 'code_cust_group',
                    'tb2.sales_org_id'    => 'sales_org_id',
                    'tb4.id'              => 'sales_unit_id',
                    'tb4.name'            => 'sales_unit_name',
                    'tb7.id'              => 'sales_group_id',
                    'tb7.name'            => 'sales_group_name',
                    'tb8.id'              => 'sales_office_id',
                    'tb8.name'            => 'sales_office_name',
                ];
                if (count($filters) > 0) {
                    if (count($filters["sales_org_id"]) > 0) {
                        $query->whereIn("tb2.sales_org_id", $filters['sales_org_id']);
                    }
                }
            } else {
                $query = DB::table('wcm_customer as tb1')
                    ->leftjoin('wcm_address as tb3', function ($q) {
                        $q->on('tb1.id', '=', 'tb3.customer_id')->where('address_type', 'FORMAL');
                    })
                    ->leftjoin('wcm_sales_unit AS tb4', 'tb3.sales_unit_id', '=', 'tb4.id')
                    ->leftjoin('wcm_sales_group AS tb7', 'tb4.sales_group_id', '=', 'tb7.id')
                    ->leftjoin('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
                    ->select('tb1.*', 'tb4.id as sales_unit_id', 'tb4.name as sales_unit_name', 'tb7.id as sales_group_id', 'tb7.name as sales_group_name', 'tb8.id as sales_office_id', 'tb8.name as sales_office_name');
                $columns = [
                    'tb1.id'              => 'id',
                    'tb1.uuid'            => 'uuid',
                    'tb1.full_name'       => 'name',
                    'tb1.code_cust_group' => 'code_cust_group',
                    'tb4.id'              => 'sales_unit_id',
                    'tb4.name'            => 'sales_unit_name',
                    'tb7.id'              => 'sales_group_id',
                    'tb7.name'            => 'sales_group_name',
                    'tb8.id'              => 'sales_office_id',
                    'tb8.name'            => 'sales_office_name',
                ];
            }
        } else {
            $query   = Customer::getAll($where);
            $columns = [
                'tb1.id'              => 'id',
                'tb1.uuid'            => 'uuid',
                'tb1.full_name'       => 'name',
                'tb2.sales_org_id'    => 'sales_org_id',
                'tb1.code_cust_group' => 'code_cust_group',
                'tb4.id'              => 'sales_unit_id',
                'tb4.name'            => 'sales_unit_name',
                'tb7.id'              => 'sales_group_id',
                'tb7.name'            => 'sales_group_name',
                'tb8.id'              => 'sales_office_id',
                'tb8.name'            => 'sales_office_name',
            ];

            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb2.sales_org_id", $filters['sales_org_id']);
                }
            }          
        }
        
        // User Manegement For Sales Group Area
        if (count($filters) > 0) {
            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0 && !$this->isDistributor) {
                $query->whereIn("tb7.id", $filters["sales_group_id"] );
            }
        }

        if ($request['type'] && $request['sales_org_id']) {
            $query->leftJoin('wcm_customer_sales_area AS tb10', 'tb2.customer_id', '=', 'tb10.customer_id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.full_name')
                ->distinct('tb1.id', 'tb1.uuid', 'tb1.full_name')
                ->whereRaw("(tb1.id LIKE '10000%' OR tb1.id LIKE '20000%')")
                ->where('tb10.status', 'y');
        }

        if($request['retail']=="retail")
        {
            $query->select('tb1.id', 'tb1.uuid', 'tb1.full_name')
                  ->whereRaw("tb1.id NOT LIKE '4000%'");
        }

        if($request['relasi']=="relasi")
        {
            $query->select('tb1.id', 'tb1.uuid', 'tb1.full_name')
                   ->whereIn('tb1.code_cust_group', ['AR01', 'AR02']);
                  // ->whereRaw("(tb1.id LIKE '10000%' OR tb1.id LIKE '20000%')");
        }

        // Condisi Distict ketika Login Sebagai Distributor
        // Data Double karena Join Ke tabel wcm_customer_assign
        if ($this->isDistributor) {
            $query->select('tb1.id', 'tb1.uuid', 'tb1.full_name')
                ->distinct('tb1.id', 'tb1.uuid', 'tb1.full_name');
        }
        // End Condisi Distict ketika Login Sebagai Distributor

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function Produsen(Request $request)
    {
        \LogActivity::addToLog('get company produsen');
        $except = '';
        $where  = [];
        if ($this->isAdminAnper) {
            $where['tb1.id'] = $this->salesOrgId;
        } elseif ($this->isDistributor) {
            $where['tb2.customer_id'] = $this->customerId;
        }

        $user = $request->user();
        $filters = $user->filterRegional;

        if (!empty($request->get('except'))) {
            $except  = 1;
            $id      = $request->get('customer_id');
            $query   = SalesOrg::getAll($except, $where, $id)->whereIn('tb1.id', ['B000', 'C000', 'D000', 'E000', 'F000']);

            if (count($filters) > 0) {
                if (count($filters["sales_org_id"]) > 0) {
                    $query->whereIn('tb1.id', $filters["sales_org_id"] );
                }
            }

            $columns = [
                'tb1.id'   => 'id',
                'tb1.uuid' => 'uuid',
                'tb1.name' => 'name',
            ];
            $model = Datatables::of($query)
                ->filter(function ($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

            $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } else {

            $except = 0;
            if ($request->get('customer_id')) {
                $where['tb2.customer_id'] = $request->get('customer_id');
                $query                    = SalesOrg::getAll($except, $where)->whereIn('tb1.id', ['B000', 'C000', 'D000', 'E000', 'F000']);
            } else {
                $query = SalesOrg::getAll($except)->whereIn('tb1.id', ['B000', 'C000', 'D000', 'E000', 'F000']);
            }

            
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn('tb1.id', $filters["sales_org_id"] );
                }
            }

            $columns = [
                'tb1.id'          => 'id',
                'tb1.uuid'        => 'uuid',
                'tb1.name'        => 'name',
                'tb2.customer_id' => 'customer_id',
            ];
            $model = Datatables::of($query)
                ->filter(function ($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

            if($request->get('roles')=="roles" && $this->isAdmin )
            {
                // return "oke";
                // $model=[ "id"=> NULL,
                //          "name"=>"Pilih Semua",];
                $model=$model->getData(true);
                $temp=["id"=> "",
                         "name"=>"Pilih Semua Produsen"];
                array_unshift($model['data'],$temp);
                // $rrr=array_push($temp,$model['data']);

                // return $rrr;
                // $model['data']+=["id"=> NULL,
                //          "name"=>"Pilih Semua"];
            }else{
                $model=$model->getData(true);
            }
            

            $response = responseDatatableSuccess(trans('messages.read-success'),$model);

            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function Retail(Request $request)
    {
        \LogActivity::addToLog('get company pengecer');

        $query   = Retail::getAll();
        $columns = [
            'tb1.id'           => 'id',
            'tb1.uuid'         => 'uuid',
            'tb1.name'         => 'name',
            'tb2.customer_id'  => 'customer_id',
            'tb2.sales_org_id' => 'sales_org_id',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
