<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankAccount;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        \LogActivity::addToLog('Get All Data Bank Account');
        $query = BankAccount::list();
        $users = request()->user();
        $filters = $users->filterRegional;
        if (@$filters["sales_org_id"]) {
            $query->whereIn("ba.sales_org_id", $filters["sales_org_id"]);
        }

        $columns = [
            "so.id" => "sales_org_id",
            "bk.name" => "bank_name",
            "ba.bank_account" => "bank_account",
            "ba.bank_gl_account" => "bank_gl_account",
            "ba.bank_clearing_acct" => "bank_clearing_acct",
            "ba.status" => "status",
        ];

        return \DataTables::of($query)
                            ->filter(function ($query) use ($columns){
                                $this->filterColumn($columns, request(), $query);
                            })
                            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        \LogActivity::addToLog('Store Data Bank Account');
        $rule = BankAccount::ruleCreate();
        $this->validate($request->all(), $rule);

        $params   = $request->only(array_keys($rule));

        try {
            $params += [
                "status" => "Y",
                "updated_by" => auth()->user()->id,
                "created_by" => auth()->user()->id,
            ];


            $data = BankAccount::create($params);
        }
        catch (\Exception $e) {
            $response = responseFail($e->getMessage());
            return response()->json($response, 500);
        }

        return responseSuccess(__("messages.create-success"), $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        \LogActivity::addToLog('Get Data Data Bank Account by ID');
        $bankAccount = $this->findData(BankAccount::class,$id);

        return responseSuccess(__("messages.read-success"), $bankAccount);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        \LogActivity::addToLog('Update Data Bank Account by ID');
        $rule = BankAccount::ruleUpdate();
        $this->validate($request->all(), $rule);

        $params = $request->only(array_keys($rule));
        $bankAccount = $this->findData(BankAccount::class, $id);

        try {
            $bankAccount->update($params);
        }
        catch(\Exception $e) {
            $response = responseFail($e->getMessage());
            return response()->json($response, 500);
        }

        return responseSuccess(__("messages.update-success"), $bankAccount);
    }

    public function updateBatch(Request $request)
    {
        \LogActivity::addToLog('Update Data Bank Account Multiple');
        $rule = BankAccount::ruleUpdateBatch();

        $this->validate($request->all(), $rule);

        $params = $request->only(array_keys($rule));

        try {
            $ids        = array_get($params, "ids");
            $status     = array_get($params, "status");

            $updated    = BankAccount::whereIn("id", $ids)
                                ->update(compact('status'));
        }
        catch(\Exception $e) {
            $response = responseFail($e->getMessage());
            return response()->json($response, 500);
        }

        return responseSuccess(__("messages.update-success"), BankAccount::whereIn("id",$ids)->get());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        \LogActivity::addToLog('Delete Data Bank Account');
        $this->validate($request->all(), [
            "id" => "required|string"
        ]);

        $id = $request->input("id");

        try {
            $bankAccount = BankAccount::whereIn("id",explode(",", $id))->get();

            if ( $bankAccount->isEmpty() ) throw new \Exception(__( "messages.read-fail"));

            $bankAccount->delete();
        }
        catch(\Exception $e) {
            $response = responseFail($e->getMessage());
            return response()->json($response, 500);
        }

        return responseSuccess(__("messages.delete-success"), $bankAccount);

    }
}
