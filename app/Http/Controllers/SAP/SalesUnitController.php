<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Models\Retail;
use App\Models\SalesUnit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesUnitController extends Controller
{

    public function store()
    {

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('sales_unit.xml');
            $result = $soap->SI_SalesUnit();

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_DATA->item as $value) {
                // if (!is_numeric(@$value->ID_UNIT)) {
                //     continue;
                // }
                $id['id']               = substr($value->ID_UNIT, -6);
                $data['name']           = $value->NAME;
                $data['sales_group_id'] = substr($value->ID_GROUP, -4);

                $so = SalesUnit::updateOrCreate($id, $data);
                if ($so->wasRecentlyCreated) {
                    $create++;
                } else if ($so->wasChanged()) {
                    $update++;
                }

                $this->storeRetail(substr(@$value->ID_UNIT, -6), $data);
                array_push($hasil, $so);
            }
            DB::commit();
            Log::info('SAP Sync sales unit', [
                'status' => 'success',
                'sap'    => count($result->T_DATA->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync sales unit', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function storeRetail($sales_unit_id, $data)
    {
        if (!Retail::where('sub_district_default', 1)->where('sales_unit_id', $sales_unit_id)->exists()) {
            return Retail::updateOrCreate([
                'name'          => $data['name'],
                'owner'         => $data['name'],
                'address'       => $data['name'],
                'sales_unit_id' => $sales_unit_id,
                'status'        => 'y',
            ], [
                'code' => $this->generateCode(
                    Retail::NotAdministratif(), 'code', 'RT', 10
                )
            ]);
        }

    }

}
