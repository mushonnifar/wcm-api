<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\Address;
use App\Models\Contact;
use App\Models\CustSalesOrgAssg;
use App\Models\Customer;
use App\Models\CustomerSalesArea;
use App\Models\PartnerFunction;
use App\Models\PaymentMethod;
use App\Models\SalesArea;
use App\Models\SalesOrg;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function store()
    {
        $param = [
            'P_CUSTOMER' => 'X',
            'R_GROUP'    => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_CUSTOMER->item as $value) {
                if ($value->ZCSST == '') {
                    continue;
                }
                $id['id'] = $value->KUNNR;
                $data     = $this->getColumnNameCustomer($value);
                $cust     = Customer::updateOrCreate($id, $data);
                if ($cust->wasRecentlyCreated) {
                    $create++;
                } else if ($cust->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $cust);
            }
            DB::commit();
            Log::info('SAP Sync customer master', [
                'status' => 'success',
                'sap'    => count($result->T_CUSTOMER->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer master', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storeAddress()
    {
        $param = [
            'P_ADDRESS' => 'X',
            'R_GROUP'   => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_ADDRESS->item as $value) {
                if (!$this->cekCustomer($value->KUNNR)) {
                    continue;
                }
                if ($value->ADRTYP1 == 'FORMAL') {
                    $data = $this->getColumnNameAddressFormal($value);
                    $cust = Address::updateOrCreate($data['id'], $data['data']);
                    if ($cust->wasRecentlyCreated) {
                        $create++;
                    } else if ($cust->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $cust);
                }
                if ($value->ADRTYP2 == 'BILLING') {
                    $data = $this->getColumnNameAddressBilling($value);
                    $cust = Address::updateOrCreate($data['id'], $data['data']);
                    if ($cust->wasRecentlyCreated) {
                        $create++;
                    } else if ($cust->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $cust);
                }
            }
            DB::commit();
            Log::info('SAP Sync customer address', [
                'status' => 'success',
                'sap'    => count($result->T_ADDRESS->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer address', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storeContact()
    {
        $param = [
            'P_CONTACT' => 'X',
            'R_GROUP'   => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_CONTACT->item as $value) {
                if (!$this->cekCustomer($value->KUNNR)) {
                    continue;
                }
                $id['customer_id'] = $value->KUNNR;
                $data              = $this->getColumnNameContact($value);
                $cust              = Contact::updateOrCreate($id, $data);
                if ($cust->wasRecentlyCreated) {
                    $create++;
                    $this->createUser($value->KUNNR, $value->EMAIL);
                } else if ($cust->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $cust);
            }
            DB::commit();
            Log::info('SAP Sync customer contact', [
                'status' => 'success',
                'sap'    => count($result->T_CONTACT->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer contact', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storePayment()
    {
        set_time_limit(0);
        $param = [
            'P_PAYMENT' => 'X',
            'R_GROUP'   => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $time   = time();
            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_PAYMENT->item as $value) {
                if (!$this->cekCustomer($value->KUNNR)) {
                    continue;
                }

                if (strlen($value->ZWELS) > 1) {
                    $str = str_split($value->ZWELS);
                    $len = strlen($value->ZWELS);
                    for ($i = 0; $i < $len; $i++) {
                        $value->ZWELS = $str[$i];
                        if ($value->ZWELS != 'E' && $value->ZWELS != 'D') {
                            continue;
                        }
                        if (!in_array($value->BUKRS, ['B000', 'C000', 'D000', 'E000', 'F000'])) {
                            continue;
                        }
                        $data               = $this->getColumnNamePayment($value);
                        $id['customer_id']  = $data['customer_id'];
                        $id['sales_org_id'] = $data['sales_org_id'];
                        $id['sap_code']     = $data['sap_code'];
                        $data['system']     = $time;
                        $cust               = PaymentMethod::updateOrCreate($id, $data);
                        if ($cust->wasRecentlyCreated) {
                            $create++;
                        } else if ($cust->wasChanged()) {
                            $update++;
                        }
                        array_push($hasil, $cust);
                    }
                } else {
                    if ($value->ZWELS != 'E' && $value->ZWELS != 'D') {
                        continue;
                    }
                    if (!in_array($value->BUKRS, ['B000', 'C000', 'D000', 'E000', 'F000'])) {
                        continue;
                    }
                    $data               = $this->getColumnNamePayment($value);
                    $id['customer_id']  = $data['customer_id'];
                    $id['sales_org_id'] = $data['sales_org_id'];
                    $id['sap_code']     = $data['sap_code'];
                    $data['system']     = $time;
                    $cust               = PaymentMethod::updateOrCreate($id, $data);
                    if ($cust->wasRecentlyCreated) {
                        $create++;
                    } else if ($cust->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $cust);
                }
            }

            PaymentMethod::whereNull('system')->orWhere('system', '<>', $time)->delete();

            DB::commit();
            Log::info('SAP Sync customer payment method', [
                'status' => 'success',
                'sap'    => count($result->T_PAYMENT->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer payment method', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storeCustSalesOrgAssg()
    {
        set_time_limit(0);
        $param = [
            'P_PAYMENT' => 'X',
            'R_GROUP'   => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_PAYMENT->item as $value) {
                if (!$this->cekCustomer($value->KUNNR)) {
                    continue;
                }
                if (!$this->cekProdusen($value->BUKRS)) {
                    continue;
                }
                $data['customer_id']  = $value->KUNNR;
                $data['sales_org_id'] = $value->BUKRS;
                $cust                 = CustSalesOrgAssg::updateOrCreate($data, $data);
                if ($cust->wasRecentlyCreated) {
                    $create++;
                } else if ($cust->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $cust);
            }
            DB::commit();
            Log::info('SAP Sync customer sales org assign', [
                'status' => 'success',
                'sap'    => count($result->T_PAYMENT->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer sales org assign', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storePartnerFunction()
    {
        set_time_limit(0);
        $param = [
            'P_PARTNER_FUNCTION' => 'X',
            'R_GROUP'            => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_PARTNER_FUNCTION->item as $value) {
                if (!$this->cekCustomer($value->KUNNR) || !$this->cekCustomer($value->KUNN2)) {
                    continue;
                }

                $param     = $this->getColumnNameSalesArea($value);
                $salesArea = $this->getSalesArea($param);
                if (!$salesArea) {
                    continue;
                }
                $paramDua['sales_area_id'] = $salesArea->id;
                $paramDua['customer_id']   = $value->KUNNR;
                $custSalesArea             = $this->getCustSalesArea($paramDua);
                if ($custSalesArea) {
                    $id['customer_id']            = $value->KUNN2;
                    $id['customer_sales_area_id'] = $custSalesArea->id;
                    $id['partner_type']           = $value->PARVW;
                    $data['partner_type_desc']    = $value->VTEXT;
                    $cust                         = PartnerFunction::updateOrCreate($id, $data);
                    if ($cust->wasRecentlyCreated) {
                        $create++;
                    } else if ($cust->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $cust);
                }
            }
            DB::commit();
            Log::info('SAP Sync partner function', [
                'status' => 'success',
                'sap'    => count($result->T_PARTNER_FUNCTION->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync partner function', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storeSalesArea()
    {
        $param = [
            'P_CUSTOMER_SALESAREA' => 'X',
            'R_GROUP'              => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR01'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR02'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'AR04'],
                ],
            ],
            "RETURN" => [],
        ];

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('master_customer.xml');
            $result = $soap->SI_Customer($param);

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_CUST_SALESAREA->item as $value) {
                if ($value->SPART != '00' || !$this->cekSalesOrg($value->VKORG)) {
                    continue;
                }
                if ($value->VTWEG == 10 || $value->VTWEG == 20) {
                    $data = $this->getColumnNameSalesArea($value);
                    $cust = SalesArea::updateOrCreate($data, $data);
                }
            }
            foreach ($result->T_CUST_SALESAREA->item as $value) {
                if (!$this->cekCustomer($value->KUNNR) || !$this->cekSalesOrg($value->VKORG)) {
                    continue;
                }
                $data      = $this->getColumnNameCustSalesArea($value);
                $param     = $this->getColumnNameSalesArea($value);
                $salesArea = $this->getSalesArea($param);
                if ($salesArea) {
                    $id['customer_id']   = $value->KUNNR;
                    $id['sales_area_id'] = $salesArea->id;
                    $cust                = CustomerSalesArea::updateOrCreate($id, $data);
                    if ($cust->wasRecentlyCreated) {
                        $create++;
                    } else if ($cust->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $cust);
                }
            }
            DB::commit();
            Log::info('SAP Sync customer sales area', [
                'status' => 'success',
                'sap'    => count($result->T_CUST_SALESAREA->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync customer sales area', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function getSalesArea($param)
    {
        $data = SalesArea::where([
            'sales_org_id'       => $param['sales_org_id'],
            'distrib_channel_id' => $param['distrib_channel_id'],
            'sales_division_id'  => $param['sales_division_id'],
        ])->first();

        return $data;
    }

    private function getCustSalesArea($param)
    {
        $data = CustomerSalesArea::where([
            'customer_id'   => $param['customer_id'],
            'sales_area_id' => $param['sales_area_id'],
        ])->first();

        return $data;
    }

    private function getColumnNameCustomer($value)
    {
        $arr = [
            "KUNNR"       => "id",
            "NAME1"       => "full_name",
            "NAME4"       => "owner",
            "ERDAT"       => "register_date",
            "ZCSST"       => "sap_status",
            "KTOKD"       => "code_cust_group",
            "TXT30"       => "name_cust_group",
            "STCEG"       => "npwp_no",
            "ZVEXP"       => "npwp_register",
            "ZSRDX"       => "recomd_letter_date",
            "ZSRDN"       => "recomd_letter",
            "ZSITUX"      => "valid_date_situ",
            "ZSITUN"      => "situ_no",
            "ZSIUPX"      => "valid_date_siup",
            "ZSIUPN"      => "siup_no",
            "ZTDPX"       => "valid_date_tdp",
            "ZTDPN"       => "tdp_no",
            "CREATE_AT"   => "create_at",
            "ZCPTP"       => "category_type",
            "ZCPTP_DESCR" => "category",
            "ZAVAL"       => "asset",
            "ZREVN"       => "revenue",
            "ZYEAR"       => "account_year",
        ];

        $status = '';
        if ($value->ZCSST == 1) {
            $status = 'y';
        } else if ($value->ZCSST == 2) {
            $status = 'n';
        }

        foreach ($value as $k => $v) {
            if (array_key_exists($k, $arr)) {
                if ($k === "NAME1" && @$value->ANRED) {
                    $v = "{$value->ANRED}. {$v}";
                }
                $data[$arr[$k]] = $v;
            }
        }

        $data['status'] = $status;

        return $data;
    }

    private function getColumnNameAddressFormal($value)
    {
        $id['customer_id']       = $value->KUNNR;
        $id['address_type']      = $value->ADRTYP1;
        $data['address']         = $value->STREET1;
        $data['sales_unit_id']   = substr($value->DISTRIK, -6);
        $data['tlp_no']          = $value->TLPNO;
        $data['fax_no']          = $value->FAXNO;
        $data['sales_office_id'] = $this->cekKodeSalesOffice($value->REGION);
        $data['sales_group_id']  = $this->cekKodeSalesGroup($value->CITY_CODE);

        return $data = [
            "id"   => $id,
            "data" => $data,
        ];
    }

    private function getColumnNameAddressBilling($value)
    {
        $id['customer_id']     = $value->KUNNR;
        $id['address_type']    = $value->ADRTYP2;
        $data['address']       = $value->STREET2;
        $data['sales_unit_id'] = substr($value->DISTRIK, -6);
        $data['tlp_no']        = $value->TLPNO;
        $data['fax_no']        = $value->FAXNO;

        return $data = [
            "id"   => $id,
            "data" => $data,
        ];
    }

    private function getColumnNameContact($value)
    {
        $arr = [
            "KUNNR" => "customer_id",
            "NAME1" => "name",
            "NOTLP" => "tlp_no",
            "EMAIL" => "email",
        ];

        foreach ($value as $k => $v) {
            if (array_key_exists($k, $arr)) {
                if ($k == "NOTLP") {
                    $v = preg_replace('/[^0-9]/', '', $v);
                }
                $data[$arr[$k]] = $v;
            }
        }

        return $data;
    }

    private function getColumnNamePayment($value)
    {
        $arr = [
            "KUNNR" => "customer_id",
            "BUKRS" => "sales_org_id",
            "ZWELS" => "sap_code",
            "ERDAT" => "created_at",
        ];

        foreach ($value as $k => $v) {
            if (array_key_exists($k, $arr)) {
                $data[$arr[$k]] = $v;
            }
        }

        $ident = '';
        if ($value->ZWELS == 'D') {
            $ident = 'DISTRIBUTOR_FINANCING';
            $name  = 'Distributor Financing';
        } else if ($value->ZWELS == 'E') {
            $ident = 'CASH';
            $name  = 'Cash';
        }

        $data['name']       = $name;
        $data['ident_name'] = $ident;

        return $data;
    }

    private function getColumnNameSalesArea($value)
    {
        $arr = [
            "VKORG" => "sales_org_id",
            "VTWEG" => "distrib_channel_id",
            "SPART" => "sales_division_id",
        ];

        foreach ($value as $k => $v) {
            if (array_key_exists($k, $arr)) {
                $data[$arr[$k]] = $v;
            }
        }

        return $data;
    }

    private function getColumnNameCustSalesArea($value)
    {
        $arr = [
            "KUNNR"  => "customer_id",
            "TOPYM"  => "term_of_payment",
            "TOPDP"  => "top_dp",
            "TAXCL"  => "tax_classification",
            "PPH22"  => "pph22",
            "TOPUOM" => "top_dp_uom",
        ];

        foreach ($value as $k => $v) {
            if (array_key_exists($k, $arr)) {
                $data[$arr[$k]] = $v;
            }
        }
        $data['top_dp'] = intval($value->TOPDP) . '' . $value->TOPUOM;
        $data['status'] = $value->STATS == "1" ? "y" : "n";

        return $data;
    }

    private function cekCustomer($customer_id)
    {
        $cek = Customer::find($customer_id);
        return $cek;
    }

    private function cekProdusen($prod)
    {
        $cek = SalesOrg::find($prod);
        return $cek;
    }

    private function createUser($customer_id, $email)
    {
        $password = $email ? str_random(10) : "indonesia2019";

        $customer = Customer::find($customer_id);

        $attributes['name']        = $customer->full_name;
        $attributes['email']       = $email ?: "{$customer_id}@pupukindonesia.com";
        $attributes['password']    = Hash::make($password);
        $attributes['customer_id'] = $customer_id;

        $user = User::firstOrCreate(['username' => $customer_id], $attributes);

        /* Find the row of user */
        $model = User::where("username", $user->username)->first();
        if ($email) {
            SendEmail::dispatch([
                'name'     => $customer->full_name,
                'username' => $customer_id,
                'password' => $password,
                'date'     => $model->created_at,
                'from'     => 'wcm.notif@gmail.com',
                'to'       => 'theyoko3@gmail.com',
            ]);

        }
        /* Sync the rule into user */
        $this->setRoles($model->id, 4);
//            Log::info('create user', $user);
    }

    private function setRoles($userID, $roleID)
    {
        $user = $this->findData(User::class, $userID);
        $data = $user->syncRoles($roleID);

        return $data;
    }

    private function cekKodeSalesOffice($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }

    private function cekKodeSalesGroup($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length > 4) {
            return substr($sales_group_id, -4);
        }
        return $sales_group_id;
    }

    private function cekSalesOrg($value)
    {
        if ($value == 'B000' || $value == 'C000' || $value == 'D000' || $value == 'E000' || $value == 'F000') {
            return true;
        }
        return false;
    }
}
