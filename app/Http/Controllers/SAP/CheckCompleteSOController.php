<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CheckCompleteSOController extends Controller
{
    /**
     * { function_description }
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @throws     \Exception                (description)
     */
    public function index(Request $request)
    {

        $so_number = Order::whereNotNull("so_number")
            ->where("status", "k")
            ->pluck("so_number")
            ->unique();

        $params = [
            "R_SALESORDER" => [],
        ];

        foreach ($so_number as $index => $value) {
            $param = [
                "LOW"    => $value,
                'SIGN'   => 'I',
                'OPTION' => 'EQ',
            ];

            data_fill($params, "R_SALESORDER.item.$index", $param);
        }

        try {
            $sap = SAPConnect::connect("check_status_so.xml");
            $res = $sap->SI_CheckStatusSO($params);

            if (!property_exists($res, "T_DATA") || !property_exists($res->T_DATA, "item")) {
                throw new \Exception($res->MESSAGE, 1);
            }

            // Casting item ke type array Karena Jika data respon dari SAP cuma satu
            $item    = is_array($res->T_DATA->item) ? $res->T_DATA->item : array($res->T_DATA->item);
            $data    = collect($item);
            $updated = $this->doUpdateSO($data);

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return responseSuccess(trans("messages.validate-check-success"), compact("so_number", "updated"));
    }

    private function doUpdateSO(Collection $data)
    {
        $updated = $data->map(function ($item) {
            $order = Order::where("so_number", Arr::get($item, "VBELN"))->first();

            if (Str::lower(Arr::get($item, "GBSTK")) == "c") {
                DB::beginTransaction(function () {
                    $order->status = Arr::get($item, "GBSTK");
                    $order->save();
                    return $order;
                });
            }

            return null;
        });

        return $updated->filter()
            ->unique()
            ->count();
    }
}
