<?php

namespace App\Http\Controllers\SAP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesArea;
use App\Helpers\SAPConnect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesAreaController extends Controller {

    //
    public function store() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('sales_area.xml');
            $result = $soap->SI_SalesArea(['P_MASTER' => 'X']);
//return response()->json($result, 201, [], JSON_PRETTY_PRINT);
            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_DATA->item as $value) {
                if ($value->SPART != '00' || ($value->VKORG != 'B000' && $value->VKORG && 'C000' 
                        && $value->VKORG && 'D000' && $value->VKORG != 'E000' && $value->VKORG != 'F000')) {
                    continue;
                }
                $data['sales_org_id'] = $value->VKORG;
                $data['distrib_channel_id'] = $value->VTWEG;
                $data['sales_division_id'] = $value->SPART;

                if ($value->VTWEG == 10 || $value->VTWEG == 20) {

                    $sa = SalesArea::updateOrCreate($data, $data);

                    if ($sa->wasRecentlyCreated) {
                        $create++;
                    } else if ($sa->wasChanged()) {
                        $update++;
                    }
                    array_push($hasil, $sa);
                }
            }
            DB::commit();
            Log::info('SAP Sync sales area', [
                'status' => 'success',
                'sap' => count($result->T_DATA->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync sales area', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
