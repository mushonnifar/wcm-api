<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Models\SalesGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesGroupController extends Controller
{

    public function store()
    {

        DB::beginTransaction();
        try {
            $soap   = SAPConnect::connect('sales_group.xml');
            $result = $soap->SI_SalesGroup();

            $create = 0;
            $update = 0;
            $hasil  = [];
            foreach ($result->T_DATA->item as $value) {
                if (!is_numeric(@$value->CITY_CODE)) {
                    continue;
                }

                $id['id']                = substr($value->CITY_CODE, -4);
                $data['name']            = $value->CITY_NAME;
                $data['sales_office_id'] = $value->REGION;
                $data['district_code']   = $value->CITY_SH10;

                $so = SalesGroup::updateOrCreate($id, $data);
                if ($so->wasRecentlyCreated) {
                    $create++;
                } else if ($so->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $so);
            }
            DB::commit();
            Log::info('SAP Sync sales group', [
                'status' => 'success',
                'sap'    => count($result->T_DATA->item),
                'data'   => count($hasil),
                'create' => $create,
                'update' => $update,
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync sales group', [
                'status' => 'fail',
            ]);
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
