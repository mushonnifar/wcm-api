<?php

namespace App\Http\Controllers\SAP;

use App\Http\Controllers\Controller;
use App\Models\Testing;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SAPToWcmTestingController extends Controller
{
    public function index()
    {
        return DataTables::of(Testing::query())->make(true);
    }

    public function store(Request $request)
    {
        $this->validate($request->all(),[
            "so_number" => "required"
        ]);

        try {
            $row = Testing::create($request->only("so_number"));
        }
        catch(\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()->json(responseSuccess(trans("messages.create-success"), $row));
    }

    public function destroy($id)
    {

    }
}
