<?php

namespace App\Http\Controllers\SAP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesOfficeAssign;
use App\Models\SalesArea;
use App\Helpers\SAPConnect;
use App\Models\SalesGroup;
use App\Models\ApprovalSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesAreaAssigController extends Controller {

    //
    public function store() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('sales_area.xml');
            $result = $soap->SI_SalesArea(array('P_ASSIGNMENT' => 'X'));

            $create = 0;
            $update = 0;
            $hasil = [];
            $loop = 0;
            foreach ($result->T_ASSIGNMENT->item as $value) {
                if ($value->SPART != '00' || ($value->VKORG != 'B000' 
                        && $value->VKORG != 'C000'
                        && $value->VKORG != 'D000'
                        && $value->VKORG != 'E000'
                        && $value->VKORG != 'F000')) {
                    continue;
                }

                if ($value->VTWEG == 10 || $value->VTWEG == 20) {
                    $datafind['sales_org_id'] = $value->VKORG;
                    $datafind['distrib_channel_id'] = $value->VTWEG;
                    $datafind['sales_division_id'] = $value->SPART;
                    $insert['sales_office_id'] = $value->VKBUR;

                    $find = SalesArea::where($datafind)->first();
                    if ($find) {
                        $insert['sales_area_id'] = $find->id;
                    }

                    $findAssingn = SalesOfficeAssign::where($insert)->first();
                    if ($findAssingn) {
                        $id['id'] = $findAssingn->id;
                        $sa = SalesOfficeAssign::updateOrCreate($id, $insert);
                    } else {
                        $sa = SalesOfficeAssign::create($insert);
                    }
                    if ($sa->wasRecentlyCreated) {
                        $create++;
                    } else if ($sa->wasChanged()) {
                        $update++;
                    }
                    $this->storeApprovalSetting($value->VKORG, $value->VKBUR);
                    array_push($hasil, $sa);
                }
            }
            DB::commit();
            Log::info('SAP Sync sales area assign', [
                'status' => 'success',
                'sap' => count($result->T_ASSIGNMENT->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync sales area assign', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function storeApprovalSetting($salesOrg, $salesOffice) {
        $salesGroup = SalesGroup::where('sales_office_id', $salesOffice)->pluck('id')->toArray();

        foreach ($salesGroup as $value) {
            $params['sales_org_id'] = $salesOrg;
            $params['sales_group_id'] = $value;

            if (ApprovalSetting::where($params)->exists()) continue;

            $params['status'] = 'n';
            ApprovalSetting::create($params);
        }
    }

}
