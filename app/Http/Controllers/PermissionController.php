<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\MAction;
use App\Models\Route;
use App\Models\User;
use App\Models\PermissionHasRoutes;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    public function index()
    {
        //Pending
    }

    public function store(Request $request)
    {

        \LogActivity::addToLog('create permission has routes');

        $attributes = $request->all();

        $this->validate($attributes, MAction::ruleCreate());

        $route_id = Route::find($attributes['route_id']);

        $id = Permission::max('id');

        $paramPermission['id'] = intval($id) + 1;
        $paramPermission['name'] = $route_id->name.'-'.$attributes['action_id'];
        $paramPermission['guard_name'] = $attributes['guard_name'];
        $paramPermission['route_id'] = $attributes['route_id'];
        $paramPermission['action_id'] = $attributes['action_id'];
        $paramPermission['created_by'] = Auth::user()->id;
        $paramPermission['updated_by'] = Auth::user()->id;

        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {
            $permission = Permission::updateOrCreate($paramPermission);
            $permission->hasRoutes()->create($attributes);

            $permissions = Permission::with('hasRoutes')->where('id', $permission->id)->get()->toArray();

            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $permissions);

            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function update(Request $request, $id)
    {
        \LogActivity::addToLog('Update permission has routes');

        $attributes = $request->all();

        $this->validate($attributes, MAction::ruleCreate());

        $route_id = Route::find($attributes['route_id']);

        $paramPermission['name'] = $route_id->name.'-'.$attributes['action_id'];
        $paramPermission['guard_name'] = $attributes['guard_name'];
        $paramPermission['updated_by'] = Auth::user()->id;

        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {
            $permission = Permission::updateOrCreate($paramPermission);
            $permission->hasRoutes()->whereId($id)->update($attributes);

            $hasRoute = PermissionHasRoutes::where('id', $id)->get()->toArray();

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $hasRoute);

            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete permission');
        
        // $model = $this->findData(\App\Models\PermissionHasRoutes::class, $id);

        // DB::beginTransaction();
        // try {
        //     $model = $model->delete();
        //     DB::commit();
        //     $response = responseSuccess(trans('messages.delete-success'), $model);

        //     return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        // } catch (\Exception $ex) {
        //     DB::rollback();
        //     $response = responseFail(trans('messages.update-fail'));
        //     $response['errors'] = $ex->getMessage();
        //     return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        // }
    }

}
