<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\CustomerRetailerAssg;
use App\Models\Retail;
use App\Models\SalesArea;
use App\Models\SalesOfficeAssign;
use App\Models\SalesGroup;
use App\Models\SalesOffice;
use App\Models\ContractItem;
use App\Models\ReportF6;
use App\Models\ReportF6Items;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class RetailAssgController extends Controller
{

    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get all retail assg');

        $where = [];
        if ($this->isDistributor) {
            $where['a.customer_id'] = $this->customerId;
        }
        $query   = CustomerRetailerAssg::getData($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("a.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("e.sales_group_id", $filters["sales_group_id"]);
            }
        }
        $columns = [
            'a.uuid'            => 'uuid',
            'a.customer_id'     => 'customer_id',
            'c.uuid'            => 'customer_uuid',
            'c.full_name'       => 'customer_name',
            'c.owner'           => 'customer_owner',
            'a.sales_org_id'    => 'sales_org_id',
            'd.name'            => 'sales_org_name',
            'a.retail_id'       => 'retail_id',
            'b.code'            => 'retail_code',
            'b.name'            => 'retail_name',
            'b.owner'           => 'retail_owner',
            'b.address'         => 'address',
            'b.sales_unit_id'   => 'sales_unit_id',
            'e.name'            => 'sales_unit_name',
            'e.sales_group_id'  => 'sales_group_id',
            'f.name'            => 'sales_group_name',
            'f.sales_office_id' => 'sales_office_id',
            'g.name'            => 'sales_office_name',
            'a.status'          => 'status',
        ];

        $data = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if ($exported) return $data->getFilteredQuery();
        $data= $data->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $data->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function provinsiAssg(Request $request)
    {
        \LogActivity::addToLog('Get Provinsi Sales Org Area');
        $attributes = $request->only(['sales_org_id','customer_id']);
        $this->validate($attributes, [
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'customer_id'  => 'required|exists:wcm_customer,id',
        ]);

        // $salesarea = SalesArea::where('sales_org_id', $attributes['sales_org_id'])->pluck('id');

        // $salesofficeassign = SalesOfficeAssign::distinct('sales_office_id')
        //     ->whereIn('sales_area_id', $salesarea)
        //     ->select('sales_office_id')
        //     ->with('salesoffice');

        $salesofficeassign=SalesOffice::select('*')->whereRaw("id != 0001");
            

        $user = $request->user();
        $filters = $user->filterRegional;
        
        
        if (count($filters) > 0) {
            if (count($filters["sales_org_id"]) > 0) {
                $salesarea = SalesArea::whereIn('sales_org_id',$filters['sales_org_id'])->get()->pluck('id');
                $officeAssign =SalesOfficeAssign::whereIn('sales_area_id',$salesarea)->get()->pluck('sales_office_id')->unique();
                $salesofficeassign->whereIn("id", $officeAssign);
            }

            if (count($filters["sales_group_id"]) > 0) {
                $sales_office_arr= SalesGroup::whereIn("id", $filters["sales_group_id"])->pluck('sales_office_id')->unique();
                $salesofficeassign->whereIn("id", $sales_office_arr );
            }
        }

        $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb2.sales_org_id'=>$attributes['sales_org_id'],'tb2.contract_type'=>'operational','tb1.status'=>'y','tb1.year'=>date('Y')])
                ->select('tb1.sales_office_id')
                ->distinct()->get()->pluck('sales_office_id');
        

        $salesofficeassign=$salesofficeassign->whereIn('id',$spjb)->get();

        // return response()->json($salesofficeassign->get());


        $multiplied = $salesofficeassign->map(function ($item, $key) {
            $data["sales_office_id"]   = $item["id"];
            $data["sales_office_name"] = $item['name'];
            return $data;
        });

        $response = responseSuccess(trans('messages.read-success'), $multiplied);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function updateBulk(Request $request)
    {

        \LogActivity::addToLog('update bulk retail assg');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);

                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(CustomerRetailerAssg::class, $iv['id']);
                /* Update Condistion When Inactive Then Only Status D and S */
                if ($model->status == 'n' and ($iv['status'] != 's' and $iv['status'] != 'd')) {
                    DB::rollback();
                    $response         = responseFail('Inactive Hanya diperbolehkan status Draft atau Submit');
                    $response['data'] = $model;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
                $retail=Retail::findorfail($model->retail_id);
                // Kondisi update status retail assg ke y or s harus pengecer dengan status active
                if(($iv['status']=='s' or $iv['status']=='y') and $retail->status!='y'){
                    DB::rollback();
                    $response         = responseFail('Gagal ubah Status Submit atau Active, Status Pengecer Harus Active, Silahkan Cek Status Pengecer Terlebih Dahulu ');
                    $response['data'] = $model;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                if($iv['status']=="n"){
                    $F6= ReportF6::where('customer_id', $model->customer_id)
                            ->where('sales_org_id', $model->sales_org_id)
                            ->whereIn('status',['s','y'])
                            ->orderby('id','desc');
                            // ->first();
                    // dd($F6->exists());
                    if($F6->exists()){
                        $F6=$F6->first();
                        $items = $F6->items()->where('stok_akhir','!=',0)->where('retail_id',$model->retail_id)
                            ->pluck('stok_akhir')
                            ->sum();

                        if($items!=0) {
                            DB::rollback();
                            $response         = responseFail('Pengecer '.$retail->code.' masih memiliki Stok Akhir');
                            $response['error'] = ['retails_stok'=>['Pengecer '.$retail->code.' masih memiliki Stok Akhir']];
                            $response['data'] = $model;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                }
                $this->validateStatusChange($model->uuid, $model->status, $iv['status']);
                unset($iv['id']);
                $model->update($iv);
                $toStatus = $iv["status"];
                if (in_array($toStatus, ['d','p','n'])) {
                    if ($retail) {
                        $childs = Retail::administratif()
                            ->where("parent_id", $retail->id);
                        if ($childs->exists()) {
                            CustomerRetailerAssg::whereIn("retail_id",$childs->pluck("id"))
                                ->update(["status"=> $toStatus]);
                        }
                    }
                }
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function assgBulk(Request $request)
    {

        $attributes = $request->all();

        \LogActivity::addToLog('create bulk retail assg');

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['retail_id']);
                // is_uuid($iv['customer_id']);
                // is_uuid($iv['sales_org_id']);
                // Cek Areaa Distributor

                /* seng saiki kan validasine berdasarkan spjb jadi code berikut perlu dicomment */

                // $ceksalesgroup = $this->cekRetailSalesGroup($iv['customer_id'], $iv['retail_id']);
                // if ($ceksalesgroup) {
                //     DB::rollback();
                //     $error = [
                //         "sales_office_id" => [trans('messages.retailer-sales-office')],
                //     ];
                //     $response = responseFail($error);
                //     return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                // }

                $where  = ['b.uuid' => $iv['retail_id'], 'c.id' => $iv['customer_id'], 'd.id' => $iv['sales_org_id']];
                $exists = CustomerRetailerAssg::getDataCheck($where)->exists();
                if ($exists) {
                    DB::rollback();

                    $response         = responseFail(trans('messages.relation-check-already'));
                    $response['data'] = CustomerRetailerAssg::getDataCheck($where)->select('b.code as retail_code', 'b.name as retail_name', 'c.id as customer_id', 'c.full_name as customer_name', 'd.id as sales_org_id', 'd.name as sales_org_name')->first();
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $retail = Retail::where('uuid', $iv['retail_id']);
                if (!$retail->exists()) {
                    DB::rollback();

                    $response              = responseFail(trans('messages.relation-check-fail'));
                    $response['retail_id'] = $iv['retail_id'];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $custReAssg = new CustomerRetailerAssg([
                    // 'customer_id' => Customer::where('id', $iv['customer_id'])->select('id')->first()['id'],
                    // 'sales_org_id' => SalesOrg::where('id', $iv['sales_org_id'])->select('id')->first()['id'],
                    'customer_id'  => $iv['customer_id'],
                    'sales_org_id' => $iv['sales_org_id'],
                    'created_by'   => Auth::user()->id,
                    'updated_by'   => Auth::user()->id,
                    'status' => 's',
                ]);
                $retail->first()->customerRetailerAssg()->save($custReAssg);
                $model[] = CustomerRetailerAssg::getDataCheck($where)->select('b.code as retail_code', 'b.name as retail_name', 'c.id as customer_id', 'c.full_name as customer_name', 'd.id as sales_org_id', 'd.name as sales_org_name')->first();
            }
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekRetailSalesGroup($customer_id, $retail_uuid)
    {
        $customer = DB::table('wcm_customer as a')
            ->leftjoin('wcm_address as b', function ($q) {
                $q->on('b.customer_id', '=', 'a.id')->where('b.address_type', 'FORMAL');
            })
            ->where('a.id', $customer_id)
            ->first();
        $retail = DB::table('wcm_retail as a')
            ->leftjoin('wcm_sales_unit as b', 'a.sales_unit_id', '=', 'b.id')
            ->leftjoin('wcm_sales_group as c', 'b.sales_group_id', '=', 'c.id')
            ->where('a.uuid', $retail_uuid)
            ->first();

        if ($customer->sales_office_id != $retail->sales_office_id) {
            return true;
        }
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request, true);
        $data = $q->select(CustomerRetailerAssg::getExportedColumns())->get();
        return Excel::download((new Download($data)), "retail-assign.xls");
    }
}
