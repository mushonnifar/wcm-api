<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Models\ContractGovItem;
use App\Models\ContractItem;
use App\Rules\SalesUnits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\SpjbOperationalController;

class CRSPJBController extends Controller
{
    //
    public function index(Request $request, $uuid = null)
    {
        $query = DB::table("wcm_cr_spjb")->where("uuid", $uuid);

        $columns = [
            'sales_org_id'      => 'sales_org_id',
            'sales_org_name'    => 'sales_org_name',
            'customer_id'       => 'customer_id',
            'customer_name'     => 'customer_name',
            'number'            => 'number',
            'contract_type'     => 'contract_type',
            'sales_office_id'   => 'sales_office_id',
            'sales_office_name' => 'sales_office_name',
            'sales_group_id'    => 'sales_group_id',
            'sales_group_name'  => 'sales_group_name',
            'product_id'        => 'product_id',
            'product_name'      => 'product_name',
            'sales_unit_id'     => 'sales_unit_id',
            'sales_unit_name'   => 'sales_unit_name',
        ];

        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("sales_org_id", $filters["sales_org_id"]);
            }
        }

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function chooseMethod($uuid, Request $request)
    {
        # code...
        \LogActivity::addToLog('Update Item CR SPJB Asal');

        $attributes = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year', 'initial_qty']);

        $this->validate($attributes, [
            'contract_id'     => 'required|exists:wcm_contract,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => ["required", new SalesUnits],
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'required',
        ]);
        unset($attributes['initial_qty']);


        $model                     = Contractitem::where($attributes)->first();
        $attributes['initial_qty'] = $request->get('initial_qty');

        DB::beginTransaction();

        // IF Item Tersedia
        if ($model) {
            if ($model->status == "n") {
                DB::rollback();
                $errors             = ['status' => "Status Item SPJB Asal Tidak Active"];
                $response           = responseFail($errors);
                $response['errors'] = 'Status Item SPJB Asal Tidak Active';
                $response['data']   = $model;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $modelAsal = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
            $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

            $model->update(['initial_qty' => $attributes['initial_qty'], 'updated_by' => Auth::user()->id]);
        } else {
            $checkitem = $this->checkOneActiveArea($attributes, 'asal');
            if ($checkitem) {
                DB::rollback();
                $response           = responseFail(['status' => ["Data Detail SPJB sudah ada yang aktif. Product : {$checkitem->product_name}, kecamatan : {$checkitem->sales_unit_name}"]]);
                $response['errors'] = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $modelAsal = $this->findDataWhere(Contract::class, ['id' => $attributes['contract_id']]);
            $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

            $findData = [
                'tb2.sales_org_id' => $modelAsal->sales_org_id,
                'product_id'       => $attributes['product_id'],
                'sales_office_id'  => $attributes['sales_office_id'],
                'sales_group_id'   => $attributes['sales_group_id'],
                'sales_unit_id'    => $attributes['sales_unit_id'],
                'month'            => $attributes['month'],
                'tb1.year'         => $attributes['year'],
                'tb2.contract_type'   => 'perbup',
                'tb1.status'          => 'y',
            ];

            // Data On Perbup
            $query = ContractGovItem::cekDataForSpjb($findData)->first();

            // Data Availabe On perbup
            if (!$query) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = 'Data tidak valid di perbup ';
                $response['data']   = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $model_item_asal = Contractitem::where(
                [
                    "contract_id" => $modelOpr->id,
                    "product_id" => $attributes['product_id'],
                    "sales_office_id" => $attributes['sales_office_id'],
                    "sales_group_id" => $attributes['sales_group_id'],
                    "sales_unit_id" => $attributes['sales_unit_id'],
                    "month" => $attributes['month'],
                    "year" => $attributes['year'],
                ]
            )->first();

            // Jika Tersedia maka Create Item SPJB Operastional
            $attributes['initial_qty'] = $request->get('initial_qty');
            $attributes['created_by'] = Auth::user()->id;
            // $attributes['contract_gov_item_id'] = $query->id;
            // Save New Item SPJB Operasional
            $spjbopr = Contractitem::create($attributes);

            // Cek Header Model Asal Jika tidak Tersedia
            if (!$model_item_asal) {
                $attributes['contract_id'] = $modelOpr->id;
                $spjbopr = Contractitem::create($attributes);
            }
        }
        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function update(Request $request, $uuid)
    {
        # code...
        \LogActivity::addToLog('Update Item CR SPJB Operational');
        is_uuid($uuid);

        $attributes = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year', 'initial_qty']);

        $this->validate($attributes, [
            'contract_id'     => 'required|exists:wcm_contract,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => ["required", new SalesUnits],
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'required',
        ]);
        unset($attributes['initial_qty']);

        $model = Contractitem::where($attributes)->first();

        $updates['initial_qty'] = $request->get('initial_qty');
        $updates['updated_by'] = Auth::user()->id;

        if ($model) {
            if ($model->status == "n") {
                DB::rollback();
                $errors             = ['status' => "Status Item SPJB Operational Tidak Active"];
                $response           = responseFail($errors);
                $response['errors'] = 'Status Item SPJB Operational Tidak Active';
                $response['data']   = $model;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
            // Save Data
            $model->update($updates);
        } else {
            // Jika ndak ada maka Cek spjb Asal
            $modelOpr = $this->findDataWhere(Contract::class, ['id' => $attributes['contract_id']]);
            $modelAsal  = Contract::where(['number' => $modelOpr->number, 'customer_id' => $modelOpr->customer_id, 'sales_org_id' => $modelOpr->sales_org_id, 'contract_type' => 'asal', 'year' => $modelOpr->year])->first();

            // Cek Header Model Asal Jika tidak Tersedia
            if (!$modelAsal) {
                DB::rollback();
                $errors             = ['status' => "SPJB ASAL Number " . $modelOpr->number . "Tidak Tersedia"];
                $response           = responseFail($errors);
                $response['errors'] = "SPJB ASAL Number " . $modelOpr->number . "Tidak Tersedia";
                $response['data']   = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $findData = [
                'tb2.sales_org_id' => $modelOpr->sales_org_id,
                'product_id'       => $attributes['product_id'],
                'sales_office_id'  => $attributes['sales_office_id'],
                'sales_group_id'   => $attributes['sales_group_id'],
                'sales_unit_id'    => $attributes['sales_unit_id'],
                'month'            => $attributes['month'],
                'tb1.year'         => $attributes['year'],
                'tb2.contract_type'   => 'perbup',
                'tb1.status'          => 'y',
            ];

            $query = ContractGovItem::cekDataForSpjb($findData)->first();

            if (!$query) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = 'Data tidak valid di perbup ';
                $response['data']   = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $model_item_asal = Contractitem::where(
                [
                    "contract_id" => $modelAsal->id,
                    "product_id" => $attributes['product_id'],
                    "sales_office_id" => $attributes['sales_office_id'],
                    "sales_group_id" => $attributes['sales_group_id'],
                    "sales_unit_id" => $attributes['sales_unit_id'],
                    "month" => $attributes['month'],
                    "year" => $attributes['year'],
                ]
            )->first();
            // Cek Header Model Asal Jika tidak Tersedia
            if (!$model_item_asal) {
                DB::rollback();
                $errors             = ['status' => "Item SPJB ASAL Number " . $modelOpr->number . " Tidak Tersedia"];
                $response           = responseFail($errors);
                $response['errors'] = "Item SPJB ASAL Number " . $modelOpr->number . " Tidak Tersedia";
                $response['data']   = $attributes;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            // Jika Tersedia maka Create Item SPJB Operastional
            $attributes['initial_qty'] = $request->get('initial_qty');
            $attributes['created_by'] = Auth::user()->id;
            // $attributes['contract_gov_item_id'] = $query->id;
            // Save New Item SPJB Operasional
            $spjbopr = Contractitem::create($attributes);
        }
        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function bulkoperasional(Request $request)
    {
        \LogActivity::addToLog('Update Status Bulk SPJB Operational');
        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {

                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataWhere(Contractitem::class, ["contract_id" => $iv["contract_id"], "product_id" => $iv["product_id"], "sales_office_id" => $iv["sales_office_id"], "sales_group_id" => $iv["sales_group_id"], "sales_unit_id" => $iv["sales_unit_id"], "month" => intval($iv["month"]), "year" => $iv["year"]]);

                if ($iv['status'] == 'y' && $model->status == 'n') {
                    $checkitem = $this->checkOneActiveArea($iv, 'operational');
                    if ($checkitem) {
                        DB::rollback();
                        $response           = responseFail("Data Detail SPJB sudah ada yang aktif. Product : {$checkitem->product_name}, kecamatan : {$checkitem->sales_unit_name}");
                        $response['errors'] = $iv;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }

                $modelOpr  = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
                $modelAsal = $this->findDataWhere(Contract::class, ['number' => $modelOpr->number, 'customer_id' => $modelOpr->customer_id, 'sales_org_id' => $modelOpr->sales_org_id, 'contract_type' => 'asal', 'year' => $modelOpr->year]);

                // Pengecualian Id Item

                // Data Untuk mencari Item yang Kembar
                $findItem["contract_id"]     = $modelAsal["id"];
                $findItem["product_id"]      = $model["product_id"];
                $findItem["sales_office_id"] = $model["sales_office_id"];
                $findItem["sales_group_id"]  = $model["sales_group_id"];
                $findItem["sales_unit_id"]   = $model["sales_unit_id"];
                $findItem["month"]           = $model["month"];
                $findItem["year"]            = $model["year"];

                // Mencari UUID Duplicate untuk update Yang data yang diduplicate
                $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);

                if ($iv['status'] == 'y' && $modelDuplicate->status == 'n') {
                    DB::rollback();
                    $response           = responseFail('SPJB ASAL Status masih Inactive');
                    $response['errors'] = 'SPJB ASAL Status masih Inactive';
                    $response['data']   = $model;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                if ($iv['status'] == 'n') {
                    $modelDuplicate->update(['status' => $iv['status']]);
                }

                $model->update(['status' => $iv['status']]);
            }

            /* Update Header ke z */
            $num         = $this->getuniqStatusForHeader(Contractitem::class, $model->contract_id);
            $modelheader = Contract::where('id', $model->contract_id)->first();
            if ($num == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            }
            if ($iv['status'] == 'n') {
                $num             = $this->getuniqStatusForHeader(Contractitem::class, $modelDuplicate->contract_id);
                $modelheaderAsal = Contract::where('id', $modelDuplicate->contract_id)->first();
                if ($num == 2) {
                    $updateHeader['status']     = 'z';
                    $updateHeader['updated_by'] = Auth::user()->id;
                    $modelheaderAsal->update($updateHeader);
                } else {
                    $updateHeader['status']     = $model->status;
                    $updateHeader['updated_by'] = Auth::user()->id;
                    $modelheaderAsal->update($updateHeader);
                }
            }
            /*End*/
            /*Cek Partisial Active Status*/

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function bulkasal(Request $request)
    {
        \LogActivity::addToLog('Update Bulk Status SPJB ASAL');
        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                // is_uuid($iv['id']);


                $iv['updated_by'] = Auth::user()->id;
                $model = $this->findDataWhere(Contractitem::class, ["contract_id" => $iv["contract_id"], "product_id" => $iv["product_id"], "sales_office_id" => $iv["sales_office_id"], "sales_group_id" => $iv["sales_group_id"], "sales_unit_id" => $iv["sales_unit_id"], "month" => intval($iv["month"]), "year" => $iv["year"]]);

                if ($iv['status'] == 'y' && $model->status == 'n') {
                    $checkitem = $this->checkOneActiveArea($iv, 'asal');
                    if ($checkitem) {
                        DB::rollback();
                        $response           = responseFail("Data Detail SPJB sudah ada yang aktif. Product : {$checkitem->product_name}, kecamatan : {$checkitem->sales_unit_name}");
                        $response['errors'] = $iv;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }

                $modelAsal = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
                $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

                // Data Untuk mencari Item yang Kembar
                $findItem["contract_id"]     = $modelOpr->id;
                $findItem["product_id"]      = $model["product_id"];
                $findItem["sales_office_id"] = $model["sales_office_id"];
                $findItem["sales_group_id"]  = $model["sales_group_id"];
                $findItem["sales_unit_id"]   = $model["sales_unit_id"];
                $findItem["month"]           = $model["month"];
                $findItem["year"]            = $model["year"];

                $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);

                $model->update(['status' => $iv['status']]);
                $modelDuplicate->update(['status' => $iv['status']]);
            }

            /* Update Header ke z */
            $num         = $this->getuniqStatusForHeader(Contractitem::class, $model->contract_id);
            $modelheader = Contract::where('id', $model->contract_id)->first();
            if ($num == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            }

            $numOpr         = $this->getuniqStatusForHeader(Contractitem::class, $modelDuplicate->contract_id);
            $modelheaderOpr = Contract::where('id', $modelDuplicate->contract_id)->first();
            if ($numOpr == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheaderOpr->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheaderOpr->update($updateHeader);
            }
            /*End*/
            /*Cek Partisial Active Status*/

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function bulkqtyasal(Request $request)
    {
        \LogActivity::addToLog('Update Item QTY Bulk Asal');

        $attributes = $request->all();

        DB::beginTransaction();

        $model = [];
        foreach ($attributes as $iv) {

            $this->validate($iv, [
                'contract_id'     => 'required|exists:wcm_contract,id',
                'product_id'      => 'required|exists:wcm_product,id',
                'sales_office_id' => 'required|exists:wcm_sales_office,id',
                'sales_group_id'  => 'required|exists:wcm_sales_group,id',
                'sales_unit_id'   => ["required", new SalesUnits],
                'month'           => 'required',
                'year'            => 'required',
                'initial_qty'     => 'required',
            ]);

            // find Exist
            $itemasal = Contractitem::where(["contract_id" => $iv["contract_id"], "product_id" => $iv["product_id"], "sales_office_id" => $iv["sales_office_id"], "sales_group_id" => $iv["sales_group_id"], "sales_unit_id" => $iv["sales_unit_id"], "month" => intval($iv["month"]), "year" => $iv["year"]])->first();

            if ($itemasal) {
                $itemasal->update(['initial_qty' => $iv['initial_qty'], 'updated_by' => Auth()->user()->id]);
            } else {

                $checkitem = $this->checkOneActiveArea($iv, 'asal');
                if ($checkitem) {
                    DB::rollback();
                    $response           = responseFail(['status' => ["Data Detail SPJB sudah ada yang aktif. Product : {$checkitem->product_name}, kecamatan : {$checkitem->sales_unit_name}"]]);
                    $response['errors'] = $iv;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $modelAsal = Contract::where('id', $iv["contract_id"])->firstOrFail();
                $modelOpr  = $this->findDataWhere(Contract::class, ['number' => $modelAsal->number, 'customer_id' => $modelAsal->customer_id, 'sales_org_id' => $modelAsal->sales_org_id, 'contract_type' => 'operational', 'year' => $modelAsal->year]);

                $item = Contractitem::create($iv);

                $itemopr = Contractitem::where(["contract_id" => $modelOpr->id, "product_id" => $iv["product_id"], "sales_office_id" => $iv["sales_office_id"], "sales_group_id" => $iv["sales_group_id"], "sales_unit_id" => $iv["sales_unit_id"], "month" => intval($iv["month"]), "year" => $iv["year"]])->first();

                if(!$itemopr){
                    $iv['contract_id'] = $modelOpr->id;
                    $iv['created_by'] = Auth()->user()->id;
                    $item = Contractitem::create($iv);
                }
            }

            array_push($model, $iv);

            $iv = [];
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function bulkqtyopr(Request $request)
    {
        \LogActivity::addToLog('Update Item QTY Bulk Asal');

        $attributes = $request->all();

        DB::beginTransaction();

        $model = [];
        foreach ($attributes as $iv) {

            $this->validate($iv, [
                'contract_id'     => 'required|exists:wcm_contract,id',
                'product_id'      => 'required|exists:wcm_product,id',
                'sales_office_id' => 'required|exists:wcm_sales_office,id',
                'sales_group_id'  => 'required|exists:wcm_sales_group,id',
                'sales_unit_id'   => ["required", new SalesUnits],
                'month'           => 'required',
                'year'            => 'required',
                'initial_qty'     => 'required',
            ]);

            // find Exist
            $item = Contractitem::where(["contract_id" => $iv["contract_id"], "product_id" => $iv["product_id"], "sales_office_id" => $iv["sales_office_id"], "sales_group_id" => $iv["sales_group_id"], "sales_unit_id" => $iv["sales_unit_id"], "month" => intval($iv["month"]), "year" => $iv["year"]])->first();

            if ($item) {
                $item->update(['initial_qty' => $iv['initial_qty'], 'updated_by' => Auth()->user()->id]);
            } else {
                $iv['created_by'] = Auth()->user()->id;
                $item = Contractitem::create($iv);
            }

            array_push($model, $iv);

            $iv = [];
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function spjbOprYear($data)
    {
        $year = $data['month'];
        unset($data['month']);

        $sum = DB::table('wcm_contract_item')
            ->whereIn('month', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            ->where($data)
            ->select(DB::raw('SUM(initial_qty) as qty'))
            ->get();
        return (is_null($sum[0]->qty)) ? 0 : $sum[0]->qty;
    }

    private function cekDuplikasiItemUpdate($model, $attrib, $uuid)
    {

        $query = $model::where($attrib)
            ->where('uuid', '!=', $uuid);

        $check = $query->first();

        return $check;
    }

    private function getDuplicateHeader($where)
    {
        # code...
        $query = Contract::where($where);
        $check = $query->first();

        $find["number"]        = $check["number"];
        $find["customer_id"]   = $check["customer_id"];
        $find["sales_org_id"]  = $check["sales_org_id"];
        $find["contract_type"] = 'operational';
        $find["year"]          = $check["year"];
        $query                 = Contract::where($find);
        $check                 = $query->first();
        return $check['id'];
    }

    private function getHeaderDetail($where)
    {
        # code...
        $query = Contract::where($where);
        $check = $query->first();
        return $check;
    }

    private function cekDuplikasiItem($model, $attrib)
    {

        $query = $model::where($attrib);
        $check = $query->first();
        return $check;
    }

    private function getuniqStatusForHeader($model, $idheader)
    {
        $allStatus = $model::select('status')
            ->distinct()
            ->where('contract_id', $idheader)
            ->get()
            ->toArray();
        return count($allStatus);
    }

    private function checkOneActiveArea($where, $type)
    {

        $dataitem = Contractitem::getItemSPJB([
            'tb1.month' => $where['month'], 'tb1.product_id' => $where['product_id'],
            'tb1.sales_group_id' => $where['sales_group_id'],
            'tb1.sales_office_id' => $where['sales_office_id'],
            'tb1.sales_unit_id' => strval($where['sales_unit_id']),
            'tb1.year' => $where['year'],
            'tb1.status' => 'y', 'tb8.contract_type' => $type
        ])->first();
        // unset($where['contract_id']);
        // $dataitem = Contractitem::where($where)->first();
        return $dataitem;
    }
}
