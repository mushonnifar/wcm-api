<?php

namespace App\Http\Controllers;

use App\Models\Plant;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class PlantController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all plant');

        $query = Plant::getAll();

        if ($request['form'] == 'subtitution') {
            $query->select('tb1.id','tb1.code','tb1.name')
                  ->distinct('tb1.id');
        }

        $columns = [
            'tb1.code'=> 'code',
            'tb1.name'=> 'name',
            'tb1.sales_group_id'=> 'sales_group_id',
            'tb1.address'=> 'address',
            'tb1.status'=> 'status',
            'tb2.id '=> 'plant_assg_id',
            'tb2.uuid '=> 'plant_assg_uuid',
            'tb2.sales_org_id'=> 'sales_org_id',
            'tb2.distrib_channel_id'=> 'distrib_channel_id' 
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function allDistinct(Request $request) {
        \LogActivity::addToLog('get distinct plant');

        $query = Plant::getAllDistinct();
        $columns = [
            'tb1.code'=> 'code',
            'tb1.name'=> 'name',
            'tb1.sales_group_id'=> 'sales_group_id',
            'tb1.address'=> 'address',
            'tb1.status'=> 'status',
            'tb2.sales_org_id'=> 'sales_org_id',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
    
    public function supply(Request $request) {
        \LogActivity::addToLog('get plant & supply point');

        $where['status1'] = 'y';
        $where['status2'] = 'y';
        $query = Plant::getPlantSupplyPoint($where);
        $columns = [
            'code'=> 'code',
            'name'=> 'name',
            'sales_group_id'=> 'sales_group_id',
            'id '=> 'plant_assg_id',
            'uuid '=> 'plant_assg_uuid',
            'sales_org_id'=> 'sales_org_id',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
