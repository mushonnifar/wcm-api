<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use Illuminate\Http\Request;
use App\Models\Farmer;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Imports\FarmerGroupImport;
use App\Models\SubSector;
use App\Models\Commodity;
use Illuminate\Support\Facades\Validator;
use App\Models\SalesUnit;
use App\Models\FarmerGroup;
use App\Models\CustomerRetailerAssg;
use App\Models\Address;
use App\Models\SalesGroup;
use App\Rules\FarmerKtpRule;
use App\Rules\OwnedSalesOrg;
use App\Traits\ValidationTemplateImport;
use Maatwebsite\Excel\Facades\Excel;

class FarmerController extends Controller {
    
    use ValidationTemplateImport;

    public function index(Request $request, $cust_uuid = null, $exported=false) {
        \LogActivity::addToLog('get all farmer');
        !is_null($cust_uuid) ? is_uuid($cust_uuid) : '';

        $where = array();
        if ($cust_uuid) {
            $where['tb9.uuid'] = $cust_uuid;
        } 
        if ($this->isDistributor) {
            $where['tb2.customer_id'] = $this->customerId;
        }
        $query = Farmer::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb2.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb4.sales_group_id", $filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb1.name' => 'name',
            'tb1.code' => 'code',
            'tb1.farmer_group_id' => 'farmer_group_id',
            'tb1.land_area' => 'land_area',
            'tb1.address' => 'address',
            'tb1.sales_unit_id' => 'sales_unit_id',
            'tb1.telp_no' => 'telp_no',
            'tb1.hp_no' => 'hp_no',
            'tb1.ktp_no' => 'ktp_no',
            'tb1.ktp_valid_date' => 'ktp_valid_date',
            'tb1.village' => 'village',
            'tb1.latitude' => 'latitude',
            'tb1.longitude' => 'longitude',
            'tb2.name' => 'farmer_group_name',
            'tb3.name' => 'retail_name',
            'tb4.name' => 'sales_unit_name',
            'tb6.name' => 'sales_org_name',
            'tb7.name' => 'sales_group_name',
            'tb7.id' => 'sales_group_id',
            'tb5.customer_id' => 'customer_id',
            'tb9.full_name' => 'customer_name',
            'tb9.uuid' => 'customer_uuid',
            'tb8.name' => 'sales_office_name',
            'tb8.id' => 'sales_office_id',
            'tb1.status' => 'status',
            'tb1.created_by' => 'created_by',
            'tb1.updated_by' => 'updated_by',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if ($exported) return $model->getFilteredQuery();
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request) {
        \LogActivity::addToLog('create farmer');

        $attributes = $request->all();

        $this->validate($attributes, Farmer::ruleCreate());
        $this->cekSalesOfficeStore($attributes['sales_unit_id'], $attributes['farmer_group_id']);

        $attributes['code'] = $this->generateCode(Farmer::class, 'code', 'FR', 10);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {
            $model = Farmer::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid) {
        \LogActivity::addToLog('get farmer by id');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model = Farmer::getAll($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id) {
        \LogActivity::addToLog('update farmer');

        $attributes = $request->except(['uuid', 'code']);
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, Farmer::ruleUpdate2());
        $this->cekSalesOfficeStore($attributes['sales_unit_id'], $attributes['farmer_group_id']);

        $model = $this->findDataUuid(Farmer::class, $id);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request) {
        \LogActivity::addToLog('update bulk farmer');

        $attributes = $request->all();

        DB::beginTransaction();

        $model = [];
        foreach ($attributes as $iv) {
            is_uuid($iv['uuid']);

            $model = $this->findDataUuid(Farmer::class, $iv['uuid']);
            $this->validateStatusChange($model->uuid, $model->status, $iv['status']);
            unset($iv['id']);
            $iv['updated_by'] = Auth::user()->id;

            $model->update($iv);
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id) {
        \LogActivity::addToLog('delete farmer');

        $model = $this->findDataUuid(Farmer::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'n'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules) {
        $messages = [
            'required' => trans('messages.required'),
            'unique' => trans('messages.unique'),
            'email' => trans('messages.email'),
            'numeric' => trans('messages.numeric'),
            'exists' => trans('messages.exists'),
            'max' => trans('messages.max'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => $newMessages
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getImport(Request $request) {
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new FarmerGroupImport, $file)[0];
            if(!$this->compareTemplate("farmer", array_keys($data[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }

            $rules = [
                'ktp_no' => new FarmerKtpRule,
                'farmer_group_code' => 'exists:wcm_farmer_groups,code',
                'land_area' => 'numeric|max:2',
                'sales_org_id' => 'required|exists:wcm_sales_org,id',
            ];

            $newData = [];
            foreach ($data as $i => $iv) {
                if (is_null($iv['kode_kecamatan'])) {
                    continue;
                }

                $forValidate['ktp_no'] = $iv['ktp'];
                $forValidate['farmer_group_code'] = $iv['kode_kelompok_tani'];
                $forValidate['land_area'] = $iv['luas_lahan'];
                $forValidate['sales_org_id'] = $iv['sales_org'];

                $error = $this->validateImport($forValidate, $rules);

                $farmergroup = FarmerGroup::select('id as farmer_group_id', 'name as farmer_group_name')->where('code', $iv['kode_kelompok_tani'])->first();


                $this->cekSalesOfficeStore(@$iv['kode_kecamatan'], @$farmergroup['farmer_group_id']);

                $newData[$i]['farmer_group_id'] = $farmergroup['farmer_group_id'];
                $newData[$i]['farmer_group_name'] = $farmergroup['farmer_group_name'];
                $newData[$i]['farmer_group_code'] = $iv['kode_kelompok_tani'];
                $newData[$i]['name'] = $iv['nama_petani'];
                $newData[$i]['address'] = $iv['alamat_petani'];
                $newData[$i]['sales_unit_id'] = $iv['kode_kecamatan'];
                $newData[$i]['sales_unit_name'] = SalesUnit::select('name')->where('id', $iv['kode_kecamatan'])->first()['name'];
                $newData[$i]['village'] = $iv['desa'];
                $newData[$i]['sales_org_id'] = $iv['sales_org'];
                $newData[$i]['telp_no'] = $iv['no_telepon'];
                $newData[$i]['hp_no'] = $iv['no_handphone'];
                $newData[$i]['land_area'] = $iv['luas_lahan'];
                $newData[$i]['ktp_no'] = $iv['ktp'];
                $subsector = $this->getValue($iv, 'sub_sektor', SubSector::class);
                $newData[$i]['sub_sector'] = $subsector;
                $commodity = $this->getValue($iv, 'komoditas', Commodity::class);
                $newData[$i]['commodity'] = $commodity;

                $newData[$i]['validate'] = $error;
            }

            $response = responseSuccess(trans('messages.import-success'), $newData);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        $response = responseFail(trans('messages.import-fail'));
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    }

    private function getValue($array, $string, $model) {
        $param = [];
        foreach ($array as $key => $val) {
            if (strpos($key, $string) !== false && strtolower($val) == "ya") {
                $kata = str_replace($string . '_', '', $key);
                $nilai = str_replace('_', ' ', $kata);
                array_push($param, $nilai);
            }
        }
        $result = $model::select('id', 'name')->whereIn('name', $param)->get();
        return $result;
    }

    public function saveImport(Request $request) {
        DB::beginTransaction();
        $flag = true;
        $attributes = $request->all();

        $newData = [];
        $valMax = $this->getMaxCode(Farmer::class);
        $attribSave = [];
        foreach ($attributes as $i => $iv) {
            $error = $this->validateImport($iv, Farmer::ruleCreateBulk());
            // var_dump($iv['farmer_group_code']);die();
            $farmerGroup = FarmerGroup::where('code', $iv['farmer_group_code'])->first();
            $cekSalesOffice = $this->cekSalesOfficeUpload($iv['sales_unit_id'], $farmerGroup->id);

            $attribSave[$i]['farmer_group_id'] = @$farmerGroup->id;
            $attribSave[$i]['name'] = $this->cekKeyExist('name', $iv);
            $attribSave[$i]['address'] = $this->cekKeyExist('address', $iv);
            $attribSave[$i]['sales_unit_id'] = $this->cekKeyExist('sales_unit_id', $iv);
            $attribSave[$i]['telp_no'] = $this->cekKeyExist('telp_no', $iv);
            $attribSave[$i]['hp_no'] = $this->cekKeyExist('hp_no', $iv);
            $attribSave[$i]['village'] = $this->cekKeyExist('village', $iv);
            $attribSave[$i]['sub_sector_id'] = $this->cekKeyExist('sub_sector_id', $iv);
            $attribSave[$i]['commodity_id'] = $this->cekKeyExist('commodity_id', $iv);
            $attribSave[$i]['land_area'] = $this->cekKeyExist('land_area', $iv);
            $attribSave[$i]['ktp_no'] = $this->cekKeyExist('ktp_no', $iv);
            $attribSave[$i]['ktp_valid_date'] = $this->cekKeyExist('ktp_valid_date', $iv);
            $attribSave[$i]['latitude'] = $this->cekKeyExist('latitude', $iv);
            $attribSave[$i]['longitude'] = $this->cekKeyExist('longitude', $iv);
            $attribSave[$i]['status'] = $this->cekKeyExist('status', $iv);
            if ($error['status'] == 0) {
                $flag = false;
                $attribSave[$i]['validate'] = $error;
            } else if ($cekSalesOffice['status'] == 0) {
                $flag = false;
                $attribSave[$i]['validate'] = $cekSalesOffice;
            } else {
                $attribSave[$i]['created_by'] = Auth::user()->id;
                $attribSave[$i]['updated_by'] = Auth::user()->id;
                $attribSave[$i]['created_at'] = now();
                $attribSave[$i]['updated_at'] = now();
                $attribSave[$i]['code'] = $this->generateCodeBulk($valMax, 'FR', 10);
                $newData[] = $attribSave[$i];
                $valMax++;
            }
        }
        if ($flag) {
            Farmer::insert($attribSave);
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $newData);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['data'] = $attribSave;
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekKeyExist($key, $array) {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        } else {
            return null;
        }
    }

    public function getDistribSalesOffice($sales_unit_id, $farmer_group_id){
        $farmerOffice = ($sales_unit = SalesUnit::find("$sales_unit_id") ) ? $sales_unit->salesGroup()->first()->sales_office_id : null;
        $retail_id = ( $farmergroup = @FarmerGroup::find("$farmer_group_id") ) ? $farmergroup->retail_id : null;
        $customers = CustomerRetailerAssg::select('customer_id')->where('retail_id', "$retail_id")->get()->toArray();

        $address = Address::whereIn('customer_id', array_flatten($customers))->where('address_type', 'FORMAL')->pluck('sales_office_id')->toArray();

        if (is_null($farmerOffice) || is_null($retail_id) || is_null($address)) {
            return false;
        }

        //$salesOfficeInAddr = sprintf('%04s', ($address *1));
        $salesOfficeUpload = sprintf('%04s', substr($sales_unit_id, 0, 2));


        // $salesGroup = SalesUnit::select('sales_group_id')->whereIn("id", array_flatten($address->toArray()))->get()->toArray();

        // $result = SalesGroup::whereIn("id", array_flatten($salesGroup))->where('sales_office_id', "$farmerOffice")->get()->count();
        // if($result > 0){
        //     return true;
        // }
        // return false;

        return in_array($salesOfficeUpload, $address);
    }

    private function cekSalesOfficeStore($sales_unit_id, $farmer_group_id) {
        $cek = $this->getDistribSalesOffice($sales_unit_id, $farmer_group_id);
        if (!$cek) {
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => ['sales_unit_id' => [trans('messages.farmer-sales-office')]],
            ];
            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    private function cekSalesOfficeUpload($sales_unit_id, $farmer_group_id) {
        $cek = $this->getDistribSalesOffice($sales_unit_id, $farmer_group_id);
        if (!$cek) {
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => ['sales_office_id' => [trans('messages.farmer-sales-office')]],
            ];
            return $response;
        }
        return responseSuccess(trans('messages.validate-check-success'), true);
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,null, true);
        $data = $q->select(Farmer::getExportedColumns())->get();
        return Excel::download((new Download($data)), "farmer.xls");
    }
}
