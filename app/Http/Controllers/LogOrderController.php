<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Jobs\DisbursementJob;
use App\Libraries\PaymentGateway;
use App\Models\LogOrderItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\SalesGroup;
use App\Http\Controllers\ManualApprovalController;
use App\Http\Controllers\SAP\SalesOrderController;
use Illuminate\Support\Facades\Auth;
use App\Models\LogOrder;
use App\Helpers\SAPConnect;
use App\Http\Requests\LogOrderRequest;
use App\Jobs\CreateSalesOrder;
use App\Models\Bank;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class LogOrderController extends Controller {

    public function index(Request $request, $exported=false) {
        \LogActivity::addToLog('get all log order');

        $query = LogOrder::getAll();
        $users = $request->user();
        $filters = $users->filterRegional;
        if (@$filters["sales_org_id"]) {
            $query->whereIn("tb2.sales_org_id", $filters["sales_org_id"]);
        }

        if (@$filters["sales_group_id"]) {
            $query->whereIn("tb2.sales_group_id", $filters["sales_group_id"]);
        }

        if ($this->isDistributor) {
            $query->where("tb2.customer_id", $this->customerId);
        }

        $columns = [
            'tb1.id' => 'id',
            'tb1.uuid' => 'uuid',
            'tb1.order_id' => 'order_id',
            'tb1.message' => 'message',
            'tb1.status' => 'status',
            'tb2.number' => 'order_number',
            'tb2.customer_id' => 'customer_id',
            'tb3.full_name' => 'customer_name',
            'tb1.created_by' => 'created_by',
            'tb1.updated_by' => 'updated_by',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
            'tb1.kode' => 'kode',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });

        if ($exported) return $model->getFilteredQuery();

        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store($uuid) {
        \LogActivity::addToLog('Create Log Order');
        is_uuid($uuid);
        $model = $this->findDataUuid(LogOrder::class, $uuid);
        $order = Order::find($model->order_id);
        $hasil = false;

        if ($model->kode == 1 || $model->kode == 2 || $model->kode == 5) {
            if ($order->status === "r") {
                $hasil = $this->createBooking($order);
            }
        } else {
            $lastCode = $model->kode;
            $hasil = $this->createSO($lastCode, $order);
        }

        if ($hasil) {
            $response = responseSuccess(trans('messages.resend-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } else {
            $response = responseFail(trans('messages.resend-fail'));
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    private function createBooking($data) {
        $manual = new ManualApprovalController();
        $cekStock = $manual->cekStok($data->id);
        $cekTaxClass = $manual->cekTaxClass($data->customer_id, $data->sales_org_id);
        DB::beginTransaction();
        try {
            if ($data->delivery_method_id == 2) {
                $route = $this->cekRoute($data->id, $data->sales_group_id);
                if (!$route) {
                    return false;
                }
            }
            $paymentDueDate = (new OrderController())->getPaymentDueDate(['customer_id' => $data->customer_id, 'sales_org_id' => $data->sales_org_id]);
            if ($cekTaxClass && $cekStock) {
                //$cekSsp = $manual->cekSspDate($data->id, $data->customer_id, $data->sales_org_id);
                $cekSsp = true;
                if ($cekSsp) {
                    $kode_booking = $manual->generateKodeBooking($data->id);
                    $data->update([
                        'status'           => 'y',
                        'booking_code'     => $kode_booking,
                        'updated_by'       => Auth::user()->id,
                        "payment_due_date" => $paymentDueDate,
                    ]);
                    $this->deleteLog($data->id, 1);
                    $this->deleteLog($data->id, 2);
                    $this->deleteLog($data->id, 5);
                    DB::commit();
                    return true;
                }
                return false;
            } else if ($cekStock) {
                $kode_booking = $manual->generateKodeBooking($data->id);
                $data->update([
                    'status'           => 'y',
                    'booking_code'     => $kode_booking,
                    'updated_by'       => Auth::user()->id,
                    "payment_due_date" => $paymentDueDate,
                ]);
                $this->deleteLog($data->id, 1);
                $this->deleteLog($data->id, 5);
                DB::commit();
                return true;
            }
            return false;
        } catch (\Exception $ex) {
            DB::rollback();
            return false;
        }
    }

    private function createSO($lastCode, $order) {

        try {
            $so = new SalesOrderController();
            if (!$order) throw new \Exception();
            $orderID = $order->id ?: 0;

            switch($lastCode) {
                case "3":
                    $so->store($order->booking_code);
                    $this->deleteLog($orderID, 3);
                break;
                case "4":
                    $so->createDP($order, $order->bank_payment);
                    $this->deleteLog($orderID, 4);
                break;
                case "6":
                    $bank = Bank::find($order->bank_payment);
                    $bankName = Str::after($bank->name, "Bank ");
                    $bankName = Str::lower($bankName) === "bsb" ? "SUMSEL" : $bankName;
                    $this->deleteLog($orderID, 6);
                    $so->clearence($orderID, $order->sap_billing_dp_doc, $bankName, $order->sales_org_id);
                break;
                case "7":
                    $bank = Bank::find($order->bank_id);
                    $bankName = Str::after($bank->name, "Bank ");
                    $bankName = Str::lower($bankName) === "bsb" ? "SUMSEL" : $bankName;
                    $transaction_date = $order->billing_fulldate instanceof Carbon ? $order->billing_fulldate : parseDate($order->billing_fulldate);
                    $disbursementParams = [
                        'I_BANK'  => $bankName,
                        'I_VBELN' => $order->so_number,
                        'I_BUDAT' => $transaction_date->format("Ymd"),
                        'RETURN'  => [],
                    ];
                    DisbursementJob::dispatch($disbursementParams);
                    $this->deleteLog($orderID, 7);
                break;
            }
        }catch(\Exception $e) {
            return false;
        }


        return true;
    }

    private function deleteLog($order_id, $kode) {

        $logOrder = LogOrder::where(['order_id' => $order_id]);
        LogOrderItem::whereIn("log_order_id", $logOrder->pluck("id"))->delete();
        $logOrder->delete();
    }

    private function cekRoute($order_id, $sales_group_id) {
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order_id])->get();
        $sales_group = SalesGroup::find($sales_group_id);
        $approve = true;

        foreach ($orderItem as $value) {
            $route = $this->getRoute($value->plant_code, $sales_group->district_code,$value->retail_sales_unit_id);

            if ($route == 'E') {
                $approve = false;
            }
        }
        return $approve;
    }

    private function getRoute($plant_code, $sales_group_id, $sales_unit_id) {
        $soap = SAPConnect::connect('cek_route.xml');

        $param = [
            'R_ROUTE' => [
                'item' => [
                    'IDPLANT' => $plant_code,
                    'SGROUP' => $sales_group_id,
                    'SUNIT' => $sales_unit_id,
                ]
            ]
        ];
        $result = $soap->SI_Check_Route($param);
        if (key_exists('item', $result->T_DATA)) {
            return $result->T_DATA->item->TYPE;
        }
        return 'E';
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length"=> -1]);
        $model = $this->index($request, true);
        $data = $model->select(LogOrder::getExportedColumns());
        return Excel::download(new Download($data->get()), "Log-order.xls");
    }

    public function item(Request $request, $logOrderId)
    {
        $query = LogOrderItem::where("log_order_id", $logOrderId);
        $columns = [
            "error_message" => "error_message",
            "error_type" => "error_type",
            "error_id" => "error_id"
        ];

        $model = Datatables::of($query)
            ->filter(function($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200);
    }

    public function edit(Logorder $log){
        //dd($log->order);

        $customer_data = Customer::where('id',$log->order->customer_id)->first(['uuid','full_name','id']);
        $bank = Bank::where('id',$log->order->bank_payment)->first();
        
        if ($log->kode == 3){
            $kode_error_name = "Gagal Create SO";
        } else if ($log->kode == 4){
            $kode_error_name = "Gagal Billing";
        } else if ($log->kode == 6){
            $kode_error_name = "Gagal Clearing";
        }

        $array = array (
            'uuid' => $log->uuid,
            'no_order' => $log->order->number,
            'customer_id' => $log->order->customer_id,
            'customer_data' => $customer_data,
            'status' => $log->status,
            'error_message' => $log->message,
            'bank_payment_name' => $bank == null ? null : $bank->name,
            'date' => $log->created_at,
            'no_so' => $log->order->so_number,
            'no_dp_req'=> $log->order->sap_booking_code,
            'no_billing_dp' => $log->order->sap_billing_dp_doc,
            'no_clearing' => $log->order->sap_clearing_doc,
            'bank_payment' => $log->order->bank_payment,
            'kode_error' => $log->kode,
            'kode_error_name' => $kode_error_name
        );

        $response = responseSuccess(trans('message.read-success'),$array);
        return response()->json($response,200);

    }

    public function update(Logorder $log,LogOrderRequest $request){
        if ($request->kode_error == 3){
            if ($request->bank_payment == null){
                $response = responseFail("Pastikan bank payment tidak kosong");
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else if ($request->kode_error == 4){
            if ($request->bank_payment == null || $request->no_so == null){
                $response = responseFail("Pastikan bank payment atau nomer SO tidak kosong");
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else if ($request->kode_error == 6){
            if ($request->bank_payment == null || $request->no_so == null || $request->no_dp_req == null || $request->no_billing_dp == null){
                $response = responseFail("Pastikan Bank Payment atau No SO atau No Dp Req atau No Billing DP tidak kosong");
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        }

        $log->order->so_number = $request->no_so == null ? null : $request->no_so;

        $log->order->sap_booking_code = $request->no_dp_req == null ? null : $request->no_dp_req;

        $log->order->sap_billing_dp_doc = $request->no_billing_dp == null ? null : $request->no_billing_dp;

        $log->order->sap_clearing_doc = $request->no_clearing == null ? null : $request->no_clearing;
        
        $log->order->bank_payment = $request->bank_payment == null ? null : $request->bank_payment;
        
        $log->kode = $request->kode_error;

        $log->save();

        $log->push();



        $response = responseSuccess(trans('message.read-success'),$log);
        return response()->json($response,200);

    }

    public function manual(Request $request)
    {
        $rules = [
            "number" => [
                "required",
                Rule::exists("wcm_orders")
                    ->whereIn("status", ["l","u", "k","c"]),
            ],
            "customer_id" => [
                "required",
                Rule::exists("wcm_orders")
                    ->where("number", $request->get("number")),
            ],
            "bank_payment" => "required",
            "so_number" => "required_if:error_code,4,6,7",
            "sap_billing_dp_doc" => "required_if:error_code,6,7",
            "sap_clearing_doc" => "required_if:error_code,7",
            "error_code" => "required|in:3,4,6,7",
        ];
        $this->validate($request->all(), $rules);
        $order = Order::where("number", $request->get("number"))->firstOrFail();

        DB::beginTransaction();
        try {
            foreach ($request->only(array_keys($rules)) as $key => $value) {
                if ($key === "error_code") continue;

                if ($key === "bank_payment" && $order->payment_method === PaymentGateway::CASH) {
                    $order->bank_id = $value;
                }

                $order->{$key} = $value;
            }

            $order->save();

            $user = Auth::user()->uuid ?? null;
            LogOrder::create([
                "order_id" => $order->id,
                "message" => "Manual Log Order",
                "created_by" => $user,
                "updated_by" => $user,
                "kode" => $request->get("error_code"),
                "status" => "y"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(responseFail($e->getMessage()), 500);
        }
        DB::commit();

        return response()->json(responseSuccess(__("messages.create-success"), $request->all()));

    }
}
