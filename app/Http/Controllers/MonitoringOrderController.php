<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Libraries\PaymentGateway;
use App\Models\LogOrder;
use App\Models\MonitoringOrder;
use App\Models\Order;
use App\Models\OrderDFStatus;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use PDF;
use Yajra\DataTables\DataTables;

class MonitoringOrderController extends Controller
{

    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get all monitoring order');
        $where = [];
        if ($this->isAdminAnper) {
            $where["sales_org_id"] = $this->salesOrgId;
        }

        $header = MonitoringOrder::getAll($where,$exported);

        $user = $request->user();
        $filters = $user->filterRegional;

        // return response()->json($user->customer_id);
        if($user->customer_id != ""){
             $header->where("B.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $header->whereIn("B.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $header->whereIn("B.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }
        if($user->customer_id != ""){
             $header->where("B.customer_id", $user->customer_id);
        }


//        return $header->toSql();
        $columns = [
            'number'                   => 'number',
            'reference_code'           => 'reference_code',
            'customer_name'            => 'customer_name',
            'sales_org_name'           => 'sales_org_name',
            'booking_code'             => 'booking_code',
            'so_number'                => 'so_number',
            'payment_due_dates'        => 'payment_due_date',
            'billing_dates'            => 'billing_date',
            'df_due_date_plans'        => 'due_date_plan',
            'pickup_due_dates'         => 'pickup_date',
            'order_dates'              => 'order_date',
            'status'                   => 'status',
            'status_name'              => 'status_name',
            'delivery_qty'             => 'delivery_qty',
            'sisa_tebus'               => 'sisa_tebus',
            'incoterm'                 => 'incoterm',
            'ppn'                      => 'ppn',
            'sales_office_name'        => 'sales_office_name',
            'sales_office_id'          => 'sales_office_id',
            'sales_group_name'         => 'sales_group_name',
            'sales_group_id'           => 'sales_group_id',
            'payment_method'           => 'payment_method',
            'total_price'              => 'total_price',
            'total_price_before_ppn'   => 'total_price_before_ppn',
            'upfront_payment'          => 'upfront_payment',
            'date_good_redemption_due' => 'good_redemption_due_date',
            'created_by'               => 'created_by',
            'created'               => 'created_at',
            'updated'               => 'updated_at',
        ];
        $model = DataTables::of($header)
            ->filter(function ($header) use ($request, $columns) {
                $this->filterColumn($columns, $request, $header);
            })
            ->order(function ($header) use ($request, $columns) {
                $this->orderColumn($columns, $request, $header);
            });

        if ($exported) return $model->getFilteredQuery()->get();

        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'order_date' || $value === 'payment_due_date' || $value === 'pickup_date' || $value === 'due_date_plan') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function orderColumn($columns, $request, $query)
    {
        $order     = $request->get('order');
        $kolom     = $request->get('columns');
        $field     = $kolom[$order[0]['column']]['name'];
        $direction = $order[0]['dir'];
        foreach ($columns as $key => $value) {
            if ($field == $value) {
                $query->orderBy($key, $direction);
            }
        }
    }

    public function booking($uuid = null)
    {
        \LogActivity::addToLog('download booking monitoring order');
        is_uuid($uuid);
        setlocale(LC_ALL, 'IND');
        $where['tb1.uuid'] = $uuid;
        $data              = MonitoringOrder::getBooking($where)->get();
        if (!$data->isEmpty()) {
            $data[0]->terbilang              = $this->terbilang($data[0]->uang_muka);
            $data[0]->order_date             = date_format(date_create($data[0]->order_date), 'd F Y');
            $data[0]->batas_akhir_pembayaran = date_format(date_create($data[0]->batas_akhir_pembayaran), 'd F Y H:i:s');
            $data[0]->total_price_rupiah     = $this->rupiah($data[0]->total_price);
            $data[0]->uang_muka_rupiah       = $this->rupiah($data[0]->uang_muka);
            $wew['data']                     = json_decode($data);
            $pdf                             = PDF::loadView('monitoring_order.booking', $wew);
            $nama_file                       = 'BOOKING ' . $data[0]->no_order . '.pdf';
            return $pdf->setPaper('a4', 'potrait')->download($nama_file);
        } else {
            $response = responseFail(trans('messages.read-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function so_number($uuid = null)
    {
        \LogActivity::addToLog('download sales order');
        is_uuid($uuid);
        setlocale(LC_ALL, 'IND');
        $where['tb1.uuid'] = $uuid;
        $data              = MonitoringOrder::getSO($where)->get();

        if (!$data->isEmpty()) {
            $data[0]->good_redemption_due_date = is_null($data[0]->good_redemption_due_date) ? "" : strftime('%d %B %Y', strtotime($data[0]->good_redemption_due_date));
            if($data[0]->so_upload==1) {
                $data[0]->order_date      = date('d M Y', strtotime($data[0]->order_date));
            }else{
                $data[0]->order_date      = date('d M Y', strtotime($data[0]->billing_date));
            }

            $where2['tb1.order_id']   = $data[0]->id;
            $product                  = MonitoringOrder::getSOProduct($where2)->get();
            $detail                   = json_decode(MonitoringOrder::getSOProductDetail($where2)->get());
            if (!$product->isEmpty()) {
                $prod  = array_unique(array_column($detail, 'product_id'));
                $dataw = array();
                foreach ($prod as $key => $value) {
                    $keyItemProduct = array_keys(array_column($detail, 'product_id'), $value);
                    $productKab     = $this->makeArr($detail, $keyItemProduct);

                    $kab        = array_unique(array_column($productKab, 'sales_group_id'));
                    $productArr = array(
                        'id'   => $detail[$key]->product_id,
                        'name' => $detail[$key]->product_name,
                        'data' => array(),
                    );
                    foreach ($kab as $keyKab => $vk) {
                        $keyItemKab = array_keys(array_column($productKab, 'sales_group_id'), $vk);
                        $kecArr     = $this->makeArr($productKab, $keyItemKab);
                        $kec        = array_unique(array_column($kecArr, 'sales_unit_id'));

                        $kabArr = array(
                            'id'    => $productKab[$keyKab]->sales_group_id,
                            'name'  => $productKab[$keyKab]->sales_group_name,
                            'prov'  => $productKab[$keyKab]->sales_office_name,
                            'total' => 0,
                            'data'  => array(),
                        );

                        foreach ($kec as $keyKec => $vkk) {
                            $keyItemKec = array_keys(array_column($kecArr, 'sales_unit_id'), $vkk);
                            $kecs       = $this->makeArr($kecArr, $keyItemKec);

                            $kacsArr = array(
                                'id'    => $kecArr[$keyKec]->sales_unit_id,
                                'name'  => $kecArr[$keyKec]->sales_unit_name,
                                'total' => 0,
                                'data'  => array(),
                            );
                            $retail = array_unique(array_column($kecs, 'retail_id'));
                            foreach ($retail as $keyRet => $vr) {
                                $keyItemRet = array_keys(array_column($kecs, 'retail_id'), $vr);
                                $ret        = $this->makeArr($kecs, $keyItemRet);
                                $retailArr  = array(
                                    'id'   => $kecs[$keyRet]->retail_id,
                                    'name' => $kecs[$keyRet]->retail_name,
                                    'qty'  => $kecs[$keyRet]->qty,
                                );
                                $kabArr['total'] += $kecs[$keyRet]->qty;
                                $kacsArr['total'] += $kecs[$keyRet]->qty;
                                array_push($kacsArr['data'], $retailArr);
                            }
                            array_push($kabArr['data'], $kacsArr);
                        }
                        array_push($productArr['data'], $kabArr);
                    }
                    array_push($dataw, $productArr);
                }
                $data[0]->tujuan_kab  = $product[0]->sales_group_name_incoterm;
                $data[0]->tujuan_prov = $product[0]->sales_office_name_incoterm;
                $cetak['data']        = json_decode($data);
                $cetak['product']     = json_decode($product);
                $cetak['lampiran']    = $dataw;
                $pdf                  = PDF::loadView('monitoring_order.so', $cetak);
                $nama_file            = 'Number Sales Order ' . $data[0]->so_number . '.pdf';
                return $pdf->setPaper('a4', 'potrait')->download($nama_file);
            } else {
                $response           = responseFail(trans('messages.read-fail'));
                $response['errors'] = "Product not found for this order";
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response           = responseFail(trans('messages.read-fail'));
            $response['errors'] = "Data order not found";
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No. Penebusan","Kode Referensi","Nama Distributor","Nama Produsen","Kode Booking", "Batas Akhir","Tgl Pengambilan","Tgl Rencana","Tgl Order","Status","Kode SO", "Total Kuantitas DO","Sisa Tebus DO","Incoterm","Provinsi","Kabupaten","Tipe Pembayaran","Total Harga","PPN","Jumlah Setelah" ,"Uang Muka","Batas Akhir Pengambilan","Dibuat Oleh","Dibuat Pada"];
        return Excel::download((new Download($data,$columns)), "Download Monitoring Order.xls");
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp  = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0) {
            $hasil = "minus " . trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }
        return $hasil;
    }

    public function rupiah($angka)
    {

        $hasil_rupiah = "Rp " . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);
        }

        return $data;
    }

    public function unApproved(Request $request)
    {
        $uuids = collect($request)->pluck("uuid");
        try {
            if ($uuids->count() == 0) {
                throw new \Exception("Invalid UUID");
            }

            $materials = MonitoringOrder::whereIn("uuid", $uuids->toArray())
                ->pluck("status")
                ->unique();

            if (\count($materials) > 1 || !$materials->contains("y")) {
                return response()->json(
                    responseFail(trans("messages.invalid-status")),
                    403
                );
            }
            $params = [
                "status"               => "d",
                "approve_status"       => "d",
                "booking_code"         => null,
            ];

            $updated = Order::whereIn("uuid", $uuids)
                    ->update($params);

            OrderDFStatus::whereHas("order", function($q) use ($uuids){
                $q->whereIn("uuid", $uuids);
            })->delete();

        } catch(\Exception $e) {
            return response()->json(
                responseFail($e->getMessage()),
                500
            );
        }

        return response()->json(
            responseSuccess("messages.update-success", $uuids)
        );
    }

    public function changeStatus(Request $request, $uuid)
    {
        $rules = [
            "billing_date" => "required|date",
            "bank_payment" => "required|exists:wcm_bank,id",
            "amount"       =>  "required",
        ];
        $this->validate($request->all(), $rules);

        $order = Order::where("uuid", $uuid)
            ->where("status", "x")
            ->firstOrFail();
        $user = $request->user();

        if (!$user || $this->isDistributor) abort(401);

        DB::beginTransaction();
        try {
            if ($request->get("amount") != $order->upfront_payment) {
                throw ValidationException::withMessages(["amount" => "invalid amount in this order"]);
            }
            $status = $order->payment_method === PaymentGateway::CASH ? "l" : "u";
            $order->billing_date = $request->get("billing_date");
            $order->status = $status;
            $order->bank_payment = $request->get("bank_payment");
            $order->updated_by = @$user->id;

            $status === "l" && $order->bank_id = $order->bank_payment;
            $status === "u" && OrderDFStatus::where("order_id", $order->id)->update(["status" => "n"]);
            $order->save();
            LogOrder::create([
                "order_id" => $order->id,
                "kode" => 3,
                "message" => "Manual Log Order",
                "status" => "y",
                "created_by" => @$user->id,
                "updated_by" => @$user->id,
            ]);
        } catch (ValidationException $e) {
            DB::rollback();
            return response()->json([
                "message" => $e->errors(),
            ], 400);
        }
        catch (\Exception $e) {
            DB::rollback();
            return  response()->json(responseFail($e->getMessage()), 500);
        }
        DB::commit();
        return response()->json(responseSuccess(__("messages.update-success")));
    }

}
