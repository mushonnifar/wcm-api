<?php

namespace App\Http\Controllers;

use App\Models\SalesOrg;
use App\Models\Retail;
use App\Models\Customer;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrgDistRetailController extends Controller
{
    public function produsen(Request $request)
    {
        $where = array();
        if ($this->isAdminAnper) {
            $where['g.sales_org_id'] = $this->salesOrgId;
        } elseif ($this->isDistributor) {
            $where['g.customer_id'] = $this->customerId;
        }

        \LogActivity::addToLog('get all produsen');
        $query = SalesOrg::getDataRelation($where);
        $columns = [
            'a.id' => 'id',
            'a.uuid' => 'uuid',
            'a.name' => 'name',
            'b.customer_id' => 'customer_id',
            'c.full_name' => 'customer_name'
        ];

        $distributor = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $distributor->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function distributor(Request $request)
    {
        $where['a.status'] = 'y';
        if ($this->isAdminAnper) {
            $where['a.id'] = $this->salesOrgId;
        } elseif ($this->isDistributor) {
            $where['b.customer_id'] = $this->customerId;
        }

        \LogActivity::addToLog('get all distributor');
        $query = Customer::getDataRelation($where);
        $columns = [
            'a.id' => 'id',
            'a.uuid' => 'uuid',
            'a.full_name' => 'name',
            'g.sales_org_id' => 'sales_org_id',
            'h.name' => 'sales_org_name'
        ];

        $distributor = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $distributor->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
