<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Models\ApprovalSetting;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Auth;
use App\Models\LogOrder;
use App\Helpers\SAPConnect;
use App\Models\CustomerSalesArea;
use App\Models\SalesGroup;
use Excel;
use App\Exports\Download;

class ManualApprovalController extends Controller {

    public function index(Request $request, $exported=false) {
        \LogActivity::addToLog('get manual approval');
        $where = [];
        // if ($this->isAdminAnper) {
        //     $where["tb1.sales_org_id"] = $this->salesOrgId;
        // }
        $query = DB::table("view_manual_approval");

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("customer_id", $user->customer_id);
        }

        if (count($filters) > 0) {
            if (count($filters["sales_org_id"]) > 0) {
                $query->whereIn("sales_org_id", $filters["sales_org_id"]);
            }

            if (count($filters["sales_group_id"]) > 0) {
                $query->whereIn("sales_group_id", $filters["sales_group_id"]);
            }
        }

        $columns = [
            'id' => 'order_id',
            'uuid' => 'order_uuid',
            'so_number' => 'so_number',
            'number' => 'number',
            'payment_method' => 'payment_method',
            'customer_id' => 'customer_id',
            'customer_name' => 'customer_name',
            'sales_office_id' => 'sales_office_id',
            'sales_office_name' => 'sales_office_name',
            'sales_group_id' => 'sales_group_id',
            'sales_group_name' => 'sales_group_name',
            'contract_id' => 'contract_id',
            'spjb' => 'spjb',
            'retail_id' => 'retail_id',
            'retail_name' => 'retail_name',
            'sales_unit_id' => 'sales_unit_id',
            'sales_unit_name' => 'sales_unit_name',
            'material_list_id' => 'material_list_id',
            'material_no' => 'material_no',
            'mat_desc' => 'mat_desc',
            'qty' => 'qty',
            'approve_status' => 'approve_status',
            'created_by' => 'created_by',
            'updated_by' => 'updated_by',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        
        if ($exported) return $model->getFilteredQuery()->get();

        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function setting(Request $request,$exported=false) {
        \LogActivity::addToLog('get approval setting');
        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        $query = ApprovalSetting::getAll($where,$exported);

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if (count($filters) > 0) {
            if (count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
            }

            if (count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
            }
        }

        $columns = [
            'tb1.sales_org_id' => 'sales_org_id',
            'tb2.name' => 'sales_org_name',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb3.name' => 'sales_group_name',
            'tb3.sales_office_id' => 'sales_office_id',
            'tb4.name' => 'sales_office_name',
            'tb1.status' => 'status',
            'tb1.created_by' => 'created_by',
            'tb1.updated_by' => 'updated_by',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if($exported) return $model->getFilteredQuery()->get();

        $model=$model->make(true);
        
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function updateStatus(Request $request) {
        $attributes = $request->all();

        \LogActivity::addToLog('ganti status approval order');

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $iv) {
                is_uuid($iv['uuid']);
                $status = $iv['status'];
                if ($status == 'y') {
                    $data = $this->createBooking($iv['uuid'], $status);
                } else {
                    $data = $this->cekManualApproveOrder($iv['uuid']);
                    $forUpdate['approve_status'] = $status;
                    $forUpdate['updated_by'] = Auth::user()->id;
                    $forUpdate['status'] = $status;
                    $data->update($forUpdate);
                }
                array_push($model, $data);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateStatusSetting(Request $request) {
        \LogActivity::addToLog('ganti status approval setting');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $iv) {
                is_uuid($iv['uuid']);

                $model = $this->findDataUuid(ApprovalSetting::class, $iv['uuid']);
                $updateAttr['status'] = $iv['status'];
                $updateAttr['updated_by'] = Auth::user()->id;

                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Status","Payment Method","Kode Distributor","Document Date","No Order","Nama Distributor","Kode Prov","Provinsi","Kode Kab","Kabupaten","Kode Kec","Kecamatan","No SPJB","Material","Material Description","Quantity"];
        return Excel::download((new Download($data,$columns)), "Download monitoring DF.xls");
    }

    public function manualapprovaldownload(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->setting($request, true);
        $columns = ["Produsen","Kode Provinsi","Provinsi","Kode Kabupaten","Kabupaten","Status"];
        return Excel::download((new Download($data,$columns)), "Download monitoring DF.xls");
    }

    public function createBooking($order_uuid, $status) {
        $data = $this->cekManualApproveOrder($order_uuid);

        if($data->delivery_method_id == 2){
            $route = $this->cekRoute($data->id, $data->sales_group_id);
            if(!$route){
                $data->update(['approve_status' => $status, 'status' => 'r', 'updated_by' => Auth::user()->id]);
                $this->logOrder($data->id, 'Cek Route', 5, 'Route tidak tersedia');
                return;
            }
        }
        $paymentDueDate = (new OrderController())->getPaymentDueDate(['customer_id' => $data->customer_id, 'sales_org_id' => $data->sales_org_id]);
        if ($data) {
            $cekStock = $this->cekStok($data->id);
            $cekTaxClass = $this->cekTaxClass($data->customer_id, $data->sales_org_id);
            if ($cekTaxClass && $cekStock) {
                $cekSsp = $this->cekSspDate($data->id, $data->customer_id, $data->sales_org_id);
                if ($cekSsp) {
                    $kode_booking = $this->generateKodeBooking($data->id);
                    $data->update(['approve_status' => $status, 'status' => 'y', 'booking_code' => $kode_booking, 'updated_by' => Auth::user()->id, "payment_due_date" => $paymentDueDate,]);
                } else {
                    $data->update(['approve_status' => $status, 'status' => 'r', 'updated_by' => Auth::user()->id]);
                }
            } else if ($cekStock) {
                $kode_booking = $this->generateKodeBooking($data->id);
                $data->update(['approve_status' => $status, 'status' => 'y', 'booking_code' => $kode_booking, 'updated_by' => Auth::user()->id, "payment_due_date" => $paymentDueDate,]);
            } else {
                $data->update(['approve_status' => $status, 'status' => 'r', 'updated_by' => Auth::user()->id]);
            }
        }

        return $data;
    }

    private function logOrder($id, $status, $kode, $message) {
        LogOrder::create([
            'order_id' => $id,
            'status' => $status,
            'kode' => $kode,
            'message' => $message
        ]);
    }

    private function cekManualApproveOrder($order_uuid) {
        $model = $this->findDataUuid(Order::class, $order_uuid);
        if ($model->approve_type == 'm') {
            return $model;
        }
        return false;
    }

    public function getQtyProduk($material_list_id, $plant_id) {
        $where = ['tb1.material_list_id' => $material_list_id, 'tb1.plant_id' => $plant_id,];
        $data = OrderItem::getItemApproved($where)->get();
        $qty = 0;
        foreach ($data as $value) {
            $qty += floatval($value->qty);
        }
        return $qty;
    }

    public function cekStok($order_id) {
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order_id])
            ->select(
                DB::raw("sum(qty) as qty"), "tb1.plant_id", "material_list_id",  "material_no",
                "tb3.code as plant_code", "tb1.product_id"
            )
            ->groupBy(["material_no", "tb3.code", "tb1.plant_id", "material_list_id", "tb1.product_id"])
            ->get();

        $approve = true;

        foreach ($orderItem as $value) {
            $otherQty = $this->getQtyProduk($value->material_list_id, $value->plant_id);
            $forCek = $value->qty + $otherQty;
            $param['R_PLANT'] = $value->plant_code;
            $param['R_MATERIAL'] = $value->material_no;
            $stok = $this->getStok(
                $value->plant_code,
                $value->material_no,
                getRLock($value->plant_code, $value->product_id),
                getUOM($value->product_id)
            );

            if ($stok < $forCek) {
                $approve = false;
            }
        }
        if (!$approve) {
            $this->logOrder($order_id, 'Cek Stok', 1, 'Stok tidak tersedia');
        }

        return $approve;
    }

    public function getStok($plant_code, $material_no, $rlock, $uom) {
        $soap = SAPConnect::connect('cekstok.xml');
        

        $param = [
            'R_MATERIAL' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $material_no]
            ],
            'R_PLANT' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $plant_code]
            ],
            'R_SLOC' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $rlock ]
            ],
            'P_DATUM' => date('Y-m-d'),
            'P_UNIT' => $uom,
            "RETURN" => [],
        ];
        $result = $soap->SI_CheckStock($param);
        $check = checkSAPResponse($result);

        if (!$check["status"]) return 0;

        if (key_exists('item', $result->T_DATA)) {
            return (float) $result->T_DATA->item->STOCK;
        }

        return 0;
    }

    public function generateKodeBooking($id) {
        return sprintf("8%09d", $id);
    }

    public function cekTaxClass($customer_id, $sales_org_id) {
        $data = CustomerSalesArea::getSalesAreaOrder($customer_id, $sales_org_id);

        if ($data->tax_classification == 2 || $data->tax_classification == 3) {
            return true;
        }

        return false;
    }

    public function cekSspDate($order_id, $customer_id, $sales_org_id) {
        $soap = SAPConnect::connect('cek_ssp_date.xml');

        $param = [
            'P_CUSTOMER' => $customer_id,
            'P_SALESORG' => $sales_org_id
        ];
        try {
            $result = $soap->SI_CheckSSPDate($param);
            if (!property_exists($result, "KODE")) throw new Exception();
        }catch (Exception $e) {
            return false;
        }

        $code = $result->KODE;
        if (is_null($code) || empty($code) || strtoupper($code) === "F") {
            return true;
        }

        $this->logOrder($order_id, 'Cek SSP Date', 2, 'SSP Date tidak tersedia');
        return false;
    }

    private function cekRoute($order_id, $sales_group_id){
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order_id])->get();
        $sales_group = SalesGroup::find($sales_group_id);
        $approve = true;

        foreach ($orderItem as $value) {
            $route = $this->getRoute($value->plant_code, $sales_group->district_code);

            if (!$route || ($route &&$route->TYPE == 'E')) {
                $approve = false;
            }
        }
        return $approve;
    }

    private function getRoute($plant_code, $sales_group_id) {
        $soap = SAPConnect::connect('cek_route.xml');

        $param = [
            'R_ROUTE' => [
                'item' => [
                    'IDPLANT' => $plant_code,
                    'SGROUP' => $sales_group_id,
                ]
            ]
        ];
        $result = $soap->SI_Check_Route($param);

        if (!$result) return null;

        $data = property_exists($result, "T_DATA") ? $result->T_DATA : null;

        return @$data->item;
    }
}
