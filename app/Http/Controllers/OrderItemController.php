<?php

namespace App\Http\Controllers;

use App\models\Contract;
use App\models\ContractGovItem;
use App\models\ContractItem;
use App\Models\DeliveryMethod;
use App\Models\LimitDate;
use App\Models\MaterialList;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Plant;
use App\Models\PlantingPeriod;
use App\Models\PricingCondition;
use App\Models\Product;
use App\Models\ProductLimit;
use App\Models\Rdkk;
use App\Models\RdkkItem;
use App\Models\Retail;
use App\Models\SalesOrg;
use App\Models\SalesUnit;
use App\Models\Subtitution;
use Carbon\Carbon;
use Exception;
use FontLib\TrueType\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderItemController extends Controller
{

    public function store(Request $request)
    {
        \LogActivity::addToLog('create item permintaan penebusan');

        DB::beginTransaction();
        $attributes = $request->all();

        if (!$attributes) abort(403);

        $order = Order::findOrFail(@$attributes[0]['order_id']);

        if ($order->status !== "d") abort(403);

        $insert = [];
        try {
            $itemOrder = 0;
            $totalProduct = collect($attributes)->sum("qty");

            $durationDate = $this->getLimitDate($order, $totalProduct);

            $order->good_redemption_due_date = Carbon::now()->addDay($durationDate);
            $order->save();
            $order->orderItems()->delete();
            $itemOrders = collect($attributes)->groupBy(["sales_unit_id", "product_id"])
                ->map(function ($r) use (&$itemOrder) {
                return $r->map(function($k) use (&$itemOrder) {
                    $itemOrder++;
                    return $itemOrder*10;
                });
            });
            foreach ($attributes as $iv) {
                $validator = Validator::make($iv, OrderItem::ruleCreate());
                if ((float)$iv["total_price"] !== ((float)$iv["total_price_before_ppn"] + (float)$iv["ppn"])) {
                    throw new Exception("Invalid Total Price");
                }

                if ($validator->fails()) continue;

                $material = $this->getMaterialList($iv['product_id'], $iv['plant_id'], $order->sales_org_id);
                if (!$material) {
                    $messages = trans('messages.material-fail');
                    $plant = Plant::find($iv["plant_id"]);
                    $product = Product::find($iv["product_id"]);
                    throw new \Exception(@$plant->name . " - " . @$product->name . " " . $messages, 1);
                }
                $iv['material_list_id'] = $material;
                $iv['created_by'] = Auth::user()->id;
                $iv['updated_by'] = Auth::user()->id;
                $iv['item_order_sap'] = $itemOrders->get($iv["sales_unit_id"])->get($iv['product_id']);
                $ivWhere['order_id'] = $iv['order_id'];
                $ivWhere['product_id'] = $iv['product_id'];
                $ivWhere['retail_id'] = $iv['retail_id'];
                $orderItem = OrderItem::updateOrCreate($ivWhere, $iv);
                array_push($insert, $orderItem);
            }
            $this->updatePriceOrder($order->id);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $insert);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        }
        catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function getMaterialList($product_id, $plant_id, $sales_org)
    {
        $now = date("Y-m-d");
        $param = [
            ['product_id', '=', $product_id],
            ['plant_id', '=', $plant_id],
            ['sales_org_id', '=', $sales_org],
            ['status', '=', 'y'],
        ];
        $paramSub = array_merge($param, [['valid_from', '<=', $now], ['valid_to', '>=', $now]]);
        $subtitution = Subtitution::where($paramSub)->first();

        if ($subtitution) {
            return $subtitution->material_no_subtitution;
        } else {
            $material = $this->defaultMaterial($param);
            return $material;
        }
    }

    private function defaultMaterial($param)
    {
        $product = $param[0][2];

        if ($product == 'P02') {
            $param = array_merge($param, [['material_no', '=', '1000147']]);
        } else if ($product == 'P03') {
            if ($param[2][2] == 'C000' || $param[2][2] == 'D000') {
                $param = array_merge($param, [['material_no', '=', '1000093']]);
            } else {
                $param = array_merge($param, [['material_no', '=', '1000076']]);
            }
        }
        else if ($product === "P01") {
            $param = array_merge($param, [['material_no', '=', '1000036']]);
        }
        else if ($product === "P04") {
            $param = array_merge($param, [['material_no', '=', '1000057']]);
        }
        else if ($product === "P05") {
            $param = array_merge($param, [['material_no', '=', '1000283']]);
        } else if ($product === "P51") {
            $param = array_merge($param, [['material_no', '=', '1001865']]);
        } else if ($product === "P54") {
            $param = array_merge($param, [['material_no', '=', '1001751']]);
        }

        $material = MaterialList::where($param)->orderBy('material_no', 'desc')->first();

        if ($material) {
            return $material->id;
        }
        return false;
    }

    private function getPrice($attributes, $view = false)
    {
        if (@$attributes["order_id"]) {
            $order = Order::find($attributes['order_id']);
        } else {
            $order = @$attributes["order"];
        }
//        $salesArea = SalesArea::getSalesArea($order->customer_id, $order->sales_org_id);

        $whereZHET = ["sales_org_id" => $order->sales_org_id, "condition_type" => "ZHET", "status" => "y"];
        $whereZB = ["sales_org_id" => $order->sales_org_id, "sales_group_id" => $order->sales_group_id, "delivery_method_id" => $order->delivery_method_id, "sales_unit_id" => $attributes['sales_unit_id'], "status" => "y"];

        $whereZBKI = $whereZB;
        $whereZBKI['condition_type'] = 'ZBKI';
        $whereZBDI = $whereZB;
        $whereZBDI['condition_type'] = 'ZBDI';
        $now = date("Y-m-d");
        $whereDate = [['valid_from', '<=', $now], ['valid_to', '>=', $now]];

        $productIds = Product::pluck("id")->unique();
        if (!$view) {
            $priceZHET = $this->getZPrice($productIds, $whereZHET, $whereDate);
            $priceZBKI = $this->getZPrice($productIds, $whereZBKI, $whereDate);
            $priceZBDI = $this->getZPrice($productIds, $whereZBDI, $whereDate);
        } else {
            $priceZHET = $order->orderItems()->whereIn('retail_id', $attributes['retail_id'])
                ->select("product_id", "zhet", "uom_zhet")
                ->get()->map(function ($i) {
                    return [
                        "product_id" => $i->product_id,
                        'amount'     => $i->zhet,
                        'per_uom'    => $i->uom_zhet,
                    ];
                });
            $priceZBKI = $order->orderItems()->whereIn('retail_id', $attributes['retail_id'])
                ->select("product_id", "zbki", "uom_zbki")
                ->get()->map(function ($i) {
                    return [
                        "product_id" => $i->product_id,
                        'amount'     => $i->zbki,
                        'per_uom'    => $i->uom_zbki,
                    ];
                });
            $priceZBDI = $order->orderItems()->whereIn('retail_id', $attributes['retail_id'])
                ->select("product_id", "zbdi", "uom_zbdi")
                ->get()->map(function ($i) {
                    return [
                        "product_id" => $i->product_id,
                        'amount'     => $i->zbdi,
                        'per_uom'    => $i->uom_zbdi,
                    ];
                });
        }
        $status = "y";

        return collect(compact("priceZHET", "priceZBKI", "priceZBDI"));
    }

    private function getZPrice($productIds, $where, $whereDate)
    {
        // dump($where, $whereDate);
        // $productIds = $order->orderItems()->pluck("product_id")->unique();
        $return = collect();
        $level1 = PricingCondition::where($where)
            ->where($whereDate)
            ->select("product_id", DB::raw("CAST(amount as decimal(18,2)) as amount"), "per_uom")
            ->orderBy('valid_from', 'desc')
            ->get();

        $existingProduct = collect();

        if (!$level1->isEmpty()) {
            $return = $return->concat($level1);
            $existingProduct = $level1->pluck("product_id")->unique();
        }
        // dump($return);

        if ($productIds->diff($existingProduct)->count() > 0) {
            unset($where["sales_unit_id"]);
            $level2 = PricingCondition::where($where)
                ->where($whereDate)
                ->where("level", "2")
                ->select("product_id", DB::raw("CAST(amount as decimal(18,2)) as amount"), "per_uom")
                ->orderBy('valid_from', 'desc')
                ->get();

            if (!$level2->isEmpty()) {
                // dump($level2);
                $return = $return->concat($level2);
            }
        }
        // dump($return);
        return $return;
    }

    private function getPPN($attributes, $qty)
    {
        $amount = $this->getPrice($attributes);
        $pajak = $this->hitungPPN($amount, $qty);
        return $pajak;
    }

    private function hitungPPN($amount, $qty)
    {
        $PPN = 0.1;
        $NET = 1.1;

        $zhet = round($amount['ZHET'] * $qty);
        $zbki = $amount['ZBKI'] * $qty;

        $zhtk = $zhet - $zbki;
        $zbdi = $amount['ZBDI'] * $qty;

        $zhpn = round($zhet / $NET);
        $mwst = round($zhpn * $PPN);
        $zhtr = $zhtk - $zbdi - $mwst;
        $zhtn = $zhtr + $mwst;
        $pph = round($zhtr * 0.015);

        return [
            'NET'   => $zhtr,
            'GROSS' => $zhtn,
            'PPN'   => $mwst,
            'PPH'   => $pph,
        ];
    }

    private function updatePriceOrder($order_id)
    {
        $price = OrderItem::sumPrice($order_id);
        $model = Order::find($order_id);
        if ((float)$price->total_price !== (float)($price->total_price_before_ppn + $price->ppn)) {
            throw new Exception("Invalid Total Price");
        }

        $update = [
            "total_price"            => $price->total_price,
            "total_price_before_ppn" => $price->total_price_before_ppn,
            "ppn"                    => $price->ppn,
            "pph22"                  => $price->pph22,
        ];
        $model->update($update);
    }

    public function getSPJBItemPenebusan(Request $request)
    {
        \LogActivity::addToLog('get list spjb for item penebusan');

        $param = $request->get('order_id');
        $units = [];
        try {
            $order = Order::findOrFail($param);
            $customer = $order->customer;
            $salesOrg = $order->salesOrg;

            if (!$customer || !$order) throw new Exception("Invalid customer");
            $contract = Contract::find($order->contract_id);

            $isView = $request->has('view') && $order->status !== "d";
            if ($isView) {
                $retails = DB::table("wcm_order_item")
                    ->where("order_id", $order->id)
                    ->selectRaw("distinct retail_id");

                $salesUnits = DB::table("wcm_retail")
                    ->whereRaw("id in ({$retails->toSql()})")
                    ->mergeBindings($retails)
                    ->selectRaw("distinct sales_unit_id")
                    ->pluck("sales_unit_id");

            } else {
                $salesUnits = DB::table("view_p_order_contract")
                    ->where("id", $param)
                    ->where("sales_group_id", $order->sales_group_id)
                    ->distinct("sales_unit_id")
                    ->pluck("sales_unit_id");

                $retails = DB::table("view_p_cust_retail_sales_unit")
                    ->where("sales_org_id", $order->sales_org_id)
                    ->whereIn("sales_unit_id", $salesUnits)
                    ->where("customer_id", $order->customer_id)
                    ->select("retail_id");

            }

            $perbups = ContractGovItem::perbupByRetail(null)->get();
            $alokasis = ContractItem::operationalRetail($order, $salesUnits, date("Y"))->get();
            // dd($penebusans);
            $rdkk = DB::table("wcm_rdkks")
                ->where("customer_id", $order->customer_id)
                ->where("planting_period_id", @PlantingPeriod::current()->id)
                ->whereIn("sales_unit_id", $salesUnits)
                ->select("id");


            $retails = DB::table("wcm_retail")->whereRaw("id in ({$retails->toSql()})")
                ->mergeBindings($retails)
                ->get();

            if ($salesOrg->status == "true" && !$isView) {
                $retails = $retails->concat(
                    Retail::whereIn("sales_unit_id", $salesUnits)
                        ->where("sub_district_default", 1)
                        ->get()
                );
            }

            $productLimits = ProductLimit::all()->mapWithKeys(function ($p) {
                return [
                    $p->product_id => $p->qty,
                ];
            });

            foreach ($salesUnits as $salesUnit) {
                $unit = SalesUnit::find($salesUnit);
                $unitRetails = $retails->where("sales_unit_id", $salesUnit)
                    ->unique();
                $rawRetailInUnit = Retail::whereRaw("sales_unit_id = '$salesUnit'")->select("id")->toSql();
                $pByRetail = DB::table("view_p_by_retail")
                    ->whereRaw("retail_id in ({$rawRetailInUnit})")
                    ->when($order->status === "d", function ($q) {
                        $q->whereRaw("YEAR(order_date) = " . Carbon::now()->format("Y"));
                    })
                    ->when($order->status !== "d", function ($q) use ($order) {
                        $q->whereBetween("order_date", [
                            parseDate($order->order_date)->startOfYear(),
                            parseDate($order->submit_date),
                        ]);
                    })
                    ->where("contract_id", $order->contract_id);

                $penebusans = $pByRetail->where("customer_id", $customer->id)
                    ->select(
                        "retail_id",
                        "product_id",
                        DB::raw("sum(qty) as qty")
                    )
                    ->groupBy("retail_id", "product_id")
                    ->get();

                $penebusansRegion = $pByRetail->select(
                    "retail_id",
                    "product_id",
                    DB::raw("sum(qty) as qty")
                )
                    ->groupBy("retail_id", "product_id")
                    ->get();

                $rdkkItem = DB::table("p_rdkk_sales_unit")
                    ->where("sales_unit_id", $salesUnit)
                    ->whereRaw("rdkk_id in ({$rdkk->toSql()})")
                    ->mergeBindings($rdkk)
                    ->get();

                $products = Product::select("id", "name")->get();
                // dump("Mencari Price {$salesUnit}");
                $prices = $this->getPrice(["retail_id" => $unitRetails->pluck("id"), "order" => $order, "sales_unit_id" => $salesUnit], $request->has('view'));
                // dump($prices->toArray());continue;
                $itemPlant = null;
                if ($order && $order->orderItems) {
                    // dump($unitRetails);
                    $itemPlant = $order->orderItems
                        ->whereIn("retail_id", $unitRetails->pluck("id"))
                        ->first();
                    $itemPlant = @$itemPlant->plant ?: null;
                }

                $retailsResult = [];

                $prods = $plant = [];

                $products->map(function ($p) use (
                    $rdkkItem, $penebusans, $penebusansRegion, $perbups, $prices,
                    &$prods, $salesUnit, $alokasis, $contract, $productLimits
                ) {
                    $rdkkQty = $rdkkItem ? $rdkkItem->where("product_id", $p->id)->sum("approved_qty") : "undefined";

                    $penebusanQty = $penebusans
                        ->where("product_id", $p->id)
                        ->sum("qty");

                    $penebusanQtyRegion = $penebusansRegion
                        ->where("product_id", $p->id)
                        ->sum("qty");

                    $perbupQty = $perbups
                        ->where("sales_unit_id", $salesUnit)
                        ->where("product_id", $p->id)
                        ->sum("qty");

                    $alokasiQty = $alokasis
                        ->where("product_id", $p->id)
                        ->where("sales_unit_id", $salesUnit)
                        ->sum("initial_qty");

                    $priceZHET = $prices->get("priceZHET")->where("product_id", $p->id)->first();
                    $priceZBKI = $prices->get("priceZBKI")->where("product_id", $p->id)->first();
                    $priceZBDI = $prices->get("priceZBDI")->where("product_id", $p->id)->first();

                    $spjbstatus = $contract->contractItems()
                        ->where('status', 'y')
                        ->where("sales_unit_id", $salesUnit)
                        ->where("month", (int)date("m"))
                        ->where("year", date("Y"))
                        ->pluck("product_id");

                    $status = (!$priceZHET || !$priceZBKI || !$priceZBDI) ? "n" : ($spjbstatus->contains($p->id) ? 'y' : 'n');

                    $prods[] = [
                        "id"                   => $p->id,
                        "name"                 => $p->name,
                        "ZHET"                 => $priceZHET,
                        "ZBKI"                 => $priceZBKI,
                        "ZBDI"                 => $priceZBDI,
                        "status"               => $status,
                        "penebusan"            => $penebusanQty,
                        "rdkk"                 => $rdkkQty != "undefined" ? (float)sprintf("%.2f", ($rdkkQty / ($p->id === "P51" ? 1 : 1000) ) - $penebusanQty) : "undefined",
                        "perbup"               => (float)sprintf("%.2f", (float)$perbupQty - (float)$penebusanQtyRegion),
                        "alokasi"              => (float)sprintf("%.2f", (float)$alokasiQty - (float)$penebusanQty),
                        'limit_qty'            => $productLimits->get($p->id),
                        "penebusan_qty_region" => $penebusanQtyRegion,
                    ];
                });

                foreach ($unitRetails as $unitretail) {
                    $retail = $unitretail;
                    $items = [];
                    $products->map(function ($p) use ($retail, &$items, $order, &$plant, $itemPlant) {
                        $currentOrder = $order->orderItems()
                            ->where("retail_id", @$retail->id)
                            ->where("product_id", $p->id)
                            ->select("qty")
                            ->first();

                        $items[] = [
                            "id"   => $p->id,
                            "name" => $p->name,
                            "qty"  => 1 * ($currentOrder ? $currentOrder->qty : null),
                        ];

                        if ($itemPlant && $currentOrder) {
                            $plant[] = [
                                "plant_id"   => $itemPlant->id,
                                "plant_name" => $itemPlant->name,
                                "plant_code" => $itemPlant->code,
                            ];
                        }
                    });

                    $r = [
                        "id"      => @$retail->id,
                        "name"    => @$retail->name,
                        "address" => @$retail->address,
                        "items"   => $items,
                    ];

                    $retailsResult[] = $r;
                }

                if (in_array($salesUnit, array_keys($units))) {
                    $units[ $salesUnit ]["retail"][] = $retailsResult;
                    if (!@$units[ $salesUnit ]["plant_id"]) {
                        $col = collect($plant);
                        if ($col->pluck("plant_id")->unique()->count() == 2) {
                            $un = $col->where("plant_id", "!=", null)->first();
                            $units[ $salesUnit ]["plant_id"] = $un["plant_id"];
                            $units[ $salesUnit ]["plant_name"] = $un["plant_name"];
                            $units[ $salesUnit ]["plant_code"] = $un["plant_code"];
                        }
                    }
                } else {
                    $u = [
                        "sales_unit_id"   => $salesUnit,
                        "sales_unit_name" => @$unit->name,
                        "products"        => $prods,
                        "retail"          => $retailsResult,
                    ];//

                    if ($plant) {
                        $u += @$plant[0];
                    } else {
                        $u += [
                            "plant_id"   => null,
                            "plant_code" => null,
                            "plant_name" => null,
                        ];
                    }

                    $units[ $salesUnit ] = $u;
                }
            }

        }
        catch (Exception $e) {
            return response()->json(responseFail("{$e->getLine()} - {$e->getMessage()} "), 500);
        }

        return response()->json(["kecamatan" => array_values($units)]);

    }

    private function getItemRdkk($attributes, $forWhereIn)
    {
        $where['tb1.customer_id'] = $attributes['customer_id'];
        // $where['tb1.sales_org_id'] = $attributes['sales_org_id'];
        $where['tb3.desc'] = $attributes['year'];
        $whereInSales = $forWhereIn['sales_unit_id'];
        $whereInRetail = $forWhereIn['retail_id'];
        $model = RdkkItem::getSumRdkk($where, $whereInSales, $whereInRetail);

        return $model;
    }

    private function getProduk()
    {
        $data = Product::all();
        $produk = [];
        foreach ($data as $value) {
            array_push($produk, [
                'id'     => $value->id,
                'name'   => $value->name,
                'status' => 'n',
                'limit'  => $this->getProdukLimit($value->id),
            ]);
        }

        return $produk;
    }

    private function getProdukLimit($produk)
    {
        $data = ProductLimit::where('product_id', $produk)->first();

        return $data->qty;
    }

    private function getProdukItem()
    {
        $data = Product::all();
        $produk = [];
        foreach ($data as $value) {
            array_push($produk, ['product_id' => $value->id, 'product_name' => $value->name, 'qty' => null]);
        }

        return $produk;
    }

    private function getRetail($salesOrg, $customer, $kecamatan)
    {
        $cekSalesOrg = SalesOrg::find($salesOrg);

        if ($cekSalesOrg->status == 1 || $cekSalesOrg->status == "true") {
            $where['tb2.sales_org_id'] = $salesOrg;
            $where['tb2.customer_id'] = $customer;
            $retail = Retail::getSubRetail($where, $kecamatan)->get();
            return $retail;
        } else if ($cekSalesOrg->status == 0 || $cekSalesOrg->status == "false") {
            $whereRetail['e.sales_org_id'] = $salesOrg;
            $whereRetail['e.customer_id'] = $customer;
            $retail = Retail::getData($whereRetail)->get();
            return $retail;
        }
    }

    private function getLimitDate($order, $qty)
    {
        $incoterm = DeliveryMethod::find($order->delivery_method_id);

        $limitDueDate = LimitDate::where("sales_org_id", $order->sales_org_id)
            ->where('sales_office_id', $order->sales_office_id)
            ->where('incoterm', $incoterm->ident_name)
            ->where('min_qty', '<=', $qty)
            ->where('max_qty', '>=', $qty)
            ->first();

        return $limitDueDate ? $limitDueDate->duration_date : 0;
    }

}
