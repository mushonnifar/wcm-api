<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Route;
use App\Models\MAction;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class RouteController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all route');

        $user = Datatables::of(Route::query())
                ->filter(function($query) use ($request) {
                    $columns = ['name' => 'name'];
                    foreach ($columns as $key => $value) {
                        if ($request->has($value)) {
                            $query->where($key, 'like', "%{$request->get($value)}%");
                        }
                    }
                })
                ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $user->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request) {
        \LogActivity::addToLog('create route');

        $attributes = $request->only(['name']);

        $this->validate($attributes, Route::ruleCreate());

        $id = Route::max('id');

        $attributes['id'] = intval($id) + 1;
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        try {
            $model = Route::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($id) {
        \LogActivity::addToLog('get route by id');

        $model = $this->findData(Route::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id) {
        \LogActivity::addToLog('update route');

        $attributes = $request->all();
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, Route::ruleUpdate());

        $model = $this->findData(Route::class, $id);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id) {
        \LogActivity::addToLog('delete route');

        $model = $this->findData(Route::class, $id);

        DB::beginTransaction();
        try {
            $model->delete();
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function permission(Request $request, $role_id) {
        \LogActivity::addToLog('get all permission route');
        $language = strtoupper($request->header('Language'));
        if (!$language) {
            $language = 'EN';
        }

        $routes = Route::all('id', 'name')->toArray();
        $actions = MAction::all()->toArray();
        foreach ($routes as $i => $iv) {
            foreach ($actions as $j => $jv) {
                $actions[$j]['id'] = trim($jv['id']);
                $actions[$j]['action_name'] = $jv['action_name'];
                $routes[$i][trim($jv['id'])] = (Route::getPermission($role_id, $iv['id'], $jv['id'])->count() ? 'Y' : 'N');
            }
        }

        $response = responseSuccess(trans('messages.read-success'), $routes);
        $response['dataAction'] = $actions;

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function setPermission(Request $request, $role_id) {
        \LogActivity::addToLog('set permission route');

        $attributes = $request->all();
        $this->validate($attributes, Route::rulePermission());

        $attributes = json_decode($attributes['data'], true);

        $route_ids = [];
        $action_ids = [];
        $route_names =[];
        $permission = [];
        foreach ($attributes as $i => $iv) {
            $action_id = $iv['action_id'];
            $route_id  = $iv['route_id'];
            $route_name = $iv['route_name'];

            $header = [
                "action_id" => $action_id,
                "route_id" =>$route_id,
            ];
        
            if ( ! Permission::where($header)->exists()) {
                $new        = new Permission();
                $new->id    = Permission::max("id") + 1;
                $new->action_id = $action_id;
                $new->name  = Str::kebab($route_name) . "-" . Str::upper($action_id);
                $new->route_id = $route_id;
                $new->save();
            }

            $route_ids[] = $route_id;
            $action_ids[] = $action_id;
            $permission[] = Permission::where($header)->pluck("id")->first();
        }

        DB::beginTransaction();
        
        try {

            $role = $this->findData(Role::class, $role_id);
            $data = $role->syncPermissions($permission);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $data);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
