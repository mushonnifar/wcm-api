<?php

namespace App\Http\Controllers;

use App\Models\CertificateRelease;
use App\Models\CertificateReleaseItems;
use Illuminate\Http\Request;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Rules\CreateCertificateReleaseRules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\URL;
use PDF;

class CertificateReleaseController extends Controller
{
    //

    public function index(Request $request){
        $query = DB::table('wcm_certificate_release as tb1')
        ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
        ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
        ->select('tb1.*', 'tb3.full_name as customer_name', 'tb4.name as sales_org_name','tb6.name as sales_group_name', DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"), DB::raw("CONVERT ( VARCHAR, tb1.updated_at, 105 ) as created_at"));

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb1.number' => 'number',
            'tb1.customer_id' => 'customer_id',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb3.full_name'=>' customer_name', 
            'tb4.name '=>' sales_org_name',
            'tb6.name '=>' sales_group_name',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at'
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);        
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function listDataCreate(Request $request)
    {
        # code...
        \LogActivity::addToLog('List PKP Surat Jalan');

        $query   =  DistribReports::detailheader(['tb1.distrib_resportable_type'=>'order','tb1.status'=>'s'])
                    ->whereNull('status_certificate_release')
                    ->addSelect('status_certificate_release');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb1.id' => 'id', 
            'tb1.uuid' => 'uuid', 
            'tb1.report_f5_id' => 'report_f5_id', 
            'tb1.number' => 'number', 
            'tb2.so_number' => 'so_number',
            'tb1.customer_id' => 'customer_id',
            'tb3.full_name' => 'customer_name', 
            'tb1.sales_group_id' => 'sales_group_id', 
            'tb4.name' => 'sales_group_name',
            'tb1.sales_org_id' => 'sales_org_id', 
            'tb5.name'=>'sales_org_name', 
            'tb2.delivery_method_id'=>'inconterm_id',
            'tb6.name' => 'inconterm_name', 
            'tb1.distribution_date' => 'distribution_dates',
            'tb1.status' => 'status',
            'tb2.contract_id' => 'contract_id', 
            'tb1.month' => 'month', 
            'tb1.year' => 'year', 
            'tb1.order_id' => 'order_id',
            'tb2.order_date' => 'order_date'
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);        
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }


    public function store(Request $request)
    {
        # code...
        \LogActivity::addToLog('Create PKP Surat Jalan');

        $attributes               = $request->only(['data','created_at']);
       
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
            'data'  => [new CreateCertificateReleaseRules]
        ];

        if($request->has('created_at') && @$request->get('created_at')!=""){
            $rules += [
                'created_at' => 'required|date'
            ];
            $created_at = $request->get('created_at');
        }else{
            $created_at = date('Y-m-d H:i:s');
        }
        
        $this->validate($attributes, $rules);

        $data = DistribReports::whereIn('uuid',$attributes['data'])->get();
        $customer_id=$data->pluck('customer_id')->first();
        $sales_org_id =$data->pluck('sales_org_id')->first();
        $sales_group_id=$data->pluck('sales_group_id')->first();   
        
        
        $firstDateDistribution = $data->sortBy('distribution_date')->pluck('distribution_date')->first();
        if($created_at < $firstDateDistribution){
            $response             = responseFail(['created_at' => 'Tanggal Surat Tidak Boleh Kurang dari Tanggal Distribusi PKP'.date_format(date_create($created_at),"d-m-Y")]);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $createData =[
                'status'=>'s',
                'created_by'=> Auth::user()->id,
                'sales_org_id' => $sales_org_id,
                'sales_group_id' =>$sales_group_id,
                'customer_id' => $customer_id
            ];
            if($request->has('created_at') && @$request->get('created_at')!=""){
                $createData += [
                    'created_at'=> date_create($created_at." ".date('H:i:s'))
                ];
            }
            $certificate = CertificateRelease::create($createData);
            $createItemData = [];
            foreach ($data as $key => $value) {
                # code...
                $createItemData[] = [
                    'certificate_release_id'=> $certificate->id,
                    'distrib_report_id' => $value['id'],
                    'created_by' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }           
            $model = CertificateReleaseItems::insert($createItemData);
            $update = DistribReports::whereIn('uuid',$attributes['data'])->update(['status_certificate_release'=>'s','updated_by'=>Auth::user()->id]);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {
        # code...
        $model['header'] = DB::table('wcm_certificate_release as tb1')
        ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
        ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
        ->select('tb1.*', 'tb3.full_name as customer_name', 'tb4.name as sales_org_name','tb6.name as sales_group_name',  DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"))
        ->where('tb1.uuid', $uuid)->first();
        $date                              = explode('-', $model['header']->created_at );
        $month                             = $this->monthName($date[1]);
        $model['header']->created_at       = $date[0] . '-' . $month . '-' . $date[2] ;
        $model['data'] = CertificateRelease::detail()
        ->where('wcr.uuid', $uuid)
        ->whereNotNull('vdcr.retail_code')
        ->get();
        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update($uuid, Request $request)
    {
        # code...
        $certificate = CertificateRelease::where('uuid', $uuid)->firstOrFail();
        $attributes  = $request->only('data');
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
            'data'  => [new CreateCertificateReleaseRules]
        ];
        $this->validate($attributes, $rules);
        $data = DistribReports::whereIn('uuid',$attributes['data'])->get();
        $pass = false; $msg="<ul>";
        if($data->pluck('sales_org_id')->first()!=$certificate->sales_org_id){
            $msg.="<li>sales_org_id tidak sama dengan data header surat jalan yang ada</li>";
            $pass = true;
        }
        if($data->pluck('customer_id')->first()!=$certificate->customer_id){
            $msg.="<li>customer_id tidak sama dengan data header surat jalan yang ada</li>";
            $pass = true;
        }
        if($data->pluck('sales_group_id')->first()!=$certificate->sales_group_id){
            $msg.="<li>sales_group_id tidak sama dengan data header surat jalan yang ada</li>";
            $pass = true;
        }

        $firstDateDistribution = $data->sortBy('distribution_date')->pluck('distribution_date')->first();
        if($certificate['created_at'] < $firstDateDistribution){
            $msg.="<li>'PKP Tidak dapat dipilih, Tanggal Distribusi harus melebihi tanggal pembuatan surat jalan.'</li>";
            $pass = true;
        }

        $msg.="</ul>";
        if($pass){
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => ["data" => $msg]
            ];
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $createItemData = [];
            foreach ($data as $key => $value) {
                # code...
                $createItemData[]= [
                    'certificate_release_id'=> $certificate->id,
                    'distrib_report_id' => $value['id'],
                    'updated_by' => Auth::user()->id,
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            CertificateRelease::where('uuid', $uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            CertificateReleaseItems::insert($createItemData);
            DistribReports::whereIn('uuid',$attributes['data'])->update(['status_certificate_release'=>'s','updated_by'=>Auth::user()->id, 'updated_at'=> date('Y-m-d H:i:s')]);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function destroyItems($uuid, Request $request)
    {
        # code...
        $attributes = ['uuid'=> $uuid];
        $rules = [
            'uuid' => 'required|exists:wcm_certificate_release,uuid', 
        ];
        $this->validate($attributes, $rules);

        $certificate = CertificateRelease::where('uuid',$uuid)->first();

        $req               = $request->only('data');
        $rules = [
            'data' => 'required|exists:wcm_distrib_reports,uuid', 
        ];
        $this->validate($req, $rules);
        $distrib = DistribReports::where('uuid',$req['data'])->first();

        DB::beginTransaction();
        try {
            CertificateReleaseItems::where(['distrib_report_id'=>$distrib->id,'certificate_release_id'=>$certificate->id])->delete();
            CertificateRelease::where('uuid',$uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            DistribReports::where('uuid',$req['data'])->update(['status_certificate_release'=>null,'updated_by'=>Auth::user()->id]);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroyBulkItems($uuid, Request $request)
    {
        # code...
        $attributes = ['uuid'=> $uuid];
        $rules = [
            'uuid' => 'required|exists:wcm_certificate_release,uuid', 
        ];
        $this->validate($attributes, $rules);

        $certificate = CertificateRelease::where('uuid',$uuid)->first();

        $items = $request->only('data');
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
        ];
        $this->validate($items, $rules);    
        $distrib = DistribReports::whereIn('uuid', $items['data'])->get();
        

        DB::beginTransaction();
        try {
            CertificateReleaseItems::whereIn('distrib_report_id',$distrib->pluck('id')->toArray())->where('certificate_release_id',$certificate->id)->delete();
            CertificateRelease::where('uuid',$uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            DistribReports::whereIn('uuid', $items['data'])->update(['status_certificate_release'=>null,'updated_by'=>Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            // return response()->json($distrib->pluck('id')->toArray());
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download($uuid)
    {
        # code...
        $aes = new \Legierski\AES\AES;

        $user           = Auth::user();
        $certificate         = DB::table('wcm_certificate_release')->where('uuid', $uuid)->first();
        // user role
        $route = DB::table('wcm_m_routes')->where('name', 'suratjalan')->first();

        $access = DB::table('wcm_role_has_menus')
            ->where('role_id', $user->role_id)
            ->where('menu_id', $route->id)
            ->get();

        $acceses = array();
        foreach ($access as $key) {
            array_push($acceses, $key->action_id);
        }

        $akses = implode(",", $acceses);
        $akses = str_replace(' ', '', $akses);

        $passphrase = "sy4hr!lb3g!tut4mp4n";

        $decrypted = $aes->encrypt($akses, $passphrase);

        $model['url'] = URL::to('/#/' . urlencode($decrypted) . '/laporan/surat-jalan/detail/' . $certificate->uuid);

        $model['header'] = DB::table('wcm_certificate_release as tb1')
        ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
        ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
        ->select('tb1.*', 'tb3.full_name as customer_name', 'tb4.name as sales_org_name','tb6.name as sales_group_name',  DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"))
        ->where('tb1.uuid', $uuid)->first();
        $model['data'] = CertificateRelease::detail()
        ->where('wcr.uuid', $uuid)
        ->whereNotNull('vdcr.retail_code')
        ->get();
        $date                              = explode('-', $model['header']->created_at );
        $month                             = $this->monthName($date[1]);
        $model['header']->created_at       = $date[0] . '-' . $month . '-' . $date[2] ;
        // return response()->json($model);
        $model['data']=json_decode($model['data'], true);
        // return view('suratjalan.suratjalan', $model);
        $model['user']=$user->username;
        $model['date'] = date('d-m-Y H:i:s');
        
        $pdf = PDF::loadView('suratjalan.suratjalan', $model);
        $nama_file = 'DOWNLOAD-SURAT-JALAN-'.$model['header']->number;
        return $pdf->setPaper('a4', 'landscape')->download($nama_file);
    }


    public function updatedCreatedAt($uuid,Request $request)
    {
        $attributes = ['uuid'=> $uuid];
        $rules = [
            'uuid' => 'required|exists:wcm_certificate_release,uuid', 
        ];
        $this->validate($attributes, $rules);

        $req = $request->only('created_at');
        $ruleCreateAt= [
            'created_at' => 'required|date', 
        ];
        $this->validate($req, $ruleCreateAt);

        $certificate= CertificateRelease::where('uuid',$uuid)->first();

        $distrib_report_id = CertificateReleaseItems::select('distrib_report_id')->where('certificate_release_id',$certificate->id)->get()->pluck('distrib_report_id')->toArray();
        $data = DistribReports::whereIn('id',$distrib_report_id)->get();

        $pass = false;
        $firstDateDistribution = $data->sortBy('distribution_date')->pluck('distribution_date')->first();
        if($req['created_at'] < $firstDateDistribution){
            $msg="Tanggal Distribusi harus melebihi tanggal pembuatan surat jalan";
            $pass = true;
        }
        if($pass){
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => ["data" => [$msg]]
            ];
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
        
        DB::beginTransaction();
        try {
            CertificateRelease::where('uuid',$uuid)->update([
                'created_at' => date_create($req['created_at']." ".date('H:i:s')),
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }

}
