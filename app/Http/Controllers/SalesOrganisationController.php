<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\SalesOrg;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class SalesOrganisationController extends Controller
{
    public function index(Request $request, $exported=false)
    {
        $model = SalesOrg::select('id', 'name', 'status');
        $users = request()->user();
        $filters = $users->filterRegional;
        if (@$filters["sales_org_id"]) {
            $model->whereIn("id", $filters["sales_org_id"]);
        }
        $model = DataTables::of($model);

        if ($exported) return $model->getFilteredQuery();
        return $model->make(true);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request->all(), ['status' => 'required|in:0,1']);

        try {
            $salesOrg = SalesOrg::findOrFail($id);
            $salesOrg->update(['status' => $request->get('status')]);
        } catch (\Exception $e) {

            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()->json(
            responseSuccess(
                trans('messages.update-success'),
                $salesOrg->only(['id', 'name', 'status'])
            )
        );

    }

    public function listlintas(Request $request)
    {
        \LogActivity::addToLog('List Config Lintas Alur');

        $columns = [
            'wcm_sales_org.id'                 => 'id',
            'wcm_sales_org.name'               => 'name',
            'wcm_sales_org.config_lintas_alur' => 'config_lintas_alur',
        ];

        $query = SalesOrg::select('id', 'uuid', 'name', 'config_lintas_alur');
        
        $users = $request->user();
        $filters = $users->filterRegional;
        if (@$filters["sales_org_id"]) {
            $query->whereIn("id", $filters["sales_org_id"]);
        }

        $model = Datatables::of($query)
                ->filter(function ($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function updatelintas(Request $request, $uuid)
    {
        $this->validate($request->only(['config_lintas_alur']), ['config_lintas_alur' => 'required|in:0,1']);

        try {
            $salesOrg = $this->findDataUuid(SalesOrg::class,$uuid);            
            $salesOrg->update(['config_lintas_alur' => $request->get('config_lintas_alur')]);
        } catch (\Exception $e) {

            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()->json(
            responseSuccess(
                trans('messages.update-success'),
                $salesOrg->only(['id', 'name', 'status'])
            )
        );

    }

    public function download(Request $request) {
        $request = $request->merge(["length" => 1]);
        $model = $this->index($request, true);
        $data = $model->select(SalesOrg::getExportedColumns());
        return Excel::download((new Download($data->get())),"lintas-alur.xls");
    }
}
