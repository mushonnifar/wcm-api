<?php

namespace App\Http\Controllers;

use App\Models\ContractItem;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\Order;
use App\Models\Product;
use App\Models\ReportF5;
use App\Models\Retail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Yajra\DataTables\DataTables;
use Excel;
use App\Exports\Download;

class ReportF5Controller extends Controller
{
    //
    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('Report Master F5 Datatable');
        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        $query   = ReportF5::MasterF5Report($where, $exported);

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb1.uuid'           => 'uuid',
            'tb1.sales_org_id'   => 'sales_org_id',
            'tb2.name'           => 'sales_org_name',
            'tb1.number'         => 'number',
            'tb1.customer_id'    => 'customer_id',
            'tb3.full_name'      => 'customer_name',
            'tb1.month'           => 'month',
            'tb1.year'           => 'year',
            'tb1.submited_date'  => 'submited_date',
            'tb5.id'             => 'sales_office_id',
            'tb5.name'           => 'sales_office_name',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb4.name'           => 'sales_group_name',
            'tb1.status'         => 'status',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if($exported) return $model->getFilteredQuery()->get();
        $model=$model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('Create F5 Manual');
        $attributes = $request->all();
        
        $this->validate($attributes, ReportF5::createf5rule());
        $attributes['month'] = intval($attributes['month']);
        $spjb_checks = $this->checkspjb($attributes);

        if (!$spjb_checks) {
            $error = [
                "customer_id"     => ['Data SPJB Tidak Ada'],
                "sales_office_id" => ['Data SPJB Tidak Ada'],
                "sales_group_id"  => ['Data SPJB Tidak Ada'],
                "month"           => ['Data SPJB Tidak Ada'],
                "year"            => ['Data SPJB Tidak Ada'],
                "sales_org_id"    => ['Data SPJB Tidak Ada'],
            ];
            $response             = responseFail($error);
            $response['messages'] = 'Data In SPJB Operational Not Found';
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        unset($attributes['sales_office_id']);

        $duplicate_checks = $this->duplicatedata(ReportF5::class, $attributes);

        if ($duplicate_checks) {
            $error = [
                "customer_id"     => [trans('messages.duplicate')],
                "sales_office_id" => [trans('messages.duplicate')],
                "sales_group_id"  => [trans('messages.duplicate')],
                "month"           => [trans('messages.duplicate')],
                "year"            => [trans('messages.duplicate')],
                "sales_org_id"    => [trans('messages.duplicate')],
            ];
            $response             = responseFail($error);
            $response['messages'] = 'Data Duplicate';
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $attributes['status'] = 'd';
            // $attributes['number'] = $this->genUniqF5();
            $data                 = ReportF5::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $data);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function bast($uuid)
    {
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Download BAST Report F5');
        is_uuid($uuid);
        // find report f5
        $where_f5['tb1.uuid'] = $uuid;
        $reportf5             = ReportF5::MasterF5Report($where_f5)->first();

        $whereDR['tb7.uuid']           = $uuid;
        $whereDR['tb3.id']             = $reportf5->customer_id;
        $whereDR['tb1.sales_org_id']   = $reportf5->sales_org_id;
        $whereDR['tb1.month']          = intval($reportf5->month);
        $whereDR['tb1.year']           = $reportf5->year;
        $whereDR['tb1.sales_group_id'] = $reportf5->sales_group_id;
        $distrib_reports               = DistribReports::where("report_f5_id", $reportf5->id)
            ->leftjoin('wcm_orders as tb2', 'tb2.id', '=', 'wcm_distrib_reports.order_id')
            ->where("wcm_distrib_reports.status", "s")
            ->select('wcm_distrib_reports.*','tb2.so_number as order_so_number')
            ->whereIn("wcm_distrib_reports.distrib_resportable_type", ["order","InitialStockF5"])
            ->get();
        // Product
        $produks = Product::all();
        
        $spjb = DB::table('wcm_contract_item AS tb1')
        ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
        ->select('tb1.sales_unit_id')
        ->where('tb2.customer_id',$reportf5->customer_id)
        ->where('tb2.sales_org_id', $reportf5->sales_org_id)
        ->where('tb1.month', intval($reportf5->month))
        ->where('tb1.year', $reportf5->year)
        ->where('tb1.sales_group_id', $reportf5->sales_group_id)
        ->where('tb2.contract_type','asal')
        ->get()
        ->pluck('sales_unit_id')->unique();

        // Uniq Distrib ID
        $uniq_id = $distrib_reports->pluck("id")->unique()->toArray();
        // Distrib Order (Penyaluran DO)
        $distribitems = DistribReportItems::wherein('distrib_report_id', $uniq_id)->where('qty','!=',0)->get();
        $retailUniq = $distribitems->pluck('retail_id')->unique();
         // Retail
         if($reportf5->status=="d"){
            // Retail
        $where_r['c.id'] = $reportf5->sales_group_id;
        $where_r['f.id'] = $reportf5->customer_id;
        $where_r['g.id'] = $reportf5->sales_org_id;
        $last            = date('Y-m-t', strtotime($reportf5->year . "-" . $reportf5->month));
        $retails = Retail::retailDitributionDO($where_r)->whereIn('a.sales_unit_id', $spjb)
                ->whereRaw("e.created_at <= '" . $last . "'")
                ->whereIn("e.status",['y','p'])
                ->whereIn('a.id', $retailUniq )
                ->whereRaw("a.sub_district_default = '0'")
                ->get();
        }else{
            $retails = Retail::whereIn('id',$retailUniq)->where('sub_district_default','!=',1)
            ->get();
        }     

        $template= $this->makeTotalCalc($produks);
        $datas = [];
        foreach ($retails as $key => $value) {
            # code...
            $retailsArr = array(
                'number'         => $this->genBastF5($reportf5->number, $reportf5->month, $reportf5->year, $value->code),
                'id'             => $value->id,
                'retail'         => $value->code,
                'retail_name'    => $value->name,
                'retail_owner'   => $value->owner,
                'customer_id'    => $reportf5->customer_id,
                'customer_name'  => $reportf5->customer_name,
                'customer_owner' => $reportf5->customer_owner,
                'month'          => $reportf5->month,
                'month_name'     => $this->monthName($reportf5->month),
                'year'           => $reportf5->year,
                'data'           => array(),
                'total'          => $template);

            foreach ($distrib_reports as $keyD => $valD) {
                # code...
                $temp  = array('so' => $valD->order_so_number, 'submited_date' => date('d', strtotime($valD->distribution_date)) . " " . $this->monthName(date('m', strtotime($valD->distribution_date))) . " " . date('Y', strtotime($valD->distribution_date)), 'data' => array());
                // $flags = 0;
                foreach ($produks as $keyP => $valP) {
                    # code...
                    $distribitems_r_keys = $distribitems->where('retail_id', $value->id)->where('distrib_report_id', $valD->id)->where('product_id',$valP->id);
                    $qty = $distribitems_r_keys->sum('qty');
                    $retailsArr['total'][$keyP]['qty'] += $qty;
                    array_push($temp['data'], array('name' => $valP->name, 'qty' => $qty));
                }
                array_push($retailsArr['data'], $temp);
            }
            array_push($datas, $retailsArr);
        }

        $data['datas']     = $datas;
        $data['product']   = $produks;
        $strtotime = strtotime($reportf5->year . "-" . $reportf5->month);
        $data['date']      = date('t', $strtotime);
        $last              = date('Y-m-t', $strtotime );
        $data['date_name'] = $this->day_name(date('D', strtotime($last)));
        // return response()->json($data);
        $pdf       = PDF::loadView('reportf5.bast', $data);
        $nama_file = 'BAST ' . $reportf5->number . '-' . $reportf5->customer_id;
        return $pdf->setPaper([0, 0, 650.00, 1008.00], 'potrait')->download($nama_file);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Kode Produsen","Produsen","No F5","Kode Distributor","Nama Distributor","Tahun","Bulan","Kode Provinsi","Provinsi","Kode Kabupaten","Kabupaten","Status","Kode Produk","Produk","Stok Awal","Penebusan","Penyaluran","Stok Akhir"];
        return Excel::download((new Download($data,$columns)), "Download Penyaluran DO.xls");
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'submited_date' || $value === 'year') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } elseif($value=="month"){
                if(strpos($request->get($value), ';') !== false){
                    $listMonth = explode(';', $request->get($value));
                    $query->whereIn($key, $listMonth);
                }else{
                    $query->where($key, '=', "{$request->get($value)}");
                }
            }else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    private function checkspjb($attributes)
    {

        $where['tb2.sales_org_id']    = $attributes['sales_org_id'];
        $where['tb2.customer_id']     = $attributes['customer_id'];
        $where['tb1.sales_office_id'] = $attributes['sales_office_id'];
        $where['tb1.sales_group_id']  = $attributes['sales_group_id'];
        $where['tb1.year']            = $attributes['year'];
        $where['tb1.month']           = $attributes['month'];
        $where['tb2.contract_type']   = 'operational';
        return ContractItem::getItemSPJBDownload($where)->first();
    }

    private function duplicatedata($model, $attributes)
    {
        $query = $model::where($attributes);

        $check = $query->first();

        return $check;
    }

    private function genUniqF5()
    {
        $maxNumber = DB::table('wcm_report_f5')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "F5" . $number;
    }

    private function genBastF5($number, $month, $year, $code)
    {

        return $number . "/BASTP/RG/" . $month . "/" . $year . "/" . $code;
    }

    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }

    private function day_name($hari)
    {

        switch ($hari) {
            case 'Sun':
                $name_of_day = "Minggu";
                break;

            case 'Mon':
                $name_of_day = "Senin";
                break;

            case 'Tue':
                $name_of_day = "Selasa";
                break;

            case 'Wed':
                $name_of_day = "Rabu";
                break;

            case 'Thu':
                $name_of_day = "Kamis";
                break;

            case 'Fri':
                $name_of_day = "Jum'at";
                break;

            case 'Sat':
                $name_of_day = "Sabtu";
                break;

            default:
                $name_of_day = "Tidak di ketahui";
                break;
        }

        return $name_of_day;

    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    private function makeTotalEksport($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'id'  => $val['id'],
                'qty' => $val['qty'],
            );
            // $datas[$val->id]=$temps;
            array_push($datas, $temps);
        }

        return $datas;
    }
    private function makeTotalCalc($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'qty' => 0,
                'id'  => $val->id,
            );
           array_push($datas,$temps);
        }
        return $datas;
    }

}
