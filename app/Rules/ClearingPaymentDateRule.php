<?php

namespace App\Rules;

use App\Models\Order;
use Illuminate\Contracts\Validation\Rule;

class ClearingPaymentDateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = parseDate($value);
        $order = Order::where("booking_code", request("booking_code"))->first();
        
        if (!@$order->submit_date || !@$order->payment_due_date ) {
            return false;
        }
        return $date >= parseDate($order->submit_date) && $date <= parseDate($order->payment_due_date);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'please select valid of :attribute';
    }
}
