<?php

namespace App\Rules;

use App\Models\Address;
use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class FarmerGroupSalesOfficeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $retail_code = request()->has('retail_id')
        ? request()->get('retail_id')
        : Arr::get($value, 'retail_code');

        $retail = Retail::where(function ($query) use ($retail_code) {
            $query->where("code", "{$retail_code}");

            if (is_numeric($retail_code)) {
                $query->orWhere("id", $retail_code);
            }

        })->first();

        if (is_null($retail)) {
            return false;
        }

        $customerAssign  = $retail->customerRetailerAssg()
            ->pluck('customer_id')
            ->unique()
            ->toArray();

        $customerAddress = Address::where('address_type', 'FORMAL')
            ->whereIn('customer_id', $customerAssign)
            ->pluck('sales_office_id')
            ->unique()
            ->toArray();

        $sales_unit_id = request()->has('sales_unit_id')
        ? request()->get('sales_unit_id')
        : Arr::get($value, 'sales_unit_id', 0);

        $salesOfficeUpload = sprintf('%04s', substr($sales_unit_id, 0, 2));
        $sameSalesOffice   = in_array($salesOfficeUpload, $customerAddress);

        if (!$sameSalesOffice) {
            $this->errorMessage = trans('messages.farmer-group-sales-office');
        }

        return $sameSalesOffice;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return property_exists($this, 'errorMessage') ? $this->errorMessage : ":attribute validation error";
    }
}
