<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use App\Models\SalesUnit;

class RetailCustomerSalesOfficeRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        $salesUnit = Arr::get($value, 'kecamatan_id');
        $customerId = Arr::get($value, 'customer_id');
        
        if (is_null($salesUnit)) {
            $this->errorMessage = trans('messages.required');
            return false;
        }

        $count = SalesUnit::where("id", "{$salesUnit}")->count();
        if ($count == 0) {
            $this->errorMessage = trans('messages.exists');
            return false;
        }
        
        $customer = DB::table('wcm_customer as a')
            ->leftjoin('wcm_address as b', function ($q) {
                $q->on('b.customer_id', '=', 'a.id')->where('b.address_type', 'FORMAL');
            })            
            ->where('a.id', "{$customerId}")
            ->first();
        $salesgroups = DB::table('wcm_sales_unit as a')
            ->leftjoin('wcm_sales_group as b', 'a.sales_group_id', '=', 'b.id')
            ->where('a.id', "{$salesUnit}")
            ->first();

        if ($customer->sales_office_id != $salesgroups->sales_office_id) {
            $this->errorMessage = trans('messages.retailer-sales-office');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
