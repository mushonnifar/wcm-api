<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ContractGovItem;

class PerbupSalesUnitOneActive implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $itemRequest)
    {
        //
        $check = ContractGovItem::getItemPerbup(
            ['tb1.year'=>$itemRequest['year'],
            'tb1.product_id'=>$itemRequest['product_id'],
            'tb1.month'=> intval($itemRequest['month']),
            'tb1.status'=>'y',
            'tb2.contract_type'=>'perbup',
            'tb1.sales_unit_id'=>$itemRequest['sales_unit_id']
            ])->where('tb2.sales_org_id','!=',$itemRequest['sales_org_id'])->first();
        if($check)
        {
            $this->errorMessage = "Kecamatan : {$check->sales_unit_id} ({$check->sales_unit_name}) , Product : {$check->product_id} ({$check->product_name}), masih berstatus aktif oleh produsen : {$check->sales_org_id}  ({$check->sales_org_name})";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
