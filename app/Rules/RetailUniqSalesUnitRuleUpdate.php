<?php

namespace App\Rules;

use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;

class RetailUniqSalesUnitRuleUpdate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $retail = Retail::where('name', @$value['name'])
            ->where('sales_unit_id', @$value['sales_unit_id'])
            ->where('uuid','!=', @$value['uuid'])
            ->exists();

        return !$retail;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nama Retail & Pemilik Sudah terdaftar di Kecamatan.';
    }
}
