<?php

namespace App\Rules;

use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;

class RetailOwnerUniqueInSalesUnit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    private $message;
    public function passes($attribute, $value)
    {
        if (!@$value["owner"]) {
            $this->message = "Owner " . trans("messages.required");
            return false;
        }

        if (!@$value["sales_unit_id"]) {
            $this->message = "Sales Unit ID " . trans("messages.required");
            return false;
        }


        return ! Retail::where('owner', $value['owner'])
                ->where('sales_unit_id', $value['sales_unit_id'])
                ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return @$this->message ? : trans("messages.retail-unique-owner");
    }
}
