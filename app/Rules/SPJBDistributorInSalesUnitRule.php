<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class SPJBDistributorInSalesUnitRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $this->errorMessage="";
        $query = DB::table('wcm_contract_item as a')
            ->leftjoin('wcm_contract as b', function ($q) use ($value) {
                $q->on('b.id', '=', 'a.contract_id');
            })
            ->where('a.month', intval($value['bulan']))
            ->where('a.product_id', $value['kode_jenis_produk'])
            ->where('a.sales_office_id', $value['kode_provinsi'])
            ->where('a.sales_group_id', $value['kode_kabupaten'])
            ->where('a.sales_unit_id', $value['kode_kecamatan'])
            ->where('a.status', 'y')
            ->where(function ($q) use ($value) {
                $q->Orwhere('b.number', '!=', $value['no_doc']);
                $q->Orwhere('b.customer_id', '!=', $value['kode_distributor']);
                $q->Orwhere('b.sales_org_id', '!=', $value['sales_organization']);
            })
            ->where('b.contract_type', 'asal')
            ->where('b.year', $value['tahun'])
            ->select('b.customer_id')
            ->first();
        if($query){
            $this->errorMessage=$query->customer_id;
            return false;            
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Distributor dan Product pada Kecamatan tersebut Sudah tersedia. ditributor : '.$this->errorMessage;
    }
}
