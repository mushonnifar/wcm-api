<?php

namespace App\Rules;

use App\Models\ContractItem;
use Illuminate\Contracts\Validation\Rule;

class SPJBItemRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $query = ContractItem::where($value)->exists();

        return $query;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'SPJB Item Tidak tersedia';
    }
}
