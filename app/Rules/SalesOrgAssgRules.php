<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SalesOrgAssgRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $request;
    public function __construct()
    {
        //
        $this->request = request();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $user = @$this->request->user();

        if (!$user) {
            return false;
        }

        // Roles Distributor
        if($user->roles->pluck('id')[0]=="4")
        {
            return true;
        }

        if($user->salesOrgAssign->pluck('sales_org_id')->contains(NULL))
        {
            return true;
        }

        return $user->salesOrgAssign ? $user->salesOrgAssign->pluck('sales_org_id')->contains($value) : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sales Organisation tidak sesuai User Management';
    }
}
