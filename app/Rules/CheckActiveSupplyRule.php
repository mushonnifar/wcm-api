<?php

namespace App\Rules;

use App\Models\SupplyPoint;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class CheckActiveSupplyRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $supplies = SupplyPoint::where(
            Arr::only(
                $value, ["sales_org_id","sales_office_id","sales_group_id",'plant_id', 'scnd_sales_group_id']
            )
        )
        ->pluck("status")
        ->unique();

        if (count($supplies) > 0) {
            return !$supplies->contains("y");
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Aktif Supply Point sudah tersedia.';
    }
}
