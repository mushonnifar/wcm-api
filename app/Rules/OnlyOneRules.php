<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class OnlyOneRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $uuid="";
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $table=$value['tables'];
        $find=$value['find'];
        $this->uuid=$value['uuid'];

        if($value['uuid']=="" or empty($value['uuid']))
        {
            return true;
        }

        $data = DB::table($table)->where($find,'!=',$value['uuid'])->where($attribute,$value[$attribute])->first();
        
        if(!$data){
            return true;
        }
        
        return false ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.duplicate');
    }
}
