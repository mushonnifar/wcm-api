<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use App\Models\Retail;
use App\Models\User;

class RetailSalesUnitExistSPJB implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $user = User::where('id',Auth::user()->id)->first();

        $filters = $user->filterRegional;

        $spjb = DB::table('wcm_contract_item as a')
            ->leftjoin('wcm_contract as b', function ($q) use ($value) {
                $q->on('b.id', '=', 'a.contract_id');
            })
            ->where('a.status', 'y')
            ->where('b.contract_type', 'asal')
            ->where('b.customer_id', $value['customer_id'])
            ->where('b.year', $value['year'])
            ->where('a.month', intval($value['month']));

            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $spjb = $spjb->whereIn("b.sales_org_id", $filters["sales_org_id"] );
                }
            }
        
        $spjb=$spjb->pluck('a.sales_unit_id')->unique();//dd($spjb);
        
        if(count($spjb)>0){
            $exists = $spjb->contains($value['sales_unit_id']);//(@$spjb, $value['sales_unit_id']);

            return $exists;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Kecamatan Pengecer Tidak DiJangkau';
    }
}
