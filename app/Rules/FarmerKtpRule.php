<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Farmer;

class FarmerKtpRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        if (is_null($value)) {
            $this->errorMessage = trans('messages.required');
            return false;
        }

        if (!is_numeric($value)) {
            $this->errorMessage = trans('messages.numeric');
            return false;
        }

        $count = Farmer::where("ktp_no", $value)->count();
        if ($count >= 1) {
            $this->errorMessage = trans('messages.exists');
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
