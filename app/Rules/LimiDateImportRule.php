<?php

namespace App\Rules;

use App\Models\LimitDate;
use Illuminate\Contracts\Validation\Rule;

class LimiDateImportRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = collect($params);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $salesOrgId = $this->params->get("sales_org_id");
        $salesOfficeId = $this->params->get("sales_office_id");
        $productId = $this->params->get("product_id", null);
        $minQty = $this->params->get("min_qty", 0);
        $maxQty = $this->params->get("max_qty", 0);
        $incoterm = $this->params->get("incoterm");

        $clause = [
            "sales_org_id" => $salesOrgId,
            "sales_office_id" => $salesOfficeId,
            // "product_id" => $productId,
            "incoterm" => $incoterm,
        ];
        

        $minLimit = LimitDate::where($clause)->pluck("min_qty")->unique();
        $maxLimit = LimitDate::where($clause)->pluck("max_qty")->unique();
        $valid = true;

        for($i = 0; $i<$minLimit->count();$i++) {
            if (
                (
                    $minQty >= $minLimit->get($i) &&
                    $minQty <= $maxLimit->get($i)
                ) || (
                    $maxQty >= $minLimit->get($i) && 
                    $maxQty <= $maxLimit->get($i)
                )
            ) {
                $this->message = trans("messages.inrange");
                $valid = false;
                break;
            }
        }

        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
