<?php

namespace App\Rules;

use App\Models\SalesGroup;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class OfficeGroupRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        $salesOffice = Arr::get($value, 'sales_office_id');
        $salesGroup = Arr::get($value, 'sales_group_id');
        $region = SalesGroup::where('id', $salesGroup)
                        ->where('sales_office_id', $salesOffice)
                        ->pluck('id')->toArray();

        if (!$region) {
            $this->errorMessage = trans('messages.group-sales-office');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
