<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;


class CustumerRetailRelationActive implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $customer_id=$value['customer_id'];
        $retail_id=$value['retail_id'];

        return DB::table("wcm_customer_retailer_assg as a")
                ->where("a.customer_id", $customer_id)
                ->where("a.retail_id", $retail_id)                
                ->where("a.status", 'y')                
                ->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Relasi Distributor dengan Retail Tidak Aktif';
    }
}
