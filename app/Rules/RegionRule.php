<?php

namespace App\Rules;

use App\Models\SalesGroup;
use App\Models\SalesUnit;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class RegionRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        $salesOffice = Arr::get($value, 'sales_office_id');
        $salesGroup = Arr::get($value, 'sales_group_id');
        $salesUnit = Arr::get($value, 'sales_unit_id');
        
        $regionGroupUnit = SalesUnit::where('id', $salesUnit)
                        ->where('sales_group_id', $salesGroup)
                        ->pluck('id')->toArray();

        if (!$regionGroupUnit) {
            $this->errorMessage = trans('messages.unit-sales-group');
            return false;
        }
        
        $regionOfficeGroup = SalesGroup::where('id', $salesGroup)
                        ->where('sales_office_id', $salesOffice)
                        ->pluck('id')->toArray();

        if (!$regionOfficeGroup) {
            $this->errorMessage = trans('messages.group-sales-office');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
