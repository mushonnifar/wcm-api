<?php

namespace App\Libraries;

use App\Http\Controllers\SAP\SalesOrderController;
use App\Jobs\CreateSalesOrder;
use App\Jobs\SendInvoice;
use App\Models\Bank;
use App\Models\HostToHostRemoteIP;
use App\Models\Order;
use App\Models\OrderDFStatus;
use App\Traits\TraitCheckLimit;
use App\Traits\TraitDisbursement;
use App\Traits\TraitPaymentInvoice;
use App\Traits\TraitPaymentLog;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class PaymentGateway
{
    use TraitPaymentInvoice, TraitPaymentLog, TraitDisbursement, TraitCheckLimit;

    const CASH = "Cash";
    const DF   = "Distributor Financing";
    const ERROR_CODE = [
        "00" => "Succeess",
        "B5" => "tagihan tidak ditemukan",
        "B8" => "Tagihan sudah dibayar",
        "89" => "RTO",
        "91" => "Link Down",
    ];

    abstract public function getRequest();

    abstract public function response(Collection $data);

    abstract public function getInquiryParams(): array;

    abstract public function getPaymentParams(): array;

    abstract protected function getParamCheckLimit(): array;

    abstract protected function getParamSendInvoice($data): array;

    abstract public function getParamDisbrusement(): array;

    abstract protected function setResponDisbrusement($params): array;

    abstract public function handleInvalidOrder(\Exception $e);
    abstract public function getParamAuthorize(Request $params): array;

    protected function doCreateSO($params)
    {
        $booking_kode = $params[$this->getBookingCode()];
        $amount       = $params[$this->getAmount()];

        // Order status berdasarkan Method pembayaran
        $order_status = $this->getPaymentMethod() == self::CASH ? "l" : "u";

        $order = $this->order;

        // Validation Amount Payment
        if ($this->getPriceAmount() != (float) $params->get($this->getAmount())) {
            throw new \Exception("Invalid Amount", 1);
        }

        $order->status           = $order_status;
        $order->billing_date     = Carbon::now();
        $bank = Bank::where("name", 'like', "%{$this->name}%")->first();
        if ($order->payment_method === self::CASH) {
            $order->bank_id = @$bank->id;
            $order->bank_payment = @$bank->id;
        }

        $order->billing_fulldate = Carbon::now();
        $order->save();

        $orderDF = OrderDFStatus::where("order_id", $order->id)->first();
        if ($orderDF) {
            $orderDF->status = "n";
            $orderDF->save();
        }
        /**
         * if payment method Distributor Financing
         * and Success send Invoice then insert to table wcm_order_df_status
         *
         */
        if ($this->getPaymentMethod() == self::DF) {
            // $invoice = $this->sendInvoice($order);
            // $orderDF = $this->writeOrderDFStatus($invoice);
            $order->bank_payment = @$bank->id;
            $order->save();
            SendInvoice::dispatch($order->bank_id, $order->id)->onQueue("high");
        }

        /**
         * Begin Create SO to SAP
         *
         */
        CreateSalesOrder::dispatch($booking_kode)->onQueue("high");
        // $order         = Order::find($order->id);
        // $order->status = "k";
        // $order->save();
    }

    public function isValidOrder()
    {
        if (property_exists($this, 'order') && $this->order ) {
            $paymentDueDate = Carbon::parse($this->order->payment_due_date);
            $now            = Carbon::now();


            return (
                (in_array($this->order->status,["l","u","c", "k" ])) ||
                ($now <= $paymentDueDate && $this->order->status == "y")
            );
        }

        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    protected function getPriceAmount(): float
    {
        //return ($order = $this->order) ? ($order->payment_method == self::DF ? $order->ppn * 1 : $order->total_price * 1) : 0.0;
        return ($order = $this->order) ? ($order->upfront_payment * 1) : 0.0;
    }

    public function getPaymentMethod()
    {
        return Str::contains(
            Str::lower($this->order->payment_method), Str::lower(self::CASH)
        ) ? self::CASH : self::DF;
    }

    public function getType()
    {
        return $this->type;
    }

    protected function getBookingCode()
    {
        return "booking_kode";
    }

    protected function getAmount()
    {
        return "amount";
    }

    public function authorize(Collection $params): bool
    {
        $bank = Bank::where("name", "like", "%{$this->name}%")
            ->first();

        $isValid = true;

        if ($bank) {
            $auth = HostToHostRemoteIP::where("bank_id", $bank->id)->get();
            if (!$auth->isEmpty()) {
                $ipAddress = $auth->pluck("ip_address");
                if ($params->has("ip_address") && !$ipAddress->isEmpty()) {
                    $isValid = ( $ipAddress
                            ->contains(
                                $params->get("ip_address")
                            ) || $ipAddress->contains("*"));
                }

                if ($params->has("password")) {
                    $isValid = $auth->pluck("password")
                        ->contains(
                            $params->get("password")
                        );
                }
                if ($isValid) {
                    $this->authorization = $auth;
                }
            }
        }

        return $isValid;
    }

    protected function checkPaidOrder()
    {
        $order = $this->order;
        // dd($this->getPaymentMethod(), $order->status);
        return in_array($order->status, ["u","l","k","c"]);

    }

    protected function getBankRequestAuth()
    {
        return strtolower($this->name) == "mandiri" ? ["scm","scm"] : null;
    }

    protected function getEndPointUrl()
    {
        $array = [
            //DEV
            // "inquiry" => [
            //     "BRI" => "http://192.168.188.32:8032/sap/h2h/bri_inq",
            //     "BNI" => "http://192.168.188.32:8032/sap/h2h/bni_inq",
            //     "BCA" => "http://192.168.188.32:8032/sap/h2h/bca_inq",
            //     "Mandiri" => "http://192.168.188.32:8032/sap/h2h/mandiri_inq",
            //     "BSB" => "http://192.168.188.32:8032/sap/h2h/sumsel_inq",
            // ],
            // "payment" => [
            //     "BRI" => "http://192.168.188.32:8032/sap/h2h/bri_pay",
            //     "BNI" => "http://192.168.188.32:8032/sap/h2h/bni_pay",
            //     "BCA" => "http://192.168.188.32:8032/sap/h2h/bca_pay",
            //     "Mandiri" => "http://192.168.188.32:8032/sap/h2h/mandiri_pay",
            //     "BSB" => "http://192.168.188.32:8032/sap/h2h/sumsel_pay",
            // ],
            // "disbursement" => [
            //     "BNI" => "http://192.168.188.32:8032/sap/h2h/bni_disbrst",
            //     "BRI" => "http://192.168.188.32:8032/sap/h2h/bri_disbrst",
            //     "Mandiri" => "http://192.168.188.32:8032/sap/h2h/mandiri_disbrst",
            // ],

            //PROD
            "inquiry" => [
                "BRI" => "http://192.168.188.43:8033/sap/h2h/bri_inq",
                "BNI" => "http://192.168.188.43:8033/sap/h2h/bni_inq",
                "BCA" => "http://192.168.188.43:8033/sap/h2h/bca_inq",
                "Mandiri" => "http://192.168.188.43:8033/sap/h2h/mandiri_inq",
                "BSB" => "http://192.168.188.43:8033/sap/h2h/sumsel_inq",
            ],
            "payment" => [
                "BRI" => "http://192.168.188.43:8033/sap/h2h/bri_pay",
                "BNI" => "http://192.168.188.43:8033/sap/h2h/bni_pay",
                "BCA" => "http://192.168.188.43:8033/sap/h2h/bca_pay",
                "Mandiri" => "http://192.168.188.43:8033/sap/h2h/mandiri_pay",
                "BSB" => "http://192.168.188.43:8033/sap/h2h/sumsel_pay",
            ],
            "disbursement" => [
                "BNI" => "http://192.168.188.43:8033/sap/h2h/bni_disbrst",
                "BRI" => "http://192.168.188.43:8033/sap/h2h/bri_disbrst",
                "Mandiri" => "http://192.168.188.43:8033/sap/h2h/mandiri_disbrst",
            ],
        ];

        return @$array[$this->type][$this->name];
    }

    protected function handleEndPoint($params)
    {
        if ($this->order && in_array(@$this->order->status, ['o','d','x','s'])) {
            throw new Exception("B5");
        }

        return $this->requestToSap($params);
    }

    protected function requestToSap($params) {
        $params = $params->toArray();
        if ($this->getType() === "disbursement" && in_array($this->name, ["BNI", "BRI"])) {
            $params["transaction_date"] = parseDate($params["transaction_date"])->format("d-m-Y");
        }

        $url = $this->getEndPointUrl();
        $client = new RestClient();
        $response = $client->postJSON($url, $params);
        return [
            "sender" => [
                "name" => "SAP",
                // "host" => "192.168.188.32:8032" //DEV
                "host" => "192.168.188.43:8033" //PROD
            ],
            "response" => $response
        ];
    }

}