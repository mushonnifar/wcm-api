<?php

namespace App\Libraries;

use PhpOffice\PhpSpreadsheet\Shared\Date;

class ExcelUtils
{
    public static function excelToDate($value)
    {
        if (is_null($value)) {
            return null;
        }

        if (!$value instanceof DateTime) {

            $value = is_string($value) ? Date::stringToExcel(preg_replace('@[^\/0-9]@', '/', $value)) : $value;
        }

        return Date::excelToDateTimeObject($value)->format("Y-m-d");
    }

    public static function excelToTime($value)
    {
        if (is_null($value)) {
            return null;
        }

        return Date::excelToDateTimeObject($value)->format("H:i:s");
    }
}
