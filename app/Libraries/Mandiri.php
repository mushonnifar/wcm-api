<?php

namespace App\Libraries;

use App\Models\CustomerSalesArea;
use App\Models\Order;
use App\Rules\CustomerExistsRule;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class Mandiri extends PaymentGateway
{
    private $parseKey = "testingpupukindonesia";
    public function __construct(string $type = 'payment')
    {
        $this->type = $type;
        $this->name = "Mandiri";
    }

    public function getRequest()
    {
        return $this->type == "payment" ? $this->getPaymentParams() : $this->getInquiryParams();
    }

    public function response(Collection $data)
    {
        $params = $data->only($this->getRequest());
        $errorCode = "00";
        try {

            $order = Order::where("booking_code", $params->get("booking_kode"))->first();
            $this->order = $order;

            if (!$this->isValidOrder()) {
                return $this->handleEndPoint($params);

            }

            if ($this->checkPaidOrder()) {
                throw new \Exception("B8");
            }


            if ($this->type === "payment") {
                $this->doCreateSO($params);
            }

        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
        }

        $response = [
            "trxid"        => $params->get("trxid"),
            "status"       => $errorCode, #$order->status,
            "booking_code" => @$order->booking_code,
            "so"           => $errorCode == "00" ? @$order->number :"",
            "distributor"  => $errorCode == "00" ? @$order->customer->full_name:"",
            "tujuan"       => $errorCode == "00" ? @$order->salesgroup->name:"",
            "amount"       => $errorCode == "00" ? (string) $this->getPriceAmount():"",
            "denda"        => (string) 0,
            "product"      => null,
            "commCodeDesc" => $errorCode == "00" ? @$order->sales_org_id . " " . $order->salesorg->name : "",

        ];


        if ($errorCode == "00" && @$order->orderItems) {
            $item    = $order->orderItems->first();
            $product = [$item->product->name, $item->qty, substr($item->plant->code, -3)];

            $products = $order->orderItems->map(function ($item) use (&$response) {
                $product = [$item->product->name, $item->qty, substr($item->plant->code, -3)];
                return $product;
            });

            if ($this->type === "inquiry") {
                Arr::set($response, "gudang", $item->plant->name);
            } else {
                Arr::set($response, "prodTonaseWarehouse1", implode(" ", $product));
            }

            Arr::set($response, "product", $products);
        }
        
        return $response;
    }

    public function getInquiryParams(): array
    {
        return ["trxid", "booking_kode", "channel"];
    }

    public function getPaymentParams(): array
    {
        return ["trxid", "booking_kode", "channel", "amount"];
    }

    public function getValidationRule()
    {
        $rule = [
            "trxid"        => "required",
            "booking_kode" => "required",
            "channel"      => "required",
        ];

        if ($this->getType() == "payment") {
            $rule += [
                "amount" => "required",
            ];
        }

        return $rule;
    }

    /**
     * Gets the parameter send invoice.
     *
     * @param      <type>  $data   The data
     *
     * @return     array   The parameter send invoice.
     */
    protected function getParamSendInvoice($data): array
    {
        $sales_area = CustomerSalesArea::getSalesAreaOrder(
            $data->customer_id,
            $data->sales_org_id
        );
        $req_uuid = sprintf(
            "sendinvpihc%s%s%s",
            date("Ymd"),
            str_random(4),
            $data->sales_org_id
        );
        $signature = hash("sha1", $req_uuid.$data->sales_org_id.$data->customer_id.$data->number.$this->parseKey);
        return [
            "rq_uuid"         => $req_uuid,
            "timestamp"       => Carbon::parse($data->created_at)->format("Y-m-d H:i:s"),
            "rq_datetime"     => Carbon::now()->format("Y-m-d H:i:s"),
            //"password"        => "s3m3n1ndCo00",
            "amount"          => (string) sprintf("%.2f",$data->total_price_before_ppn),
            "ccy"             => "IDR",
            "doc_no"          => $data->number,
            "due_date"        => Carbon::parse($data->df_due_date)->format("d/m/Y"),
            "issue_date"      => Carbon::parse($data->payment_due_date)->format("d/m/Y"),
            "comm_code"       => $data->sales_org_id,
            "term_of_payment" => $sales_area->term_of_payment,
            "member_code"     => $data->customer_id,
            "signature"       => $signature,
            "buyer_account"   => "",
            "seller_account"  => "",
        ];
    }

    protected function getParamCheckLimit(): array
    {
        $salesOrgID = request()->get("sales_org_id");
        $customerID = request()->get("customer_id");
        $req_uuid = sprintf(
            "limitpihc%s%s%s",
            date("Ymd"),
            str_random(4),
            $salesOrgID
        );
        $signature = hash("sha1", $req_uuid.$salesOrgID.$customerID.$this->parseKey);
        return [
            "rq_uuid"      => $req_uuid,
            "timestamp"    => Carbon::now()->format("Y-m-d H:i:s"),
            "rq_datetime" => Carbon::now()->format("Y-m-d H:i:s"),
            // "password"     => env("mandiri", "s3m3n1ndCo00"),
            "ccy"          => "IDR",
            "partner_code" => "",
            "comm_code"    => $salesOrgID,
            "member_code"  => $customerID,
            "signature"    => $signature,
        ];
        
    }

    public function getParamDisbrusement(): array
    {

        return [
            "rq_uuid"          => "required",
            "doc_no"           => "required",
            "rq_datetime"      => "required",
            "comm_code"        => "required|exists:wcm_sales_org,id",
            "member_code"      => ["required", new CustomerExistsRule],
            "payment_ref"      => "required",
            "amount"           => "required",
            "ccy"              => "required",
            "transaction_date" => "required",
            "error_code"       => "required",
            "error_msg"        => "required",
            "signature"        => "required",
        ];
    }

    protected function setResponDisbrusement($request): array
    {
        return [
            "rq_uuid"    => $request->get("rq_uuid"),
            "timestamp"  => Carbon::now()->format("Y-m-d H:i:s"),
            "error_code" => $request->get("error_code"),
            "error_msg"  => $request->get("error_msg"),
            "message" => ""
        ];
    }

    public function handleInvalidOrder(\Exception $e)
    {
        $notFoundRespon = '{"trxid":"12162QC3002001","status":"B7","booking_code":"","so":"","distributor":"","tujuan":"","amount":"","denda":"","product":"","gudang":"","commCodeDesc":""}';

        return response()->json(json_decode($notFoundRespon),500);
    }

    public function getParamAuthorize(Request $params): array 
    {
        return [
            "ip_address" => $params->ip(),
        ];
    }
}
