<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesUnit extends Model {

    protected $table = 'wcm_sales_unit';
    protected $fillable = [
        'id', 'code', 'name', 'sales_office_id', 'sales_group_id', 'created_by', 'updated_by',
    ];

    public $incrementing = false;
    
    public function salesGroup(){
        return $this->belongsTo("App\Models\SalesGroup");
    }
}
