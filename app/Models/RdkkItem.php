<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RdkkItem extends Model {

    protected $table = 'wcm_rdkk_item';
    protected $fillable = [
        'rdkk_id', 'product_id', 'required_qty', 'status',
        'create_at', 'update_at', 'created_by', 'updated_by',
        'approved_qty','approved_by','approved_date'
    ];

    public static function RuleCreate() {
        $rules = [
            'product_id' => 'required|exists:wcm_product,id',
            'required_qty' => 'sometimes|numeric',
            'approved_qty' => 'sometimes|numeric',
            'status' => 'required'
        ];
        return $rules;
    }

    public static function RuleUpdate() {
        $rules = [
            'required_qty' => 'sometimes|numeric',
            'approved_qty' => 'sometimes|numeric',
            'status' => 'required'
        ];
        return $rules;
    }

    public static function getSumRdkk($where = [], $whereInSales = [], $whereInRetail = []) {
        $query = DB::table('wcm_rdkks AS tb1')
                ->join('wcm_rdkk_item AS tb2', 'tb2.rdkk_id', '=', 'tb1.id')
                ->join('wcm_planting_period AS tb3', 'tb1.planting_period_id', '=', 'tb3.id')
                ->select('tb1.sales_unit_id', 'tb2.product_id', DB::RAW('SUM ( tb2.required_qty ) AS qty'))
                ->where($where)
                ->whereIn('tb1.sales_unit_id', $whereInSales)
                ->whereIn('tb1.retail_id', $whereInRetail)
                ->groupBy('tb1.sales_unit_id', 'tb2.product_id')
                ->get();

        return $query;
    }

    public function rdkk() {
        return $this->belongsTo("App\Models\Rdkk", "rdkk_id", "id");
    }

    public function scopeRdkkItemByPeriode($query, $periode)
    {
        $now = Carbon::now();        
        $query->whereHas("rdkk", function($q) use ($periode) {
                $q->where("planting_period_id", $periode);
            })
        ->select('*');
    }   


    // public static function boot()
    // {
    //     parent::boot();

    //     static::updating(function ($model) {
    //         $parent = Rdkk::find($model->rdkk_id);
    //         $parent->touchStatus();
    //     });
    //     static::saving(function ($model) {
    //         $parent = Rdkk::find($model->rdkk_id);
    //         $parent->touchStatus();
    //     });
    // }

}
