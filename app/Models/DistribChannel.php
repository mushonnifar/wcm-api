<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistribChannel extends Model {

    protected $table = 'wcm_distrib_channel';
    protected $fillable = [
        'id', 'name', 'code', 'desc', 'status', 'created_by', 'updated_by',
    ];

    public $incrementing = false;
}
