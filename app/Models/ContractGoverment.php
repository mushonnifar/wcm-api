<?php

namespace App\models;

//use App\Rules\OfficeGroupRule;
use App\Rules\RegionRule;
use App\Rules\PerbupNumberRule;
use App\Rules\PermentanRule;
use App\Rules\RayonisasiRule;
use App\Rules\SalesOrgAssgRules;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContractGoverment extends Model
{

    protected $table    = 'wcm_contract_goverment';
    protected $fillable = [
        'number', 'contract_type', 'sales_org_id', 'status', 'year',
        'from_date', 'thru_date', 'create_at', 'update_at', 'created_by', 'updated_by',
    ];
    public static function PermentanRuleCreate()
    {
        $rules = [
            'number'        => 'required',
            'contract_type' => 'required',
            'sales_org_id'  => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'year'          => 'required|numeric',
            'rayonisasi'    => [
                new RayonisasiRule,
            ],
            // 'permentan'     => new PermentanRule,
        ];
        return $rules;
    }

    public static function PermentanRuleUpdate()
    {
        $rules = [
            'number'          => 'required',
            'contract_type'   => 'required',
            'sales_org_id'    => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'year'            => 'required',
        ];
        return $rules;
    }

    public static function headerUpdateStatus()
    {
        $rules =[
            'permentan'       => [new PermentanRule],
        ];
        return $rules;
    }

    public static function perbupRuleCreate()
    {
        $rules = [
//            'number'     => 'required|unique:wcm_contract_goverment,number',
            // 'region'     => [
            //     new RegionRule,
            // ],
            'sales_org_id' => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            // 'document'  => [
            //     new PerbupNumberRule,
            // ],
        ];
        return $rules;
    }

    public static function pergubRuleCreate()
    {
        $rules = [
            'rayonisasi'   => [
                new RayonisasiRule,
            ],
            'sales_org_id' => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'year'         => 'required',

        ];

        return $rules;
    }

    public static function pergubStore()
    {
        $rules = [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'month'           => 'required|numeric',
            'year'            => 'required|numeric',
            'initial_qty'     => 'numeric|min:0',
        ];
        return $rules;
    }

    // public static function ruleUpdate() {
    //     $rules = [
    //         'telp_no' => 'numeric',
    //         'fax_no' => 'numeric',
    //         'latitude' => 'numeric',
    //         'longitude' => 'numeric',
    //     ];
    //     return $rules;
    // }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function ruleFileImport()
    {
        $rules = [
            'file' => 'required',
        ];
        return $rules;
    }
    public static function getAllPermentan($where = [],$exported=false)
    {
        $query = DB::table('wcm_contract_goverment AS tb1')
            ->join('wcm_sales_org AS tb3', 'tb1.sales_org_id', '=', 'tb3.id');
        if($exported){
            $query->select('tb1.number','tb3.name','tb1.year','tb1.created_at',DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"));
        }else{
            $query->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb3.id as sales_id', 'tb3.name as sales_name',
                'tb1.year', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by','tb1.status',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            );
        }
        $query->where($where);

        return $query;
    }

    public static function getHeaderPerbup($where=[], $exported=false)
    {
        $query = DB::table('wcm_contract_goverment AS tb1')
            ->leftJoin(DB::raw('( SELECT DISTINCT contract_gov_id, sales_office_id, sales_group_id FROM wcm_contract_gov_item ) AS tb2'), 'tb1.id', '=', 'tb2.contract_gov_id')
            ->leftJoin('wcm_sales_org AS tb3', 'tb1.sales_org_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_office AS tb4', 'tb2.sales_office_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_group AS tb5', 'tb2.sales_group_id', '=', 'tb5.id');
        if($exported){
            $query->select('tb1.number', 'tb3.name AS sales_org_name','tb4.name AS sales_office_name',
                 'tb5.name AS sales_group_name','tb1.year', DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"));
        }else{
            $query->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.sales_org_id', 'tb3.name AS sales_org_name',
                'tb1.year', 'tb2.sales_office_id', 'tb4.name AS sales_office_name', 'tb2.sales_group_id',
                'tb5.name AS sales_group_name', 'tb1.status',
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            );
        }
        $query->where('tb1.contract_type', 'perbup')
            ->where($where);

        return $query;
    }

    public static function getHeaderPergub($where = [],$exported=false)
    {
        # code...
        $query = DB::table('wcm_contract_goverment AS tb1')
            ->leftJoin(DB::raw('( SELECT DISTINCT contract_gov_id, sales_office_id FROM wcm_contract_gov_item ) AS tb2'), 'tb2.contract_gov_id', '=', 'tb1.id')
            ->leftJoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb1.sales_org_id')
        // ->leftjoin('wcm_sales_group AS tb4', function ($join) {
        //     $join->on('tb4.id', 'tb2.sales_group_id');
        //     $join->on('tb4.sales_office_id', 'tb2.sales_office_id');
        // })
            ->leftjoin('wcm_sales_office AS tb5', 'tb5.id', '=', 'tb2.sales_office_id');
        if($exported){
            $query->select('tb1.number','tb5.name as provinsi_name','tb3.name as produsen_name','tb1.year','tb1.created_at',DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"));
        }else{
            $query->select('tb1.id', 'tb1.uuid', 'tb1.year', 'tb1.number as pergub', 'tb5.id as provinsi_id', 'tb5.name as provinsi_name', 'tb1.sales_org_id as produsen_id', 'tb3.name as produsen_name', DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"),'tb1.status',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            );
        }
        $query->where('tb1.contract_type', 'pergub')
            ->where($where);

        return $query;
    }

    public function items()
    {
        return $this->hasMany("App\Models\ContractGovItem", "contract_gov_id", "id");
    }

    public function touchStatus()
    {
        $childsStatus = $this->items()->pluck('status')->unique();

        if ($childsStatus->count() === 1) {
            $status = $childsStatus->first();
        } elseif ($childsStatus->count() === 2) {
            $status = "z";
        }

        $this->update(compact('status'));

    }
}
