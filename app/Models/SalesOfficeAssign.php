<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOfficeAssign extends Model
{
    //
    protected $table = 'wcm_sales_office_assg';
    protected $fillable = [
        'sales_area_id','sales_office_id','from_date','thru_date','status','created_by','updated_by'];

    public function salesoffice()
    {
        return $this->hasOne("App\Models\SalesOffice", "id", "sales_office_id");
    }
}
