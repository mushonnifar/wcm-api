<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderPartnerFunc extends Model {

    protected $table = 'wcm_order_partner_functions';
    protected $fillable = [
        'order_id', 'customer_id', 'partner_id', 'ident_name_funct', 'name_funct', 'desc_funct', 'status', 'created_by', 'updated_by'
    ];

}
