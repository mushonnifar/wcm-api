<?php

namespace App\Models;

use App\Rules\LimitDateRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LimitDate extends Model {

    //
    protected $table = 'wcm_limit_due_date';
    protected $fillable = [
        'sales_org_id','sales_office_id', 'incoterm', 'product_id', 
        'min_qty', 'max_qty', 'duration_date', 'created_by', 'updated_by',
    ];

    public static function ruleCreate() {
        $rules = [
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'incoterm' => 'required',
            'min_qty' => 'required|numeric',
            'max_qty' => 'required|numeric',
            'duration_date' => 'required|integer',
            'product_id' =>'nullable|exists:wcm_product,id',
            'sales_org_id' => ["required" ,new LimitDateRule()],
        ];
        return $rules;
    }

    public static function ruleUpdate($param) {
        $rules = [
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'incoterm' => 'required',
            'min_qty' => 'required|numeric',
            'max_qty' => 'required|numeric',
            'duration_date' => 'required|integer',
            'product_id' =>'nullable|exists:wcm_product,id',
            'sales_org_id' => ["required" ,new LimitDateRule($param)],
        ];
        return $rules;
    }

    public static function getAll($where = []) {
        $query = DB::table('wcm_limit_due_date as tb1')
                ->leftjoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
                ->leftJoin('wcm_sales_office AS tb3', 'tb1.sales_office_id', 'tb3.id')
                ->leftJoin('wcm_product AS tb4', 'tb1.product_id', 'tb4.id')
                ->select(
                    'tb1.uuid', 'tb1.sales_org_id', 'tb1.min_qty', 'tb1.max_qty', 
                    'tb1.sales_office_id', 'tb1.product_id',
                    'tb1.duration_date','tb1.incoterm', 'tb2.name as sales_org_name', 
                    'tb3.name as sales_office_name','tb4.name as product_name'
                )
                ->where($where);
        return $query;
    }

    public static function getExportedColumns()
    {
        return [
            'tb2.name as Produsen',
            'tb1.sales_office_id AS Provinsi',
            'tb1.incoterm',
            'tb4.name as Produk',
            'tb1.min_qty AS Minimum Order',
            'tb1.max_qty AS Maximum Order',
            'tb1.duration_date as Jumlah Hari',
        ];
    }

}
