<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PartnerFunction extends Model {

    protected $table = 'wcm_partner_function';
    protected $fillable = [
        'customer_id', 'customer_sales_area_id', 'partner_type', 
        'partner_type_desc',
    ];

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
}
