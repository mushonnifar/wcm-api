<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrgUserAssg extends Model
{
    //
    protected $table = 'sales_org_user_assg';
    protected $fillable = [
	'sales_org_id',	'created_at',	'updated_at',	'created_by',	'updated_by',	'user_id',
    ];

}
