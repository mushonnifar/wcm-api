<?php

namespace App\Models;

use App\Rules\OwnedSalesOrg;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PricingCondition extends Model {

    protected $table = 'wcm_pricing_condition';
    protected $fillable = [
        'sales_org_id', 'sales_group_id', 'sales_unit_id', 'product_id', 'delivery_method_id',
        'sales_office_id', 'sales_division_id', 'distrib_channel_id', 'product_desc', 'condition_type', 'calculate_type',
        'amount', 'current', 'uom', 'per_uom', 'valid_from', 'valid_to', 'sap_tax_classification',
        'level', 'status', 'created_by', 'updated_by',
    ];

    public static function ruleCreate($level) {
        $rulesBasic = [
            'condition_type' => 'required',
            'amount' => 'required',
            'uom' => 'required|in:TON,LITER',
            'per_uom' => 'required',
            'valid_from' => 'required|date',
            'valid_to' => 'required|date',
            'level' => 'required|in:1,2,10',
            'status' => 'required',
            'current' => 'required|in:IDR'
        ];
        $rulesType = [
            1 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_group_id' => 'required|exists:wcm_sales_group,id',
                'sales_unit_id' => 'required|exists:wcm_sales_unit,id',
                'product_id' => 'required|exists:wcm_product,id',
                'delivery_method_id' => 'required|exists:wcm_delivery_method,id',
            ],
            2 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_group_id' => 'required|exists:wcm_sales_group,id',
                'product_id' => 'required|exists:wcm_product,id',
                'delivery_method_id' => 'required|exists:wcm_delivery_method,id',
            ],
            3 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_group_id' => 'required|exists:wcm_sales_group,id',
                'product_id' => 'required|exists:wcm_product,id',
            ],
            4 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_group_id' => 'required|exists:wcm_sales_group,id',
            ],
            5 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_office_id' => 'required|exists:wcm_sales_office,id',
                'product_id' => 'required|exists:wcm_product,id',
                'delivery_method_id' => 'required|exists:wcm_delivery_method,id',
            ],
            6 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_office_id' => 'required|exists:wcm_sales_office,id',
                'product_id' => 'required|exists:wcm_product,id',
            ],
            7 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_office_id' => 'required|exists:wcm_sales_office,id',
            ],
            8 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'sales_division_id' => 'required|exists:wcm_sales_division,id',
            ],
            9 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
            ],
            10 => [
                'sales_org_id' => ['required', new OwnedSalesOrg],
                'product_id' => 'required|exists:wcm_product,id',
                'distrib_channel_id' => 'required|in:00',
            ],
        ];

        $rules = array_merge($rulesBasic, @$rulesType[$level] ?: []);
        return $rules;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getAll($where = []) {
        $query = DB::table('wcm_pricing_condition AS tb1')
                ->leftJoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
                ->leftJoin('wcm_sales_group AS tb3', 'tb1.sales_group_id', '=', 'tb3.id')
                ->leftJoin('wcm_sales_unit AS tb4', 'tb1.sales_unit_id', '=', 'tb4.id')
                ->leftJoin('wcm_product AS tb5', 'tb1.product_id', '=', 'tb5.id')
                ->leftJoin('wcm_delivery_method AS tb6', 'tb1.delivery_method_id', '=', 'tb6.id')
                ->leftJoin('wcm_sales_office AS tb7', 'tb1.sales_office_id', '=', 'tb7.id')
                ->leftJoin('wcm_sales_division AS tb8', 'tb1.sales_division_id', '=', 'tb8.id')
                ->leftJoin('wcm_distrib_channel AS tb9', 'tb1.distrib_channel_id', '=', 'tb9.id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.product_desc', 'tb1.condition_type', 'tb1.calculate_type',
                        DB::raw("FORMAT(tb1.amount , 'N2', 'de-de') as amount"),
                        'tb1.current', 'tb1.uom', 'tb1.per_uom', 'tb1.level', 'tb1.sap_tax_classification', 'tb1.sales_org_id', 
                        'tb2.name AS sales_org_name', 'tb1.sales_group_id', 'tb3.name AS sales_group_name', 'tb1.sales_unit_id', 
                        'tb4.name AS sales_unit_name', 'tb1.product_id', 'tb5.name AS product_name', 'tb1.delivery_method_id', 
                        'tb6.name AS delivery_method_name', 'tb1.sales_office_id', 'tb7.name AS sales_office_name', 
                        'tb1.sales_division_id', 'tb8.name AS sales_division_name', 'tb1.distrib_channel_id', 
                        'tb9.name AS distrib_channel_name', DB::raw("CONVERT ( VARCHAR, tb1.valid_from, 105 ) as valid_from"), 
                        DB::raw("CONVERT ( VARCHAR, tb1.valid_to, 105 ) as valid_to"), 'tb1.status', DB::raw("
                        (CASE  
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by', 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"), 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                )
                ->where($where);

        return $query;
    }

    public static function getBySalesGroup($sales_group_id) {
        $query = DB::table('wcm_pricing_condition AS tb1')
                ->leftJoin('wcm_delivery_method AS tb2', 'tb1.delivery_method_id', '=', 'tb2.id')
                ->select(DB::raw('DISTINCT tb1.delivery_method_id AS id'), 'tb2.name')
                ->where('tb1.sales_group_id', $sales_group_id)
                ->get();

        return $query;
    }

    public static function getExportedColumns() {
        return [
            'tb2.name as Produsen',
            'tb3.name as Kabupaten',
            'tb4.name as Kecamatan',
            'tb5.name as Product',
            'tb6.name as Incoterm',
            'tb1.condition_type as Kondisi',
            'tb1.amount',
            'tb1.current',
            'tb1.uom as UOM',
            'tb1.per_uom as Per UOM',
            'tb1.valid_from as Valid From',
            'tb1.valid_to as Valid To',
            DB::raw("
            (CASE
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status"),
        ];
    }
}
