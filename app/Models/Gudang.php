<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gudang extends Model {

    public static function getAll($where = []) {
        $query = DB::table('wcm_customer_sales_area as tb1')
                ->join('wcm_partner_function as tb2', function($q) {
                    $q->on('tb1.id', '=', 'tb2.customer_sales_area_id')
                            ->where('tb2.partner_type', '=', 'WE')
                            ->whereRaw("tb2.customer_id like '400%'");
                })
                ->join('wcm_customer as tb3', 'tb1.customer_id', '=', 'tb3.id')
                ->join('wcm_customer as tb4', 'tb2.customer_id', '=', 'tb4.id')
                ->join('wcm_sales_area as tb5', 'tb1.sales_area_id', '=', 'tb5.id')
                ->leftJoin('wcm_address as tb6', function($q) {
                    $q->on('tb3.id', '=', 'tb6.customer_id')->where('tb6.address_type', '=', 'FORMAL');
                })
                ->leftJoin('wcm_sales_unit as tb7', 'tb6.sales_unit_id', '=', 'tb7.id')
                ->leftJoin('wcm_sales_group as tb8', 'tb6.sales_group_id', '=', 'tb8.id')
                ->leftJoin('wcm_sales_office as tb9', 'tb6.sales_office_id', '=', 'tb9.id')
                ->select('tb2.id', 'tb2.uuid', 'tb2.customer_id', 'tb4.uuid AS customer_uuid', 'tb4.full_name AS customer_name',
                        'tb1.sales_area_id', 'tb1.term_of_payment', 'tb1.top_dp', 'tb1.tax_classification', 'tb1.pph22', 
                        'tb1.top_dp_uom', 'tb4.owner','tb6.address', 'tb6.tlp_no', 'tb6.fax_no', 'tb7.name as sales_unit_name', 
                        'tb8.name as sales_group_name', 'tb9.name as sales_office_name', 'tb5.sales_org_id', 'tb3.status', 
                        'tb3.created_by', 'tb3.updated_by', 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb3.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb3.created_at, 108 )) as created_at"), 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb3.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb3.updated_at, 108 )) as updated_at")
                )
                ->where($where);

        return $query;
    }

    public static function getGudangShipToParty($sales_org, $customer) {
        $query = DB::table('wcm_customer_sales_area as tb1')
                ->join('wcm_partner_function as tb2', 'tb1.id', '=', 'tb2.customer_sales_area_id')
                ->join('wcm_customer as tb3', 'tb1.customer_id', '=', 'tb3.id')
                ->join('wcm_customer as tb4', 'tb2.customer_id', '=', 'tb4.id')
                ->join('wcm_sales_area as tb5', 'tb1.sales_area_id', '=', 'tb5.id')
                ->select('tb4.id AS customer_id', 'tb4.full_name AS customer_name', 'tb2.partner_type', 'tb2.partner_type_desc')
                ->where(['tb3.id' => $customer, 'tb5.sales_org_id' => $sales_org, 'tb2.partner_type' => 'WE'])
                ->get();

        return $query;
    }

    public static function getExportedColumns() {
        return [
            'tb5.sales_org_id AS Produsen', 
            'tb4.full_name AS Distributor',
            'tb2.customer_id AS Kode', 
            'tb4.owner AS Nama',
            'tb6.address AS Alamat', 
            'tb9.name as Provinsi', 
            'tb8.name as Kabupaten', 
            'tb7.name as Kecamatan', 
            'tb6.tlp_no', 
            'tb6.fax_no', 
        ];
    }
}
