<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class PlantAssg extends Model
{
    //
    protected $table = 'wcm_plant_assg';
    protected $fillable = [
        'plant_id','sales_org_id','distrib_channel_id','from_date','thru_date','status','created_by','update_by'
    ];
}
