<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InitialStockItem extends Model {

    //
    protected $table = 'wcm_initial_stock_item';
    protected $fillable = [
        'initial_stock_id', 'product_id', 'qty', 'distributed_qty', 'status',
        'created_by', 'updated_by', 'created_at', 'updated_at',
    ];


    public function product ()
    {
    	return $this->hasOne('App\Models\Product', 'id','product_id');
    }

    public static function stokforf6($where=[])
    {
        $query = DB::table("wcm_initial_stock_item as tb1")
                    ->leftJoin('wcm_initial_stocks as tb2','tb2.id','=','tb1.initial_stock_id')
                    ->select('tb1.*','tb2.sales_org_id','tb2.customer_id','tb2.sales_office_id','tb2.sales_group_id','tb2.sales_unit_id','tb2.retail_id')
                    ->where($where);

         return $query;
    }

}
