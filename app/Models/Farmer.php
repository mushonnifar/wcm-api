<?php

namespace App\Models;

use App\Rules\SalesUnits;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Farmer extends Model
{

    protected $table    = 'wcm_farmer';
    protected $fillable = [
        'code', 'name', 'sales_unit_id', 'address', 'farmer_group_id',
        'telp_no', 'hp_no', 'ktp_no', 'ktp_valid_date', 'village', 'sub_sector_id',
        'commodity_id', 'latitude', 'longitude', 'land_area',
        'status', 'created_by', 'updated_by',
    ];

    public static function ruleCreate()
    {
        $rules = [
            'name'            => 'required',
            'farmer_group_id' => 'required|exists:wcm_farmer_groups,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'ktp_no'          => 'nullable|numeric', // required|unique:wcm_farmer,ktp_no|
            // 'village' => 'required',
            'address'         => 'required',
            'status'          => 'required',
            'telp_no'         => 'nullable|numeric',
            'fax_no'          => 'nullable|numeric',
            'latitude'        => 'numeric',
            'longitude'       => 'numeric',
        ];
        return $rules;
    }
    public static function ruleUpdate2()
    {
        $rules = [
            'name'            => 'required',
            'farmer_group_id' => 'required|exists:wcm_farmer_groups,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'ktp_no'          => 'nullable|numeric',
            'village'         => 'nullable',
            'address'         => 'required',
            'status'          => 'required',
            'telp_no'         => 'nullable|numeric',
            'fax_no'          => 'nullable|numeric',
            'latitude'        => 'numeric',
            'longitude'       => 'numeric',
        ];
        return $rules;
    }
    public static function ruleCreateBulk()
    {
        $rules = [
            'name'              => 'required',
            'farmer_group_code' => 'required|exists:wcm_farmer_groups,code',
            'sales_unit_id'     => ["required", new SalesUnits],
            'ktp_no'            => 'required|unique:wcm_farmer,ktp_no|numeric',
            'village'           => 'required',
            'address'           => 'required',
            'status'            => 'required',
            'telp_no'           => 'numeric',
            'fax_no'            => 'numeric',
            'latitude'          => 'numeric',
            'longitude'         => 'numeric',
        ];
        return $rules;
    }

    public static function ruleUpdate()
    {
        $rules = [
            'telp_no'   => 'numeric',
            'fax_no'    => 'numeric',
            'latitude'  => 'numeric',
            'longitude' => 'numeric',
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getAll($where = [])
    {
        $query = DB::table('wcm_farmer AS tb1')
            ->join('wcm_farmer_groups AS tb2', 'tb1.farmer_group_id', '=', 'tb2.id')
            ->join('wcm_retail AS tb3', 'tb2.retail_id', '=', 'tb3.id')
            ->join('wcm_sales_unit AS tb4', 'tb1.sales_unit_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_org AS tb6', 'tb2.sales_org_id', '=', 'tb6.id')
            ->join('wcm_sales_group AS tb7', 'tb4.sales_group_id', '=', 'tb7.id')
            ->join('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id');

        if (isset($where["tb9.uuid"])) {
            $query
                ->join('wcm_customer_retailer_assg AS tb99', 'tb3.id', 'tb99.retail_id')
                ->join('wcm_customer AS tb9', 'tb99.customer_id', 'tb9.id');
        } else {
            $query->leftJoin('wcm_customer AS tb9', 'tb2.customer_id', 'tb9.id');
        }

        $query->select('tb1.id', 'tb1.uuid', 'tb1.name', 'tb1.code', 'tb1.farmer_group_id', 'tb2.code AS farmer_group_code', 'tb2.name as farmer_group_name',
            'tb1.address', 'tb1.sales_unit_id', 'tb1.telp_no', 'tb1.hp_no', 'tb1.ktp_no', DB::raw('CONVERT ( VARCHAR, tb1.ktp_valid_date, 105 ) as ktp_valid_date'),
            'tb1.village', 'tb1.sub_sector_id', 'tb1.commodity_id', 'tb1.land_area', 'tb1.latitude',
            'tb1.longitude', 'tb2.retail_id', 'tb3.code AS retail_code', 'tb3.name AS retail_name', 'tb4.name AS sales_unit_name',
            'tb2.sales_org_id', 'tb6.name AS sales_org_name', 'tb2.customer_id', 'tb9.uuid AS customer_uuid', 'tb9.full_name AS customer_name', 'tb4.sales_group_id',
            'tb7.name AS sales_group_name', 'tb7.sales_office_id', 'tb8.name AS sales_office_name',
            'tb1.status', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
            DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
            DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
        )
        ->where($where);

        return $query;
    }

    public static function getExportedColumns() {
        return [
            'tb6.name as Produsen',
            'tb1.code as Kode',
            'tb1.name as Nama Lengkap',
            DB::raw("
            (CASE
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status"),
            'tb1.address as Alamat',
            'tb8.name as Provinsi',
            'tb7.name as Kota/Kabupaten',
            'tb4.name as Kecamatan',
            'tb1.village as Desa/Kelurahan',
            'tb1.telp_no as No Telepon',
            'tb1.hp_no as No Handphone',
            'tb1.land_area as Luas Lahan',
            'tb1.ktp_no as No KTP',
            'tb1.created_at as Dibuat Tanggal',
            'tb1.updated_at as Diperbarui',
        ];
    }

}
