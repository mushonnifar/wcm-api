<?php

namespace App\Models;

use App\models\Contract;
use App\models\ContractItem;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Rules\OnlyOneRules;

class User extends Authenticatable implements JWTSubject {

    use Notifiable;
    use HasRoles;

    // protected $dateFormat = 'Y-m-d H:i:s';

    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'email', 'sign_in_count', 'last_sign_in', 'system', 'sales_org_id', 'customer_id', 'status', 'created_by', 'updated_by',
    ];
    protected $appends = ['role_id',"sales_org_name"];
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'roles', 'role',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function role() {
        return $this->hasOne('App\Models\ModelHasRole', 'model_id');
    }

    public function getRoleIdAttribute() {
        return $this->role->role_id;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function ruleLogin() {
        $rules = [
            'username' => 'required',
            'password' => 'required',
        ];
        return $rules;
    }

    public static function ruleCreate() {
        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role_id' => 'required',
            'sales_org_id' => 'required_if:role_id,3',
        ];
        return $rules;
    }

    public static function newruleCreate() {
        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role_id' => 'required',            
            'status'=> 'required'
        ];
        return $rules;
    }

    public static function ruleUpdate() {
        $rules = [
            'username' => 'unique:users',
            'email' => 'email|unique:users',
            'name' => 'required',
            'role_id' => 'required'
        ];
        return $rules;
    }

    public static function newruleUpdate() {
        $rules = [
            'username' => ["required", new OnlyOneRules()],
            'email' => ["required", new OnlyOneRules()],
            'name' => 'required',
            'role_id' => 'required'
        ];
        return $rules;
    }

    public static function newruleUpdateDistributor() {
        $rules = [
            'username' => ["required", new OnlyOneRules()],
            'email' => 'required',
            'name' => 'required',
            'customer_id' => 'sometimes|exists:wcm_customer,id'
        ];
        return $rules;
    }

    public static function ruleUpdatePassword() {
        $rules = [
            'password_new' => 'required',
            'retype_password_new' => 'same:password_new'
        ];
        return $rules;
    }

    public static function getUser() {
        $query = DB::table('users as tb1')
                ->join('model_has_roles as tb2', 'tb1.id', '=', 'tb2.model_id')
                ->join('roles as tb3', 'tb2.role_id', '=', 'tb3.id')
                ->select('tb1.id', 'tb1.name', 'tb1.username', 'tb1.email','tb1.customer_id',
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at"),
                        'tb2.role_id', 'tb3.name as role_name', 'tb1.sign_in_count')
                ->where('tb1.status', '=', 'y');

        return $query;
    }

    public function salesOrg()
    {
        return $this->hasOne("App\Models\SalesOrg","id","sales_org_id");
    }

    public function getSalesOrgNameAttribute()
    {
        return $this->salesOrg ? $this->salesOrg->name : "";
    }

    public function salesOrgAssign()
    {
        return $this->hasMany("App\Models\SalesOrgUserAssg","user_id", "id");
    }

    public function salesGroupAssign()
    {
        return $this->hasMany("App\Models\SalesGroupUserAssg", "user_id", "id");
    }

    public function getFilterRegionalAttribute()
    {
        $customerID = $this->attributes["customer_id"];
        $isDistributor = !is_null($customerID);
        $filters = [];

        if ($isDistributor) {
            $contract = Contract::where("contract_type", "asal")
                            ->where("customer_id", $customerID)
                            ->where("year", Carbon::now()->year);

            if ($contract->exists()) {
                $filters["sales_org_id"] = $contract
                    ->pluck("sales_org_id")
                    ->unique()
                    ->toArray();
                $items = ContractItem::whereIn("contract_id", $contract->pluck("id"))
                            ->pluck("sales_group_id")
                            ->unique();

                if (!$items->isEmpty()) {
                    $filters["sales_group_id"] = $items->toArray();
                }
            } else {
                $filters["sales_group_id"] = [];
            }
        } else {
            if ($this->salesOrgAssign()->exists()) {
                $salesOrgIds = $this->salesOrgAssign()
                        ->pluck("sales_org_id")
                        ->unique()
                        ->filter();
                if (!$salesOrgIds->isEmpty()) {
                    $filters["sales_org_id"] = $salesOrgIds->toArray();
                }
                
            }

            if ($this->salesGroupAssign()->exists()) {
                $salesGroupIds = $this->salesGroupAssign()
                        ->pluck("sales_group_id")
                        ->unique()
                        ->filter();
                if (!$salesOrgIds->isEmpty()) {
                    $filters["sales_group_id"] = $salesGroupIds->toArray();
                }
            }
        }

        return $filters;
    }

    public function getIsAdminBaruAttribute() {
        return $this->hasRole(2);
    }
}
