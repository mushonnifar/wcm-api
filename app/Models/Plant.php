<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Plant extends Model {

    protected $table = 'wcm_plant';
    protected $fillable = [
        'code', 'name', 'sales_group_id', 'address',
        'from_date', 'thru_date',
        'status', 'created_by', 'updated_by',
    ];

    public static function getAll($where = []) {
        $query = DB::table('wcm_plant as tb1')
                ->join('wcm_plant_assg as tb2', 'tb1.id', '=', 'tb2.plant_id')
                ->select('tb1.id', 'tb1.code', 'tb1.name', 'tb1.sales_group_id', 'tb1.address', 'tb1.from_date', 'tb1.thru_date', 
                        'tb1.status', 'tb2.id as plant_assg_id', 'tb2.uuid as plant_assg_uuid', 'tb2.sales_org_id', 'tb2.distrib_channel_id')
                ->where($where);

        return $query;
    }

    public static function getAllDistinct($where = []) {
        $query = DB::table('wcm_plant as tb1')
                ->join('wcm_plant_assg as tb2', 'tb1.id', '=', 'tb2.plant_id')
                ->select('tb1.id', 'tb1.code', 'tb1.name', 'tb1.sales_group_id', 'tb1.address', 
                        'tb1.from_date', 'tb1.thru_date', 'tb1.status', 'tb2.sales_org_id')
                ->where($where)
                ->distinct();

        return $query;
    }

    public static function getPlantSupplyPoint($where = []) {
        $querySupply = DB::table('wcm_supply_point as tb1')
                ->leftJoin('wcm_plant as tb2', 'tb1.plant_id', '=', 'tb2.id')
                ->select('tb2.id', 'tb2.uuid', 'tb2.code', 'tb2.name', 'tb1.scnd_sales_group_id AS sales_group_id',
                        'tb1.sales_org_id', 'tb1.status as status1', 'tb2.status as status2')
                ->distinct();

        $queryPlant = DB::table('wcm_plant as tb1')
                ->join('wcm_plant_assg as tb2', 'tb1.id', '=', 'tb2.plant_id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.code', 'tb1.name', 'tb1.sales_group_id', 'tb2.sales_org_id',
                        'tb1.status as status1', 'tb2.status as status2')
                ->distinct()
                ->union($querySupply);

        $query = DB::table(DB::raw("(".$queryPlant->toSql().") as tb0"))->where($where);
        return $query;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function salesGroup() {
        return $this->hasOne("App\Models\SalesGroup", "id", "sales_group_id");
    }

}
