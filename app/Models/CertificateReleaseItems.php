<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateReleaseItems extends Model
{
    //
    protected $table = 'wcm_certificate_release_items';
    protected $fillable = [
        'uuid','certificate_release_id', 'distrib_report_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];
}
