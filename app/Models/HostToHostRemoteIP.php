<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostToHostRemoteIP extends Model
{
    public $primaryKey = "uuid";
    public $table = "wcm_h2h_remote_ip";
    public $fillable = [
        "bank_id", "base_url", "url_endpoint","ip_address", "password"
    ];
    
}
