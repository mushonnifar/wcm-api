<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CustomerSalesArea extends Model {
    protected $table = 'wcm_customer_sales_area';
    protected $fillable = [
        'customer_id', 'sales_area_id', 'from_date', 'thru_date',
        'term_of_payment', 'top_dp', 'tax_classification', 'pph22', 'top_dp_uom',
        'status', 'created_by', 'updated_by',
    ];

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    
    public static function getAll($where = []) {
        $query = DB::table('wcm_customer_sales_area as tb1')
                ->join('wcm_customer as tb2', 'tb1.customer_id', '=', 'tb2.id')
                ->leftJoin('wcm_sales_area as tb3', 'tb1.sales_area_id', '=', 'tb3.id')
                ->leftJoin('wcm_distrib_channel as tb4', 'tb3.distrib_channel_id', '=', 'tb4.id')
                ->leftJoin('wcm_sales_division as tb5', 'tb3.sales_division_id', '=', 'tb5.id')
                ->leftJoin('wcm_sales_org as tb6', 'tb3.sales_org_id', '=', 'tb6.id')
                ->leftJoin('view_payment_mothod_sales_area as tb7', function ($q) {
                    $q->on('tb1.customer_id', '=', 'tb7.customer_id');
                    $q->on('tb3.sales_org_id', '=', 'tb7.sales_org_id');
                })
                ->leftJoin("wcm_address as tb8", function($q){
                    $q->on("tb1.customer_id", "tb8.customer_id");
                    $q->where("tb8.address_type", "FORMAL");
                })
                ->select('tb1.id', 'tb1.uuid', 'tb1.customer_id', 'tb2.uuid as customer_uuid','tb2.full_name as customer_name', 'tb3.sales_org_id', 'tb6.name as sales_org_name',
                        'tb3.distrib_channel_id', 'tb4.name as distrib_channel_name', 'tb3.sales_division_id', 'tb5.name as sales_division_name',
                        'tb1.term_of_payment', 'tb1.top_dp', 'tb1.tax_classification', 'tb1.pph22', 'tb1.top_dp_uom', DB::raw("isnull( tb7.name, '' ) AS payment_method "),
                        DB::raw("(CASE  
                        WHEN tb1.status = 'y' THEN 'Active'
                        WHEN tb1.status = 'n' THEN 'Inactive'
                        WHEN tb1.status = 'p' THEN 'Suspend'
                        WHEN tb1.status = 'd' THEN 'Draft'
                        WHEN tb1.status = 's' THEN 'Submited'
                        ELSE '-'
                    END) as status"), 'tb1.created_by', 'tb1.updated_by',
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"), 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                        )
                ->where($where);

        return $query;
    }
    
    public static function getSalesAreaOrder($customer, $sales_org) {
        $query = DB::table('wcm_customer_sales_area as tb1')
                ->join('wcm_sales_area as tb2', 'tb1.sales_area_id', '=', 'tb2.id')
                ->select('tb1.*', 'tb2.sales_org_id', 'tb2.distrib_channel_id', 'tb2.sales_division_id')
                ->where('tb1.customer_id', $customer)
                ->where('tb2.sales_org_id', $sales_org)
                ->where('tb2.sales_division_id', '00')
                ->first();

        return $query;
    }

    public static function getExportedColumns(){
        return [
            'tb1.customer_id AS Kode Distributor',
            'tb2.full_name AS Distributor',
            'tb3.sales_org_id AS Kode Produsen',
            'tb6.name AS Produsen',
            'tb3.distrib_channel_id AS Kode Saluran Distribusi',
            'tb4.name AS Saluran Distribusi',
            'tb3.sales_division_id AS Kode Divisi',
            'tb5.name AS Divisi',
            'tb1.term_of_payment AS Term Of Payment',
            'tb1.top_dp AS Top DP',
            'tb1.tax_classification AS Tax Classification',
            'tb1.pph22 AS PPH 22',
            'tb7.name AS Tipe Pembayaran',
            DB::raw("(CASE  
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status")
        ];
    }

}