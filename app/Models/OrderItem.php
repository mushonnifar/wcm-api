<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderItem extends Model
{

    protected $table    = 'wcm_order_item';
    protected $fillable = [
        'order_id', 'product_id', 'plant_id', 'retail_id', 'material_list_id', 'qty', 'sap_prodt_desc',
        'total_price', 'total_price_before_ppn', 'upfront_payment', 'ppn', 'pph22', 'zhet', 'zbki', 'zbdi',
        'status', 'created_by', 'updated_by','item_order_sap',"uom_zhet", "uom_zbki", "uom_zbdi"
    ];

    public static function ruleCreate()
    {
        $rules = [
            'order_id'               => 'required|exists:wcm_orders,id',
            'product_id'             => 'required|exists:wcm_product,id',
            'plant_id'               => 'required|exists:wcm_plant,id',
            'retail_id'              => 'required|exists:wcm_retail,id',
            'qty'                    => 'required|numeric|min:0|not_in:0',
            'total_price'            => 'required|numeric',
            'total_price_before_ppn' => 'required|numeric',
            'ppn'                    => 'required|numeric',
//            'pph22' => 'required|numeric',
            'status'                 => 'required',
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getQtyItem($where = [])
    {
        $query = DB::table('wcm_order_item AS tb1')
            ->leftJoin('wcm_product AS tb2', 'tb1.product_id', '=', 'tb2.id')
            ->leftJoin('wcm_plant AS tb3', 'tb1.plant_id', '=', 'tb3.id')
            ->leftJoin('wcm_retail AS tb4', 'tb1.retail_id', '=', 'tb4.id')
            ->leftJoin('wcm_material_list as tb5', 'tb5.id', '=', 'tb1.material_list_id')
            ->leftJoin('wcm_sales_group as tb7', 'tb7.id', '=', 'tb3.sales_group_id')
            ->select('tb1.id', 'tb1.uuid', 'tb1.order_id', 'tb1.qty', 'tb1.product_id', 'tb2.uuid AS product_uuid',
                'tb2.name AS product_name', 'tb1.plant_id', 'tb3.uuid AS plant_uuid', 'tb3.code AS plant_code', 'tb3.name AS plant_name',
                'tb1.retail_id', 'tb4.uuid AS retail_uuid', 'tb4.name AS retail_name', 'tb4.sales_unit_id as retail_sales_unit_id',
                'tb1.total_price', 'tb1.material_list_id', 'tb5.material_no', 'tb7.name as sales_group_name',
                'tb1.total_price_before_ppn', 'tb1.upfront_payment', 'tb1.ppn', 'tb1.pph22', 'tb1.zhet', 'tb1.zbki', 'tb1.zbdi',
                'tb1.status', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by','tb1.item_order_sap',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            )
            ->where($where);

        return $query;
    }
    public static function getSumQtyItem($where = [])
    {
        $query = DB::table('view_qty_item_penebusan')
            ->select('*')
            ->where($where);

        return $query;
    }

    public static function sumPrice($order_id)
    {
        $query = DB::table('wcm_order_item')
            ->select(
                DB::raw("SUM ( total_price ) AS total_price"),
                DB::raw("SUM ( total_price_before_ppn ) AS total_price_before_ppn"),
                DB::raw("SUM ( ppn ) AS ppn"),
                DB::raw("SUM ( pph22 ) AS pph22")
            )
            ->where('order_id', $order_id)
            ->groupBy('order_id')
            ->first();

        return $query;
    }

    public static function getForCalculationSO($where = [])
    {
        $query = DB::table("wcm_order_item as tb1")
            ->leftJoin('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
            ->leftJoin('wcm_retail as tb3', 'tb3.id', '=', 'tb1.retail_id')
            ->select('tb1.qty', 'tb1.product_id', 'tb1.order_id', 'tb1.retail_id',
                'tb2.number', 'tb2.sales_org_id', 'tb2.contract_id', 'tb2.customer_id', 'tb2.sales_office_id', 'tb2.sales_group_id', 'tb2.delivery_method_id',
                'tb3.sales_unit_id')
            ->where($where);
        return $query;
    }

    public static function getItemApproved($where = [])
    {
        $query = DB::table("wcm_order_item as tb1")
                ->leftJoin('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
                ->leftJoin('wcm_material_list as tb3', 'tb3.id', '=', 'tb1.material_list_id')
                ->select('tb1.*', 'tb3.material_no')
                ->where(function($q){
                    $q->where('tb2.status', 'y')
                        ->orWhere(function($qq){
                            $qq->whereIn("tb2.status", ["l", "u"])->whereNull("tb2.so_number");
                        });
                })
                ->where($where);
        return $query;
    }

    public function plant()
    {
        return $this->hasOne("App\Models\Plant", "id", "plant_id");
    }

    public function product()
    {
        return $this->hasOne("App\Models\Product", "id", "product_id");
    }

    public function order() 
    {
        return $this->belongsTo("App\Models\Order", "order_id", "id");
    }

    public function scopePenebusanByRetail($query, $retails,Order $order, $region=true)
    {
        return $query->whereIn("retail_id", $retails)
            ->whereHas("order", function($q) use($order, $region){
                $q->whereIn("status", ["s","y","l","u","c", "k"]);
                if ($order->status == "d") {
                    $q->whereRaw("YEAR(order_date) = " .Carbon::now()->format("Y"));
                } else {
                    $q->whereBetween("order_date",[
                        parseDate($order->order_date)->startOfYear(),
                        parseDate($order->submit_date),
                    ]);
                }
                $q->where("so_upload", "!=", 1)->where("contract_id", $order->contract_id);

                if (!$region) {
                    $q->where("customer_id", $order->customer_id);
                }
            })
            ->select(
                "retail_id",
                "product_id", 
                DB::raw("sum(qty) as qty")
            )
            ->groupBy("retail_id","product_id");
    }

    public function material()
    {
        return $this->hasOne("App\Models\MaterialList", "id", "material_list_id");
    }
    

}
