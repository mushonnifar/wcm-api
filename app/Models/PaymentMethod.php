<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PaymentMethod extends Model {

    protected $table = 'wcm_cust_payment_method';
    protected $fillable = [
        'customer_id', 'sales_org_id', 'ident_name', 
        'name', 'desc', 'system', 'sap_code',
        'status', 'created_by', 'updated_by',
    ];

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
}
