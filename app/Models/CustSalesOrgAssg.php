<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustSalesOrgAssg extends Model {

    protected $table = 'wcm_cust_sales_org_assg';
    public $incrementing = false;
    protected $fillable = [
        'id', 'customer_id', 'sales_org_id', 'sap_status', 'sap_status', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

}
