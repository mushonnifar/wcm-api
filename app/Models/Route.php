<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'wcm_m_routes';

    protected $fillable = [
        'id', 'name', 'status', 'created_by', 'updated_by',
    ];
    
    public $incrementing = false;
    //
    public static function ruleCreate() {
        $rules = [
            'name' => 'required|unique:wcm_m_routes',
        ];
        
        return $rules;
    }

    public static function ruleUpdate() {
        $rules = [
            'name' => 'required',
        ];

        return $rules;
    }

    public static function getPermission($role_id, $route_id, $action_id){

        $result = DB::table('role_has_permissions as a')
        ->join('permissions as b', 'a.permission_id', '=', 'b.id')
        ->select('a.role_id','b.route_id','b.action_id')
        ->where(['a.role_id' => $role_id, 'b.route_id' => $route_id, 'b.action_id' => $action_id]); 

        return $result;
    }
    public static function rulePermission() {
        $rules = [
            'data' => 'required' 
        ];
        return $rules;
    }

}
