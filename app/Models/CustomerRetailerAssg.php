<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerRetailerAssg extends Model {

    protected $table = 'wcm_customer_retailer_assg';

    public $incrementing = false;

    protected $fillable = [
        'id', 'customer_id', 'retail_id', 'sales_org_id', 'start_date', 'end_date', 'status', 'created_by', 'updated_by','created_at', 'updated_at',
    ];  

    public static function getData($where = array()){
        $query = DB::table('wcm_customer_retailer_assg as a')
        ->join('wcm_retail as b', function ($q) {
            $q->on('a.retail_id', '=', 'b.id');
        })
        ->join('wcm_customer as c', function ($q) {
            $q->on('a.customer_id', '=', 'c.id');
        })
        ->join('wcm_sales_org as d', function ($q) {
            $q->on('a.sales_org_id', '=', 'd.id');
        })
        ->leftJoin('wcm_sales_unit as e', function($q)
        {
            $q->on('b.sales_unit_id', '=', 'e.id');
        })
        ->leftJoin('wcm_sales_group as f', function($q)
        {
            $q->on('e.sales_group_id', '=', 'f.id');
        })
        ->leftJoin('wcm_sales_office as g', function($q)
        {
            $q->on('f.sales_office_id', '=', 'g.id');
        })
        ->select('a.uuid', 'a.customer_id', 'c.uuid as customer_uuid', 'c.full_name as customer_name','c.owner as customer_owner', 'a.sales_org_id', 'd.name as sales_org_name', 'a.retail_id','b.code as retail_code', 'b.name as retail_name','b.owner as retail_owner', 'b.sales_unit_id', 'e.name as sales_unit_name','b.address', 'e.sales_group_id as sales_group_id', 'f.name as sales_group_name', 'f.sales_office_id', 'g.name as sales_office_name', 'a.status', 
        DB::raw("
        (CASE  
            WHEN a.status = 'y' THEN 'Active'
            WHEN a.status = 'n' THEN 'Inactive'
            WHEN a.status = 'p' THEN 'Suspend'
            WHEN a.status = 'd' THEN 'Draft'
            WHEN a.status = 's' THEN 'Submited'
            ELSE '-'
        END) as status_name"),
        'a.created_by', 'a.updated_by', 'a.created_at', 'a.updated_at')
        ->where($where); 
        return($query); 
    }

    public static function getDataCheck($where = array())
    {
        $query = DB::table('wcm_customer_retailer_assg as a')
            ->join('wcm_retail as b', function ($q) {
                $q->on('a.retail_id', '=', 'b.id');
            })
            ->join('wcm_customer as c', function ($q) {
                $q->on('a.customer_id', '=', 'c.id');
            })
            ->join('wcm_sales_org as d', function ($q) {
                $q->on('a.sales_org_id', '=', 'd.id');
            })
            ->where($where);
        return ($query);
    }

    /**
     * Foreign To Customer
     *
     * @return     <Object>  Customer
     */
    public function customer()
    {
        return $this->hasOne("App\Models\Customer", "id", "customer_id");
    }

    /**
     * Foreign To Retail 
     *
     * @return     Object  Retail
     */
    public function retail()
    {
        return $this->hasOne("App\Models\Retail","id", "retail_id");
    }

    public static function getExportedColumns() {
        return [
            'd.name as Produsen', 
            'a.customer_id AS No Distributor', 
            'c.full_name as Nama Distributor',
            'b.code as Kode Pengecer', 
            'b.name as Nama Pengecer ',
            'b.owner as Nama Pemilik Perusahaan', 
            'b.address as Alamat pengecer',
            'g.id as Kode Provinsi  ', 
            'g.name as Nama Provinsi  ', 
            'f.id as Kode Kota/Kabupaten ', 
            'f.name as Nama kota/Kabupaten ', 
            'e.id as Kode Kecamatan',
            'e.name as Nama Kecamatan',
            'b.village as Desa',
            'b.tlp_no as No Telepon Kantor',
            'b.fax_no as No Faks No',
            'b.tlp_no as No Handphone',
            'b.email as Email',
            'b.npwp_no as NPWP',
            'b.npwp_register as Masa Berlaku NPWP',
            'b.siup_no as SIUP',
            'b.valid_date_siup as Masa Berlaku SIUP',
            'b.situ_no as SITU',
            'b.valid_date_situ as Masa Berlaku SITU',
            'b.tdp_no as TDP',
            'b.valid_date_tdp as Masa Berlaku TDP',
            'b.recomd_letter as Surat Rekomendasi Dinas',
            'b.recomd_letter_date as Masa Berlaku Rekomendasi Dinas',
            DB::raw("
            (CASE  
                WHEN b.status = 'y' THEN 'Active'
                WHEN b.status = 'n' THEN 'Inactive'
                WHEN b.status = 'p' THEN 'Suspend'
                WHEN b.status = 'd' THEN 'Draft'
                WHEN b.status = 's' THEN 'Submited'
                ELSE '-'
            END) as [Status Pengecer]"),
            DB::raw("
            (CASE  
                WHEN a.status = 'y' THEN 'Active'
                WHEN a.status = 'n' THEN 'Inactive'
                WHEN a.status = 'p' THEN 'Suspend'
                WHEN a.status = 'd' THEN 'Draft'
                WHEN a.status = 's' THEN 'Submited'
                ELSE '-'
            END) as [Status Relasi]"),
        ];
    }
}
