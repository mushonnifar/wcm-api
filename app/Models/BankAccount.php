<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BankAccount extends Model
{
    protected $table 	= "wcm_bank_account";
    protected $fillable = [
    	"sales_org_id","bank_id", "currency","bank_account","bank_gl_account",
    	"bank_clearing_acct", "status", "created_by","updated_by"
    ];

    protected $hidden   = ["updated_by","created_by","bank","salesOrg"];
    protected $appends  = ["bank_name","sales_org_name"];


    public function getSalesOrgNameAttribute()
    {
        return $this->salesOrg->name;
    }

    public function getBankNameAttribute()
    {
        return $this->bank->name;
    }

    public function bank()
    {
    	return $this->hasOne("App\Models\Bank","id","bank_id");
    }

    public function salesOrg()
    {
    	return $this->hasOne("App\Models\SalesOrg","id","sales_org_id");
    }

    public function scopeList($query)
    {
    	$columns = ["ba.id","so.id as sales_org_id","bk.name as bank_name", "ba.bank_account","ba.bank_gl_account","ba.bank_clearing_acct"];

    	return \DB::table("{$this->table} as ba")
    				->join("wcm_sales_org as so", "so.id" ,"ba.sales_org_id")
    				->join("wcm_bank as bk", "bk.id" ,"ba.bank_id")
    				->select($columns)
    				->addSelect(\DB::raw("
                    (CASE WHEN ba.status = 'y' THEN 'Active' ELSE 'InActive' END) as status"));
    }

    public function scopeRuleCreate()
    {
    	return [
            "sales_org_id" => "required|exists:wcm_sales_org,id",
            "bank_id" => "required|exists:wcm_bank,id",
            "currency" => "string|max:5|min:1",
            "bank_account" => "required",
            "bank_gl_account" => "required",
            "bank_clearing_acct" => "required"
        ];
    }

    public function scopeRuleUpdate()
    {
    	return [
            "sales_org_id" => "required|exists:wcm_sales_org,id",
            "bank_id" => "required|exists:wcm_bank,id",
            "currency" => "string|max:5|min:1",
            "bank_account" => "required",
            "bank_gl_account" => "required",
            "bank_clearing_acct" => "required"
        ];
    }

    public function scopeRuleUpdateBatch()
    {
    	return [
    		"ids" => "required|array",
            "status" => "required|in:y,d"
    	];
    }

    public static function getExportedColumns() 
    {
        return [
            "so.id AS Produsen",
            "bk.name AS Bank",
            "ba.bank_account AS Bank Account",
            "ba.bank_gl_account AS Bank GL Account",
            "ba.bank_clearing_acct AS Bank Clearing Account",
            DB::raw("(CASE WHEN ba.status = 'y' THEN 'Active' ELSE 'InActive' END) as Status")
        ];
    }
}
