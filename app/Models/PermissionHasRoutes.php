<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionHasRoutes extends Model
{
    protected $table = 'wcm_permission_has_routes';
    protected $fillable = [
        'permission_id', 'menu_id', 'method', 'url', 'path', 'middleware', 'status', 'created_by', 'updated_by'
    ];
}
