<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApprovalSetting extends Model {

    protected $table = 'wcm_approval_setting';
    protected $fillable = [
        'sales_org_id', 'sales_group_id', 
        'status', 'created_by', 'updated_by',
    ];
    
    public static function getAll($where = [],$exported=false) {
        $query = DB::table('wcm_approval_setting AS tb1')
                ->leftJoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
                ->join('wcm_sales_group AS tb3', 'tb1.sales_group_id', '=', 'tb3.id')
                ->join('wcm_sales_office AS tb4', 'tb3.sales_office_id', '=', 'tb4.id');
        if($exported){
            $query->select('tb2.name AS sales_org_name','tb3.sales_office_id', 'tb4.name AS sales_office_name','tb1.sales_group_id', 
                        'tb3.name AS sales_group_name', DB::raw("
                        (CASE  
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"));
        }else{


                $query->select('tb1.id', 'tb1.uuid', 'tb1.sales_org_id', 'tb2.name AS sales_org_name', 'tb1.sales_group_id', 
                        'tb3.name AS sales_group_name', 'tb3.sales_office_id', 'tb4.name AS sales_office_name', 
                        'tb1.status', DB::raw("
                        (CASE  
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by', 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"), 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                );
        }
            $query->where($where);

        return $query;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
