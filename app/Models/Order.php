<?php

namespace App\Models;

use App\Models\Delivery;
use App\Models\DeliveryItem;
use App\Traits\OutstandingSOTraits;
use App\Traits\ReportSOTraits;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Order extends Model
{
    use ReportSOTraits;
    use OutstandingSOTraits;

    protected $table    = 'wcm_orders';
    protected $fillable = [
        'number', 'sales_org_id', 'contract_id', 'customer_id', 'sales_office_id', 'sales_group_id', 'delivery_method_id',
        'order_date', 'payment_method', 'desc', 'booking_code', 'total_price', 'total_price_before_ppn', 'upfront_payment', 'ppn',
        'pph22', 'billing_date', 'payment_due_date', 'bank_id', 'good_redemption_due_date', 'df_due_date', 'df_due_date_plan',
        'pickup_due_date', 'delivery_location', 'so_number', 'sap_status', 'billing_fulldate', 'sap_product_desc', 'sap_miscs',
        'sap_booking_code', 'sap_billing_dp_doc', 'sap_clearing_doc','submit_date',
        'reference_code', 'send_order_date', 'flag', 'seq', 'approve_date', 'approve_type', 'approve_status', 'status', 'created_by', 'updated_by',
        'so_upload'
    ];

    public static function ruleCreate()
    {
        $rules = [
            'sales_org_id'       => 'required|exists:wcm_sales_org,id',
            'payment_method'     => 'required',
            'order_date'         => 'required|date',
            'contract_id'        => 'required|exists:wcm_contract,id',
            'customer_id'        => 'required|exists:wcm_customer,id',
            'partner_id'         => 'required|exists:wcm_customer,id',
            'delivery_method_id' => 'required|exists:wcm_delivery_method,id',
            'sales_office_id'    => 'required|exists:wcm_sales_office,id',
            'sales_group_id'     => 'required|exists:wcm_sales_group,id',
//            'bank_id' => 'required',
            'desc'               => 'required',
            'status'             => 'required',
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    // public function getgoodRedemptionDueDateAttribute($value)
    // {
    //     return $value ? Carbon::createFromFormat("M j Y h:i:s:A", $value)->format('d-m-Y h:i:s') : null;
    // }
    //
    // public function getPaymentDueDateAttribute($value)
    // {
    //     return Carbon::createFromFormat("M j Y h:i:s:A", $value)->format('d-m-Y h:i:s');
    // }

    public static function getAll($where = [],$exported=false)
    {
        $query = DB::table('wcm_orders AS tb1')
            ->leftJoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
            ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_office AS tb4', 'tb1.sales_office_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_group AS tb5', 'tb1.sales_group_id', '=', 'tb5.id')
            ->leftJoin('wcm_delivery_method AS tb6', 'tb1.delivery_method_id', '=', 'tb6.id')
            ->leftJoin('wcm_contract AS tb7', 'tb1.contract_id', '=', 'tb7.id')
            ->leftJoin('users AS tb8', 'tb1.created_by', '=', 'tb8.id');
            if($exported){
            $query->select('tb1.number','tb1.reference_code','tb3.full_name as customer_names','tb2.name as sales_org_name',
                DB::raw('CONVERT(VARCHAR(10), tb1.order_date, 105) as order_date'),
                DB::raw("(CASE WHEN tb1.status = 'y' THEN 'Active'
                                WHEN tb1.status = 'n' THEN 'Inactive'
                                WHEN tb1.status = 'p' THEN 'Suspend'
                                WHEN tb1.status = 'd' THEN 'Draft'
                                WHEN tb1.status = 's' THEN 'Submited'
                                WHEN tb1.status = 'r' THEN 'Rejected'
                                WHEN tb1.status = 'o' THEN 'Cancel SO'
                                ELSE '-'
                            END) as status_name"),'tb6.name as delivery_method_name','tb4.name as sales_office_name','tb5.name as sales_group_name','tb1.payment_method','tb1.payment_method','tb1.total_price','tb1.ppn','tb1.total_price_before_ppn','tb1.upfront_payment','tb1.pickup_due_date','tb8.name as created_name','tb1.created_at'
                );
            }else{
                $query->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.sales_org_id', 'tb2.name AS sales_org_name',
                    'tb1.contract_id', 'tb7.number as contract_number', 'tb1.customer_id', 'tb3.full_name AS customer_name', 'tb1.sales_office_id',
                    'tb4.name AS sales_office_name', 'tb1.sales_group_id', 'tb5.name AS sales_group_name', 'tb5.district_code',
                    'tb1.delivery_method_id', 'tb6.name AS delivery_method_name', 'tb6.ident_name AS delivery_method_ident', DB::raw('CONVERT(VARCHAR(10), tb1.order_date, 105) as order_date'),
                    'tb1.payment_method', 'tb1.desc', DB::raw("ISNULL(tb1.booking_code, '') as booking_code"), 'tb1.total_price', 'tb1.total_price_before_ppn', 'tb1.upfront_payment',
                    'tb1.ppn', 'tb1.pph22', DB::raw('CONVERT(VARCHAR(10), tb1.billing_date, 105) as billing_date'),
                    DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.payment_due_date, 105 ), ' ', CONVERT ( VARCHAR, tb1.payment_due_date, 108 )) as payment_due_date"),
                    'tb1.bank_id', DB::raw('CONVERT(VARCHAR(10), tb1.good_redemption_due_date, 105) as good_redemption_due_date'),
                    'tb1.df_due_date', 'tb1.df_due_date_plan', 'tb1.pickup_due_date',
                    'tb1.delivery_location', DB::raw("ISNULL(tb1.so_number, '') as so_number"), 'tb1.sap_status', 'tb1.billing_fulldate', 'tb1.sap_product_desc',
                    'tb1.sap_miscs', 'tb1.reference_code', 'tb1.flag', 'tb1.seq', 'tb1.approve_date', 'tb1.approve_type',
                    'tb1.status', DB::raw("
                            (CASE
                                WHEN tb1.status = 'y' THEN 'Active'
                                WHEN tb1.status = 'n' THEN 'Inactive'
                                WHEN tb1.status = 'p' THEN 'Suspend'
                                WHEN tb1.status = 'd' THEN 'Draft'
                                WHEN tb1.status = 's' THEN 'Submited'
                                WHEN tb1.status = 'r' THEN 'Rejected'
                                WHEN tb1.status = 'o' THEN 'Cancel SO'
                                ELSE '-'
                            END) as status_name"), 'tb1.created_by', 'tb8.name as created_name', 'tb1.updated_by',
                    DB::raw('CONVERT(VARCHAR(10), tb1.created_at, 105) as created_at'),
                    DB::raw('CONVERT(VARCHAR(10), tb1.updated_at, 105) as updated_at')
                );
            }
            $query->where($where);

        return $query;
    }

    public static function getAllAndPartner($where = [])
    {
        $query = DB::table('wcm_orders AS tb1')
            ->leftJoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
            ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_office AS tb4', 'tb1.sales_office_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_group AS tb5', 'tb1.sales_group_id', '=', 'tb5.id')
            ->leftJoin('wcm_delivery_method AS tb6', 'tb1.delivery_method_id', '=', 'tb6.id')
            ->leftJoin('wcm_order_partner_functions AS tb8', function ($q) {
                $q->on('tb1.id', '=', 'tb8.order_id')->where('tb8.ident_name_funct', '=', 'WE');
            })
            ->leftJoin('wcm_contract AS tb9', 'tb1.contract_id', '=', 'tb9.id')
            ->leftJoin('wcm_customer AS tb10', 'tb10.id', '=', 'tb8.partner_id')
            ->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.sales_org_id', 'tb2.name AS sales_org_name',
                'tb1.contract_id', 'tb9.uuid as contract_uuid', 'tb9.number as contract_number', 'tb1.customer_id', 'tb3.full_name AS customer_name', 'tb1.sales_office_id',
                'tb4.name AS sales_office_name', 'tb1.sales_group_id', 'tb5.name AS sales_group_name',
                'tb1.delivery_method_id', 'tb6.name AS delivery_method_name', 'tb1.order_date', 'tb1.payment_method', 'tb8.partner_id',
                'tb10.full_name AS partner_name', 'tb1.desc', 'tb1.booking_code', 'tb1.total_price', 'tb1.total_price_before_ppn',
                'tb1.upfront_payment', 'tb1.ppn', 'tb1.pph22', 'tb1.billing_date', DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.payment_due_date, 105 ), ' ', CONVERT ( VARCHAR, tb1.payment_due_date, 108 )) as payment_due_date"),
                'tb1.bank_id', DB::raw('CONVERT(VARCHAR(10), tb1.good_redemption_due_date, 105) as good_redemption_due_date'),
                'tb1.df_due_date', 'tb1.df_due_date_plan', 'tb1.pickup_due_date',
                'tb1.delivery_location', 'tb1.so_number', 'tb1.sap_status', 'tb1.billing_fulldate', 'tb1.sap_product_desc',
                'tb1.sap_miscs', 'tb1.reference_code', 'tb1.flag', 'tb1.seq', 'tb1.approve_date', 'tb1.approve_type',
                'tb1.status', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Approved'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'o' THEN 'Cancel SO'
                            WHEN tb1.status = 'l' THEN 'Paid'
                            WHEN tb1.status = 'u' THEN 'DP Paid'
                            WHEN tb1.status = 'r' THEN 'Reviewed'
                            WHEN tb1.status = 'k' THEN 'Good Issued'
                            WHEN tb1.status = 'c' THEN 'Completed'
                            WHEN tb1.status = 'x' THEN 'Closed'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at"),
                'tb1.so_upload'
            )
            ->where($where);

        return $query;
    }

    public static function getManualApproval($where=[],$exported=false)
    {
        $query = DB::table('wcm_orders AS tb1')
            ->leftJoin('wcm_order_item AS tb2', 'tb1.id', '=', 'tb2.order_id')
            ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_office AS tb4', 'tb1.sales_office_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_group AS tb5', 'tb1.sales_group_id', '=', 'tb5.id')
            ->leftJoin('wcm_contract AS tb6', 'tb1.contract_id', '=', 'tb6.id')
            ->leftJoin('wcm_retail AS tb7', 'tb2.retail_id', '=', 'tb7.id')
            ->leftJoin('wcm_sales_unit AS tb8', 'tb7.sales_unit_id', '=', 'tb8.id')
            ->leftJoin('wcm_material_list AS tb9', 'tb2.material_list_id', '=', 'tb9.id');
            if($exported){
            $query->select(DB::raw("
                            (CASE
                                WHEN tb1.approve_status = 'd' THEN 'Manual Approval'
                                WHEN tb1.approve_status = 'r' THEN 'Reviewed'
                                ELSE '-'
                            END) as status"),'tb1.payment_method','tb1.customer_id','tb1.created_at','tb1.number','tb3.full_name','tb1.sales_office_id','tb4.name as sales_office_name','tb1.sales_group_id','tb5.name as sales_group_name','tb7.sales_unit_id','tb8.name as sales_unit_name','tb6.number as spjb','tb2.material_list_id','tb9.mat_desc','tb2.qty');
            }else{
                $query->select('tb1.id AS order_id', 'tb1.uuid AS order_uuid', 'tb2.id AS order_item_id', 'tb2.uuid AS order_item_uuid',
                    'tb1.so_number', 'tb1.number', 'tb1.payment_method', 'tb1.customer_id', 'tb3.full_name AS customer_name',
                    'tb1.sales_office_id', 'tb4.name AS sales_office_name', 'tb1.sales_group_id', 'tb5.name AS sales_group_name',
                    'tb1.contract_id', 'tb6.number AS spjb', 'tb2.retail_id', 'tb7.name AS retail_name', 'tb7.sales_unit_id',
                    'tb8.name AS sales_unit_name', 'tb2.material_list_id', 'tb9.material_no', 'tb9.mat_desc', 'tb2.qty', 'tb1.approve_status',
                        DB::raw("
                            (CASE
                                WHEN tb1.approve_status = 'd' THEN 'Manual Approval'
                                WHEN tb1.approve_status = 'r' THEN 'Reviewed'
                                ELSE '-'
                            END) as status"), 'tb1.created_by', 'tb1.updated_by',
                    DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                    DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                );
            }
            
            $query->where('tb1.approve_type', 'm')
            ->whereIn('tb1.status', ['s', 'r'])
            ->whereIn('tb1.approve_status', ['d', 'r'])
            ->where($where);

        return $query;
    }

    public static function getForSPJBOpr($where = [])
    {
        $query = DB::table('wcm_orders as tb1')
            ->leftjoin('wcm_order_item as tb2', 'tb1.id', '=', 'tb2.order_id')
            ->leftjoin('wcm_retail as tb3', 'tb3.id', '=', 'tb2.retail_id')
            ->select('tb1.id', 'tb1.contract_id', 'so_number', DB::raw("MONTH(order_date) as order_date"), 'tb2.product_id', 'tb2.retail_id', 'tb3.sales_unit_id', 'tb2.status', 'tb2.qty')
            ->where($where);
        return $query;
    }

    public static function getOrderDetailItem($where = [])
    {
        $query = DB::table('wcm_order_item as tb1')
            ->leftjoin('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
            ->leftjoin('wcm_retail as tb3', 'tb3.id', '=', 'tb1.retail_id')
            ->select('tb1.id', 'tb2.contract_id', 'tb1.order_id', DB::raw("MONTH(tb2.order_date) as order_date"), 'tb1.product_id', 'tb1.retail_id', 'tb3.sales_unit_id', 'tb2.status', 'tb1.qty')
            ->where($where);
        return $query;
    }

    public static function masterPenyaluranDO($where = [],$exported=false)
    {
        $subQuery = DB::table('wcm_orders as tb1')
            ->leftjoin(DB::raw('(SELECT SUM (qty) as qty,order_id  FROM [dbo].[wcm_order_item] GROUP BY order_id) as tb2'), 'tb2.order_id', '=', 'tb1.id')
            ->leftjoin('wcm_sales_org AS tb3', 'tb1.sales_org_id', '=', 'tb3.id')
            ->leftjoin('wcm_customer AS tb4', 'tb1.customer_id', '=', 'tb4.id')
            ->leftjoin('wcm_sales_office AS tb5', 'tb1.sales_office_id', '=', 'tb5.id')
            ->leftjoin('wcm_sales_group AS tb6', 'tb1.sales_group_id', '=', 'tb6.id')
        // ->leftjoin('wcm_delivery AS tb7', 'tb7.order_id', '=', 'tb1.id')
            ->leftjoin(DB::raw('(SELECT SUM (delivery_qty) AS delivery_qty,[tb21].order_id FROM [dbo].[wcm_delivery_item] as [tb20] LEFT JOIN [wcm_delivery] AS [tb21] ON [tb20].[delivery_id] = [tb21].[id] GROUP BY [tb21].order_id) as tb8'), 'tb8.order_id', '=', 'tb1.id')
            ->leftjoin('wcm_delivery_method AS tb9', 'tb9.id', '=', 'tb1.delivery_method_id')
            ->leftjoin('view_ditribution_do AS tb10', 'tb10.order_id', '=', 'tb1.id') // ini dirubah
        // ->join(DB::raw('(SELECT count(order_id) as c_order_id,order_id,id FROM [dbo].[wcm_distrib_reports] GROUP BY order_id) as tb10'), 'tb10.order_id', '=', 'tb1.id')
            ->leftjoin(DB::raw("(SELECT SUM (qty) AS qty, b.order_id FROM [dbo].[wcm_distrib_report_item] as a LEFT JOIN wcm_distrib_reports as b ON a.distrib_report_id=b.id WHERE (a.status='d' or a.status='s') AND (a.qty > 0) GROUP BY b.order_id) as tb11"), 'tb11.order_id', '=', 'tb1.id')
            ->select('tb1.id', 'tb1.number as number_order', 'tb1.uuid', 'tb1.sales_org_id',
                DB::raw("(CASE WHEN tb10.qty_submit is null THEN 0 ELSE tb10.qty_submit END) as qty_submit"),
                DB::raw("(CASE WHEN tb10.status_submit is null THEN 'submited' ELSE tb10.status_submit END) as status_submit"),
                DB::raw("(CASE WHEN tb10.qty_draft is null THEN 0 ELSE tb10.qty_draft END) as qty_draft"),
                DB::raw("(CASE WHEN tb10.status_draft is null THEN 'draft' ELSE tb10.status_draft END) as status_draft"),
                'tb3.name as sales_org_name', 'tb1.so_number', 'tb1.number', 'tb1.customer_id',
                'tb4.full_name as customer_name',
                DB::raw("(CASE WHEN tb2.qty is null THEN 0 ELSE tb2.qty END) as so_qty"),
                DB::raw("(CASE WHEN tb8.delivery_qty is null THEN 0 ELSE tb8.delivery_qty END) as do_qty"),
                DB::raw("(CASE WHEN tb11.qty is null THEN 0 ELSE tb11.qty END) as totals"),
                DB::raw("ROUND((CASE WHEN " . DB::raw('tb2.qty - tb11.qty') . " is null THEN 0 ELSE " . DB::raw('tb2.qty - tb11.qty') . " END),3) as sisa_qty_do"),
                'tb1.delivery_method_id', 'tb9.name as delivery_method_name',
                'tb1.sales_office_id', 'tb5.name as sales_office_name',
                'tb1.sales_group_id', 'tb6.name as sales_group_name')
            ->where($where);
        $query = DB::table(DB::raw('(' . $subQuery->toSql() . ') as zz'))
            ->mergeBindings($subQuery);
        if($exported){
            $query->select('sales_org_name','so_number','number','customer_name','so_qty','do_qty','totals',
                DB::raw('ROUND((do_qty - totals),3) as sisa_final'),'delivery_method_name','sales_office_name','sales_group_name');
        }else{
            $query->select(DB::raw('ROUND((do_qty - totals),3) as sisa_final, *'));
        }
        return $query;
    }

    public static function getEntryDo($where = [])
    {
        $query = DB::table('wcm_orders as tb1')
            ->leftJoin('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
            ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_group AS tb4', 'tb1.sales_group_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_office AS tb5', 'tb1.sales_office_id', '=', 'tb5.id')
            ->leftJoin('wcm_delivery_method AS tb6', 'tb6.id', '=', 'tb1.delivery_method_id')
            ->leftJoin(DB::raw("(
                    SELECT order_id,MIN(delivery_date) as delivery_date FROM wcm_delivery 
                    GROUP BY order_id
                ) AS tb7"), "tb7.order_id", "tb1.id")
            ->select('tb1.sales_org_id', 'tb2.name as sales_org_name',
                'tb1.customer_id', 'tb3.full_name as customer_name',
                'tb1.sales_office_id', 'tb5.name as sales_office_name',
                'tb1.sales_group_id', 'tb4.name as sales_group_name',
                'tb1.delivery_method_id', 'tb6.name as delivery_method_name',
                'tb1.so_number', 'tb1.id as order_id',
                DB::raw('(CONVERT ( VARCHAR, tb1.order_date, 105 )) as order_date'),
                DB::raw('(CONVERT ( VARCHAR, tb7.delivery_date, 105 )) as delivery_date')
            )
            ->where($where);
        return $query;
    }

    public function orderItems()
    {
        return $this->hasMany("App\Models\OrderItem", "order_id", "id");
    }

    public function salesGroup()
    {
        return $this->hasOne("App\Models\SalesGroup", "id", "sales_group_id");
    }

    public function salesOrg()
    {
        return $this->hasOne("App\Models\SalesOrg", "id", "sales_org_id");
    }

    public function customer()
    {
        return $this->hasOne("App\Models\Customer", "id", "customer_id");
    }

    public function validationQtySoAndDo()
    {
        $order_id = @$this->id;
        $qtySO    = ($orderItems = $this->orderItems) ? $orderItems->sum('qty') : 0;
        $delivery = Delivery::where('order_id', $order_id)->pluck('id')->unique();

        if ($delivery) {
            $qtyDO = DeliveryItem::whereIn('delivery_id', $delivery)->sum('delivery_qty');
        }

        return $qtySO == $qtyDO;
    }

    public function getDfDueDateAttribute() {
        $dueDate = $this->attributes["df_due_date"];
        return Str::replaceLast(":AM", ":0000AM", $dueDate);
    }

    public function bank(){
        return $this->hasOne("App\Models\Bank", "id", "bank_id");
    }
}
