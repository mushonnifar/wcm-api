<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesGroupUserAssg extends Model
{
    //
    protected $table = 'sales_group_user_assg';
    protected $fillable = [
	"sales_group_id","created_at","updated_at","created_by","updated_by","user_id",
    ];
}
