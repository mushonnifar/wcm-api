<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $table = 'wcm_m_menus';

    protected $fillable = [
        'name_ID', 'name_EN', 'url', 'order_no', 'icon', 'parent_id', 'status', 'created_by', 'updated_at'
    ];

    protected $hidden = [
        'isactive'
    ];

    public static function boot()
    {
        parent::boot();
        static::saving(function($model){
            $model->parent_id = (int) $model->parent_id;
        });
    }

    public static function ruleCreate() {
        $rules = [
            'name_ID' => 'required',
            'name_EN' => 'required'
        ];
        return $rules;
    }

    public static function rulePermission() {
        $rules = [
            'data' => 'required'
        ];
        return $rules;
    }


    public static function byRole($role_id, $language){

        $result = DB::table('wcm_role_has_menus as a')
        ->join('wcm_m_menus as b', 'a.menu_id', '=', 'b.id')
        ->select('b.id', 'b.name_'.$language.' as name', 'b.url', 'b.order_no', 'b.icon', 'b.parent_id')
        ->groupBy('b.id', 'b.name_'.$language, 'b.url', 'b.order_no', 'b.icon', 'b.parent_id')
        ->where('b.status', 'y')
        ->where('a.role_id', $role_id)
        ->orderBy('b.order_no', 'asc')
        ->get();

        return $result;
    }

    public static function getMenu(){
        $query = DB::table('wcm_m_menus as a')
        ->leftJoin('wcm_m_menus as b', function($q)
        {
            $q->on('a.parent_id', '=', 'b.id')
                ->where('b.status', '=', 'y');
        })
        ->select('a.id', 'a.name_ID', 'a.name_EN', 'a.url', 'a.order_no', 'a.icon', 'b.name_ID as parent_name_ID', 'b.name_EN as parent_name_EN')
        ->where('a.status', 'y');
        return($query);
    }

    public function roleHasMenu()
    {
        return $this->hasMany("App\Models\RoleHasMenu", "menu_id", "id");
    }
}
