<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesGroup extends Model {

    protected $table = 'wcm_sales_group';
    protected $fillable = [
        'id', 'code', 'name', 'sales_office_id', 'status','district_code', 'created_by', 'updated_by',
    ];

    public $incrementing = false;
    
    public function salesUnit(){
        return $this->hasMany("App\Models\SalesUnit", "sales_group_id", "id");
    }
    
}
