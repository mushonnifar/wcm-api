<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuURLMAPS extends Model
{
    //
    protected $table = 'wcm_menu_permission_maps';
    protected $fillable = ['menu_id','action_id','method','url'];
}
