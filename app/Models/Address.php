<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model {

    protected $table = 'wcm_address';
    
    public $incrementing = false;

    protected $fillable = [
        'address', 'tlp_no', 'fax_no', 'address_type', 'customer_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id',
        'village_id', 'created_by', 'updated_by','created_at', 'updated_at',
    ];


    public static function getAddessArea ($where=[])
    {
    	 $query = DB::table('wcm_address as tb1')
                ->leftjoin('wcm_sales_unit AS tb2', 'tb1.sales_unit_id', '=', 'tb2.id')
                ->leftjoin('wcm_sales_group AS tb3', 'tb1.sales_group_id', '=', 'tb3.id')
                ->leftjoin('wcm_sales_office AS tb4', 'tb1.sales_office_id', '=', 'tb4.id')
                ->select('tb1.*','tb2.name as sales_unit_name','tb3.name as sales_group_name','tb4.name as sales_office_name')
                ->where($where);
        return $query;
    }

}
