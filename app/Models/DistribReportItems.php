<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DistribReportItems extends Model
{
    //
    protected $table = 'wcm_distrib_report_item';
    protected $fillable = [
       'distrib_report_id','retail_id','product_id','qty','report_f5_id','status',
       'created_by','updated_by','created_at','updated_at',
    ];


    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function ruleCreate() {
        $rules = [
            'distrib_report_id' => 'required|exists:wcm_distrib_reports,id',
            'report_f5_id' => 'required|exists:wcm_report_f5,id',
            'retail_id' => 'required|exists:wcm_retail,id',
            'qty' => 'required',
            'product_id' => 'required|exists:wcm_product,id',
            'status' => 'required|in:d,y,n,p,s',
        ];
        return $rules;
    }

    public static function getForCalculationDO ($where = [])
    {
        $query = DB::table("wcm_distrib_report_item as tb1")
                ->leftJoin('wcm_retail as tb2','tb2.id','=','tb1.retail_id')
                ->leftJoin('wcm_distrib_reports as tb3','tb3.id','=','tb1.distrib_report_id')
                ->select('tb1.*','tb2.sales_unit_id', 'tb3.month')
                ->where($where);
                // ->where('tb3.distribution_date','<=',$whereStr);
         return $query;
    }

    public static function bastPrint($where = [])
    {
        $query = DB::table("wcm_distrib_report_item as tb1")
                ->leftJoin('wcm_retail as tb2','tb2.id','=','tb1.retail_id')
                ->leftJoin('wcm_distrib_reports as tb3','tb3.id','=','tb1.distrib_report_id')
                ->leftJoin('wcm_product as tb4','tb4.id','=','tb1.product_id')
                ->leftJoin('wcm_sales_unit as tb5','tb5.id','=','tb2.sales_unit_id')
                ->leftJoin('wcm_sales_group as tb6','tb6.id','=','tb5.sales_group_id')
                ->select('tb1.*','tb2.code','tb2.owner','tb2.address','tb2.name as retail_name','tb2.sales_unit_id','tb4.name as product_name','tb5.name as sales_unit_name','tb6.name as sales_group_name','tb3.distribution_date')
                ->where($where);
                // ->where('tb3.distribution_date','<=',$whereStr);
         return $query;
    }

    public static function DitribItems ($where=[])
    {
        $query = DB::table("wcm_distrib_report_item as tb1")
                    ->leftJoin('wcm_retail as tb2','tb2.id','=','tb1.retail_id')
                    ->leftJoin('wcm_distrib_reports as tb3','tb3.id','=','tb1.distrib_report_id')
                    ->leftJoin('wcm_product as tb4','tb4.id','=','tb1.product_id')
                    ->leftJoin('wcm_sales_unit as tb5','tb5.id','=','tb2.sales_unit_id')
                    ->leftJoin('wcm_sales_group as tb6','tb6.id','=','tb5.sales_group_id')
                    ->leftJoin('wcm_sales_office as tb7','tb7.id','=','tb6.sales_office_id')
                    ->leftJoin('wcm_customer as tb8','tb8.id','=','tb3.customer_id')
                    ->leftJoin('wcm_sales_org as tb9','tb9.id','=','tb3.sales_org_id')
                    ->leftJoin('wcm_orders as tb10','tb10.id','=','tb3.order_id')
                    ->leftJoin('wcm_months as tb11','tb11.id','=','tb3.month')
                    ->select('tb3.uuid as uuid','tb3.number as number_pkp','tb9.id as produsen_id','tb9.name as produsen_name','tb8.full_name as customer_name',
                             'tb8.id as customer_id','tb3.distrib_resportable_type as distrib_type',
                             'tb10.number as order_number','tb3.so_number','tb3.month','tb11.name as month_name','tb3.year',
                             DB::raw('CONVERT(VARCHAR(10), tb3.distribution_date, 105) as distribution_date'),
                             'tb7.id as sales_office_id','tb7.name as sales_office_name',
                             'tb6.id as sales_group_id','tb6.name as sales_group_name',
                             'tb5.id as sales_unit_id','tb5.name as sales_unit_name',
                            'tb2.id as retail_id', 'tb2.code as retail_code','tb2.name as retail_name',
                            'tb4.id as product_id', 'tb4.name as product_name',DB::raw("CAST(CONVERT(DECIMAL(10,3),qty) as nvarchar) as qty"),'tb1.status',
                             DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Approve'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name")
                            )
                    ->where($where);
                    //->where('tb1.qty','!=', '0');
         return $query;
    }

    public static function distrib_items($where=[])
    {
        $query = DB::table("wcm_distrib_report_item as tb1")
                    ->leftJoin('wcm_distrib_reports as tb2','tb2.id','=','tb1.distrib_report_id')
                    ->select('tb1.*')
                    ->where($where);

         return $query;
    }
}
