<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testing extends Model
{
    //
    protected $table    = "wcm_testing";
    protected $fillable = ["so_number"];
    public $timestamps  = false;
}
