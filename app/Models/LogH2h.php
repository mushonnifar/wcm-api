<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class LogH2h extends Model
{
    protected $table    = "wcm_log_h2h";
    protected $fillable = [
        "order_id", "pengirim_req", "isi_req",
        "http_status", "status", "isi_respon",
    ];

    public $timestamps = false;

    public function scopeGetList($query)
    {
        return $query;
    }

    public function gettimestampReqAttribute($value)
    {
        return Carbon::parse($value)->format("d-m-Y H:i:s");
    }

    public static function getExportedColumns()
    {
        return [
            "order_id AS No Penebusan",
            "timestamp_req AS Timestamp",
            "pengirim_req AS Nama Pengirim",
            "ip_address_req AS IP Address Pengirim",
            "isi_req as Isi Request",
            "http_status AS HTTP",
            "status",
            "timestamp_respon AS Timestamp Response",
            "pengirim_respon AS Pengirim Respon",
            "ip_address_respon" => "ip_address_respon",
            "isi_respon as Isi Response",
            "request_type AS Request Type",
        ];
    }
}
