<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportF6Items extends Model
{
    //
    protected $table    = 'wcm_report_f6_items';
    protected $fillable = [
        'product_id', 'stok_awal', 'penebusan', 'penyaluran', 'stok_akhir', 'report_f6_id', 'retail_id', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

    public static function createf6ItemsRule()
    {
        $rules = [
            'product_id'   => 'required|exists:wcm_product,id',
            'retail_id'    => 'required|exists:wcm_retail,id',
            'report_f6_id' => 'required|exists:wcm_report_f6,id',
            'stok_awal'    => 'required',
            'penebusan'    => 'required',
            'penyaluran'   => 'required',
            'stok_akhir'   => 'required',
        ];
        return $rules;
    }
    public static function createf6ItemsHead()
    {
        $rules = [
            'status'       => 'required',
            'report_f6_id' => 'required',
        ];
        return $rules;
    }

    public static function getStokReportF6($where = [])
    {
        $query = DB::table('wcm_report_f6_items as tb1')
            ->leftjoin('wcm_report_f6 as tb2', 'tb2.id', '=', 'tb1.report_f6_id')
            ->select('tb1.*')
            ->where($where);
        return $query;
    }

    public function scopeRekap($query, $uuid=null)
    {
        $headQuery = DB::table(
            DB::raw("
                (
                    SELECT tb_f6.*, tb_customer.full_name as customer_name from wcm_report_f6 as tb_f6 LEFT JOIN wcm_customer as tb_customer
                    ON (tb_f6.customer_id = tb_customer.id)
                ) as tbl
            ")
        );

        if (!is_null($uuid)) {
            $headQuery->where("uuid", $uuid);
        }


        $query = DB::table(DB::raw("(". $headQuery->toSql().") as tb_f6"))
            ->mergeBindings($headQuery)
            ->leftJoin("wcm_sales_org as tb_sales_org","tb_f6.sales_org_id", "tb_sales_org.id")
            ->leftJoin("wcm_report_f6_items as tb_f6_items", "tb_f6_items.report_f6_id", "tb_f6.id")
            ->leftJoin("wcm_retail as tb_retail", "tb_retail.id", "tb_f6_items.retail_id")
            ->leftJoin("wcm_sales_office as tb_sales_office",
                "tb_sales_office.id",
                DB::raw("RIGHT('00'+ SUBSTRING(tb_f6.sales_group_id, 1,2), 4)")
            )
            ->leftJoin("wcm_sales_group as tb_sales_group", "tb_sales_group.id", "tb_f6.sales_group_id")
            ->leftJoin("wcm_sales_unit as tb_sales_unit", "tb_sales_unit.id", "tb_retail.sales_unit_id")
            ->select(
                "tb_f6.*",
                "tb_sales_org.name as sales_org_name",
                "tb_sales_office.id as sales_office_id",
                "tb_sales_office.name as sales_office_name",
                "tb_sales_group.district_code as district_code",
                "tb_sales_group.name as sales_group_name",
                "tb_sales_unit.name as sales_unit_name",
                "tb_f6.month", "tb_f6.year", "tb_f6.number",
                "tb_retail.sales_unit_id as sales_unit_id",
                "tb_retail.name as retail_name",
                "tb_retail.code as retail_code",
                "tb_f6_items.*"
            );
        return $query;
    }

}
