<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryMethod extends Model {

    protected $table = 'wcm_delivery_method';
    protected $fillable = [
        'ident_name', 'name', 'desc', 'created_by', 'updated_by',
    ];

}
