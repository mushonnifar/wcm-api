<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MonitoringDF extends Model
{
    protected $table = 'wcm_order_df_status';
    public static function getAll($where = [],$exported=false)
    {
        $query = DB::table('wcm_orders AS tb1')
            ->join('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
            ->join('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
            ->leftjoin('wcm_contract AS tb4', 'tb1.contract_id', '=', 'tb4.id')
            ->leftjoin('wcm_sales_office AS tb5', 'tb1.sales_office_id', '=', 'tb5.id')
            ->leftjoin('wcm_sales_group AS tb6', 'tb1.sales_group_id', '=', 'tb6.id')
            ->leftjoin('wcm_delivery_method AS tb7', 'tb1.delivery_method_id', '=', 'tb7.id')
            ->leftjoin('wcm_bank AS tb8', 'tb1.bank_id', '=', 'tb8.id')
            ->join('wcm_order_df_status AS tb9', function ($join){
                $join->on('tb1.id', 'tb9.order_id');
                // $join->where("tb9.status", "n");
            })
            ->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.reference_code', 'tb3.id as customer_id', 'tb3.full_name as customer_name', 'tb2.id as sales_org_id', 'tb2.name as sales_org_name', 'tb8.id as bank_id', 'tb8.name as bank_name', 'tb1.billing_fulldate', 'tb1.order_date','tb1.status', DB::raw("
                (CASE
                    WHEN tb1.status = 'l' THEN 'PAID'
                    WHEN tb1.status = 'u' THEN 'DPPAID'
                    WHEN tb1.status = 'x' THEN 'CLOSE'
                    WHEN tb1.status = 'r' THEN 'REVIEWED'
                    WHEN tb1.status = 'c' THEN 'COMPLETE'
                    WHEN tb1.status = 's' THEN 'SUBMITTED'
                    WHEN tb1.status = 'k' THEN 'GOOD ISSUE'
                    ELSE '-'
                END) as status_order"), 'tb9.error_message_send_order', 'tb1.so_number', 'tb5.id as sales_office_id', 'tb5.name as sales_office_name', 'tb6.id as sales_group_id', 'tb6.name as sales_group_name', 'tb1.payment_method',
                'tb1.total_price_before_ppn', 'tb1.total_price' , DB::raw("
                (CASE
                    WHEN tb9.status = 'y' THEN 'Success'
                    WHEN tb9.status = 'n' THEN 'Failed'
                    ELSE '-'
                END) as status_sent"),
                DB::raw("(CASE
                    WHEN tb1.disbursement_status ='v' THEN 'BELUM'
                    WHEN tb1.disbursement_status ='w' THEN 'SUDAH'
                    ELSE '-'
                END) as disbursement_status_name "),'tb1.disbursement_status','tb9.error_message_disbursement', 'tb9.send_order_count',
                DB::raw('CONVERT(VARCHAR(10), tb1.created_at, 105) as created_at'),
                DB::raw('CONVERT(VARCHAR(10), tb1.updated_at, 105) as updated_at'),
                DB::raw('CONVERT(VARCHAR(10), tb1.df_due_date, 105) as df_due_date'),
                "tb9.status as status_send_invoice"
            )
            ->where('tb1.status', '!=', 'd')
            ->whereIn("tb1.payment_method", ["Distributor Financing", "DF", "D"])
            ->where($where);

        $final = DB::table(DB::raw('(' . $query->toSql(). ') as tbl'));
        if($exported){
            $final->select(
            'tbl.number','tbl.reference_code','tbl.customer_name','tbl.sales_org_name','tbl.bank_name','tbl.billing_fulldate','tbl.df_due_date','tbl.order_date','tbl.status_order','tbl.disbursement_status_name','tbl.error_message_send_order','tbl.order_date','tbl.so_number','tbl.sales_office_name','tbl.sales_group_name','tbl.payment_method','tbl.total_price_before_ppn','tbl.status_sent','tbl.error_message_disbursement','tbl.send_order_count');
        }else{
            $final->select('*');
        }
        return $final->mergeBindings($query);
    }
}
