<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model {

    protected $table = 'wcm_commodity';
    protected $fillable = [
        'name', 'status', 'created_by', 'updated_by',
    ];

    public static function getBulkIDCommodities() {
        $rules = [
            'commodities' => 'required',
        ];
        return $rules;
    }

}
