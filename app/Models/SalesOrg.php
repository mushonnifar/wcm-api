<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class SalesOrg extends Model {

    protected $table = 'wcm_sales_org';
    public $incrementing = false;
    protected $fillable = [
        'id', 'code', 'name', 'from_date', 'thru_date', 'bca_df_code', 'bca_h2h_code', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at','config_lintas_alur'
    ];

    public static function getDataRelation($where = array()) {
        $select = array('a.id', 'a.uuid', 'a.name', 'b.customer_id', 'c.full_name as customer_name');
        $query = DB::table('wcm_sales_org as a')
                ->leftJoin('wcm_customer_retailer_assg as b', function ($q) {
                    $q->on('a.id', '=', 'b.sales_org_id');
                })
                ->leftJoin('wcm_customer as c', function ($q) {
                    $q->on('b.customer_id', '=', 'c.id');
                })
                ->select($select)
                ->where($where)
                ->groupBy('a.id', 'a.uuid', 'a.name', 'b.customer_id', 'c.full_name');
        return ($query);
    }

    public static function getAll($except, $where = [], $id = null) {
        if ($except === 1) {
            $query = DB::table('wcm_sales_org AS tb1')
                    ->where('id', '!=', function($sub) use ($id) {
                        $sub->select('sales_org_id')->from('wcm_cust_sales_org_assg')->where('customer_id', $id);
                    })
                    ->select('tb1.*');
        } else {
            if (count($where) > 0) {
                $query = DB::table('wcm_sales_org AS tb1')
                        ->leftJoin('wcm_cust_sales_org_assg AS tb2', 'tb1.id', '=', 'tb2.sales_org_id')
                        ->select('tb1.*', 'tb2.customer_id')
                        ->where($where);
            } else {
                $query = DB::table('wcm_sales_org AS tb1');
            }

        }
        return $query;
    }

    public function getStatusAttribute($value)
    {
        return $value ? 'true': 'false';
    }

    public function salesArea()
    {
        return $this->hasMany("App\Models\SalesArea","sales_org_id", "id");
    }

    public static function getExportedColumns() {
        return [
            "id as Kode Produsen", 
            "name as Nama Produsen", 
            DB::raw(
                "(CASE WHEN status = '1' THEN 'TRUE' ELSE 'false' END) as Status"
            )
        ];
    }
}
