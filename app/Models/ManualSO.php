<?php

namespace App\Models;

use App\Rules\ClearingPaymentDateRule;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ManualSO extends Model {

    protected $table = 'wcm_manual_so';
    protected $fillable = [
        'booking_code', 'sender', 'bank_id', 'payment_date', 'amount',
        'status', 'created_by', 'updated_by',
    ];

    public static function ruleCreate() {
        $rules = [
            'booking_code' => 'required|exists:wcm_orders,booking_code',
            'sender' => 'required',
            'bank_id' => 'required|exists:wcm_bank,id',
            'payment_date' => ["required", new ClearingPaymentDateRule],
            'amount' => 'required|integer',
        ];
        return $rules;
    }
    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

}
