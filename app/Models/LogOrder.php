<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LogOrder extends Model {

    protected $table = 'wcm_log_order';
    protected $fillable = [
        'order_id', 'message', 'kode',
        'status', 'created_by', 'updated_by',
    ];

    public static function ruleCreate() {
        $rules = [
            'order_id' => 'required|exists:wcm_orders,id',
            'message' => 'required',
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getAll() {
        $query = DB::table('wcm_log_order AS tb1')
                ->join('wcm_orders AS tb2', 'tb1.order_id', '=', 'tb2.id')
                ->join('wcm_customer AS tb3', 'tb2.customer_id', '=', 'tb3.id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.order_id', 'tb1.message', 
                        'tb1.status', 'tb2.number AS order_number', 'tb2.customer_id', 'tb3.full_name as customer_name','tb1.kode as kode_error', 
                        'tb1.created_by', 'tb1.updated_by', 'tb1.kode','tb2.so_number','tb2.bank_payment','tb2.sap_billing_dp_doc','tb2.sap_clearing_doc','tb2.sap_booking_code',
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"), 
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                );

        return $query;
    }

    public static function getExportedColumns()
    {
        return [
            'tb2.number AS Nomer Order',
            'tb3.full_name AS Distributor',
            'tb1.status',
            'tb1.message AS Message Error',
            'tb1.created_at AS Date',
        ];
    }

    public function items()
    {
        return $this->hasMany("App\Models\LogOrderItem");
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function order(){
        return $this->belongsTo("App\Models\Order");
    }
}
