<?php

namespace App\Helpers;

use Request;
use App\Models\LogActivity as LogActivityModel;

class LogActivity {

    public static function addToLog($subject, $count = 0, $status = '') {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : 0;
        $log['count_data'] = $count;
        $log['status'] = $status;
        LogActivityModel::create($log);
    }

    public static function logActivityLists() {
        return LogActivityModel::latest()->get();
    }

}
