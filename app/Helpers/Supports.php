<?php

use Carbon\Carbon;
use Illuminate\Support\Str;
function is_uuid($uuid) {
    $re = '/^[a-z0-9\-]{36}$/i';
    preg_match_all($re, $uuid, $matches, PREG_SET_ORDER, 0);
    if(!$matches){
        $response = responseFail(trans('messages.check_uuid-fail'));
        $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
        $return->throwResponse();
    }
}


function getBankClass($bankName) {
    return "\App\Libraries\\" . Str::title(Str::after($bankName, "Bank "));
}

function currentTriwulan($showPrevius = false) 
{
    $currentMonth = (int) Carbon::now()->format("m");
    $triwulan = collect([
        1 => [1,2,3],
        2 => [4,5,6],
        3 => [7,8,9],
        4 => [10,11,12]
    ]);

    $key = (int) ceil($currentMonth/3);
    if ($showPrevius) {
        return $triwulan->slice(0,$key)
            ->flatten()
            ->toArray();
    }

    return $triwulan->get($key);

}

function isValidRangeDate(array $values) {
    if (count($values) == 2) {
        $rangeStart = $rangeEnd = false;
        try {
            $rangeStart = Carbon::parse($values[0]);
            $rangeEnd = Carbon::parse($values[1]);
        }catch(Exception $e){}

        if ($rangeStart && $rangeEnd) {
            $diff = $rangeStart->diff($rangeEnd);
            if ($diff->d > 0 && $diff->invert == 0) {
                return  true;
            } 
        }
    }
    return false;

}

function parseDate($str) {
    if (!$str) {
        return null;
    }
    $str = preg_replace("@\\\/@", "-", $str);
    $str = preg_replace("@/@", "-", $str);

    if (strpos($str, "PM") !== false || strpos($str,"AM") !== false) {
        $s = preg_replace("@([0-9]{2}:|AM|PM)@", "",$str);
        return Carbon::parse($s);
    }

    return Carbon::parse($str);
}

function checkSAPResponse($params)
{
    $response = ["status" => false, "data" => null];

    if (is_object($params) && property_exists($params, "RETURN")) {
        $items = property_exists($params->RETURN, "item") ? $params->RETURN->item : $params->RETURN;
        if (is_array($items)) {
            $items = collect($items);
            $isError = $items->pluck("TYPE")->contains("E");
            $response["status"] = !$isError;

            if ($isError) {
                $response["data"] = $items->map(function($item){
                    return [
                        "error_message" => @$item->MESSAGE,
                        "error_type" => @$item->TYPE,
                        "error_id" => @$item->ID,
                        "error_number" => @$item->NUMBER,
                    ];
                });
            }
        }
        elseif (is_object($items)) {
            $response["status"] = (@$items->TYPE != "E");
            // Same Format Because create Many
            $response["data"] = [
                [
                    "error_message" => @$items->MESSAGE,
                    "error_type" => @$items->TYPE,
                    "error_id" => @$items->ID,
                    "error_number" => @$items->NUMBER,
                ]
            ];
        }
    }
    
    return $response;
}

function getRLock($plantCode, $productId) {
    if ($plantCode !== "B101" && $plantCode !== "C740") return "2N01";
    if ($plantCode === "C740") {
        if ($productId == "P02") return "4N01";
        return "2N01";
    }
    
    switch ($productId) {
        case "P01":
            return "2N02";
        case "P51":
        case "P02":
            return "2N05";
        case "P03":
            return "2N09";
        case "P04":
            return "2N01";
        case "P05":
            return "2N03";
    }
}

function getUOM($productId)
{
    return $productId === "P51" ? "L" : "TON";
}

function validateUOM($uom, $productId)
{
    $uomTxt = getUOM($productId) === "L" ? "LITER" : "TON";
    return $uom === $uomTxt;
}