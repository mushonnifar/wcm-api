<?php

namespace App\Jobs;

use App\Libraries\PaymentGateway;
use App\Models\Bank;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class SendInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $bankID;
    private $orderID;

    public function __construct($bankID, $orderID)
    {
        $this->bankID = $bankID;
        $this->orderID = $orderID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bank = Bank::find($this->bankID);
        if ($bank) {
            $bankClass = getBankClass($bank->name);
            $payment = new $bankClass;
            DB::transaction(function() use ($payment) {
                $order = Order::find($this->orderID);
                $invoice = $payment->sendInvoice($order);
                $payment->setOrder($order);
                $payment->writeOrderDFStatus($invoice);
            });
        }
    }

}
