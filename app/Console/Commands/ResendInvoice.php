<?php

namespace App\Console\Commands;

use App\Jobs\SendInvoice;
use App\Models\Bank;
use App\Models\Order;
use App\Models\OrderDFStatus;
use Illuminate\Console\Command;

class ResendInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend Failed Invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info("Preparing for Resend Invoice");
        $failed = OrderDFStatus::where("status", "n")
            ->where("send_order_count", "<=5")
            ->pluck("order_id");
        $orders = Order::whereIn("id", $failed)->get();
        $this->info("Processing " . $orders->count() . " data.");
        $bar = $this->output->createProgressBar($orders->count());
        $orders->each(function($order) use (&$bar) {
            dispatch((new SendInvoice($order->bank_id, $order->id))->onQueue("low"));
            $bar->advance();
        });

        $bar->finish();
    }

    
}
