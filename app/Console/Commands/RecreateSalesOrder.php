<?php

namespace App\Console\Commands;

use App\Jobs\CreateSalesOrder;
use App\Libraries\PaymentGateway;
use App\Models\Order;
use Illuminate\Console\Command;

class RecreateSalesOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recreate:so';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recreate SO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cashs = Order::where("status", "l")
            ->where("payment_method", PaymentGateway::CASH)
            ->where("so_number", null)
            ->pluck("booking_code")
            ->filter();

        $this->info("Founding Cash Order " . $cashs->count());
        $cashs->each(function($bookingCode){
            $this->info("Dispatching " . $bookingCode);
            CreateSalesOrder::dispatch($bookingCode);
        });
        
        $dfs =  Order::where("status", "u")
            ->where("payment_method", PaymentGateway::DF)
            ->where("so_number", null)
            ->pluck("booking_code")
            ->filter();
        $this->info("Founding DF Order " . $dfs->count());
        $dfs->each(function($bookingCode){
            $this->info("Dispatching " . $bookingCode);
            dispatch((new CreateSalesOrder($bookingCode)))->onQueue("low");
        });


    }
}
