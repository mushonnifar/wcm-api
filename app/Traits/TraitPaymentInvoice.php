<?php

namespace App\Traits;

use App\Libraries\RestClient;
use Illuminate\Support\Arr;

trait TraitPaymentInvoice
{
    public function sendInvoice($data)
    {
        $params = $this->getParamSendInvoice($data);

        return $this->doSendInvoice($params);
    }

    private function doSendInvoice($params)
    {
        $countSendOrder = 1;
        $client         = new RestClient();
        
        $urlSend = $this->getRequestUriInvoice();
        if (!$urlSend) {
            $response = collect($this->getErrorInvoice());
        } else {
            if ($this->name === "BNI") {
                $response = collect($client->post($urlSend, $params));
            } else {
                $response = collect(
                    $client->postJSON(
                        $urlSend, 
                        $params,
                        $this->getBankRequestAuth()
                    )
                );
            }
        }
        // if ($response->isEmpty()) {
        //     $response = collect($this->getResponseInvoice());
        // }
        $response  = $response->merge(["ip_response" => parse_url($this->getRequestUriInvoice(), PHP_URL_HOST)]);

        $this->writeLogSend(collect($params), $response, "SEND_INVOICE");

        return $response;
    }

    private function getResponseInvoice()
    {
        $response = [
            "Mandiri" => json_decode('{"rq_uuid":"97c77a3a-100f-4c16-838b-48c259ff31e6","rs_datetime":"2016-10-05 13:00:07","error_code":"0000","error_msg":"success","payment_ref":"284139","message":""}'),
            "BRI"     => json_decode('{"rq_uuid":"470cd7fe-aec2-4793-b958-97c761ea2ab6","timestamp":"2016-10-04 14:05:17","error_code":"0000","error_msg":"-","message":"Send invoice berhasil diproses","payment_ref":"201610043280017761"}'),
            "BNI"     => json_decode('{"rq_uuid":"a63437e6-f8a5-4784-80d3-02321512f2d6","timestamp":"2016-10-18 17:18:10","PASSWORD":"","comm_code":"C000","customerNo":"","payment_ref":"","salesOrderNumber":"","amount":"","currency":"","transaction_date":"","error_code":"0000","error_msg":"Sukses"}'),
        ];

        return Arr::get($response, $this->name, null);
    }

    private function getErrorInvoice()
    {
        $response = [
            "Mandiri" => [
                json_decode('{"rq_uuid":"38ff5926-9553-40a5-bd18-4df90e0293b9","rs_datetime":"2016-11-14 13:00:31","error_code":"4204","error_msg":"Due Date is a holiday","payment_ref":"","message":""}'),
                json_decode('{"rq_uuid":"6b682f2d-93ec-450c-b81d-9e3fcd845d4c","rs_datetime":"2016-11-14 09:00:32","error_code":"3102","error_msg":"Document No is duplicate","payment_ref":"","message":""}'),
            ],
            "BRI"     => [
                json_decode('{"rq_uuid":"0fa49a7e-7d06-48c5-b623-3d79b56228af","timestamp":"2016-12-05 12:39:48","error_code":"0017","error_msg":"Pupuk Indonesia kirim pesan dengan error : Duplicate Sales Order Number)","message":"Send invoice tidak dapat diproses","payment_ref":"201612053280023894"}'),
                json_decode('{"rq_uuid":"9f0c3e57-e092-4ada-863b-a6e22d6c9b82","timestamp":"2017-05-10 15:35:55","error_code":"0004","error_msg":"Bank Core Banking System still doing the EOD process","message":"Send invoice Gagal diproses","payment_ref":"201705103100145242"}'),
                json_decode('{"rq_uuid":"6ecbb58a-6848-46b0-9c29-326f9c98ea94","timestamp":"2018-01-04 10:05:46","error_code":"0021","error_msg":"Due Date tidak valid","message":"Send invoice tidak dapat diproses","payment_ref":null}'),
            ],
            "BNI"     => [
                json_decode('{"rq_uuid":"c488267c-9e5c-48a2-8329-899d6b7c0c81","rs_datetime":"2019-05-07 11:24:43","error_code":"0009","error_msg":"Configuration error: docstore relation not found"}'),
                json_decode('{"rq_uuid":"be422717-879c-4266-a6e4-05cdd9b0129e","rs_datetime":"2019-03-22 17:49:06","error_code":"0019","error_msg":"Duplicate Invoice Reference... Found 1 invoice with reference [3460103843] and customer [1000000151]"}'),
                json_decode('{"rq_uuid":"dc924694-0061-47c2-bb10-7cf8c7e587e3","rs_datetime":"2019-02-01 12:49:37","error_code":"0014","error_msg":"Cannot fetch CPO: Incorrect result size: expected 1, actual 2"}'),
            ],
        ];

        return Arr::get($response, $this->name, null);
    }

    private function getRequestUriInvoice()
    {
        $url = [
            // DEV
            // "Mandiri" => 'https://10.243.129.200:11201/rest/mdr/scmconsumer/restservices/sendInvoice',
            // "BRI"     => 'http://192.168.188.193:177/PihcWS/index.php/ws/transactionpihc/send_invoice/format/json',
            // "BNI"     => 'https://s4.asyx.com/api/bni/send_invoice',

            // PROD
            "Mandiri" => 'https://10.20.30.4:4001/rest/mdr/scmconsumer/restservices/sendInvoice',
            "BRI"     => 'http://192.168.188.193:99/PihcWS/index.php/ws/transactionpihc/send_invoice/format/json',
            "BNI"     => 'https://bnifscm.bni.co.id/api/bni/send_invoice',
        ];

        return Arr::get($url, $this->name, null);
    }
}
