<?php

namespace App\Traits;

use App\Libraries\RestClient;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDFStatus;
use App\Models\SalesOrg;
use Illuminate\Support\Arr;

trait TraitCheckLimit
{

    /**
     * send request check Limit
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function requestCheckLimit()
    {
        $params   = $this->getParamCheckLimit();
        $client   = new RestClient;
        if ($this->name == "BNI") {
            $response = collect($client->post($this->getRequestUriCheckLimit(), $params));
        } else {
            $response = collect(
                $client->postJSON(
                    $this->getRequestUriCheckLimit(), 
                    $params, 
                    $this->getBankRequestAuth()
                )
            );
        }
        // if ($response->isEmpty()) {
        //     $response = collect($this->responseCheckLimit());
        // }
        $domain = parse_url($this->getRequestUriCheckLimit(), PHP_URL_HOST);
        $response = $response->merge(["ip_response" => $domain]);
        $this->writeLogSend(collect($params), $response, "CHECK_LIMIT");
        //return $this->errorResponseCheckLimit();
        // 
        // if (Arr::get($response, 'error_code') !== '0000') {
        //     return $response;
        // }

        return $this->parseResponse($response);
    }

    private function responseCheckLimit()
    {
        $response = [
            "Mandiri" => json_decode('{"rq_uuid":"f8652864-cc17-412e-8c15-717774ed07aa","rs_datetime":"2016-09-25 23:54:41","error_code":"0000","error_msg":"success","message":"","plafon_limit":"1200000000.0000","outstanding_limit":"0.00","available_limit":"1200000000.00","ccy_code":"IDR"}'),
            "BRI"     => json_decode('{"rq_uuid":"cb6fafb9-609a-4b34-bcc5-bf9c9d6b01d9","timestamp":"2016-09-16 15:51:28","error_code":"0000","error_msg":"-","message":"Inquiry limit berhasil. Plafon: 500000000, Outstanding: 24054339.09, Available Limit: 475945660.91, Hold: 0","plafon_limit":500000000,"outstanding_limit":24054339.09,"available_limit":475945660.91,"ccy_code":"IDR","Hold":0,"maturity_date":"2016-12-29"}'),
            "BNI"     => json_decode('{"rq_uuid":"94457731-f0db-4daa-afa9-9bba5d17d81f","rs_datetime":"2016-10-06 15:19:42","error_code":"0000","error_msg":"SUCCESS","message":"OK BNI API","plafon_limit":1000000000.0,"outstanding_limit":0.0,"available_limit":1000000000.0,"hold_limit":0.0,"ccy_code":"IDR","expired_date":"2017-01-13"}'),
        ];

        return Arr::get($response, $this->name, null);
    }

    private function errorResponseCheckLimit()
    {
        $errorResponse = [
            "Mandiri" => [
                json_decode('{"rq_uuid":"ea7968f9-146e-4074-8a21-e8612dab243a","rs_datetime":"2016-08-31 09:24:41","error_code":"1003","error_msg":"member_id not found","message":"","plafon_limit":"","outstanding_limit":"","available_limit":"","hold_limit":"0.00","expired_date":"31/12/2016","ccy_code":""}'),
                json_decode('{"rq_uuid":"79950e97-23d9-4363-b8ef-d1f390142d3e","rs_datetime":"2016-09-23 09:25:41","error_code":"0001","error_msg":"Invalid Password"}'),
            ],
            "BRI"     => [
                json_decode('{"rq_uuid":"ecc1bac5-9b79-4ce8-9014-2b5f1ac2b89a","timestamp":"2016-05-09 09:56:13","error_code":"0011","error_msg":"Pupuk Indonesia kirim pesan dengan format salah","message":"Inquiry limit tidak dapat diproses","plafon_limit":null,"outstanding_limit":null,"available_limit":null,"ccy_code":"IDR","Hold":null,"maturity_date":""}'),
            ],
            "BNI"     => [
                json_decode('{"rq_uuid":"77c0fb7f-6642-4ba1-afc8-adc28c7d6d95","rs_datetime":"2017-07-18 14:13:47","error_code":"0010","error_msg":"Configuration error: credit summary not found"}'),
                json_decode('{"rq_uuid":"8e844cd7-f21c-442f-896e-d588786c4c9a","rs_datetime":"2019-01-07 14:32:45","error_code":"0011","error_msg":"Cannot get holding limit: blocked by: [SERVICE_UNAVAILABLE/1/state not recovered / initialized];[SERVICE_UNAVAILABLE/2/no master];"}'),

            ],
        ];

        $response = Arr::get($errorResponse, $this->name, null);

        return Arr::random($response);
    }

    private function parseResponse($params)
    {
        $keys   = array_keys($this->getParamResponsePlavon());
        $values = collect($keys)->transform(function ($item) use ($params) {
            return $params->get($this->getKeyReponseParam($item), "");
        })->toArray();

        $response = collect($keys)->combine($values)->toArray();
        $response += $this->calculateOutstanding();
        $response["available"] = ((float) $response["available_amount"]) - ((float) $response["amount"]);

        if ($this->name === "Mandiri" && $response["status"] !== "Active") {
            $response["available"] = -1;
        }

        return (object) $response;
    }

    private function calculateOutstanding()
    {
        $customer = Customer::find(request()->get("customer_id"));
        $salesOrg = SalesOrg::find(request()->get("sales_org_id"));

        $orders = Order::where("sales_org_id", @$salesOrg->id)
            ->where("customer_id", @$customer->id)
            ->where("payment_method", self::DF)
            ->whereHas("bank", function($qb){
                $qb->where("name", "like", "%{$this->name}");
            })
            ->whereIn("id", OrderDFStatus::whereIn("status", ["n","s"])->pluck("order_id"))
            ->whereIn("status", ["u","s","y","c","k","r"])
            ->get();

        $order    = $orders->first();
        $customer = [
            "customer_id"    => $order ? $order->customer_id: @$customer->id,
            "customer_name"  => $order ? $order->customer->full_name: @$customer->full_name,
            "sales_org_id"   => $order ? $order->sales_org_id: @$salesOrg->id,
            "sales_org_name" => $order ? $order->salesorg->name: @$salesOrg->name,
            "bank_name"      => $this->name,
        ];

        $outstanding = [
            "outstanding_so_number" => $order ? $orders->pluck("number")->toArray(): "",
            "outstanding_so"        => $order ? $orders->count(): 0,
            "amount"                => $order ? $orders->sum("total_price_before_ppn"): 0,
            "available"             => 0,
        ];

        return $customer + $outstanding;
    }

    private function getKeyReponseParam($key)
    {
        return Arr::get($this->getParamResponsePlavon(), "{$key}." . strtolower($this->name));
    }

    private function getParamResponsePlavon()
    {
        return [
            "expired_date"     => [
                "bri"     => "maturity_date",
                "bni"     => "expired_date",
                "mandiri" => "expired_date",
            ],
            "credit_limit"     => [
                "bri"     => "plafon_limit",
                "bni"     => "plafon_limit",
                "mandiri" => "plafon_limit",
            ],
            "outstanding_df"   => [
                "bri"     => "outstanding_limit",
                "bni"     => "outstanding_limit",
                "mandiri" => "outstanding_limit",
            ],
            "available_amount" => [
                "bri"     => "available_limit",
                "bni"     => "available_limit",
                "mandiri" => "available_limit",
            ],
            "hold_amount"      => [
                "bri"     => "Hold",
                "bni"     => "hold_limit",
                "mandiri" => "hold_limit",
            ],
            "status"      => [
                "bri"     => null,
                "bni"     => null,
                "mandiri" => "limit_status",
            ],
        ];
    }

    private function getRequestUriCheckLimit()
    {
        $url = [
            //DEV
            // "Mandiri" => 'https://10.243.129.200:11201/rest/mdr/scmconsumer/restservices/distributionLimitInquiry',
            // "BRI"     => 'http://192.168.188.193:177/PihcWS/index.php/ws/transactionpihc/inquiry_limit/format/json',
            // "BNI"     => 'https://s4.asyx.com/api/bni/check_limit',

            //PROD
            "Mandiri" => 'https://10.20.30.4:4001/rest/mdr/scmconsumer/restservices/distributionLimitInquiry',
            "BRI"     => 'http://192.168.188.193:99/PihcWS/index.php/ws/transactionpihc/inquiry_limit/format/json',
            "BNI"     => 'https://bnifscm.bni.co.id/api/bni/check_limit',
        ];

        return Arr::get($url, $this->name, null);
    }
}
