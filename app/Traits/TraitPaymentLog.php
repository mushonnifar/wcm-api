<?php

namespace App\Traits;

use App\Models\LogH2h;
use App\Models\OrderDFStatus;
use Carbon\Carbon;
use Illuminate\Support\Collection;

trait TraitPaymentLog
{
    public function writeLog($params, $response)
    {
        $sender = false;
        if (isset($response["sender"])) {
            $sender = $response["sender"];
            $response = $response["response"];
        }
        if (!$response instanceof Collection) {
            $response = collect($response);
        }

        $order                = property_exists($this, "order") ? $this->order : false;
        $logH2h               = new LogH2h;
        $logH2h->order_id     = $order ? $order->number : 0;
        $logH2h->pengirim_req = $this->name;
        $logH2h->isi_req      = json_encode($params->all());
        $http_status          = $order ? 'OK' : 'ERROR';
        $status               = $order ? 200 : 500;

        if (in_array($this->type, ["checkLimit", "disbursement"])) {
            $http_status = $response->get("error_code") == "0000" ? "OK" : "ERROR";
            $status      = $response->get("error_code") == "0000" ? 200 : 500;
        }

        $logH2h->http_status       = $http_status;
        $logH2h->status            = $status;
        $logH2h->isi_respon        = json_encode($response);
        $logH2h->ip_address_req    = request()->ip();
        $logH2h->timestamp_req     = Carbon::now();
        $logH2h->pengirim_respon   = $sender ? $sender["name"] : env("app_name", "wcm");
        $logH2h->ip_address_respon = $sender ? $sender["host"] :(request()->header("host")?:"-");
        $logH2h->request_type      = $this->type;
        $logH2h->timestamp_respon  = Carbon::now();

        $logH2h->save();
    }

    public function writeLogSend($params, $response, $request_type)
    {
        if (!$response instanceof Collection) {
            $response = collect($response);
        }
        
        $order                     = property_exists($this, "order") ? $this->order : false;
        $logH2h                    = new LogH2h;
        $logH2h->order_id          = $order ? $order->number : 0;
        $logH2h->pengirim_req      = env("app_name", "wcm");
        $logH2h->isi_req           = json_encode($params->all());
        $http_status               = $response->get("error_code") == "0000" ? "OK" : "ERROR";
        $status                    = $response->get("error_code") == "0000" ? 200 : 500;
        $logH2h->http_status       = $order ? "0K" :  $http_status;
        $logH2h->status            = $order ? 200 : $status;
        $logH2h->isi_respon        = json_encode($response);
        $logH2h->timestamp_req     = Carbon::now();
        $logH2h->ip_address_req    = request()->ip();
        $logH2h->pengirim_respon   = $this->name;
        $logH2h->ip_address_respon = $response->get("ip_response")?:"-";
        $logH2h->request_type      = $request_type;
        $logH2h->timestamp_respon  = Carbon::now();

        $logH2h->save();
    }

    public function writeOrderDFStatus($params)
    {
        if (!$params instanceof Collection) {
            $params = collect($params);
        }
        try {
            $order                                = property_exists($this, "order") ? $this->order : false;
            $order_df                             = OrderDFStatus::firstOrNew(['order_id' => $order->id]);
            $order_df->order_id                   = $order->id;
            $order_df->is_sent                    = true;
            $order_df->error_message_send_order   = $params->get("error_msg");
            $order_df->error_message_disbursement = $params->get("message");
            $order_df->send_order_count           += 1;
            $order_df->status                     = $params->get("error_code") !== "0000" ? "n" : "y"; 
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);

        }
        $order_df->save();

        return $order_df;

    }

    public function updateOrderDFStatus($order_df, $params)
    {
        try {
            $order_df->error_message_disbursement = $params->{$this->getKeyErrorMessage()};
        } catch (\Exception $e) {
            dd($e);
        } finally {
            $order_df->update();
        }

    }

}
