<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait OutstandingSOTraits {

    public function scopeOutstandSO($query) {
        return DB::table('vreport_so_outstand');
    }

}
