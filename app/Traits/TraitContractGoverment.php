<?php

namespace App\Traits;

use App\models\ContractGoverment;
use App\models\ContractGovItem;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait TraitContractGoverment {
    public function updateStatus(Request $request) {
        
        try {
            $validation = Validator::make(
                $request->all(),
                [
                    "status" => "in:y,n",
                    "uuid" => "required|array"
                ]
            );

            if ($validation->fails()) {
                throw new Exception($validation->errors());
            }
            $uuids = $request->get("uuid");
            $contracts = ContractGoverment::whereIn("uuid", $uuids)->get();
            $status = $contracts->where("status","!=","z")
                ->pluck("status")
                ->unique();

            if ( $status->count() > 1) {
                throw new Exception("Status yang dipilih lebih dari satu");
            }
            $paramUpdate = [
                "status"=> $request->get("status"),
            ];
            
            DB::beginTransaction();
            ContractGoverment::whereIn("uuid", $uuids)
                ->update($paramUpdate);
            ContractGovItem::whereIn("contract_gov_id", $contracts->pluck("id"))
                ->update($paramUpdate);

        }catch(Exception $e) {
            DB::rollback();
            return response()->json(
                responseFail($e->getMessage()),
                500
            );
        }
        DB::commit();
        return response()->json(
            responseSuccess("update-succes", $uuids)
        );
    }
}