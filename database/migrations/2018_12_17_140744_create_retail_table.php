<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_retail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12)->unique();
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('owner');
            $table->string('address');
            $table->char('sales_unit_id', 6);
            $table->foreign('sales_unit_id')
                    ->references('id')
                    ->on('wcm_sales_unit');
            $table->string('village')->nullable();
            $table->string('tlp_no',20)->nullable();
            $table->string('hp_no',14)->nullable();
            $table->string('fax_no',12)->nullable();
            $table->decimal('latitude',11,10)->nullable();
            $table->decimal('longitude',11,10)->nullable();
            $table->string('npwp_no')->nullable();
            $table->date('npwp_register')->nullable();
            $table->string('siup_no');
            $table->date('valid_date_siup')->nullable();
            $table->string('situ_no')->nullable();
            $table->date('valid_date_situ')->nullable();
            $table->string('tdp_no')->nullable();
            $table->date('valid_date_tdp')->nullable();
            $table->string('recomd_letter')->nullable();
            $table->date('recomd_letter_date')->nullable();
            $table->string('old_number')->nullable();
            $table->char('status', 1)->default('y');
            $table->char('retail_administratif', 1)->default('0');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->boolean('sub_district_default')->default(true);
            $table->timestamps();

            $table->index(['sales_unit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_customer_retailer_assg');
        Schema::dropIfExists('wcm_retail');
    }
}
