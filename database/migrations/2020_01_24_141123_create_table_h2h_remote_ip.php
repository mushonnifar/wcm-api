<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableH2hRemoteIp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("wcm_h2h_remote_ip", function(Blueprint $table){
            $table->uuid("uuid");
            $table->primary("uuid");
            $table->char("bank_id", 4);
            $table->foreign("bank_id")->references("id")->on("wcm_bank");
            $table->ipAddress("ip_address");
            $table->string("base_url")->nullable();
            $table->string("password");
            $table->string("url_endpoint");
            $table->uuid("created_by");
            $table->uuid("updated_by");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("wcm_h2h_remote_ip");
    }
}
