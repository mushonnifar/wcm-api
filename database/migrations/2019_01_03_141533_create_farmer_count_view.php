<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFarmerCountView extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("
      CREATE VIEW view_farmer_count AS
      (
        SELECT
                tb1.id AS farmer_group_id,
                COUNT ( tb2.id ) AS qty_group,
                ISNULL( SUM ( tb2.land_area ), 0 ) AS land_area 
        FROM
                wcm_farmer_groups AS tb1
                LEFT JOIN wcm_farmer AS tb2 ON tb1.id = tb2.farmer_group_id 
                AND tb2.status IN ( 'y', 'a' ) 
        GROUP BY
                tb1.id 
      )
    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP VIEW IF EXISTS view_farmer_count');
    }

}
