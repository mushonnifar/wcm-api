<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateQtyItemPenebusanView extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("
            CREATE VIEW view_qty_item_penebusan AS
            (
                SELECT
                        tb1.product_id,
                        SUM ( tb1.qty ) AS qty,
                        tb3.customer_id,
                        tb3.sales_org_id,
                        tb4.sales_group_id,
                        tb2.sales_unit_id,
                        MONTH ( tb5.order_date ) bulan,
			YEAR ( tb5.order_date ) tahun 	
                FROM
                        wcm_order_item AS tb1
                        LEFT JOIN wcm_retail AS tb2 ON tb1.retail_id = tb2.id
                        LEFT JOIN wcm_customer_retailer_assg AS tb3 ON tb1.retail_id = tb3.retail_id
                        LEFT JOIN wcm_sales_unit AS tb4 ON tb2.sales_unit_id = tb4.id 
                        LEFT JOIN wcm_orders AS tb5 ON tb1.order_id = tb5.id 
                WHERE
                        tb5.status IN ( 'y', 's', 'k', 'l', 'u', 'r', 'c', 'd' ) 
                GROUP BY
                        tb1.product_id,
                        tb3.customer_id,
                        tb3.sales_org_id,
                        tb4.sales_group_id,
                        tb2.sales_unit_id,
                        MONTH ( tb5.order_date ),
			YEAR ( tb5.order_date )
            )"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP VIEW IF EXISTS view_qty_item_penebusan');
    }

}
