<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSalesOfficeAssgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_sales_office_assg', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('sales_area_id');
            $table->foreign('sales_area_id')
                    ->references('id')
                    ->on('wcm_sales_area');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                ->references('id')
                ->on('wcm_sales_office');
            $table->date('from_date')->nullable();
            $table->date('thru_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['sales_area_id', 'sales_office_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_sales_office_assg');
    }
}
