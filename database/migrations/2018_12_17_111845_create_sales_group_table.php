<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSalesGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_sales_group', function (Blueprint $table) {
            $table->char('id', 4);
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('code')->nullable();
            $table->string('name');
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                ->references('id')
                ->on('wcm_sales_office');
            $table->string('district_code')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_sales_area');
    }
}
