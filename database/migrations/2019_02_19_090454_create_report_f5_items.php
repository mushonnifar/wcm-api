<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportF5Items extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('wcm_report_f5_items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->integer('report_f5_id');
            $table->foreign('report_f5_id')
                    ->references('id')
                    ->on('wcm_report_f5');
            $table->integer('stok_awal');
            $table->integer('penebusan');
            $table->integer('penyaluran');
            $table->integer('stok_akhir');
            $table->char('number_f5', 12);                     
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['product_id', 'report_f5_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('wcm_report_f5_items');
    }
}
