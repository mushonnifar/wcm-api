<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_distrib_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('report_f5_id');
            $table->foreign('report_f5_id')
                    ->references('id')
                    ->on('wcm_report_f5');
            $table->char('number', 13);
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->integer('order_id')->nullable();
            $table->string('so_number')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->integer('initial_stock_id')->nullable();
            $table->foreign('initial_stock_id')
                    ->references('id')
                    ->on('wcm_initial_stocks');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group'); 
            $table->date('distribution_date');
            $table->string('distrib_resportable_type');
            $table->char('status', 1)->default('y');          
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['report_f5_id', 'customer_id', 'sales_org_id', 'initial_stock_id', 'sales_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_distrib_reports');
    }
}
