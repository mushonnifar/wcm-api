<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWcmBankAcccount extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //
        Schema::create("wcm_bank_account", function (Blueprint $table) {
            $table->increments("id");
           $table->char("sales_org_id",4);
            $table->foreign("sales_org_id")
                    ->references("id")
                    ->on("wcm_sales_org");
            $table->char("bank_id",4);
            $table->foreign("bank_id")
                    ->references("id")
                    ->on("wcm_bank");
            $table->string("currency")->nullable();
            $table->string("bank_account");
            $table->string("bank_gl_account");
            $table->string("bank_clearing_acct");
            $table->char("status", 1)->default("y");
            $table->uuid('created_by');
            $table->uuid("updated_by");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
        Schema::dropIfExists("wcm_bank_acccount");
    }

}
