<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDocTaxTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_doc_tax_type', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('npwp_no')->nullable();
            $table->string('npwp_register')->nullable();
            $table->string('siup_no')->nullable();
            $table->timestamp('valid_date_siup')->nullable();
            $table->string('situ_no')->nullable();
            $table->timestamp('valid_date_situ')->nullable();
            $table->string('tdp_no')->nullable();
            $table->timestamp('valid_date_tdp')->nullable();
            $table->string('recomd_letter')->nullable();
            $table->timestamp('recomd_letter_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_doc_tax_type');
    }
}
