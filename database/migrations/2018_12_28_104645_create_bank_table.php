<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_bank', function (Blueprint $table) {
            $table->char('id', 4)->unique()->primary();
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('name')->unique();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_bank_account');
        Schema::dropIfExists('wcm_customer_bank_assg');
        Schema::dropIfExists('wcm_bank');
    }
}
