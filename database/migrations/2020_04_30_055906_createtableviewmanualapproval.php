<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Createtableviewmanualapproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        select
                [tb1].[id] as [order_id],
                [tb1].[uuid] as [order_uuid],
                [tb1].[so_number],
                [tb1].[number],
                [tb1].[payment_method],
                [tb1].[customer_id],
                (
                SELECT
                    TOP 1 full_name 
                from
                    wcm_customer wc
                where
                    id = tb1.customer_id ) as customer_name,
                [tb1].[sales_office_id],
                (
                SELECT
                    TOP 1 name
                from
                    wcm_sales_office wso
                where
                    id = tb1.sales_office_id ) as sales_office_name,
                [tb1].[sales_group_id],
                (
                SELECT
                    TOP 1 name
                from
                    wcm_sales_group wsg
                where
                    id = tb1.sales_group_id ) as sales_group_name,
                [tb1].[contract_id],
                (
                SELECT
                    TOP 1 [wc].[number]
                from
                    wcm_contract wc
                where
                    id = tb1.contract_id ) as spjb,
                Stuff ( (
                SELECT
                    ', ' + cast(wrt.id as varchar)
                FROM
                    wcm_retail wrt
                WHERE
                    id in (
                    SELECT
                        retail_id
                    from
                        wcm_order_item woi
                    where
                        order_id = tb1.id) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As retail_id,
                Stuff ( (
                SELECT
                    ', ' + wrt.name
                FROM
                    wcm_retail wrt
                WHERE
                    id in (
                    SELECT
                        retail_id
                    from
                        wcm_order_item woi
                    where
                        order_id = tb1.id) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As retail_name,
                Stuff ( (
                SELECT
                    ', ' + wsu.id
                FROM
                    wcm_sales_unit wsu
                WHERE
                    id in (
                    SELECT
                        sales_unit_id
                    FROM
                        wcm_retail wrt
                    WHERE
                        id in (
                        SELECT
                            retail_id
                        from
                            wcm_order_item woi
                        where
                            order_id = tb1.id)) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As sales_unit_id ,
                Stuff ( (
                SELECT
                    ', ' + wsu.name
                FROM
                    wcm_sales_unit wsu
                WHERE
                    id in (
                    SELECT
                        sales_unit_id
                    FROM
                        wcm_retail wrt
                    WHERE
                        id in (
                        SELECT
                            retail_id
                        from
                            wcm_order_item woi
                        where
                            order_id = tb1.id)) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As sales_unit_name ,
                Stuff ( (
                SELECT
                    ', ' + cast(wml.id as varchar)
                FROM
                    wcm_material_list wml
                WHERE
                    id in (
                    SELECT
                        woi.material_list_id
                    from
                        wcm_order_item woi
                    where
                        order_id = tb1.id) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As material_id ,
                Stuff ( (
                SELECT
                    ', ' + wml.material_no
                FROM
                    wcm_material_list wml
                WHERE
                    id in (
                    SELECT
                        woi.material_list_id
                    from
                        wcm_order_item woi
                    where
                        order_id = tb1.id) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As material_no ,
                Stuff ( (
                SELECT
                    ', ' + wml.mat_desc
                FROM
                    wcm_material_list wml
                WHERE
                    id in (
                    SELECT
                        woi.material_list_id
                    from
                        wcm_order_item woi
                    where
                        order_id = tb1.id) For Xml Path(''),
                    type ).value('.',
                'nvarchar(max)'),
                1,
                2,
                '') As mat_desc ,
                [tb1].[approve_status],
                (
                SELECT
                    SUM(woi.qty)
                from
                    wcm_order_item woi
                where
                    woi.order_id = tb1.id ) as qty,
                (CASE WHEN tb1.approve_status = 'd' THEN 'Manual Approval'
                WHEN tb1.approve_status = 'r' THEN 'Reviewed'
                ELSE '-' END) as status,
                (
                    SELECT TOP 1 name FROM wcm_plant wp WHERE wp.id IN (SELECT plant_id FROM wcm_order_item woi2 where woi2.order_id = tb1.id)
                ) as plant_name,
                [tb1].[created_by],
                [tb1].[updated_by],
                CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at,
                CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at
            from
                [wcm_orders] as [tb1]
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP view view_manual_approval;");
    }
}
