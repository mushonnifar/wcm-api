<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCustomerSalesAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_customer_sales_area', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->integer('sales_area_id');
            $table->foreign('sales_area_id')
                    ->references('id')
                    ->on('wcm_sales_area');
            $table->string('term_of_payment')->nullable();
            $table->string('top_dp')->nullable();
            $table->string('tax_classification')->nullable();
            $table->string('pph22')->nullable();
            $table->string('top_dp_uom')->nullable();
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['customer_id', 'sales_area_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_customer_sales_area');
    }
}
