<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserForgetPassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_forget_password',function(Blueprint $table){
            $table->uuid('uuid')->default(DB::raw('NEWID()'))->primary();
            $table->uuid('user_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->timestamp('link_create_date')->nullable();
            $table->timestamp('link_valid_to_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("user_forget_password");
    }
}
