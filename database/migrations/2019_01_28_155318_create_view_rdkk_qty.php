<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewRdkkQty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW view_rdkk_qty AS
        (
            SELECT p.number
                ,p.rdkk_id
                ,p.P01
                ,p.P02
                ,p.P03
                ,p.P04
                        ,p.P05
            FROM
            (
                SELECT e.number,e.id as rdkk_id,ec.product_id, ec.required_qty
                    FROM wcm_rdkks AS e
                    INNER JOIN wcm_rdkk_item AS ec ON e.id=ec.rdkk_id
                    GROUP BY e.id, e.number,e.id, ec.product_id, ec.required_qty
            ) AS tbl 
            PIVOT
            (
                MAX(required_qty) FOR product_id IN(P01,P02,P03,P04,P05)
            ) AS p
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS view_rdkk_qty');
    }
}
