<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_m_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ID');
            $table->string('name_EN');
            $table->string('url')->default('#');
            $table->integer('order_no')->default('0');
            $table->integer('icon')->nullable();
            $table->integer('parent_id')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_role_has_menus');
        Schema::dropIfExists('wcm_m_menus');
    }

}
