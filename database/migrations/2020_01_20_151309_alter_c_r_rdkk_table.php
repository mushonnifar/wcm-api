<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCRRdkkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wcm_rddks', function (Blueprint $table) {
            $table->string("reference_rdkk")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wcm_rddks', function (Blueprint $table) {
            $table->removeColumn("reference_rdkk");
        });
    }
}
