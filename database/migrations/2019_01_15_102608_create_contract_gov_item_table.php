<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractGovItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_contract_gov_item', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('contract_gov_id');
            $table->foreign('contract_gov_id')
                    ->references('id')
                    ->on('wcm_contract_goverment')
                    ->onDelete('cascade');
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                    ->references('id')
                    ->on('wcm_sales_office');
            $table->char('sales_group_id', 4)->nullable();
            $table->char('sales_unit_id', 6)->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('initial_qty')->nullable();
            $table->date('active_date')->nullable();
            $table->date('inactive_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['contract_gov_id', 'product_id', 'sales_office_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_contract_gov_item');
    }
}
