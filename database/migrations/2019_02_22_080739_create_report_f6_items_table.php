<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportF6ItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_report_f6_items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->float('stok_awal');
            $table->float('penebusan');
            $table->float('penyaluran');
            $table->float('stok_akhir');
            $table->integer('report_f6_id');
            $table->foreign('report_f6_id')
                    ->references('id')
                    ->on('wcm_report_f6');
            $table->integer('retail_id');
            $table->foreign('retail_id')
                    ->references('id')
                    ->on('wcm_retail');
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['report_f6_id', 'retail_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_report_f6_items');
    }
}

