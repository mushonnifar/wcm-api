<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFarmerGroupsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_farmer_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('code', 12)->unique();
            $table->string('name');
            $table->integer('retail_id');
            $table->foreign('retail_id')
                    ->references('id')
                    ->on('wcm_retail');
            $table->char('customer_id', 10)->nullable();
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->string('address');
            $table->char('sales_unit_id', 6);
            $table->foreign('sales_unit_id')
                    ->references('id')
                    ->on('wcm_sales_unit');
            $table->string('telp_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('village')->nullable();
            $table->string('sub_sector_id')->nullable();
            $table->string('commodity_id')->nullable();
            $table->integer('qty_group')->default(0);
            $table->float('land_area',11)->default(0);
            $table->float('latitude', 11, 10)->nullable();
            $table->float('longitude', 11, 10)->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['retail_id', 'customer_id', 'sales_org_id', 'sales_unit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_farmer_groups');
    }

}
