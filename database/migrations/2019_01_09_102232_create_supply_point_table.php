<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSupplyPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_supply_point', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('number', 13)->unique();
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                    ->references('id')
                    ->on('wcm_sales_office');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group');
            $table->integer('plant_id');
            $table->foreign('plant_id')
                    ->references('id')
                    ->on('wcm_plant');
            $table->char('scnd_sales_group_id', 4);
            $table->foreign('scnd_sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group');
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['sales_org_id', 'sales_office_id', 'sales_group_id', 'plant_id', 'scnd_sales_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_supply_point');
    }
}
