<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnItemOrderSap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wcm_order_item', function (Blueprint $table) {
            $table->string("item_order_sap", 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wcm_order_item', function (Blueprint $table) {
            $table->dropColumn("item_order_sap");
        });
    }
}
