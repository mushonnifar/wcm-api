<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('number', 12)->unique();
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->integer('contract_id');
            $table->foreign('contract_id')
                    ->references('id')
                    ->on('wcm_contract');
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                    ->references('id')
                    ->on('wcm_sales_office');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group');
            $table->integer('delivery_method_id');
            $table->foreign('delivery_method_id')
                    ->references('id')
                    ->on('wcm_delivery_method');
            $table->string('payment_method');
            $table->date('order_date');
            $table->string('desc')->nullable();
            $table->string('booking_code')->nullable();
            $table->decimal('total_price', 38, 10)->nullable();
            $table->decimal('total_price_before_ppn', 38, 10)->nullable();
            $table->decimal('upfront_payment', 38, 10)->nullable();
            $table->decimal('ppn', 38, 10)->nullable();
            $table->decimal('pph22', 38, 10)->nullable();
            $table->date('billing_date')->nullable();
            $table->timestamp('payment_due_date')->nullable();
            $table->string('bank_id')->nullable();
            $table->date('good_redemption_due_date')->nullable();
            $table->date('df_due_date')->nullable();
            $table->date('df_due_date_plan')->nullable();
            $table->date('pickup_due_date')->nullable();
            $table->string('delivery_location')->nullable();
            $table->string('so_number')->nullable();
            $table->string('sap_status')->nullable();
            $table->string('sap_booking_code')->nullable();
            $table->string('sap_billing_dp_doc')->nullable();
            $table->string('sap_clearing_doc')->nullable();
            $table->timestamp('billing_fulldate')->nullable();
            $table->string('sap_product_desc')->nullable();
            $table->string('sap_miscs')->nullable();
            $table->string('reference_code')->nullable();
            $table->date('send_order_date')->nullable();
            $table->string('flag')->nullable();
            $table->string('seq')->nullable();
            $table->date('approve_date')->nullable();
            $table->string('approve_type')->default('s');
            $table->char('approve_status', 1)->default('d');
            $table->char('disbursement_status', 1)->default('v');
            $table->char('status', 1)->default('d');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();

            $table->index(['sales_org_id', 'contract_id', 'customer_id', 'sales_office_id', 'sales_group_id', 'delivery_method_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_orders');
    }

}
