<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_customer', function (Blueprint $table) {
            $table->char('id', 10);
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('full_name');
            $table->string('owner')->nullable();
            $table->string('register_date')->nullable();
            $table->string('code_cust_group')->nullable();
            $table->string('name_cust_group')->nullable();
            $table->string('old_number')->nullable();
            $table->string('npwp_no')->nullable();
            $table->string('npwp_register')->nullable();
            $table->string('recomd_letter_date')->nullable();
            $table->string('recomd_letter')->nullable();
            $table->string('valid_date_tdp')->nullable();
            $table->string('tdp_no')->nullable();
            $table->string('situ_no')->nullable();
            $table->string('valid_date_situ')->nullable();
            $table->string('valid_date_siup')->nullable();
            $table->string('siup_no')->nullable();
            $table->string('category_type', 50)->nullable();
            $table->string('category', 50)->nullable();
            $table->string('asset', 50)->nullable();
            $table->string('revenue', 50)->nullable();
            $table->string('account_year', 50)->nullable();
            $table->char('sap_status', 1)->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_customer_bank_assg');
        Schema::dropIfExists('wcm_cust_sales_org_assg');
        Schema::dropIfExists('wcm_sales_area');
        Schema::dropIfExists('wcm_customer_retailer_assg');
        Schema::dropIfExists('wcm_address');

        Schema::dropIfExists('wcm_contact');
        Schema::dropIfExists('wcm_customer_bank_assg');
        Schema::dropIfExists('wcm_bank');

        Schema::dropIfExists('wcm_customer');
    }
}
