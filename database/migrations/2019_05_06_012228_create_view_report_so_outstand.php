<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewReportSoOutstand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('DROP VIEW IF EXISTS vreport_so_outstand');
        DB::statement("
            DROP
                VIEW vreport_so_outstand;

            CREATE
                VIEW vreport_so_outstand AS SELECT
                    *
                FROM
                    (
                    SELECT
                        tb2.id,
                        tb1.sales_org_id,
                        tb1.id AS order_id,
                        tb1.so_number,
                        tb1.payment_method,
                        tb1.order_date,
                        FORMAT(tb2.qty,
                        'N2',
                        'id-ID') as qty,
                        FORMAT((tb2.total_price_before_ppn),
                        'N2',
                        'id-ID') AS total_price_before_ppn,
                        FORMAT((tb2.total_price),
                        'N2',
                        'id-ID') AS total_price,
                        FORMAT((tb2.ppn),
                        'N2',
                        'id-ID') AS ppn,
                        FORMAT((tb2.total_price_before_ppn / tb2.qty),
                        'N2',
                        'id-ID') AS harga_jual,
                        FORMAT((( tb2.total_price_before_ppn / tb2.qty ) + ( tb2.ppn / tb2.qty )),
                        'N2',
                        'id-ID') AS harga_ton,
                        tb1.sales_office_id,
                        tb3.name AS sales_office_name,
                        tb1.sales_group_id,
                        tb4.name AS sales_group_name,
                        tb1.customer_id,
                        tb5.full_name AS customer_name,
                        tb5.owner AS customer_owner,
                        tb6.number AS contract_number,
                        tb7.name AS delivery_method_name,
                        tb2.retail_id,
                        tb8.code AS retail_code,
                        tb8.name AS retail_name,
                        tb9.material_no,
                        tb9.mat_desc,
                        tb9.unit,
                        tb10.distrib_channel_id,
                        tb10.sales_division_id,
                        tb11.term_of_payment,
                        tb8.sales_unit_id,
                        tb12.name AS sales_unit_name,
                        tb2.product_id,
                        tb13.name AS product_name,
                        FORMAT(tb14.initial_qty,
                        'N2',
                        'id-ID') AS oprasional_qty,
                        FORMAT(tb16.initial_qty,
                        'N2',
                        'id-ID') AS asal_qty,
                        'IDR' AS currency,
                        tb19.numbers AS delivery_number,
                        tb19.delivery_date,
                        tb18.code AS plant_code,
                        tb18.name AS plant_name,
                        FORMAT(tb19.qty,
                        'N2',
                        'id-ID') AS delivery_qty,
                        FORMAT(tb2.qty - tb19.qty,
                        'N2',
                        'id-ID') AS sodo_qty,
                        (
                        CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'o' THEN 'Cancel SO'
                            ELSE '-'
                        END ) AS status_name,
                        'Z3SU' AS so_type,
                        'Penj.Subsidi (Web)' AS so_type_desc,
                        tb20.name AS plant_sales_group_name,
                        tb21.name AS plant_sales_office_name,
                        (
                        CASE
                            WHEN tb1.payment_method = 'cash' THEN 'E'
                            WHEN tb1.payment_method = 'distributor financing' THEN 'D'
                            ELSE '-'
                        END ) AS payment_method_code,
                        tb1.good_redemption_due_date,
                        tb1.number AS order_number,
                        tb22.name AS created_name,
                        'SUBSIDI' AS sector,
                        tb1.sap_billing_dp_doc,
                        tb1.billing_date,
                        (
                        CASE
                            WHEN RIGHT ( tb11.top_dp,
                            1 ) = 'D' THEN CONCAT ( SUBSTRING ( tb11.top_dp,
                            1,
                            LEN(tb11.top_dp) - 1 ),
                            ' Days' )
                            WHEN RIGHT ( tb11.top_dp,
                            1 ) = 'H' THEN CONCAT ( SUBSTRING ( tb11.top_dp,
                            1,
                            LEN(tb11.top_dp) - 1 ),
                            ' Hours' )
                            ELSE '-'
                        END ) AS payment_term,
                        tb25.name AS customer_sales_group_name,
                        tb26.name AS customer_sales_office_name
                    FROM
                        wcm_orders AS tb1
                    JOIN wcm_order_item AS tb2 ON
                        tb1.id = tb2.order_id
                    JOIN wcm_sales_office AS tb3 ON
                        tb1.sales_office_id = tb3.id
                    JOIN wcm_sales_group AS tb4 ON
                        tb1.sales_group_id = tb4.id
                    JOIN wcm_customer AS tb5 ON
                        tb1.customer_id = tb5.id
                    JOIN wcm_contract AS tb6 ON
                        tb1.contract_id = tb6.id
                    JOIN wcm_delivery_method AS tb7 ON
                        tb1.delivery_method_id = tb7.id
                    JOIN wcm_retail AS tb8 ON
                        tb2.retail_id = tb8.id
                    JOIN wcm_material_list AS tb9 ON
                        tb2.material_list_id = tb9.id
                    JOIN wcm_sales_area AS tb10 ON
                        tb1.sales_org_id = tb10.sales_org_id
                        AND tb10.distrib_channel_id = '20'
                    JOIN wcm_customer_sales_area AS tb11 ON
                        tb1.customer_id = tb11.customer_id
                        AND tb10.id = tb11.sales_area_id
                    JOIN wcm_sales_unit AS tb12 ON
                        tb8.sales_unit_id = tb12.id
                    JOIN wcm_product AS tb13 ON
                        tb2.product_id = tb13.id
                    JOIN wcm_contract_item AS tb14 ON
                        tb6.id = tb14.contract_id
                        AND tb2.product_id = tb14.product_id
                        AND tb1.sales_office_id = tb14.sales_office_id
                        AND tb1.sales_group_id = tb14.sales_group_id
                        AND tb8.sales_unit_id = tb14.sales_unit_id
                        AND YEAR (tb1.order_date) = tb14.year
                        AND MONTH (tb1.order_date) = tb14.month
                    JOIN wcm_contract AS tb15 ON
                        tb1.contract_id = tb6.id
                        AND tb15.contract_type = 'asal'
                    JOIN wcm_contract_item AS tb16 ON
                        tb15.id = tb16.contract_id
                        AND tb2.product_id = tb16.product_id
                        AND tb1.sales_office_id = tb16.sales_office_id
                        AND tb1.sales_group_id = tb16.sales_group_id
                        AND tb8.sales_unit_id = tb16.sales_unit_id
                        AND YEAR (tb1.order_date) = tb16.year
                        AND MONTH (tb1.order_date) = tb16.month
                    JOIN wcm_delivery AS tb17 ON
                        tb1.id = tb17.order_id
                    JOIN wcm_plant AS tb18 ON
                        tb2.plant_id = tb18.id
                    LEFT JOIN (
                        SELECT
                            D2.order_id AS order_id,
                            DI.product_id AS product_id,
                            SUM (DI.delivery_qty) AS qty,
                            SUBSTRING ( (
                            SELECT
                                ', ' + RTRIM(D1.number) AS [text()]
                            FROM
                                wcm_delivery AS D1
                            JOIN wcm_delivery_item AS DI1 ON
                                D1.id = DI1.delivery_id
                                AND DI1.product_id = DI.product_id
                            WHERE
                                D1.order_id = D2.order_id
                            ORDER BY
                                d1.number FOR XML PATH ('') ),
                            2,
                            1000 ) [numbers],
                            SUBSTRING ( (
                            SELECT
                                ', ' + FORMAT ( D1.delivery_date,
                                'dd-MM-yyyy' ) AS [text()]
                            FROM
                                wcm_delivery AS D1
                            JOIN wcm_delivery_item AS DI1 ON
                                D1.id = DI1.delivery_id
                                AND DI1.product_id = DI.product_id
                            WHERE
                                D1.order_id = D2.order_id
                            ORDER BY
                                d1.number FOR XML PATH ('') ),
                            2,
                            1000 ) [delivery_date]
                        FROM
                            wcm_delivery AS D2
                        JOIN wcm_delivery_item AS DI ON
                            D2.ID = DI.delivery_id
                        GROUP BY
                            D2.order_id,
                            DI.product_id ) AS tb19 ON
                        tb1.id = tb19.order_id
                        AND tb2.product_id = tb19.product_id
                    JOIN wcm_sales_group AS tb20 ON
                        tb18.sales_group_id = tb20.id
                    JOIN wcm_sales_office AS tb21 ON
                        tb20.sales_office_id = tb21.id
                    JOIN users AS tb22 ON
                        tb1.created_by = tb22.id
                    LEFT JOIN wcm_address AS tb23 ON
                        tb5.id = tb23.customer_id
                        AND tb23.address_type = 'FORMAL'
                    LEFT JOIN wcm_sales_unit AS tb24 ON
                        tb23.sales_unit_id = tb24.id
                    LEFT JOIN wcm_sales_group AS tb25 ON
                        tb23.sales_group_id = tb25.id
                    LEFT JOIN wcm_sales_office AS tb26 ON
                        tb23.sales_office_id = tb26.id
                    WHERE
                        tb1.so_number IS NOT NULL ) AS tb1
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('DROP VIEW IF EXISTS vreport_so_outstand');
    }
}
