<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePartnerFunctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_partner_function', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->integer('customer_sales_area_id');
            $table->foreign('customer_sales_area_id')
                    ->references('id')
                    ->on('wcm_customer_sales_area');
            $table->string('partner_type')->nullable();
            $table->string('partner_type_desc')->nullable();
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['customer_id', 'customer_sales_area_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_partner_function');
    }
}
