<?php

use Illuminate\Database\Seeder;

class ModelHasRoleSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = App\Models\User::where('username', 'developer')->first();
        App\Models\ModelHasRole::create([
            "role_id" => 1,
            "model_type" => "App\Models\User",
            "model_id" => DB::raw("CONVERT(uniqueidentifier,'$user->id')"),
        ]);
        $this->command->info("developer has role developer berhasil diinsert");

        $user = App\Models\User::where('username', 'admin')->first();
        App\Models\ModelHasRole::create([
            "role_id" => 2,
            "model_type" => "App\Models\User",
            "model_id" => DB::raw("CONVERT(uniqueidentifier,'$user->id')"),
        ]);
        $this->command->info("admin has role admin berhasil diinsert");

        $user = App\Models\User::where('username', 'admin_anper')->first();
        App\Models\ModelHasRole::create([
            "role_id" => 3,
            "model_type" => "App\Models\User",
            "model_id" => DB::raw("CONVERT(uniqueidentifier,'$user->id')"),
        ]);
        $this->command->info("admin_anper has role admin_anper berhasil diinsert");

        $user = App\Models\User::where('username', 'distributor')->first();
        App\Models\ModelHasRole::create([
            "role_id" => 4,
            "model_type" => "App\Models\User",
            "model_id" => DB::raw("CONVERT(uniqueidentifier,'$user->id')"),
        ]);
        $this->command->info("Distributor has role distributor berhasil diinsert");
        // try {

        // } catch (\Exception $e) {
        //     $this->command->info($user->id);
        // }

    }

}
