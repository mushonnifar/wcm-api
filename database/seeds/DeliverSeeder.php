<?php

use Illuminate\Database\Seeder;
use App\Models\Delivery;

class DeliverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array('number'=> 'ABC1000','order_id'=> 1,'delivery_date'=> date('2019-01-20')),
        ];

        Delivery::insert($datas);
        $this->command->info('Sukses Make Delivery Seeder');
    }
}
