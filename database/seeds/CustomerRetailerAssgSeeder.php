<?php

use Illuminate\Database\Seeder;
use App\Models\CustomerRetailerAssg;

class CustomerRetailerAssgSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['customer_id' => '1000000000', 'retail_id' => '1', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '2', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '11', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '12', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '3', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '16', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '17', 'sales_org_id' => 'B000'],
            ['customer_id' => '1000000000', 'retail_id' => '18', 'sales_org_id' => 'B000'],
        ];

        foreach ($data as $value) {
            CustomerRetailerAssg::create([
                'customer_id' => $value['customer_id'], 
                'retail_id' => $value['retail_id'], 
                'sales_org_id' => $value['sales_org_id'], 
            ]);
        }
    }

}
