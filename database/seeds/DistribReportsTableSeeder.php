<?php

use Illuminate\Database\Seeder;
use App\Models\DistribReports;

class DistribReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array(
                'report_f5_id' => 1,
                'number' => 'PKP0000000001',
                'customer_id' => 1000000000,
                'order_id' => 1,
                'so_number' => 3,
                'month' => '01',
                'year' => '2019',
                'sales_org_id' => 'B000',
                'initial_stock_id' => 1,
                'sales_group_id' => 1114,
                'distribution_date' =>  date('Y-m-d'),
                'distrib_resportable_type' => 'cas',
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
        ];
        DistribReports::insert($datas);
        $this->command->info('Sukses Insert DistribReportsTableSeeder');
        
    }
}
