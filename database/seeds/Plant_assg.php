<?php

use Illuminate\Database\Seeder;
use App\Models\PlantAssg;

class Plant_assg extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $data = [
           ['plant_id'=> '1','sales_org_id'=> 'C000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '2','sales_org_id'=> 'C000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '2','sales_org_id'=> 'B000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '1','sales_org_id'=> 'B000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '2','sales_org_id'=> 'C000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '3','sales_org_id'=> 'D000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
           ['plant_id'=> '1','sales_org_id'=> 'D000','distrib_channel_id'=> '00','from_date'=> '2019-01-14','thru_date'=> '2019-01-21'],
        ];

        foreach ($data as $value) {
            PlantAssg::create(['plant_id'=>$value['plant_id'],'sales_org_id'=>$value['sales_org_id'],'distrib_channel_id'=>$value['distrib_channel_id'],'from_date'=>$value['from_date'],'thru_date'=> $value['thru_date']]);            
        }

        $this->command->info("Plant Assg berhasil diinsert");
    }
}
