<?php

use App\Models\Route;
use Illuminate\Database\Seeder;

class RouteSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dtRoute = array(
            array(
                'id'   => 1,
                'name' => 'user',
            ), //1
            array(
                'id'   => 2,
                'name' => 'menu',
            ), //2
            array(
                'id'   => 3,
                'name' => 'permission',
            ), //3
            array(
                'id'   => 4,
                'name' => 'role',
            ), //4
            array(
                'id'   => 5,
                'name' => 'route',
            ), //5
            array(
                'id'   => 6,
                'name' => 'distributor',
            ), //6
            array(
                'id'   => 7,
                'name' => 'retail',
            ), //7
            array(
                'id'   => 8,
                'name' => 'salesarea',
            ),
            array(
                'id'   => 9,
                'name' => 'farmergroup',
            ),
            array(
                'id'   => 10,
                'name' => 'region',
            ),
            array(
                'id'   => 11,
                'name' => 'farmer',
            ),
            array(
                'id'   => 12,
                'name' => 'Retail Assignment',
            ),
            array(
                'id'   => 13,
                'name' => 'gudang',
            ),
            array(
                'id'   => 14,
                'name' => 'company',
            ),
            array(
                'id'   => 15,
                'name' => 'subsector',
            ),
            array(
                'id'   => 16,
                'name' => 'commodity',
            ),
            array(
                'id'   => 17,
                'name' => 'supplypoint',
            ),
            array(
                'id'   => 18,
                'name' => 'plant',
            ),
            array(
                'id'   => 19,
                'name' => 'subtitution',
            ),
            array(
                'id'   => 20,
                'name' => 'pricing',
            ),
            array(
                'id'   => 21,
                'name' => 'product',
            ),
            array(
                'id'   => 22,
                'name' => 'deliverymethod',
            ),
            array(
                'id'   => 23,
                'name' => 'salesdivision',
            ),
            array(
                'id'   => 24,
                'name' => 'distribchannel',
            ),
            array(
                'id'   => 25,
                'name' => 'materiallist',
            ),
            array(
                'id'   => 26,
                'name' => 'bankdf',
            ),
            array(
                'id'   => 27,
                'name' => 'bank',
            ),
            array(
                'id'   => 28,
                'name' => 'permentan',
            ),
            array(
                'id'   => 29,
                'name' => 'pergub',
            ),
            array(
                'id'   => 30,
                'name' => 'perbup',
            ),
            array(
                'id'   => 31,
                'name' => 'spjb',
            ),
            array(
                'id'   => 32,
                'name' => 'rdkk',
            ),
            array(
                'id'   => 33,
                'name' => 'spjbopr',
            ),
            array(
                'id'   => 34,
                'name' => 'order',
            ),
            array(
                'id'   => 35,
                'name' => 'monitoringorder',
            ),
            array(
                'id'   => 36,
                'name' => 'paymentmethod',
            ),
            array(
                'id'   => 37,
                'name' => 'monitoringdo',
            ),
            array(
                'id'   => 38,
                'name' => 'penyalurando',
            ),
            array(
                'id'   => 39,
                'name' => 'monitoringdf',
            ),
            array(
                'id'   => 40,
                'name' => 'distribitem',
            ),
            array(
                'id'   => 41,
                'name' => 'reportf5',
            ),
            array(
                'id'   => 42,
                'name' => 'manualapproval',
            ),
            array(
                'id'   => 43,
                'name' => 'initialstockf5',
            ),
            array(
                'id'   => 44,
                'name' => 'penyaluraninitialstockf5',
            ),
            array(
                'id'   => 45,
                'name' => 'reportf6',
            ),
            array(
                'id'   => 46,
                'name' => 'initialstockf6',
            ),
            array(
                'id'   => 47,
                'name' => 'penyaluraninitialstockf6',
            ),
            array(
                'id'   => 48,
                'name' => 'yncronize',
            ),
            array(
                'id'   => 49,
                'name' => 'BankAccount',
            ),
            array(
                'id'   => 50,
                'name' => 'manualso',
            ),
            array(
                'id'   => 51,
                'name' => 'logorder',
            ),
            array(
                'id'   => 52,
                'name' => 'limit',
            ),
            array(
                'id'   => 53,
                'name' => 'h2h',
            ),
            array(
                'id'   => 54,
                'name' => 'plantingperiod',
            ),
            array(
                'id'   => 55,
                'name' => 'reportSO',
            ),
            array(
                'id'   => 56,
                'name' => 'configuration',
            ),
            array(
                'id' => 57,
                'name' => 'village',
            ),
            array(
                'id' => 58,
                'name' => 'configlintasalur',
            )

        );
        Route::insert($dtRoute);

        $this->command->info("Route berhasil diinsert");
    }

}
