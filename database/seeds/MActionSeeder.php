<?php
use App\Models\MAction;
use Illuminate\Database\Seeder;

class MActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dtMAction = array(
            array(
                'id' => 'C',
                'action_name' => 'Create'
            ),
            array(
                'id' => 'R',
                'action_name' => 'Read'
            ),
            array(
                'id' => 'U',
                'action_name' => 'Update'
            ),
            array(
                'id' => 'D',
                'action_name' => 'Delete'
            ),
            array(
                'id' => 'A',
                'action_name' => 'Approve'
            ),
        );
        MAction::insert($dtMAction);

        $this->command->info("Master Action berhasil diinsert");
    }
}
