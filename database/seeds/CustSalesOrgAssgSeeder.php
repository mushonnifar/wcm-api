<?php

use Illuminate\Database\Seeder;
use App\Models\CustSalesOrgAssg;

class CustSalesOrgAssgSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['customer_id' => '1000000000', 'sales_org_id' => 'B000']
        ];

        foreach ($data as $value) {
            CustSalesOrgAssg::create([
                'customer_id' => $value['customer_id'], 
                'sales_org_id' => $value['sales_org_id']
            ]);
        }
    }

}
