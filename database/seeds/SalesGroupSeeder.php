<?php

use Illuminate\Database\Seeder;
use App\Models\SalesGroup;

class SalesGroupSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => '1101', 'name' => 'KAB. ACEH SELATAN', 'sales_office_id' => '0012'],
            ['id' => '1102', 'name' => 'KAB. ACEH TENGGARA', 'sales_office_id' => '0012'],
            ['id' => '1103', 'name' => 'KAB. ACEH TIMUR', 'sales_office_id' => '0012'],
            ['id' => '1104', 'name' => 'KAB. ACEH TENGAH', 'sales_office_id' => '0012'],
            ['id' => '1105', 'name' => 'KAB. ACEH BARAT', 'sales_office_id' => '0012'],
            ['id' => '1106', 'name' => 'KAB. ACEH BESAR', 'sales_office_id' => '0012'],
            ['id' => '1111', 'name' => 'KAB. BLORA', 'sales_office_id' => '0033'],
            ['id' => '1112', 'name' => 'KAB. CEPU', 'sales_office_id' => '0033'],
            ['id' => '1113', 'name' => 'KAB. LAMONGAN', 'sales_office_id' => '0035'],
            ['id' => '1114', 'name' => 'KAB. GRESIK', 'sales_office_id' => '0035'],
            ['id' => '1115', 'name' => 'KAB. SURABAYA', 'sales_office_id' => '0035'],
            ['id' => '1116', 'name' => 'KAB. DEPOK', 'sales_office_id' => '0032'],
            ['id' => '1117', 'name' => 'KAB. BEKASI', 'sales_office_id' => '0032'],
        ];

        foreach ($data as $value) {
            SalesGroup::create(['id' => $value['id'], 'name' => $value['name'], 'sales_office_id' => $value['sales_office_id']]);
        }
    }

}
