<?php

use Illuminate\Database\Seeder;
use App\Models\CustomerBankAssg;

class CustomerBankAssgSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['customer_id' => '1000000000', 'sales_org_id' => 'B000', 'bank_id' => 'B001'],
        ];

        foreach ($data as $value) {
            CustomerBankAssg::create([
                'customer_id' => $value['customer_id'], 
                'sales_org_id' => $value['sales_org_id'], 
                'bank_id' => $value['bank_id'], 
            ]);
        }
    }

}
