<?php

use Illuminate\Database\Seeder;
use App\Models\Village;

class VillageSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['code' => '001', 'name' => 'Malaka', 'sales_unit_id' => '110101'],
            ['code' => '002', 'name' => 'Lawe Melang', 'sales_unit_id' => '110101'],
            ['code' => '003', 'name' => 'Pulo Air', 'sales_unit_id' => '110101'],
        ];

        foreach ($data as $value) {
            Village::create(['code' => $value['code'], 'name' => $value['name'], 'sales_unit_id' => $value['sales_unit_id']]);
        }
    }

}
