<?php

use Illuminate\Database\Seeder;
use App\Models\SalesOrg;

class SalesOrgSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => 'B000', 'name' => 'PT Petrokimia Gresik'],
            ['id' => 'C000', 'name' => 'PT Pupuk Kujang'],
            ['id' => 'D000', 'name' => 'PT Pupuk Kaltim'],
            ['id' => 'E000', 'name' => 'PT Pupuk Iskandar Md'],
            ['id' => 'F000', 'name' => 'PT Pupuk Sriwidjaja'],
        ];

        foreach ($data as $value) {
            SalesOrg::create([
                'id' => $value['id'], 
                'name' => $value['name'], 
            ]);
        }
    }

}
