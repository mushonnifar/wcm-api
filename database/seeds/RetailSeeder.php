<?php

use Illuminate\Database\Seeder;
use App\Models\Retail;


class RetailSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        
        $data = array();
        for ($i = 0; $i < 10; $i++) {
            $data[] = array(
                        'name' => $faker->name, 
                        'email' => $faker->email, 
                        'owner' => $faker->name, 
                        'address' => $faker->address, 
                        'sales_unit_id' => '110112',
                        'npwp_no' => $i,
                        'siup_no' => $i,
                        'tlp_no' => $faker->phoneNumber
                    );
        }
        foreach ($data as $value) {
            Retail::create([
                'code' => $this->generateCode(), 
                'name' => $value['name'], 
                'email' => $value['email'], 
                'owner' => $value['owner'], 
                'address' => $value['address'], 
                'sales_unit_id' => $value['sales_unit_id'],
                'tlp_no' => $value['tlp_no'],
                'siup_no' => $value['siup_no'],
                'npwp_no' => $value['npwp_no']
            ]);
        }

        $datas=[
            array(
                'code' => 'RT0000000012', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110112',
                'npwp_no' => 12,
                'siup_no' => 12,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000013', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110112',
                'npwp_no' => 13,
                'siup_no' => 13,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000014', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110113',
                'npwp_no' => 14,
                'siup_no' => 14,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000015', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110114',
                'npwp_no' => 15,
                'siup_no' => 15,
                'tlp_no' => $faker->phoneNumber
            ),
             array(
                'code' => 'RT0000000016', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110114',
                'npwp_no' => 16,
                'siup_no' => 16,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000017', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110114',
                'npwp_no' => 17,
                'siup_no' => 17,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000018', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110114',
                'npwp_no' => 18,
                'siup_no' => 18,
                'tlp_no' => $faker->phoneNumber
            ),
            array(
                'code' => 'RT0000000019', 
                'name' => $faker->name, 
                'email' => $faker->email, 
                'owner' => $faker->name, 
                'address' => $faker->address, 
                'sales_unit_id' => '110114',
                'npwp_no' => 19,
                'siup_no' => 19,
                'tlp_no' => $faker->phoneNumber
            ),
        ];
         Retail::insert($datas);
    }

    private function generateCode() {
        $maxCode = Retail::max('code');
        if(!$maxCode){
            $maxCode = 0;
        }
        preg_match_all('!\d+!', $maxCode, $matches);
        $val = intval($matches[0][0]);

        $code = "RT" . str_pad($val + 1, 10, "0", STR_PAD_LEFT);
        return $code;
    }

}
