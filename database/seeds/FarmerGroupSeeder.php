<?php

use Illuminate\Database\Seeder;
use App\Models\FarmerGroup;

class FarmerGroupSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            'name' => 'testing',
            'retail_id' => 1,
            'sales_org_id' => 'B000',
            'customer_id' => '1000000000',
            'address' => 'alamat',
            'sales_unit_id' => '110101',            
            'telp_no' => '0812',
            'fax_no' => '0812',
            'village' => 'desa testing',
            'sub_sector_id' => '1;2;3;',
            'commodity_id' => '001;002;',
            'latitude' => -7.8989898,
            'longitude' => 112.8989898,
            'code' => 'FG0000000001'
        ];

//        foreach ($data as $value) {
            FarmerGroup::create($data);
//        }
    }

}
