<?php

use Illuminate\Database\Seeder;
use App\Models\SalesDivision;

class SalesDivisionSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => '00', 'name' => 'Com. Div Subsidi', 'desc' => 'Com. Div Subsidi'],
        ];

        foreach ($data as $value) {
            SalesDivision::create($value);
        }
    }

}
