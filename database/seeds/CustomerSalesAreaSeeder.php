<?php

use Illuminate\Database\Seeder;
use App\Models\CustomerSalesArea;

class CustomerSalesAreaSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [
                'customer_id' => '1000000000',
                'sales_area_id' => '1',
                'term_of_payment' => 'Z003',
                'top_dp' => '3',
                'tax_classification' => '1',
                'pph22' => 'YZ',
                'top_dp_uom' => 'H',
            ],
            [
                'customer_id' => '1000000000',
                'sales_area_id' => '2',
                'term_of_payment' => 'Z003',
                'top_dp' => '3',
                'tax_classification' => '1',
                'pph22' => 'YZ',
                'top_dp_uom' => 'H',
            ]
        ];

        foreach ($data as $value) {
            CustomerSalesArea::create($value);
        }
    }

}
