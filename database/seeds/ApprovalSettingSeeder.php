<?php

use Illuminate\Database\Seeder;
use App\Models\ApprovalSetting;

class ApprovalSettingSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['sales_org_id' => 'B000', 'sales_group_id' => '1101'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1102'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1103'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1104'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1112'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1113'],
            ['sales_org_id' => 'B000', 'sales_group_id' => '1114'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1102'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1103'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1104'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1112'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1113'],
            ['sales_org_id' => 'C000', 'sales_group_id' => '1114'],
        ];

        ApprovalSetting::insert($data);
        $this->command->info('Sukses Insert approval setting');
    }

}
