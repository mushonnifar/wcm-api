<?php

use Illuminate\Database\Seeder;
use App\Models\Rdkk;
use App\Models\RdkkItem;
use App\Models\PlantingPeriod;

class RdkkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        $product=DB::table('wcm_product')->get();                
        $planing = array();                
        for ($i = 0; $i < 5; $i++) {
            $planting[] = array(
                        'code' => 4444+$i,
                        'desc' => 2017+$i,                         
                        'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',                        
                    );                    
        }        
        foreach ($planting as $value) {
            PlantingPeriod::create([
                'code' => $value['code'],
                'desc' => $value['desc'],                                          
                'created_by' => $value['created_by'],                        
            ]);
        }            
        $huruf='RDKK';        
        for($y=1;$y<3;$y++){ 
            $nominal=$y;
            $prefix=$huruf.$nominal;                        
            Rdkk::create([
                'number'=>$prefix,
                'planting_period_id' => '1',
                'customer_id' => 1000000000,                         
                'retail_id' => 1,
                'farmer_group_id' => 1,      
                'planting_area' => 1001,      
                'planting_from_date' => null,      
                'planting_thru_date' => null,      
                'sales_org_id' => 'B000',      
                'sales_unit_id' => 110101,                      
                'commodity' => '1;6;11',  
                'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',                        
            ]);     
            foreach ($product as $value) {
                RdkkItem::create([
                            'rdkk_id'=>$y,
                            'product_id' => $value->id,
                            'required_qty' => 100,                         
                            'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',                        
                ]);                    
            }
            
        }                                   
    } 
}
