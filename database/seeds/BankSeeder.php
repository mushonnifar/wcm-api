<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => 'B001', 'name' => 'Mandiri'],
            ['id' => 'B002', 'name' => 'BCA'],
        ];

        foreach ($data as $value) {
            Bank::create([
                'id' => $value['id'], 
                'name' => $value['name'], 
            ]);
        }
    }

}
