<?php

use Illuminate\Database\Seeder;
use  App\Models\InitialStocks;

class InitialStockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array(
                'type' => 'abc',
                'sales_org_id' => 'B000',
                'customer_id' => '1000000000',
                'sales_office_id' => '0035',
                'sales_group_id'=> '1114',
                'sales_unit_id' => '110112',
                'retail_id' => 1,
                'month' => '01',
                'year'=> '2019',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
        ];
        InitialStocks::insert($datas);
        $this->command->info('Sukses Insert InitialStockTableSeeder');
    }
}
