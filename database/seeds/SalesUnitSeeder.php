<?php

use Illuminate\Database\Seeder;
use App\Models\SalesUnit;

class SalesUnitSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => '110101', 'name' => 'Bakongan', 'sales_group_id' => '1101'],
            ['id' => '110102', 'name' => 'Kluet Utara', 'sales_group_id' => '1101'],
            ['id' => '110103', 'name' => 'Kluet Selatan', 'sales_group_id' => '1101'],
            ['id' => '110104', 'name' => 'Perak', 'sales_group_id' => '1115'],
            ['id' => '110105', 'name' => 'Benowo', 'sales_group_id' => '1115'],
            ['id' => '110106', 'name' => 'Tikung', 'sales_group_id' => '1113'],
            ['id' => '110107', 'name' => 'Karangbinangun', 'sales_group_id' => '1113'],
            ['id' => '110108', 'name' => 'Depok A', 'sales_group_id' => '1116'],
            ['id' => '110109', 'name' => 'Depok B', 'sales_group_id' => '1116'],
            ['id' => '110110', 'name' => 'Bekasi A', 'sales_group_id' => '1117'],
            ['id' => '110111', 'name' => 'Bekasi B', 'sales_group_id' => '1117'],
            ['id' => '110112', 'name' => 'Sidayu', 'sales_group_id' => '1114'],
            ['id' => '110113', 'name' => 'Manyar', 'sales_group_id' => '1114'],
            ['id' => '110114', 'name' => 'Kebomas', 'sales_group_id' => '1114'],
            ['id' => '110115', 'name' => 'Blora A', 'sales_group_id' => '1111'],
            ['id' => '110116', 'name' => 'Blora B', 'sales_group_id' => '1111'],
            ['id' => '110117', 'name' => 'Cepu A', 'sales_group_id' => '1112'],
            ['id' => '110118', 'name' => 'Cepu A', 'sales_group_id' => '1112'],
        ];

        foreach ($data as $value) {
            SalesUnit::create(['id' => $value['id'], 'name' => $value['name'], 'sales_group_id' => $value['sales_group_id']]);
        }
    }

}
