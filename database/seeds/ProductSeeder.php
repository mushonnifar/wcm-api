<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

Class ProductSeeder extends Seeder {

    public function run() {

        $data = array(
            array(
                'id' => 'P01',
                'name' => 'UREA',
                'multiplier' => '0.0500',
                'ident_measr_name' => 'KILOGRAM',
                'measr_name' => 'Kilogram',
                'measr_symbol' => 'kg',
                'measr_desc' => '',
                'status' => 'y'
            ),
            array(
                'id' => 'P02',
                'name' => 'NPK',
                'multiplier' => '0.0500',
                'ident_measr_name' => 'KILOGRAM',
                'measr_name' => 'Kilogram',
                'measr_symbol' => 'kg',
                'measr_desc' => 'Npk',
                'status' => 'y'
            ),
            array(
                'id' => 'P03',
                'name' => 'ORGANIK',
                'multiplier' => '0.0400',
                'ident_measr_name' => 'KILOGRAM',
                'measr_name' => 'Kilogram',
                'measr_symbol' => 'kg',
                'measr_desc' => 'Organik',
                'status' => 'y'
            ),
            array(
                'id' => 'P04',
                'name' => 'ZA',
                'multiplier' => '0.0500',
                'ident_measr_name' => 'KILOGRAM',
                'measr_name' => 'Kilogram',
                'measr_symbol' => 'kg',
                'measr_desc' => 'za',
                'status' => 'y'
            ),
            array(
                'id' => 'P05',
                'name' => 'SP-36',
                'multiplier' => '0.0500',
                'ident_measr_name' => 'KILOGRAM',
                'measr_name' => 'Kilogram',
                'measr_symbol' => 'kg',
                'measr_desc' => 'sp36',
                'status' => 'y'
            )
        );

        Product::insert($data);
        $this->command->info("Produk berhasil diinsert");
    }

}
