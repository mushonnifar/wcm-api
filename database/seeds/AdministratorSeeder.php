<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        App\Models\User::create([
            "username" => "developer",
            "name" => "Developer",
            "email" => "developer@tes.tes",
            "password" => \Hash::make("developer"),
        ]);
        App\Models\User::create([
            "username" => "admin",
            "name" => "Admin",
            "email" => "admin@tes.tes",
            "password" => \Hash::make("admin"),
        ]);
        App\Models\User::create([
            "username" => "admin_anper",
            "name" => "Admin Anper",
            "email" => "admin_anper@tes.tes",
            "password" => \Hash::make("admin_anper"),
            "sales_org_id" => '0001', // example
        ]);
        App\Models\User::create([
            "username" => "distributor",
            "name" => "Distributor",
            "email" => "distributor@tes.tes",
            "password" => \Hash::make("distributor"),
            "sales_org_id" => '0001', // example
            "customer_id" => '1000000000' // example
        ]);

        $this->command->info("Pengguna berhasil diinsert");
    }

}
