<?php

use Illuminate\Database\Seeder;
use App\Models\ReportF6Items;

class ReportF6ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas = [
            [
                'stok_awal'=>0,
                'penebusan'=>0,
                'penyaluran'=>0,
                'stok_akhir'=>0,
                'report_f6_id'=>1,
                'retail_id'=>2,
                'product_id'=>'P01',
               
            ],
            [
                'stok_awal'=>0,
                'penebusan'=>0,
                'penyaluran'=>0,
                'stok_akhir'=>0,
                'report_f6_id'=>2,
                'retail_id'=>2,
                'product_id'=>'P01',
            ],
        ];
        ReportF6Items::insert($datas);
        $this->command->info('Sukses Insert ReportF6ItemsSeeder');
    }
}
