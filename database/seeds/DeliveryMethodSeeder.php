<?php

use Illuminate\Database\Seeder;
use App\Models\DeliveryMethod;

class DeliveryMethodSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['ident_name' => 'FOT', 'name' => 'FOT'],
            ['ident_name' => 'FRA', 'name' => 'FRANCO'],
        ];

        foreach ($data as $value) {
            DeliveryMethod::create($value);
        }
    }

}
