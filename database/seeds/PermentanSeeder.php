<?php

use Illuminate\Database\Seeder;
use App\Models\ContractGoverment;
use App\Models\ContractGovItem;

class PermentanSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        
        $planting_period = array();
        $data2 = array();
        for ($i = 1; $i < 5; $i++) {
            $data1[] = array(
                        'number' => 'PKC/Permentan/SR.33/12/2018',
                        'contract_type' => 'permentan', 
                        'sales_org_id' => 'C000', 
                        'status' => 'y',
                        'year' => '2018',                         
                        'from_date' => null,                         
                        'thru_date' => null,                         
                        'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',                        
                    );
                    for($x=1;$x<13;$x++){
                        $data2[] = array(
                            'contract_gov_id' => $i,
                            'product_id' => $i, 
                            'sales_office_id' => '0031', 
                            'sales_group_id' => null, 
                            'sales_unit_id' => null, 
                            'month' => $x,
                            'year' => '2019',     
                            'initial_qty' => $i*$x+10,                                                     
                            'status' => 'y',
                            'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',                        
                        );
                    }
        }
        foreach ($data1 as $value) {
            ContractGoverment::create([
                'number' => $value['number'],
                'contract_type' => $value['contract_type'], 
                'sales_org_id' => $value['sales_org_id'], 
                'year' => $value['year'], 
                'from_date' => $value['from_date'],                         
                'thru_date' => $value['thru_date'],                          
                'created_by' => $value['created_by'],                        
            ]);
        }
        foreach ($data2 as $value) {
            ContractGovItem::create([
                'contract_gov_id' => $value['contract_gov_id'],
                'product_id' => 'P02', 
                'sales_office_id' => $value['sales_office_id'], 
                'sales_group_id' => $value['sales_group_id'], 
                'sales_unit_id' => $value['sales_unit_id'], 
                'month' => $value['month'], 
                'year' => $value['year'],     
                'initial_qty' => $value['initial_qty'],                                                     
                'status' => $value['status'],
                'created_by' => $value['created_by'],                        
            ]);
        }
    }    
}
