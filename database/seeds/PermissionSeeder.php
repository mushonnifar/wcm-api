<?php

use App\Models\PermissionHasRoutes;
use App\Models\RoleHasMenu;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         * Permission route -----------------------------------
         */
        $dtPermission = array(
            array('id' => 1, 'name' => 'menu-C', 'guard_name' => 'api', 'route_id' => 2, 'action_id' => 'C'), //1
            array('id' => 2, 'name' => 'menu-R', 'guard_name' => 'api', 'route_id' => 2, 'action_id' => 'R'), //2
            array('id' => 3, 'name' => 'menu-U', 'guard_name' => 'api', 'route_id' => 2, 'action_id' => 'U'), //3
            array('id' => 4, 'name' => 'menu-D', 'guard_name' => 'api', 'route_id' => 2, 'action_id' => 'D'), //4
            array('id' => 5, 'name' => 'user-C', 'guard_name' => 'api', 'route_id' => 1, 'action_id' => 'C'), //5
            array('id' => 6, 'name' => 'user-R', 'guard_name' => 'api', 'route_id' => 1, 'action_id' => 'R'), //6
            array('id' => 7, 'name' => 'user-U', 'guard_name' => 'api', 'route_id' => 1, 'action_id' => 'U'), //7
            array('id' => 8, 'name' => 'user-D', 'guard_name' => 'api', 'route_id' => 1, 'action_id' => 'D'), //8
            array('id' => 9, 'name' => 'role-C', 'guard_name' => 'api', 'route_id' => 4, 'action_id' => 'C'), //9
            array('id' => 10, 'name' => 'role-R', 'guard_name' => 'api', 'route_id' => 4, 'action_id' => 'R'), //10
            array('id' => 11, 'name' => 'role-U', 'guard_name' => 'api', 'route_id' => 4, 'action_id' => 'U'), //11
            array('id' => 12, 'name' => 'role-D', 'guard_name' => 'api', 'route_id' => 4, 'action_id' => 'D'), //12
            array('id' => 13, 'name' => 'permission-C', 'guard_name' => 'api', 'route_id' => 3, 'action_id' => 'C'), //13
            array('id' => 14, 'name' => 'permission-R', 'guard_name' => 'api', 'route_id' => 3, 'action_id' => 'R'), //14
            array('id' => 15, 'name' => 'permission-U', 'guard_name' => 'api', 'route_id' => 3, 'action_id' => 'U'), //15
            array('id' => 16, 'name' => 'permission-D', 'guard_name' => 'api', 'route_id' => 3, 'action_id' => 'D'), //16
            array('id' => 17, 'name' => 'route-C', 'guard_name' => 'api', 'route_id' => 5, 'action_id' => 'C'), //17
            array('id' => 18, 'name' => 'route-R', 'guard_name' => 'api', 'route_id' => 5, 'action_id' => 'R'), //18
            array('id' => 19, 'name' => 'route-U', 'guard_name' => 'api', 'route_id' => 5, 'action_id' => 'U'), //19
            array('id' => 20, 'name' => 'route-D', 'guard_name' => 'api', 'route_id' => 5, 'action_id' => 'D'), //20
            array('id' => 21, 'name' => 'distributor-R', 'guard_name' => 'api', 'route_id' => 6, 'action_id' => 'R'), //21
            array('id' => 22, 'name' => 'retail-C', 'guard_name' => 'api', 'route_id' => 7, 'action_id' => 'C'), //22
            array('id' => 23, 'name' => 'retail-R', 'guard_name' => 'api', 'route_id' => 7, 'action_id' => 'R'), //23
            array('id' => 24, 'name' => 'retail-U', 'guard_name' => 'api', 'route_id' => 7, 'action_id' => 'U'), //24
            array('id' => 25, 'name' => 'retail-D', 'guard_name' => 'api', 'route_id' => 7, 'action_id' => 'D'), //25
            array('id' => 26, 'name' => 'salesarea-R', 'guard_name' => 'api', 'route_id' => 8, 'action_id' => 'R'),
            array('id' => 27, 'name' => 'farmergroup-C', 'guard_name' => 'api', 'route_id' => 9, 'action_id' => 'C'),
            array('id' => 28, 'name' => 'farmergroup-R', 'guard_name' => 'api', 'route_id' => 9, 'action_id' => 'R'),
            array('id' => 29, 'name' => 'farmergroup-U', 'guard_name' => 'api', 'route_id' => 9, 'action_id' => 'U'),
            array('id' => 30, 'name' => 'farmergroup-D', 'guard_name' => 'api', 'route_id' => 9, 'action_id' => 'D'),
            array('id' => 31, 'name' => 'region-R', 'guard_name' => 'api', 'route_id' => 10, 'action_id' => 'R'),
            array('id' => 32, 'name' => 'farmer-C', 'guard_name' => 'api', 'route_id' => 11, 'action_id' => 'C'),
            array('id' => 33, 'name' => 'farmer-R', 'guard_name' => 'api', 'route_id' => 11, 'action_id' => 'R'),
            array('id' => 34, 'name' => 'farmer-U', 'guard_name' => 'api', 'route_id' => 11, 'action_id' => 'U'),
            array('id' => 35, 'name' => 'farmer-D', 'guard_name' => 'api', 'route_id' => 11, 'action_id' => 'D'),
            array('id' => 36, 'name' => 'retailassignment-C', 'guard_name' => 'api', 'route_id' => 12, 'action_id' => 'C'),
            array('id' => 37, 'name' => 'retailassignment-R', 'guard_name' => 'api', 'route_id' => 12, 'action_id' => 'R'),
            array('id' => 38, 'name' => 'retailassignment-U', 'guard_name' => 'api', 'route_id' => 12, 'action_id' => 'U'),
            array('id' => 39, 'name' => 'gudang-R', 'guard_name' => 'api', 'route_id' => 13, 'action_id' => 'R'),
            array('id' => 40, 'name' => 'company-R', 'guard_name' => 'api', 'route_id' => 14, 'action_id' => 'R'),
            array('id' => 41, 'name' => 'subsector-R', 'guard_name' => 'api', 'route_id' => 15, 'action_id' => 'R'),
            array('id' => 42, 'name' => 'commodity-R', 'guard_name' => 'api', 'route_id' => 16, 'action_id' => 'R'),
            array('id' => 43, 'name' => 'supplypoint-C', 'guard_name' => 'api', 'route_id' => 17, 'action_id' => 'C'),
            array('id' => 44, 'name' => 'supplypoint-R', 'guard_name' => 'api', 'route_id' => 17, 'action_id' => 'R'),
            array('id' => 45, 'name' => 'supplypoint-U', 'guard_name' => 'api', 'route_id' => 17, 'action_id' => 'U'),
            array('id' => 46, 'name' => 'supplypoint-D', 'guard_name' => 'api', 'route_id' => 17, 'action_id' => 'D'),
            array('id' => 47, 'name' => 'plant-R', 'guard_name' => 'api', 'route_id' => 18, 'action_id' => 'R'),
            array('id' => 48, 'name' => 'pricing-C', 'guard_name' => 'api', 'route_id' => 20, 'action_id' => 'C'),
            array('id' => 49, 'name' => 'pricing-R', 'guard_name' => 'api', 'route_id' => 20, 'action_id' => 'R'),
            array('id' => 50, 'name' => 'pricing-U', 'guard_name' => 'api', 'route_id' => 20, 'action_id' => 'U'),
            array('id' => 51, 'name' => 'pricing-D', 'guard_name' => 'api', 'route_id' => 20, 'action_id' => 'D'),
            array('id' => 52, 'name' => 'subtitution-C', 'guard_name' => 'api', 'route_id' => 19, 'action_id' => 'C'),
            array('id' => 53, 'name' => 'subtitution-R', 'guard_name' => 'api', 'route_id' => 19, 'action_id' => 'R'),
            array('id' => 54, 'name' => 'subtitution-U', 'guard_name' => 'api', 'route_id' => 19, 'action_id' => 'U'),
            array('id' => 55, 'name' => 'product-R', 'guard_name' => 'api', 'route_id' => 21, 'action_id' => 'R'),
            array('id' => 56, 'name' => 'deliverymethod-R', 'guard_name' => 'api', 'route_id' => 22, 'action_id' => 'R'),
            array('id' => 57, 'name' => 'salesdivision-R', 'guard_name' => 'api', 'route_id' => 23, 'action_id' => 'R'),
            array('id' => 58, 'name' => 'distribchannel-R', 'guard_name' => 'api', 'route_id' => 24, 'action_id' => 'R'),
            array('id' => 59, 'name' => 'materiallist-R', 'guard_name' => 'api', 'route_id' => 25, 'action_id' => 'R'),
            array('id' => 60, 'name' => 'bankdf-C', 'guard_name' => 'api', 'route_id' => 26, 'action_id' => 'C'),
            array('id' => 61, 'name' => 'bankdf-R', 'guard_name' => 'api', 'route_id' => 26, 'action_id' => 'R'),
            array('id' => 62, 'name' => 'bankdf-U', 'guard_name' => 'api', 'route_id' => 26, 'action_id' => 'U'),
            array('id' => 63, 'name' => 'bank-R', 'guard_name' => 'api', 'route_id' => 27, 'action_id' => 'R'),
            array('id' => 64, 'name' => 'permentan-C', 'guard_name' => 'api', 'route_id' => 28, 'action_id' => 'C'),
            array('id' => 65, 'name' => 'permentan-R', 'guard_name' => 'api', 'route_id' => 28, 'action_id' => 'R'),
            array('id' => 66, 'name' => 'permentan-U', 'guard_name' => 'api', 'route_id' => 28, 'action_id' => 'U'),
            array('id' => 67, 'name' => 'permentan-D', 'guard_name' => 'api', 'route_id' => 28, 'action_id' => 'D'),
            array('id' => 68, 'name' => 'pergub-C', 'guard_name' => 'api', 'route_id' => 29, 'action_id' => 'C'),
            array('id' => 69, 'name' => 'pergub-R', 'guard_name' => 'api', 'route_id' => 29, 'action_id' => 'R'),
            array('id' => 70, 'name' => 'pergub-U', 'guard_name' => 'api', 'route_id' => 29, 'action_id' => 'U'),
            array('id' => 71, 'name' => 'pergub-D', 'guard_name' => 'api', 'route_id' => 29, 'action_id' => 'D'),
            array('id' => 72, 'name' => 'perbup-C', 'guard_name' => 'api', 'route_id' => 30, 'action_id' => 'C'),
            array('id' => 73, 'name' => 'perbup-R', 'guard_name' => 'api', 'route_id' => 30, 'action_id' => 'R'),
            array('id' => 74, 'name' => 'perbup-U', 'guard_name' => 'api', 'route_id' => 30, 'action_id' => 'U'),
            array('id' => 75, 'name' => 'perbup-D', 'guard_name' => 'api', 'route_id' => 30, 'action_id' => 'D'),
            array('id' => 76, 'name' => 'spjb-C', 'guard_name' => 'api', 'route_id' => 31, 'action_id' => 'C'),
            array('id' => 77, 'name' => 'spjb-R', 'guard_name' => 'api', 'route_id' => 31, 'action_id' => 'R'),
            array('id' => 78, 'name' => 'spjb-U', 'guard_name' => 'api', 'route_id' => 31, 'action_id' => 'U'),
            array('id' => 79, 'name' => 'spjb-D', 'guard_name' => 'api', 'route_id' => 31, 'action_id' => 'D'),
            array('id' => 80, 'name' => 'rdkk-C', 'guard_name' => 'api', 'route_id' => 32, 'action_id' => 'C'),
            array('id' => 81, 'name' => 'rdkk-R', 'guard_name' => 'api', 'route_id' => 32, 'action_id' => 'R'),
            array('id' => 82, 'name' => 'rdkk-U', 'guard_name' => 'api', 'route_id' => 32, 'action_id' => 'U'),
            array('id' => 83, 'name' => 'rdkk-D', 'guard_name' => 'api', 'route_id' => 32, 'action_id' => 'D'),
            array('id' => 84, 'name' => 'spjbopr-C', 'guard_name' => 'api', 'route_id' => 33, 'action_id' => 'C'),
            array('id' => 85, 'name' => 'spjbopr-R', 'guard_name' => 'api', 'route_id' => 33, 'action_id' => 'R'),
            array('id' => 86, 'name' => 'spjbopr-U', 'guard_name' => 'api', 'route_id' => 33, 'action_id' => 'U'),
            array('id' => 87, 'name' => 'spjbopr-D', 'guard_name' => 'api', 'route_id' => 33, 'action_id' => 'D'),
            array('id' => 88, 'name' => 'order-C', 'guard_name' => 'api', 'route_id' => 34, 'action_id' => 'C'),
            array('id' => 89, 'name' => 'order-R', 'guard_name' => 'api', 'route_id' => 34, 'action_id' => 'R'),
            array('id' => 90, 'name' => 'order-U', 'guard_name' => 'api', 'route_id' => 34, 'action_id' => 'U'),
            array('id' => 91, 'name' => 'order-D', 'guard_name' => 'api', 'route_id' => 34, 'action_id' => 'D'),
            array('id' => 92, 'name' => 'monitoringOrder-R', 'guard_name' => 'api', 'route_id' => 35, 'action_id' => 'R'),
            array('id' => 93, 'name' => 'paymentmethod-R', 'guard_name' => 'api', 'route_id' => 36, 'action_id' => 'R'),
            array('id' => 94, 'name' => 'monitoringdo-R', 'guard_name' => 'api', 'route_id' => 37, 'action_id' => 'R'),
            array('id' => 95, 'name' => 'penyalurando-C', 'guard_name' => 'api', 'route_id' => 38, 'action_id' => 'C'),
            array('id' => 96, 'name' => 'penyalurando-R', 'guard_name' => 'api', 'route_id' => 38, 'action_id' => 'R'),
            array('id' => 97, 'name' => 'penyalurando-U', 'guard_name' => 'api', 'route_id' => 38, 'action_id' => 'U'),
            array('id' => 98, 'name' => 'penyalurando-D', 'guard_name' => 'api', 'route_id' => 38, 'action_id' => 'D'),
            array('id' => 99, 'name' => 'monitoringdf-C', 'guard_name' => 'api', 'route_id' => 39, 'action_id' => 'C'),
            array('id' => 100, 'name' => 'monitoringdf-R', 'guard_name' => 'api', 'route_id' => 39, 'action_id' => 'R'),
            array('id' => 101, 'name' => 'monitoringdf-U', 'guard_name' => 'api', 'route_id' => 39, 'action_id' => 'U'),
            array('id' => 102, 'name' => 'distribitem-R', 'guard_name' => 'api', 'route_id' => 40, 'action_id' => 'R'),
            array('id' => 103, 'name' => 'reportf5-C', 'guard_name' => 'api', 'route_id' => 41, 'action_id' => 'C'),
            array('id' => 104, 'name' => 'reportf5-R', 'guard_name' => 'api', 'route_id' => 41, 'action_id' => 'R'),
            array('id' => 105, 'name' => 'reportf5-U', 'guard_name' => 'api', 'route_id' => 41, 'action_id' => 'U'),
            array('id' => 106, 'name' => 'reportf5-D', 'guard_name' => 'api', 'route_id' => 41, 'action_id' => 'D'),
            array('id' => 107, 'name' => 'manualapproval-R', 'guard_name' => 'api', 'route_id' => 42, 'action_id' => 'R'),
            array('id' => 108, 'name' => 'manualapproval-A', 'guard_name' => 'api', 'route_id' => 42, 'action_id' => 'A'),
            array('id' => 109, 'name' => 'initialstockf5-C', 'guard_name' => 'api', 'route_id' => 43, 'action_id' => 'C'),
            array('id' => 110, 'name' => 'initialstockf5-R', 'guard_name' => 'api', 'route_id' => 43, 'action_id' => 'R'),
            array('id' => 111, 'name' => 'penyaluraninitialstockf5-C', 'guard_name' => 'api', 'route_id' => 44, 'action_id' => 'C'),
            array('id' => 112, 'name' => 'penyaluraninitialstockf5-R', 'guard_name' => 'api', 'route_id' => 44, 'action_id' => 'R'),
            array('id' => 113, 'name' => 'reportf6-C', 'guard_name' => 'api', 'route_id' => 45, 'action_id' => 'C'),
            array('id' => 114, 'name' => 'reportf6-R', 'guard_name' => 'api', 'route_id' => 45, 'action_id' => 'R'),
            array('id' => 115, 'name' => 'reportf6-U', 'guard_name' => 'api', 'route_id' => 45, 'action_id' => 'U'),
            array('id' => 116, 'name' => 'reportf6-D', 'guard_name' => 'api', 'route_id' => 45, 'action_id' => 'D'),
            /* Initial Stock F6 */
            array('id' => 117, 'name' => 'initialstockf6-C', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'C'),
            array('id' => 118, 'name' => 'initialstockf6-R', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'R'),
            array('id' => 119, 'name' => 'initialstockf6-U', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'U'),
            array('id' => 120, 'name' => 'initialstockf6-D', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'D'),
            /* Penyaluran Initial Stock F6 */
            array('id' => 121, 'name' => 'penyaluraninitialstockf6-C', 'guard_name' => 'api', 'route_id' => 47, 'action_id' => 'C'),
            array('id' => 122, 'name' => 'penyaluraninitialstockf6-R', 'guard_name' => 'api', 'route_id' => 47, 'action_id' => 'R'),
            array('id' => 123, 'name' => 'penyaluraninitialstockf6-U', 'guard_name' => 'api', 'route_id' => 47, 'action_id' => 'U'),
            array('id' => 124, 'name' => 'penyaluraninitialstockf6-D', 'guard_name' => 'api', 'route_id' => 47, 'action_id' => 'D'),
            // Syncronize
            // array('id' => 125, 'name' => 'syncronize-R', 'guard_name' => 'api', 'route_id' => 48, 'action_id' => 'R'),
            
            //
            array('id' => 126, 'name' => 'bankaccount-C', 'guard_name' => 'api', 'route_id' => 49, 'action_id' => 'C'),
            array('id' => 127, 'name' => 'bankaccount-R', 'guard_name' => 'api', 'route_id' => 49, 'action_id' => 'R'),
            array('id' => 128, 'name' => 'bankaccount-U', 'guard_name' => 'api', 'route_id' => 49, 'action_id' => 'U'),
            array('id' => 129, 'name' => 'bankaccount-D', 'guard_name' => 'api', 'route_id' => 49, 'action_id' => 'D'),

            array('id' => 130, 'name' => 'manualso-C', 'guard_name' => 'api', 'route_id' => 50, 'action_id' => 'C'),
            array('id' => 131, 'name' => 'logorder-R', 'guard_name' => 'api', 'route_id' => 51, 'action_id' => 'R'),
            array('id' => 132, 'name' => 'monitoringdo-C', 'guard_name' => 'api', 'route_id' => 37, 'action_id' => 'C'),

            array('id' => 133, 'name' => 'limit-C', 'guard_name' => 'api', 'route_id' => 52, 'action_id' => 'C'),
            array('id' => 134, 'name' => 'limit-R', 'guard_name' => 'api', 'route_id' => 52, 'action_id' => 'R'),
            array('id' => 135, 'name' => 'limit-U', 'guard_name' => 'api', 'route_id' => 52, 'action_id' => 'U'),
            array('id' => 136, 'name' => 'limit-D', 'guard_name' => 'api', 'route_id' => 52, 'action_id' => 'D'),
            array('id' => 137, 'name' => 'h2h-R', 'guard_name' => 'api', 'route_id' => 53, 'action_id' => 'D'),

            /*
             * Planting Periode
             */

            array('id' => 138, 'name' => 'platingperiode-C', 'guard_name' => 'api', 'route_id' => 54, 'action_id' => 'C'),
            array('id' => 139, 'name' => 'platingperiode-R', 'guard_name' => 'api', 'route_id' => 54, 'action_id' => 'R'),
            array('id' => 140, 'name' => 'platingperiode-U', 'guard_name' => 'api', 'route_id' => 54, 'action_id' => 'U'),
            array('id' => 141, 'name' => 'platingperiode-D', 'guard_name' => 'api', 'route_id' => 54, 'action_id' => 'D'),

            /*
             * Report SO
             */

            array('id' => 142, 'name' => 'monitoringorder-C', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'C'),

            // Configuration
            array('id' => 143, 'name' => 'initialstockf6-A', 'guard_name' => 'api', 'route_id' => 46, 'action_id' => 'A'),
            array('id' => 144, 'name' => 'monitoringorder-D', 'guard_name' => 'api', 'route_id' => 37, 'action_id' => 'D'),
            array('id' => 145, 'name' => 'monitoringorder-U', 'guard_name' => 'api', 'route_id' => 37, 'action_id' => 'U'),
            array('id' => 146, 'name' => 'monitoringorder-A', 'guard_name' => 'api', 'route_id' => 37, 'action_id' => 'A'),

            // syncronize
            array('id' => 147, 'name' => 'syncronizes-U', 'guard_name' => 'api', 'route_id' => 57, 'action_id' => 'U'),
            array('id' => 148, 'name' => 'syncronizes-R', 'guard_name' => 'api', 'route_id' => 57, 'action_id' => 'R'),
            array('id' => 149, 'name' => 'syncronizes-D', 'guard_name' => 'api', 'route_id' => 57, 'action_id' => 'D'),
            array('id' => 150, 'name' => 'syncronizes-C', 'guard_name' => 'api', 'route_id' => 57, 'action_id' => 'C'),
            array('id' => 151, 'name' => 'syncronizes-A', 'guard_name' => 'api', 'route_id' => 57, 'action_id' => 'A'),
            array('id'=>152,'name'=>'configuration-C', 'guard_name' => 'api', 'route_id'=>	56, 'action_id' => 'C'),
            array('id'=>153,'name'=>'region-C', 'guard_name' => 'api', 'route_id'=>	10, 'action_id' => 'C'),
            array('id'=>154,'name'=>'retail-assignment-D', 'guard_name' => 'api', 'route_id'=>	12, 'action_id' => 'D'),
            array('id'=>155,'name'=>'subsector-D', 'guard_name' => 'api', 'route_id'=>	15, 'action_id' => 'D'),
            array('id'=>156,'name'=>'subsector-C', 'guard_name' => 'api', 'route_id'=>	15, 'action_id' => 'C'),
            array('id'=>157,'name'=>'subsector-U', 'guard_name' => 'api', 'route_id'=>	15, 'action_id' => 'U'),
            array('id'=>158,'name'=>'commodity-D', 'guard_name' => 'api', 'route_id'=>	16, 'action_id' => 'D'),
            array('id'=>159,'name'=>'commodity-C', 'guard_name' => 'api', 'route_id'=>	16, 'action_id' => 'C'),
            array('id'=>160,'name'=>'commodity-U', 'guard_name' => 'api', 'route_id'=>	16, 'action_id' => 'U'),
            array('id'=>161,'name'=>'plant-C', 'guard_name' => 'api', 'route_id'=>	18, 'action_id' => 'C'),
            array('id'=>162,'name'=>'plant-D', 'guard_name' => 'api', 'route_id'=>	18, 'action_id' => 'D'),
            array('id'=>163,'name'=>'plant-U', 'guard_name' => 'api', 'route_id'=>	18, 'action_id' => 'U'),
            array('id'=>164,'name'=>'deliverymethod-C', 'guard_name' => 'api', 'route_id'=>	22, 'action_id' => 'C'),
            array('id'=>165,'name'=>'deliverymethod-D', 'guard_name' => 'api', 'route_id'=>	22, 'action_id' => 'D'),
            array('id'=>166,'name'=>'deliverymethod-U', 'guard_name' => 'api', 'route_id'=>	22, 'action_id' => 'U'),
            array('id'=>167,'name'=>'bankdf-D', 'guard_name' => 'api', 'route_id'=>	26, 'action_id' => 'D'),
            array('id'=>168,'name'=>'monitoringorder-D', 'guard_name' => 'api', 'route_id'=>	35, 'action_id' => 'D'),
            array('id'=>169,'name'=>'monitoringorder-U', 'guard_name' => 'api', 'route_id'=>	35, 'action_id' => 'U'),
            array('id'=>170,'name'=>'paymentmethod-C', 'guard_name' => 'api', 'route_id'=>	36, 'action_id' => 'C'),
            array('id'=>171,'name'=>'paymentmethod-D', 'guard_name' => 'api', 'route_id'=>	36, 'action_id' => 'D'),
            array('id'=>172,'name'=>'paymentmethod-U', 'guard_name' => 'api', 'route_id'=>	36, 'action_id' => 'U'),
            array('id'=>173, 'name'=>'monitoringdf-D', 'route_id'=>39, 'action_id' => 'D'),
            array('id'=>174, 'name'=>'distribitem-C'	, 'route_id'=>40, 'action_id' => 'C'),
            array('id'=>175, 'name'=>'distribitem-D'	, 'route_id'=>40, 'action_id' => 'D'),
            array('id'=>176, 'name'=>'distribitem-U'	, 'route_id'=>40, 'action_id' => 'U'),
            array('id'=>177, 'name'=>'initialstockf5-D'	, 'route_id'=>43, 'action_id' => 'D'),
            array('id'=>178, 'name'=>'initialstockf5-U'	, 'route_id'=>43, 'action_id' => 'U'),
            array('id'=>179, 'name'=>'penyaluraninitialstockf5-D'	, 'route_id'=>44, 'action_id' => 'D'),
            array('id'=>180, 'name'=>'penyaluraninitialstockf5-U'	, 'route_id'=>44, 'action_id' => 'U'),
            array('id'=>181, 'name'=>'logorder-C'	, 'route_id'=>51, 'action_id' => 'C'),
            array('id'=>182, 'name'=>'logorder-D'	, 'route_id'=>51, 'action_id' => 'D'),
            array('id'=>183, 'name'=>'logorder-U'	, 'route_id'=>51, 'action_id' => 'U'),
            array('id'=>184, 'name'=>'distributor-C', 'route_id'=>	6, 'action_id' => 'C'),
            array('id'=>185, 'name'=>'distributor-U', 'route_id'=>	6, 'action_id' => 'U'),
            array('id'=>186, 'name'=>'salesarea-C', 'route_id'=>	8, 'action_id' => 'C'),
            array('id'=>187, 'name'=>'salesarea-U', 'route_id'=>	8, 'action_id' => 'U'),
            array('id'=>188, 'name'=>'gudang-C'	, 'route_id'=>13, 'action_id' => 'C'),
            array('id'=>189, 'name'=>'company-C'	, 'route_id'=>14, 'action_id' => 'C'),
            array('id'=>190, 'name'=>'configuration-C'	, 'route_id'=>57, 'action_id' => 'C'),
            array('id'=>191, 'name'=>'distributor-D', 'route_id'=>	6, 'action_id' => 'D'),
            array('id'=>192, 'name'=>'salesarea-D', 'route_id'=>	8, 'action_id' => 'D'),
            array('id'=>193, 'name'=>'region-D'	, 'route_id'=>10, 'action_id' => 'D'),
            array('id'=>194, 'name'=>'region-U'	, 'route_id'=>10, 'action_id' => 'U'),
            array('id'=>195, 'name'=>'gudang-D'	, 'route_id'=>13, 'action_id' => 'D'),
            array('id'=>196, 'name'=>'gudang-U'	, 'route_id'=>13, 'action_id' => 'U'),
            array('id'=>197, 'name'=>'company-U'	, 'route_id'=>14, 'action_id' => 'U'),
            array('id'=>198, 'name'=>'company-D'	, 'route_id'=>14, 'action_id' => 'D'),
            array('id'=>199, 'name'=>'user-A', 'route_id'=>	1, 'action_id' => 'A'),
            array('id'=>200, 'name'=>'menu-A', 'route_id'=>	2, 'action_id' => 'A'),
            array('id'=>201, 'name'=>'permission-A', 'route_id'=>	3, 'action_id' => 'A'),
            array('id'=>202, 'name'=>'role-A', 'route_id'=>	4, 'action_id' => 'A'),
            array('id'=>203, 'name'=>'route-A', 'route_id'=>	5, 'action_id' => 'A'),
            array('id'=>204, 'name'=>'distributor-A', 'route_id'=>	6, 'action_id' => 'A'),
            array('id'=>205, 'name'=>'retail-A', 'route_id'=>	7, 'action_id' => 'A'),
            array('id'=>206, 'name'=>'salesarea-A', 'route_id'=>	8, 'action_id' => 'A'),
            array('id'=>207, 'name'=>'farmergroup-A', 'route_id'=>	9, 'action_id' => 'A'),
            array('id'=>208, 'name'=>'region-A'	, 'route_id'=>10, 'action_id' => 'A'),
            array('id'=>209, 'name'=>'farmer-A'	, 'route_id'=>11, 'action_id' => 'A'),
            array('id'=>210, 'name'=>'retail-assignment-A'	, 'route_id'=>12, 'action_id' => 'a'),
            array('id'=>211, 'name'=>'gudang-A'	, 'route_id'=>13, 'action_id' => 'A'),
            array('id'=>212, 'name'=>'company-A'	, 'route_id'=>14, 'action_id' => 'A'),
            array('id'=>213, 'name'=>'subsector-A'	, 'route_id'=>15, 'action_id' => 'A'),
            array('id'=>214, 'name'=>'commodity-A'	, 'route_id'=>16, 'action_id' => 'A'),
            array('id'=>215, 'name'=>'supplypoint-A'	, 'route_id'=>17, 'action_id' => 'A'),
            array('id'=>216, 'name'=>'plant-A'	, 'route_id'=>18, 'action_id' => 'A'),
            array('id'=>217, 'name'=>'subtitution-A'	, 'route_id'=>19, 'action_id' => 'A'),
            array('id'=>218, 'name'=>'pricing-A'	, 'route_id'=>20, 'action_id' => 'A'),
            array('id'=>219, 'name'=>'subtitution-D'	, 'route_id'=>19, 'action_id' => 'D'),
            array('id'=>220, 'name'=>'product-A'	, 'route_id'=>21, 'action_id' => 'A'),
            array('id'=>221, 'name'=>'product-C'	, 'route_id'=>21, 'action_id' => 'C'),
            array('id'=>222, 'name'=>'product-D'	, 'route_id'=>21, 'action_id' => 'D'),
            array('id'=>223, 'name'=>'product-U'	, 'route_id'=>21, 'action_id' => 'U'),
            array('id'=>224, 'name'=>'deliverymethod-A'	, 'route_id'=>22, 'action_id' => 'A'),
            array('id'=>225, 'name'=>'salesdivision-A'	, 'route_id'=>23, 'action_id' => 'A'),
            array('id'=>226, 'name'=>'distribchannel-A'	, 'route_id'=>24, 'action_id' => 'A'),
            array('id'=>227, 'name'=>'materiallist-A'	, 'route_id'=>25, 'action_id' => 'A'),
            array('id'=>228, 'name'=>'bankdf-A'	, 'route_id'=>26, 'action_id' => 'A'),
            array('id'=>229, 'name'=>'bank-A'	, 'route_id'=>27, 'action_id' => 'A'),
            array('id'=>230, 'name'=>'permentan-A'	, 'route_id'=>28, 'action_id' => 'A'),
            array('id'=>231, 'name'=>'pergub-A'	, 'route_id'=>29, 'action_id' => 'A'),
            array('id'=>232, 'name'=>'perbup-A'	, 'route_id'=>30, 'action_id' => 'A'),
            array('id'=>233, 'name'=>'spjb-A'	, 'route_id'=>31, 'action_id' => 'A'),
            array('id'=>234, 'name'=>'rdkk-A'	, 'route_id'=>32, 'action_id' => 'A'),
            array('id'=>235, 'name'=>'spjbopr-A'	, 'route_id'=>33, 'action_id' => 'A'),
            array('id'=>236, 'name'=>'order-A'	, 'route_id'=>34, 'action_id' => 'A'),
            array('id'=>237, 'name'=>'monitoringorder-A'	, 'route_id'=>35, 'action_id' => 'A'),
            array('id'=>238, 'name'=>'salesdivision-C'	, 'route_id'=>23, 'action_id' => 'C'),
            array('id'=>239, 'name'=>'distribchannel-C'	, 'route_id'=>24, 'action_id' => 'C'),
            array('id'=>240, 'name'=>'materiallist-C'	, 'route_id'=>25, 'action_id' => 'C'),
            array('id'=>241, 'name'=>'bank-C'	, 'route_id'=>27, 'action_id' => 'C'),
            array('id'=>242, 'name'=>'salesdivision-D'	, 'route_id'=>23, 'action_id' => 'D'),
            array('id'=>243, 'name'=>'distribchannel-D'	, 'route_id'=>24, 'action_id' => 'D'),
            array('id'=>244, 'name'=>'materiallist-D'	, 'route_id'=>25, 'action_id' => 'D'),
            array('id'=>245, 'name'=>'bank-D'	, 'route_id'=>27, 'action_id' => 'D'),
            array('id'=>246, 'name'=>'salesdivision-U'	, 'route_id'=>23, 'action_id' => 'U'),
            array('id'=>247, 'name'=>'distribchannel-U'	, 'route_id'=>24, 'action_id' => 'U'),
            array('id'=>248, 'name'=>'materiallist-U'	, 'route_id'=>25, 'action_id' => 'U'),
            array('id'=>249, 'name'=>'bank-U'	, 'route_id'=>27, 'action_id' => 'U'),
            array('id'=>250, 'name'=>'paymentmethod-A'	, 'route_id'=>36, 'action_id' => 'A'),
            array('id'=>251, 'name'=>'penyalurando-A'	, 'route_id'=>38, 'action_id' => 'A'),
            array('id'=>252, 'name'=>'monitoringdf-A'	, 'route_id'=>39, 'action_id' => 'A'),
            array('id'=>253, 'name'=>'distribitem-A'	, 'route_id'=>40, 'action_id' => 'A'),
            array('id'=>254, 'name'=>'reportf5-A'	, 'route_id'=>41, 'action_id' => 'A'),
            array('id'=>255, 'name'=>'initialstockf5-A'	, 'route_id'=>43, 'action_id' => 'A'),
            array('id'=>256, 'name'=>'penyaluraninitialstockf5-A'	, 'route_id'=>44, 'action_id' => 'A'),
            array('id'=>257, 'name'=>'reportf6-A'	, 'route_id'=>45, 'action_id' => 'A'),
            array('id'=>258, 'name'=>'penyaluraninitialstockf6-A'	, 'route_id'=>47, 'action_id' => 'A'),
            array('id'=>259, 'name'=>'bank-account-A'	, 'route_id'=>49, 'action_id' => 'a'),
            array('id'=>260, 'name'=>'manualso-A'	, 'route_id'=>50, 'action_id' => 'A'),
            array('id'=>261, 'name'=>'logorder-A'	, 'route_id'=>51, 'action_id' => 'A'),
            array('id'=>262, 'name'=>'limit-A'	, 'route_id'=>52, 'action_id' => 'A'),
            array('id'=>263, 'name'=>'h2h-A'	, 'route_id'=>53, 'action_id' => 'A'),
            array('id'=>264, 'name'=>'plantingperiod-A'	, 'route_id'=>54, 'action_id' => 'A'),
            array('id'=>265, 'name'=>'report-s-o-C'	, 'route_id'=>55, 'action_id' => 's'),
            array('id'=>266, 'name'=>'report-s-o-A'	, 'route_id'=>55, 'action_id' => 's'),
            array('id'=>267, 'name'=>'h2h-C'	, 'route_id'=>53, 'action_id' => 'C'),
            array('id'=>268, 'name'=>'manualapproval-C'	, 'route_id'=>42, 'action_id' => 'C'),
            array('id'=>269, 'name'=>'manualapproval-D'	, 'route_id'=>42, 'action_id' => 'D'),
            array('id'=>270, 'name'=>'manualso-D'	, 'route_id'=>50, 'action_id' => 'D'),
            array('id'=>271, 'name'=>'report-s-o-D'	, 'route_id'=>55, 'action_id' => 's'),
            array('id'=>272, 'name'=>'report-s-o-R'	, 'route_id'=>55, 'action_id' => 's'),
            array('id'=>273, 'name'=>'h2h-R'	, 'route_id'=>53, 'action_id' => 'R'),
            array('id'=>274, 'name'=>'manualso-R'	, 'route_id'=>50, 'action_id' => 'R'),
            array('id'=>275, 'name'=>'manualapproval-U'	, 'route_id'=>42, 'action_id' => 'U'),
            array('id'=>276, 'name'=>'manualso-U'	, 'route_id'=>50, 'action_id' => 'U'),
            array('id'=>277, 'name'=>'h2h-U'	, 'route_id'=>53, 'action_id' => 'U'),
            array('id'=>278, 'name'=>'report-s-o-U'	, 'route_id'=>55, 'action_id' => 's'),
            array('id'=>279, 'name'=>'configuration-A'	, 'route_id'=>56, 'action_id' => 'A'),
            array('id'=>280, 'name'=>'village-A'	, 'route_id'=>57, 'action_id' => 'A'),
            array('id'=>281, 'name'=>'configuration-D'	, 'route_id'=>56, 'action_id' => 'D'),
            array('id'=>282, 'name'=>'village-D'	, 'route_id'=>57, 'action_id' => 'D'),
            array('id'=>283, 'name'=>'village-R'	, 'route_id'=>57, 'action_id' => 'R'),
            array('id'=>284, 'name'=>'configuration-R'	, 'route_id'=>56, 'action_id' => 'R'),
            array('id'=>285, 'name'=>'configuration-U'	, 'route_id'=>56, 'action_id' => 'U'),
            array('id'=>286, 'name'=>'village-U'	, 'route_id'=>57, 'action_id' => 'U'),
            array('id'=>287, 'name'=>'configlintasalur-A'	, 'route_id'=>58, 'action_id' => 'A'),
            array('id'=>288, 'name'=>'configlintasalur-C'	, 'route_id'=>58, 'action_id' => 'C'),
            array('id'=>289, 'name'=>'configlintasalur-D'	, 'route_id'=>58, 'action_id' => 'D'),
            array('id'=>290, 'name'=>'configlintasalur-R'	, 'route_id'=>58, 'action_id' => 'R'),
            array('id'=>291, 'name'=>'configlintasalur-U'	, 'route_id'=>58, 'action_id' => 'U'),
        );
        Permission::insert($dtPermission);

        /*
         * Permission has route -----------------------------------
         */
        $dtHasPermission = array(
            /*
             * menu
             */
            array('permission_id' => 1, 'method' => 'POST', 'url' => 'menu', 'path' => 'MenuController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 2, 'method' => 'GET', 'url' => 'menu', 'path' => 'MenuController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 2, 'method' => 'GET', 'url' => 'sidebar', 'path' => 'MenuController@sidebar', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 2, 'method' => 'GET', 'url' => 'menu/id/{id}', 'path' => 'MenuController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 3, 'method' => 'PUT', 'url' => 'menu/{id}', 'path' => 'MenuController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 4, 'method' => 'DELETE', 'url' => 'menu/{id}', 'path' => 'MenuController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * user
             */
            array('permission_id' => 5, 'method' => 'POST', 'url' => 'user', 'path' => 'UserController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 6, 'method' => 'GET', 'url' => 'user', 'path' => 'UserController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 6, 'method' => 'GET', 'url' => 'user/id/{id}', 'path' => 'UserController@show', 'middleware' => 'jwt.authz,localization'),
            // array('permission_id' => 6, 'method' => 'GET', 'url' => 'user/fetch', 'path' => 'ApiController@detail', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 6, 'method' => 'GET', 'url' => 'user/refresh', 'path' => 'ApiController@refresh', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 7, 'method' => 'PUT', 'url' => 'user/{id}', 'path' => 'UserController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 7, 'method' => 'PUT', 'url' => 'user/password/{id}', 'path' => 'UserController@updatePassword', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 8, 'method' => 'DELETE', 'url' => 'user/{id}', 'path' => 'UserController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * role
             */
            array('permission_id' => 9, 'method' => 'POST', 'url' => 'role', 'path' => 'RoleController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 10, 'method' => 'GET', 'url' => 'role', 'path' => 'RoleController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 10, 'method' => 'GET', 'url' => 'role/id/{id}', 'path' => 'RoleController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 11, 'method' => 'PUT', 'url' => 'role/{id}', 'path' => 'RoleController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 12, 'method' => 'DELETE', 'url' => 'role/{id}', 'path' => 'RoleController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * route
             */
            array('permission_id' => 17, 'method' => 'POST', 'url' => 'route', 'path' => 'RouteController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 18, 'method' => 'GET', 'url' => 'route', 'path' => 'RouteController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 18, 'method' => 'GET', 'url' => 'route/id/{id}', 'path' => 'RouteController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 19, 'method' => 'PUT', 'url' => 'route/{id}', 'path' => 'RouteController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 20, 'method' => 'DELETE', 'url' => 'route/{id}', 'path' => 'RouteController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // //Belum tentu dipakai ----------------------------------
            // array('permission_id' => 9, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/permission/{givePermissionTo}', 'path' => 'RoleController@setPermissions', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            // array('permission_id' => 11, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/permission/{syncPermissions}', 'path' => 'RoleController@setPermissions', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            // array('permission_id' => 12, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/permission/{assignRole}', 'path' => 'RoleController@setPermissions', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            // array('permission_id' => 9, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/{assignRole}', 'path' => 'RoleController@setRoles', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            // array('permission_id' => 11, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/{syncRole}', 'path' => 'RoleController@setRoles', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            // array('permission_id' => 12, 'menu_id' => 4, 'method' => 'POST', 'url' => 'role/{removeRole}', 'path' => 'RoleController@setRoles', 'middleware' => 'jwt.refresh,jwt.authz,localization'),
            /*
             * permission
             */
            array('permission_id' => 13, 'method' => 'POST', 'url' => 'permission', 'path' => 'PermissionController@store', 'middleware' => 'jwt.authz,jwt.refresh,localization'),
            array('permission_id' => 15, 'method' => 'PUT', 'url' => 'permission/{id}', 'path' => 'PermissionController@update', 'middleware' => 'jwt.authz,jwt.refresh,localization'),
            array('permission_id' => 16, 'method' => 'DELETE', 'url' => 'permission/{id}', 'path' => 'PermissionController@destroy', 'middleware' => 'jwt.authz,jwt.refresh,localization'),
            array('permission_id' => 2, 'method' => 'GET', 'url' => 'menu/permission/{role_id}', 'path' => 'MenuController@permission', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 1, 'method' => 'POST', 'url' => 'menu/setpermission/{role_id}', 'path' => 'MenuController@setPermission', 'middleware' => 'jwt.authz,jwt.refresh,localization'),
            array('permission_id' => 14, 'method' => 'GET', 'url' => 'permission', 'path' => 'PermissionController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 18, 'method' => 'GET', 'url' => 'route/permission/{role_id}', 'path' => 'RouteController@permission', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 17, 'method' => 'POST', 'url' => 'route/setpermission/{role_id}', 'path' => 'RouteController@setPermission', 'middleware' => 'jwt.authz,jwt.refresh,localization'),
            /*
             * Distributor
             */
            array('permission_id' => 21, 'method' => 'GET', 'url' => 'distributor', 'path' => 'DistributorController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 21, 'method' => 'GET', 'url' => 'distributor/id/{id}', 'path' => 'DistributorController@show', 'middleware' => 'jwt.authz,localization'),
            /*
             * Retail
             */
            array('permission_id' => 22, 'method' => 'POST', 'url' => 'retail', 'path' => 'RetailController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 23, 'method' => 'GET', 'url' => 'retail', 'path' => 'RetailController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 23, 'method' => 'GET', 'url' => 'retail/cust_id/{id}', 'path' => 'RetailController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 23, 'method' => 'GET', 'url' => 'retail/id/{id}', 'path' => 'RetailController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 24, 'method' => 'PUT', 'url' => 'retail/{id}', 'path' => 'RetailController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 24, 'method' => 'POST', 'url' => 'retail/update/bulk', 'path' => 'RetailController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 25, 'method' => 'DELETE', 'url' => 'retail/{id}', 'path' => 'RetailController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 23, 'method' => 'POST', 'url' => 'retail/upload', 'path' => 'RetailController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 22, 'method' => 'POST', 'url' => 'retail/json/bulk', 'path' => 'RetailController@saveJsonImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 22, 'method' => 'POST', 'url' => 'retail/bulk', 'path' => 'RetailController@saveImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * salesarea
             */
            array('permission_id' => 26, 'method' => 'GET', 'url' => 'salesarea', 'path' => 'SalesAreaController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 26, 'method' => 'GET', 'url' => 'salesarea/id/{id}', 'path' => 'SalesAreaController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 26, 'method' => 'GET', 'url' => 'salesarea/customer', 'path' => 'SalesAreaController@statusWapu', 'middleware' => 'jwt.authz,localization'),
            /*
             * farmer group
             */
            array('permission_id' => 28, 'method' => 'GET', 'url' => 'farmergroup/commodities', 'path' => 'FarmerGroupController@getKomidities', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 28, 'method' => 'GET', 'url' => 'farmergroup/subsectors', 'path' => 'FarmerGroupController@getSubsector', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 27, 'method' => 'POST', 'url' => 'farmergroup', 'path' => 'FarmerGroupController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 27, 'method' => 'POST', 'url' => 'farmergroup/upload', 'path' => 'FarmerGroupController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 27, 'method' => 'POST', 'url' => 'farmergroup/json/bulk', 'path' => 'FarmerGroupController@saveImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 28, 'method' => 'GET', 'url' => 'farmergroup', 'path' => 'FarmerGroupController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 28, 'method' => 'GET', 'url' => 'farmergroup/id/{id}', 'path' => 'FarmerGroupController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 28, 'method' => 'GET', 'url' => 'farmergroup/cust_id/{id}', 'path' => 'FarmerGroupController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 29, 'method' => 'PUT', 'url' => 'farmergroup/{id}', 'path' => 'FarmerGroupController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 29, 'method' => 'POST', 'url' => 'farmergroup/update/bulk', 'path' => 'FarmerGroupController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 30, 'method' => 'DELETE', 'url' => 'farmergroup/{id}', 'path' => 'FarmerGroupController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * region
             */
            array('permission_id' => 31, 'method' => 'GET', 'url' => 'region/provinsi', 'path' => 'RegionController@provinsi', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 31, 'method' => 'GET', 'url' => 'region/kabupaten', 'path' => 'RegionController@kabupaten', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 31, 'method' => 'GET', 'url' => 'region/kecamatan', 'path' => 'RegionController@kecamatan', 'middleware' => 'jwt.authz,localization'),
            /*
             * farmer
             */
            array('permission_id' => 32, 'method' => 'POST', 'url' => 'farmer', 'path' => 'FarmerController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 32, 'method' => 'POST', 'url' => 'farmer/upload', 'path' => 'FarmerController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 32, 'method' => 'POST', 'url' => 'farmer/json/bulk', 'path' => 'FarmerController@saveImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 33, 'method' => 'GET', 'url' => 'farmer', 'path' => 'FarmerController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 33, 'method' => 'GET', 'url' => 'farmer/id/{id}', 'path' => 'FarmerController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 33, 'method' => 'GET', 'url' => 'farmer/cust_id/{id}', 'path' => 'FarmerController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 34, 'method' => 'PUT', 'url' => 'farmer/{id}', 'path' => 'FarmerController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 34, 'method' => 'POST', 'url' => 'farmer/update/bulk', 'path' => 'FarmerController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 35, 'method' => 'DELETE', 'url' => 'farmer/{id}', 'path' => 'FarmerController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * Retail Assignment
             */
            array('permission_id' => 36, 'method' => 'POST', 'url' => '/retail_assg/assg/bulk', 'path' => 'RetailAssgController@assgBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 37, 'method' => 'GET', 'url' => '/retail_assg', 'path' => 'RetailAssgController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 38, 'method' => 'POST', 'url' => '/retail_assg/update/bulk', 'path' => 'RetailAssgController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 37, 'method' => 'GET', 'url' => 'retail/produsen', 'path' => 'OrgDistRetailController@produsen', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 37, 'method' => 'GET', 'url' => 'retail/distributor', 'path' => 'OrgDistRetailController@distributor', 'middleware' => 'jwt.authz,localization'),
            /*
             * gudang
             */
            array('permission_id' => 39, 'method' => 'GET', 'url' => 'gudang', 'path' => 'GudangController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 39, 'method' => 'GET', 'url' => 'gudang/id/{id}', 'path' => 'GudangController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 39, 'method' => 'GET', 'url' => 'gudang/shipto', 'path' => 'GudangController@shipToParty', 'middleware' => 'jwt.authz,localization'),
            /*
             * Factory
             */
            array('permission_id' => 40, 'method' => 'GET', 'url' => 'company/distributor', 'path' => 'CompanyController@Distributor', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 40, 'method' => 'GET', 'url' => 'company/produsen', 'path' => 'CompanyController@Produsen', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 40, 'method' => 'GET', 'url' => 'company/pengecer', 'path' => 'CompanyController@Retail', 'middleware' => 'jwt.authz,localization'),
            /*
             * sub sector & commodity
             */
            array('permission_id' => 41, 'method' => 'GET', 'url' => 'subsector', 'path' => 'SubsectorController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 42, 'method' => 'GET', 'url' => 'commodity', 'path' => 'CommodityController@index', 'middleware' => 'jwt.authz,localization'),
            /*
             * supply point
             */
            array('permission_id' => 43, 'method' => 'POST', 'url' => 'supplypoint', 'path' => 'SupplyPointController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 43, 'method' => 'POST', 'url' => 'supplypoint/upload', 'path' => 'SupplyPointController@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 44, 'method' => 'GET', 'url' => 'supplypoint', 'path' => 'SupplyPointController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 44, 'method' => 'GET', 'url' => 'supplypoint/id/{id}', 'path' => 'SupplyPointController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 45, 'method' => 'PUT', 'url' => 'supplypoint/{id}', 'path' => 'SupplyPointController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 45, 'method' => 'POST', 'url' => 'supplypoint/update/bulk', 'path' => 'SupplyPointController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 46, 'method' => 'DELETE', 'url' => 'supplypoint/{id}', 'path' => 'SupplyPointController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * plant
             */
            array('permission_id' => 47, 'method' => 'GET', 'url' => 'plant', 'path' => 'PlantController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 47, 'method' => 'GET', 'url' => 'plant/distinct', 'path' => 'PlantController@allDistinct', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 47, 'method' => 'GET', 'url' => 'plant/supply', 'path' => 'PlantController@supply', 'middleware' => 'jwt.authz,localization'),
            /*
             * pricing
             */
            array('permission_id' => 48, 'method' => 'POST', 'url' => 'pricing', 'path' => 'PricingController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 48, 'method' => 'POST', 'url' => 'pricing/upload', 'path' => 'PricingController@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 49, 'method' => 'GET', 'url' => 'pricing', 'path' => 'PricingController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 49, 'method' => 'GET', 'url' => 'pricing/id/{id}', 'path' => 'PricingController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 50, 'method' => 'PUT', 'url' => 'pricing/{id}', 'path' => 'PricingController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 50, 'method' => 'POST', 'url' => 'pricing/update/bulk', 'path' => 'PricingController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 51, 'method' => 'DELETE', 'url' => 'pricing/{id}', 'path' => 'PricingController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /**
             * Subtitution
             */
            array('permission_id' => 52, 'method' => 'POST', 'url' => 'subtitution/bulk', 'path' => 'SubtitutionController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 52, 'method' => 'POST', 'url' => 'subtitution', 'path' => 'SubtitutionController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 53, 'method' => 'GET', 'url' => 'subtitution', 'path' => 'SubtitutionController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 53, 'method' => 'GET', 'url' => 'subtitution/cust_id/{id}', 'path' => 'SubtitutionController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 53, 'method' => 'GET', 'url' => 'subtitution/id/{uuid}', 'path' => 'SubtitutionController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 54, 'method' => 'PUT', 'url' => 'subtitution/{uuid}', 'path' => 'SubtitutionController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * product
             */
            array('permission_id' => 55, 'method' => 'GET', 'url' => 'product', 'path' => 'ProductController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 55, 'method' => 'GET', 'url' => 'product/id/{id}', 'path' => 'ProductController@show', 'middleware' => 'jwt.authz,localization'),
            /*
             * delivery method
             */
            array('permission_id' => 56, 'method' => 'GET', 'url' => 'deliverymethod', 'path' => 'DeliveryMethodController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 56, 'method' => 'GET', 'url' => 'deliverymethod/id/{id}', 'path' => 'DeliveryMethodController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 56, 'method' => 'GET', 'url' => 'deliverymethod/kabupaten/{id}', 'path' => 'DeliveryMethodController@bySalesGroup', 'middleware' => 'jwt.authz,localization'),
            /*
             * sales division
             */
            array('permission_id' => 57, 'method' => 'GET', 'url' => 'salesdivision', 'path' => 'SalesDivisionController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 57, 'method' => 'GET', 'url' => 'salesdivision/id/{id}', 'path' => 'SalesDivisionController@show', 'middleware' => 'jwt.authz,localization'),
            /*
             * distribution channel
             */
            array('permission_id' => 58, 'method' => 'GET', 'url' => 'distribchannel', 'path' => 'DistribChannelController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 58, 'method' => 'GET', 'url' => 'distribchannel/id/{id}', 'path' => 'DistribChannelController@show', 'middleware' => 'jwt.authz,localization'),

            /**
             * Material List
             */
            array('permission_id' => 59, 'method' => 'GET', 'url' => 'material', 'path' => 'MaterialListController@MaterialList', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 59, 'method' => 'GET', 'url' => 'material/product', 'path' => 'MaterialListController@MaterialListProduct', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 59, 'method' => 'GET', 'url' => 'material/list', 'path' => 'MaterialListController@showList', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 59, 'method' => 'GET', 'url' => 'material/product/list', 'path' => 'MaterialListController@showProduct', 'middleware' => 'jwt.authz,localization'),

            /**
             * Bank DF
             */
            array('permission_id' => 61, 'method' => 'GET', 'url' => 'bankdf/cust_id/{id}', 'path' => 'BankDfController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 61, 'method' => 'GET', 'url' => 'bankdf/id/{id}', 'path' => 'BankDfController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 60, 'method' => 'POST', 'url' => 'bankdf', 'path' => 'BankDfController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 62, 'method' => 'PUT', 'url' => 'bankdf/{uuid}', 'path' => 'BankDfController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /**
             * Bank
             */
            array('permission_id' => 63, 'method' => 'GET', 'url' => 'bank', 'path' => 'BankController@index', 'middleware' => 'jwt.authz,localization'),
            /**
             * PERMENTAN
             */
            array('permission_id' => 64, 'method' => 'POST', 'url' => 'permentan', 'path' => 'PermentanItemController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 64, 'method' => 'POST', 'url' => 'permentan/upload', 'path' => 'PermentanController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 65, 'method' => 'GET', 'url' => 'permentan', 'path' => 'PermentanController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 65, 'method' => 'GET', 'url' => 'permentan/{id}', 'path' => 'PermentanController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 65, 'method' => 'GET', 'url' => 'permentan/item/{id}', 'path' => 'PermentanItemController@single', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 65, 'method' => 'GET', 'url' => 'permentan/item/list/{id}', 'path' => 'PermentanItemController@list', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 65, 'method' => 'GET', 'url' => 'permentan/detail/{id}', 'path' => 'PermentanItemController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 66, 'method' => 'PUT', 'url' => 'permentan/item/{id}', 'path' => 'PermentanItemController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 67, 'method' => 'DELETE', 'url' => 'permentan/{id}', 'path' => 'PermentanController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 64, 'method' => 'POST', 'url' => 'permentan/update/bulk', 'path' => 'PermentanItemController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Add Export

            array('permission_id' => 64, 'method' => 'GET', 'url' => 'permentan/export', 'path' => 'PermentanController@export', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 64, 'method' => 'GET', 'url' => 'permentan/download', 'path' => 'PermentanController@download', 'middleware' => 'jwt.authz,localization'),
            /**
             * Goverment GUbernurs
             */
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub', 'path' => 'PergubController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub/id/{id}', 'path' => 'PergubController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub/item/id/{id}', 'path' => 'PergubController@showitem', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub/detail/id/{id}', 'path' => 'PergubController@detailpergubuuid', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 68, 'method' => 'POST', 'url' => 'pergub/upload', 'path' => 'PergubController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 68, 'method' => 'POST', 'url' => 'pergub', 'path' => 'PergubController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 70, 'method' => 'PUT', 'url' => 'pergub/{id}', 'path' => 'PergubController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 70, 'method' => 'PUT', 'url' => 'pergub/update/bulk', 'path' => 'PergubController@updatesbulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 71, 'method' => 'DELETE', 'url' => 'pergub/{id}', 'path' => 'PergubController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Export
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub/export', 'path' => 'PergubController@export', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 69, 'method' => 'GET', 'url' => 'pergub/download', 'path' => 'PergubController@download', 'middleware' => 'jwt.authz,localization'),
            /*
             * perbup
             */
            array('permission_id' => 72, 'method' => 'POST', 'url' => 'perbup/upload', 'path' => 'PerbupController@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 72, 'method' => 'POST', 'url' => 'perbup', 'path' => 'PerbupItemController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup', 'path' => 'PerbupController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup/detail/{id}', 'path' => 'PerbupItemController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup/item/list/{id}', 'path' => 'PerbupItemController@listItem', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup/item/{id}', 'path' => 'PerbupItemController@detail', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 74, 'method' => 'PUT', 'url' => 'perbup/item/{id}', 'path' => 'PerbupItemController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 74, 'method' => 'POST', 'url' => 'perbup/update/bulk', 'path' => 'PerbupItemController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 75, 'method' => 'DELETE', 'url' => 'perbup/{id}', 'path' => 'PerbupController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Add Export
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup/export', 'path' => 'PerbupController@export', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 73, 'method' => 'GET', 'url' => 'perbup/download', 'path' => 'PerbupController@download', 'middleware' => 'jwt.authz,localization'),

            /*
             * SPJB
             */
            array('permission_id' => 76, 'method' => 'POST', 'url' => 'spjb/upload', 'path' => 'SpjbController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb', 'path' => 'SpjbController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb/detail/{id}', 'path' => 'SpjbController@hirarki', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb/item/detail/{id}', 'path' => 'SpjbController@detailitem', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb/item/{id}', 'path' => 'SpjbController@masteritems', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 76, 'method' => 'POST', 'url' => 'spjb/item', 'path' => 'SpjbController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 78, 'method' => 'PUT', 'url' => 'spjb/item/{id}', 'path' => 'SpjbController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 76, 'method' => 'POST', 'url' => 'spjb/item/update/bulk', 'path' => 'SpjbController@updates', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb/group', 'path' => 'SpjbController@getGroupByUUIDSPJB', 'middleware' => 'jwt.authz,localization'),
            //Export
            array('permission_id' => 77, 'method' => 'GET', 'url' => 'spjb/export', 'path' => 'SpjbController@export', 'middleware' => 'jwt.authz,localization'),
            /**
             * RDKK
             */
            array('permission_id' => 80, 'method' => 'POST', 'url' => 'rdkk', 'path' => 'RdkkController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 80, 'method' => 'POST', 'url' => 'rdkk/upload', 'path' => 'RdkkController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 81, 'method' => 'GET', 'url' => 'rdkk', 'path' => 'RdkkController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 81, 'method' => 'GET', 'url' => 'rdkk/{id}', 'path' => 'RdkkController@detail', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 81, 'method' => 'GET', 'url' => 'rdkk/product/list', 'path' => 'RdkkController@product', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 81, 'method' => 'GET', 'url' => 'rdkk/planting/list', 'path' => 'RdkkController@planting', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 82, 'method' => 'PUT', 'url' => 'rdkk/{id}', 'path' => 'RdkkController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 83, 'method' => 'DELETE', 'url' => 'rdkk/{id}', 'path' => 'RdkkController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 80, 'method' => 'POST', 'url' => 'rdkk/update/bulk', 'path' => 'RdkkController@updateBulk', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * SPJB Operational
             */
            array('permission_id' => 84, 'method' => 'POST', 'url' => 'spjbopr/upload', 'path' => 'SpjbOperationalController@getImport', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr', 'path' => 'SpjbOperationalController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr/{id}', 'path' => 'SpjbOperationalController@hirarki', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr/item/{id}', 'path' => 'SpjbOperationalController@dataheader', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr/item/detail/{id}', 'path' => 'SpjbOperationalController@detailitem', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 86, 'method' => 'PUT', 'url' => 'spjbopr/item/update/{id}', 'path' => 'SpjbOperationalController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 86, 'method' => 'POST', 'url' => 'spjbopr/item/update/bulk', 'path' => 'SpjbOperationalController@updates', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr/download/{id}', 'path' => 'SpjbOperationalController@exports', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 85, 'method' => 'GET', 'url' => 'spjbopr/download', 'path' => 'SpjbOperationalController@exports', 'middleware' => 'jwt.authz,localization'),
            /*
             * order
             */
            array('permission_id' => 88, 'method' => 'POST', 'url' => 'order', 'path' => 'OrderController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 88, 'method' => 'POST', 'url' => 'order/item', 'path' => 'OrderItemController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 89, 'method' => 'GET', 'url' => 'order', 'path' => 'OrderController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 89, 'method' => 'GET', 'url' => 'order/id/{id}', 'path' => 'OrderController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 89, 'method' => 'GET', 'url' => 'order/item/spjb', 'path' => 'OrderItemController@getSPJBItemPenebusan', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 90, 'method' => 'PUT', 'url' => 'order/{id}', 'path' => 'OrderController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 90, 'method' => 'PUT', 'url' => 'order/status/{id}', 'path' => 'OrderController@status', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
//            array('permission_id' => 91, 'method' => 'DELETE', 'url' => 'order/{id}', 'path' => 'OrderController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * monitoring order
             */
            array('permission_id' => 92, 'method' => 'GET', 'url' => 'monitoringorder', 'path' => 'MonitoringOrderController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 92, 'method' => 'GET', 'url' => 'monitoringorder/downloadbooking/{id}', 'path' => 'MonitoringOrderController@booking', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 92, 'method' => 'GET', 'url' => 'monitoringorder/downloadso/{id}', 'path' => 'MonitoringOrderController@so_number', 'middleware' => 'jwt.authz,localization'),
            /*
             * payment method
             */
            array('permission_id' => 93, 'method' => 'GET', 'url' => 'paymentmethod', 'path' => 'PaymentMethodController@index', 'middleware' => 'jwt.authz,localization'),

            /*
             * Penyaluran DO
             */
            array('permission_id' => 95, 'method' => 'POST', 'url' => 'supplydo/detail', 'path' => 'DetailPenyaluranDOController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 96, 'method' => 'GET', 'url' => 'supplydo/detail/{id}', 'path' => 'DetailPenyaluranDOController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 96, 'method' => 'GET', 'url' => 'supplydo', 'path' => 'PenyaluranDOController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 96, 'method' => 'GET', 'url' => 'supplydo/download/{id}', 'path' => 'PenyaluranDOController@bast', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 96, 'method' => 'GET', 'url' => 'supplydo/entry/{id}', 'path' => 'PenyaluranDOController@getAtrributesEntry', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 95, 'method' => 'POST', 'url' => 'supplydo', 'path' => 'PenyaluranDOController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 96, 'method' => 'GET', 'url' => 'supplydo/{id}', 'path' => 'PenyaluranDOController@ListDistripReport', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 98, 'method' => 'DELETE', 'url' => 'supplydo/{id}', 'path' => 'PenyaluranDOController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            /*
             * monitoring DO
             */
            array('permission_id' => 94, 'method' => 'GET', 'url' => 'monitoringdo', 'path' => 'MonitoringDOController@index', 'middleware' => 'jwt.authz,localization'),
            /*
             * monitoring DF
             */
            array('permission_id' => 100, 'method' => 'GET', 'url' => 'monitoringdf', 'path' => 'MonitoringDFController@index', 'middleware' => 'jwt.authz,localization'),
            /*
             * Distrib item / penyaluran Item
             */
            array('permission_id' => 102, 'method' => 'GET', 'url' => 'distribitem', 'path' => 'DistribItemController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 102, 'method' => 'GET', 'url' => 'distribitem/download', 'path' => 'DistribItemController@download', 'middleware' => 'jwt.authz,localization'),
            /*
             * REPORT F5
             */
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'reportf5/download/{id}', 'path' => 'DetailReportF5Controller@download', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'reportf5/bast/{id}', 'path' => 'ReportF5Controller@bast', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'reportf5', 'path' => 'ReportF5Controller@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'detailreportf5/{id}', 'path' => 'DetailReportF5Controller@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'detailreportf5/sonumber/{id}', 'path' => 'DetailReportF5Controller@findso', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 104, 'method' => 'GET', 'url' => 'detailreportf5/pkpnumber/{id}', 'path' => 'DetailReportF5Controller@findpkp', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 103, 'method' => 'POST', 'url' => 'reportf5', 'path' => 'ReportF5Controller@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 103, 'method' => 'POST', 'url' => 'detailreportf5', 'path' => 'DetailReportF5Controller@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * manual approval
             */
            array('permission_id' => 107, 'method' => 'GET', 'url' => 'manualapproval/setting', 'path' => 'ManualApprovalController@setting', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 107, 'method' => 'GET', 'url' => 'manualapproval', 'path' => 'ManualApprovalController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 108, 'method' => 'POST', 'url' => 'manualapproval/setting/status', 'path' => 'ManualApprovalController@updateStatusSetting', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 108, 'method' => 'POST', 'url' => 'manualapproval/status', 'path' => 'ManualApprovalController@updateStatus', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * initial stock f5
             */
            array('permission_id' => 109, 'method' => 'POST', 'url' => 'initialstockf5/upload', 'path' => 'InitialStockF5Controller@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 110, 'method' => 'GET', 'url' => 'initialstockf5', 'path' => 'InitialStockF5Controller@index', 'middleware' => 'jwt.authz,localization'),
            /*
             * penyaluran initial stock f5
             */
            array('permission_id' => 111, 'method' => 'POST', 'url' => 'penyaluraninitialstockf5', 'path' => 'PenyaluranInitialStockF5Controller@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 111, 'method' => 'POST', 'url' => 'penyaluraninitialstockf5/item', 'path' => 'PenyaluranInitialStockF5Controller@storeItem', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 112, 'method' => 'GET', 'url' => 'penyaluraninitialstockf5/id/{id}', 'path' => 'PenyaluranInitialStockF5Controller@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 112, 'method' => 'GET', 'url' => 'penyaluraninitialstockf5/item/{id}', 'path' => 'PenyaluranInitialStockF5Controller@getItemPenyaluran', 'middleware' => 'jwt.authz,localization'),
            /*
             * Report f6
             */
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6', 'path' => 'ReportF6Controller@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6/sonumber/{id}', 'path' => 'ReportF6ItemsController@findso', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6/detail/{id}', 'path' => 'ReportF6ItemsController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6/download/{id}', 'path' => 'ReportF6ItemsController@download', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6/rekap/{id}/{file_type}', 'path' => 'ReportF6ItemsController@rekap', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 114, 'method' => 'GET', 'url' => 'reportf6/bast/{id}', 'path' => 'ReportF6ItemsController@bast', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 113, 'method' => 'POST', 'url' => 'reportf6', 'path' => 'ReportF6ItemsController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /* Initial Stock F6 */
            array('permission_id' => 117, 'method' => 'POST', 'url' => 'initialstockf6', 'path' => 'InitialStockF6Controller@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 117, 'method' => 'POST', 'url' => 'initialstockf6/create', 'path' => 'InitialStockF6Controller@create', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 118, 'method' => 'GET', 'url' => 'initialstockf6', 'path' => 'InitialStockF6Controller@index', 'middleware' => 'jwt.authz,localization'),

            /*Penyaluran Initial Stock F6 */
            array('permission_id' => 121, 'method' => 'POST', 'url' => 'penyaluraninitialstockf6', 'path' => 'PenyaluranInitialStockF6Controller@create', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 122, 'method' => 'GET', 'url' => 'penyaluraninitialstockf6/id/{uuid}', 'path' => 'PenyaluranInitialStockF6Controller@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 122, 'method' => 'GET', 'url' => 'penyaluraninitialstockf6/item/{uuid}', 'path' => 'PenyaluranInitialStockF6Controller@getItemPenyaluran', 'middleware' => "jwt.authz,localization"),
            array('permission_id' => 121, 'method' => 'POST', 'url' => 'penyaluraninitialstockf6/item', 'path' => 'PenyaluranInitialStockF6Controller@createItem', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 124, 'method' => 'DELETE', 'url' => 'penyaluraninitialstockf6/{id}/', 'path' => 'PenyaluranInitialStockF6Controller@delete', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 123, 'method' => 'PUT', 'url' => 'penyaluraninitialstockf6/{id}/', 'path' => 'PenyaluranInitialStockF6Controller@update', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            // Syncronize
            array('permission_id' => 122, 'method' => 'GET', 'url' => 'syncronize/{type}', 'path' => 'SyncronizeController@index', 'middleware' => "jwt.authz,localization"),
            array('permission_id' => 147, 'method' => 'POST', 'url' => 'syncronizes/plant/updatesbulk', 'path' => 'SyncronizeController@sync_updatePlant', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 126, 'method' => 'GET', 'url' => 'bankaccounts', 'path' => 'BankAccountController@index', 'middleware' => "jwt.authz,localization"),
            array('permission_id' => 126, 'method' => 'GET', 'url' => 'bankaccounts/{id}', 'path' => 'BankAccountController@show', 'middleware' => "jwt.authz,localization"),
            array('permission_id' => 127, 'method' => 'POST', 'url' => 'bankaccounts', 'path' => 'BankAccountController@store', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 128, 'method' => 'PUT', 'url' => 'bankaccounts/{id}', 'path' => 'BankAccountController@update', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 129, 'method' => 'DELETE', 'url' => 'bankaccounts', 'path' => 'BankAccountController@destroy', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 128, 'method' => 'PUT', 'url' => 'bankaccounts', 'path' => 'BankAccountController@updateBatch', 'middleware' => "jwt.authz,localization,jwt.refresh"),
            array('permission_id' => 6, 'method' => 'GET', 'url' => 'user/profile', 'path' => 'UserController@profile', 'middleware' => 'jwt.authz,localization'),
            /*
             * Manual SO
             */
            array('permission_id' => 130, 'method' => 'POST', 'url' => 'manualso', 'path' => 'ManualSOController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            /*
             * log order
             */
            array('permission_id' => 131, 'method' => 'GET', 'url' => 'logorder', 'path' => 'LogOrderController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 88, 'method' => 'PUT', 'url' => 'logorder/{id}', 'path' => 'LogOrderController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Create Monitoring DO
            array('permission_id' => 132, 'method' => 'POST', 'url' => 'monitoringdo', 'path' => 'MonitoringDOController@store', 'middleware' => 'localization'),
            array('permission_id' => 132, 'method' => 'POST', 'url' => 'monitoringdo/reverse', 'path' => 'MonitoringDOController@reverse', 'middleware' => 'localization'),

            /*
             * Limit Order Due Date
             */

            array('permission_id' => 133, 'method' => 'POST', 'url' => 'limit', 'path' => 'LimitDateController@store', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 135, 'method' => 'PUT', 'url' => 'limit/{id}', 'path' => 'LimitDateController@update', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array('permission_id' => 134, 'method' => 'GET', 'url' => 'limit', 'path' => 'LimitDateController@index', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 134, 'method' => 'GET', 'url' => 'limit/{id}', 'path' => 'LimitDateController@show', 'middleware' => 'jwt.authz,localization'),
            array('permission_id' => 136, 'method' => 'DELETE', 'url' => 'limit/{id}', 'path' => 'LimitDateController@destroy', 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            /* H2h */
            array("method" => "POST", "permission_id" => 137, "url" => "payment_gateway/{bank_name}/payment", "path" => "PaymentGatewayController@payment", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 137, "url" => "payment_gateway/{bank_name}/inquiry", "path" => "PaymentGatewayController@inquiry", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 137, "url" => "payment_gateway/{bank_name}/check-limit", "path" => "PaymentGatewayController@checkLimit", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 137, "url" => "payment_gateway/{bank_name}/disbursement", "path" => "PaymentGatewayController@disbursement", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 137, "url" => "payment_gateway/{bank_name}/invoice", "path" => "PaymentGatewayController@invoice", 'middleware' => 'jwt.authz,localization'),
            array("method" => "GET", "permission_id" => 137, "url" => "log-host-to-host", "path" => "LogHostToHostController@index", 'middleware' => 'jwt.authz,localization'),

            /* Periode Plating */
            /* H2h */
            array("method" => "GET", "permission_id" => 139, "url" => "plantingperiode", "path" => "PlantingperiodController@index", 'middleware' => 'jwt.authz,localization'),
            array("method" => "GET", "permission_id" => 139, "url" => "plantingperiode/{id}", "path" => "PlantingperiodController@show", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 138, "url" => "plantingperiode", "path" => "PlantingperiodController@store", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "POST", "permission_id" => 138, "url" => "plantingperiode/bulk", "path" => "PlantingperiodController@bulk", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "PUT", "permission_id" => 140, "url" => "plantingperiode/{id}", "path" => "PlantingperiodController@update", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "DELETE", "permission_id" => 141, "url" => "plantingperiode/{id}", "path" => "PlantingperiodController@destroy", 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            /*
             * Report SO
             */

            array("method" => "POST", "permission_id" => 142, "url" => "reportso/header", "path" => "ReportSOController@header", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "POST", "permission_id" => 142, "url" => "reportso/outstand", "path" => "ReportSOController@outstand", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "POST", "permission_id" => 142, "url" => "reportso/detail", "path" => "ReportSOController@detail", 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            // Sales Org Configuration
            array("method" => "GET", "permission_id" => 144, "url" => "salesorg", "path" => "SalesOrganisationController@index", 'middleware' => 'jwt.authz,localization'),
            array("method" => "PUT", "permission_id" => 145, "url" => "salesorg/{id}", "path" => "SalesOrganisationController@index", 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            // Village Master
            array("method" => "GET", "permission_id" => 148, "url" => "village", "path" => "VillageController@index", 'middleware' => 'jwt.authz,localization'),
            array("method" => "POST", "permission_id" => 147, "url" => "village", "path" => "VillageController@store", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "POST", "permission_id" => 147, "url" => "village/import", "path" => "VillageController@import", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "PUT", "permission_id" => 149, "url" => "village/{id}", "path" => "VillageController@update", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "DELETE", "permission_id" => 149, "url" => "village/{id}", "path" => "VillageController@destroy", 'middleware' => 'jwt.authz,localization,jwt.refresh'),

            array("method" => "GET", "permission_id" => 81, "url" => "rdkk/approval/list", "path" => "RdkkController@approvalList", 'middleware' => 'jwt.authz,localization'),
            array("method" => "PUT", "permission_id" => 80, "url" => "rdkk/approval/approved", "path" => "RdkkController@doApproved", 'middleware' => 'jwt.authz,localization,jwt.refresh'),


            array("method" => "PUT", "permission_id" => 78, "url" => "spjb/crqty/asal", "path" => "CRSPJBController@bulkqtyasal", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            array("method" => "PUT", "permission_id" => 86, "url" => "spjb/crqty/operational", "path" => "CRSPJBController@bulkqtyopr", 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Upload SO
            array('permission_id' => 88, 'method' => 'POST', 'url' => 'order/upload', 'path' => 'OrderController@upload', 'middleware' => 'jwt.authz,localization,jwt.refresh'),
            // Change plant
            array('permission_id' => 90, 'method' => 'PUT', 'url' => 'order/{uuid}/changeplant', 'path' => 'OrderController@changeso', 'middleware' => 'jwt.authz,localization,jwt.refresh'),

        );

        PermissionHasRoutes::insert($dtHasPermission);

        /*
         * Set Permission route -----------------------------------
         */

        $dtPermission = [
            '1' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 121, 122, 123, 124], // Developer
            '2' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 121, 122, 123, 124, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 147], // Admin
            '3' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31], // Admin Anper
            '4' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31], // Distributor
        ];

        foreach ($dtPermission as $i => $iv) {
            $role = Role::findOrFail($i);
            $role->givePermissionTo($iv);
        }

        /*
         * Set Permission menu -----------------------------------
         */
        $dtPermMenu = [];
        $roleIdList = [1, 2, 3, 4];
        $perm       = ['C', 'R', 'U', 'D'];

        for ($i = 1; $i <= 43; $i++) {
            foreach ($roleIdList as $jv) {
                foreach ($perm as $k => $kv) {
                    $dtPermMenu_   = [];
                    $dtPermMenu_[] = ['role_id' => $jv, 'menu_id' => $i, 'action_id' => $kv];

                    $dtPermMenu[] = $dtPermMenu_;
                }
            }
        }

        foreach ($dtPermMenu as $i => $iv) {
            RoleHasMenu::insert($iv);
        }

        $this->command->info("Permission berhasil diinsert");
    }

}
