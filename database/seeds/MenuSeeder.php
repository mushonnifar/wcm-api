<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dtMenu = array(
            //Penjualan ---------------- No. 1
            array(
                'name_ID' => 'Penjualan',
                'name_EN' => 'Sales',
                'order_no' => 1,
                'parent_id' => null,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'SPJB',
                'name_EN' => 'SPJB',
                'order_no' => 2,
                'parent_id' => 1,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'RDKK',
                'name_EN' => 'RDKK',
                'order_no' => 3,
                'parent_id' => 1,
                'url' => '#',
                'status' => 'n'
            ),
            //Order ---------------- No. 4
            array(
                'name_ID' => 'Order',
                'name_EN' => 'Order',
                'order_no' => 4,
                'parent_id' => null,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Monitoring Order',
                'name_EN' => 'Order Monitoring',
                'order_no' => 5,
                'parent_id' => 4,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Rencana Penebusan',
                'name_EN' => 'Redemption Plan',
                'order_no' => 6,
                'parent_id' => 4,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Monitoring DO',
                'name_EN' => 'DO Monitoring',
                'order_no' => 7,
                'parent_id' => 4,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Monitoring Order DF',
                'name_EN' => 'Order DF Monitoring',
                'order_no' => 8,
                'parent_id' => 4,
                'url' => '#',
                'status' => 'n'
            ),
            //Laporan ---------------- No. 9
            array(
                'name_ID' => 'Laporan',
                'name_EN' => 'Report',
                'order_no' => 9,
                'parent_id' => null,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Penyaluran DO',
                'name_EN' => 'Retailer Distribution',
                'order_no' => 10,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Item Penyaluran',
                'name_EN' => 'Distrib Report Item',
                'order_no' => 11,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Laporan Bulanan Distributor',
                'name_EN' => 'Distributor Monthly Report',
                'order_no' => 12,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Rekapitulasi Laporan Bulan Pengecer',
                'name_EN' => "Recapitulation of Retailer's Month Report",
                'order_no' => 13,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Initial Stocks F5',
                'name_EN' => 'Initial Stocks F5',
                'order_no' => 14,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Initial Stocks F6',
                'name_EN' => 'Initial Stocks F6',
                'order_no' => 15,
                'parent_id' => 9,
                'url' => '#',
                'status' => 'n'
            ),
            //Laporan ---------------- No. 16
            array(
                'name_ID' => 'Data Master',
                'name_EN' => 'Master Data',
                'order_no' => 16,
                'parent_id' => null,
                'url' => '#',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Distributor',
                'name_EN' => 'Distributors',
                'order_no' => 17,
                'parent_id' => 16,
                'url' => 'master_distributor',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Sales Area',
                'name_EN' => 'Sales Area',
                'order_no' => 18,
                'parent_id' => 16,
                'url' => 'sales_area',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Gudang',
                'name_EN' => 'Warehouses',
                'order_no' => 19,
                'parent_id' => 16,
                'url' => 'master_warehouse',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Pengecer',
                'name_EN' => 'Retailer Assgs',
                'order_no' => 20,
                'parent_id' => 16,
                'url' => 'master_retail',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Relasi Distributor Pengecer',
                'name_EN' => 'Retailer Assgs',
                'order_no' => 21,
                'parent_id' => 16,
                'url' => 'retail_distributor_relation',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Kelompok Tani',
                'name_EN' => 'Farmer Groups',
                'order_no' => 22,
                'parent_id' => 16,
                'url' => 'farmergroup_master',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Petani',
                'name_EN' => 'Farmer Group',
                'order_no' => 23,
                'parent_id' => 16,
                'url' => 'master_petani',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Subtitusi',
                'name_EN' => 'Subtitution',
                'order_no' => 24,
                'parent_id' => 16,
                'url' => 'master_subtitution',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Pricing',
                'name_EN' => 'Priving Master',
                'order_no' => 31,
                'parent_id' => 16,
                'url' => 'master_pricing',
                'status' => 'y'
            ),
            //Config ---------------- No. 25
            array(
                'name_ID' => 'Pengaturan Web',
                'name_EN' => 'Web Setting',
                'order_no' => 24,
                'parent_id' => null,
                'url' => '#',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Menu',
                'name_EN' => 'Master Menu',
                'order_no' => 25,
                'parent_id' => 26,
                'url' => 'master_menu',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Hak Akses',
                'name_EN' => 'Master Permission',
                'order_no' => 26,
                'parent_id' => 26,
                'url' => 'master_permission',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Master Rule',
                'name_EN' => 'Master Roles',
                'order_no' => 27,
                'parent_id' => 26,
                'url' => 'master_roles',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Route',
                'name_EN' => 'Master Routes',
                'order_no' => 28,
                'parent_id' => 26,
                'url' => 'master_routes',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Pengguna',
                'name_EN' => 'Master User',
                'order_no' => 29,
                'parent_id' => 24,
                'url' => 'master_user_all',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Perizinan Route',
                'name_EN' => 'Prvilage Routes',
                'order_no' => 30,
                'parent_id' => 26,
                'url' => 'master_routes',
                'status' => 'n'
            ),
            array(
                'name_ID' => 'Perizinan Menu',
                'name_EN' => 'Prvilage Menu',
                'order_no' => 31,
                'parent_id' => 26,
                'url' => 'privilege_menu',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Pricing',
                'name_EN' => 'Priving Master',
                'order_no' => 31,
                'parent_id' => 24,
                'url' => 'master_pricing',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Master Subtitution',
                'name_EN' => 'Subtitution Master',
                'order_no' => 32,
                'parent_id' => 24,
                'url' => 'master_pricing',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Alokasi',
                'name_EN' => 'Alocation',
                'order_no' => 33,
                'parent_id' => 16,
                'url' => '#',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Data Permentan',
                'name_EN' => 'Data Permentan',
                'order_no' => 34,
                'parent_id' => 36,
                'url' => 'permentan',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Data Pergub',
                'name_EN' => 'Data Pergub',
                'order_no' => 35,
                'parent_id' => 36,
                'url' => 'pergub',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Data Perbup',
                'name_EN' => 'Data Perbup',
                'order_no' => 36,
                'parent_id' => 36,
                'url' => 'perbup',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'Supply Point',
                'name_EN' => 'Supply Point',
                'order_no' => 37,
                'parent_id' => null,
                'url' => 'supply_point',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'SPJB Asal',
                'name_EN' => 'SPJB Origin',
                'order_no' => 38,
                'parent_id' => 36,
                'url' => 'spjb_asal',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'SPJB Operasional',
                'name_EN' => 'SPJB Operational',
                'order_no' => 39,
                'parent_id' => 36,
                'url' => 'spjb_operasional',
                'status' => 'y'
            ),
            array(
                'name_ID' => 'RDKK',
                'name_EN' => 'RDKK',
                'order_no' => 40,
                'parent_id' => 36,
                'url' => 'rdkk',
                'status' => 'y'
            ),
        );
        Menu::insert($dtMenu);

        $this->command->info("Menu berhasil diinsert");
    }

}
