<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserDummySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create('id_ID');

        for ($i = 0; $i < 1000; $i++) {
            $user = User::create([
                "username" => $faker->username,
//                "id" => $faker->uuid,
                "name" => $faker->name,
                "email" => $faker->email,
                "password" => \Hash::make("123456"),
            ]);
            $user->syncRoles(1);
        }

        $this->command->info("1000 User berhasil diinsert");
    }

}
