<?php

use Illuminate\Database\Seeder;
use App\Models\ReportF6;

class ReportF6Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas = [
            [
                'number' => 'F60000000001',
                'report_f5_id'=>1,
                'sales_org_id' => 'B000',
                'customer_id' => '1000000000',
                'sales_group_id' => '1114',
                'month' => '01',
                'year' => '2019',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ],
            [
                'number' => 'F60000000001',
                'report_f5_id'=>1,
                'sales_org_id' => 'B000',
                'customer_id' => '1000000000',
                'sales_group_id' => '1114',
                'month' => '02',
                'year' => '2019',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ],
        ];
        ReportF6::insert($datas);
        $this->command->info('Sukses Insert ReportF6TableSeeder');
    }
}
