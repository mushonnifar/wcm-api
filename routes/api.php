<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/', function () {
    return 'WCM API';
});

Route::post('/login', 'ApiController@login')->middleware('localization');
Route::post('/forgetpassword', 'ForgetPasswordController@index')->middleware('localization');
Route::get('/forgetpassword/{id}', 'ForgetPasswordController@dateactive')->middleware('localization');
Route::post('/forgetpassword/{id}', 'ForgetPasswordController@store')->middleware('localization');
Route::post('/logout', 'ApiController@logout')->middleware(['jwt.authz', 'localization']);
Route::get('/user/fetch', 'ApiController@detail')->middleware(['jwt.authz', 'localization']);

Route::get('/today', 'OrderController@hariToday')->middleware(['localization']);

/*
 * Job Create F5
 */
Route::get('/f5/store', 'F5\JobCreateF5Controller@create')->middleware('localization');

/*
 * scheduler check payment due date
 */
Route::get('/job/payment-due-date', 'Scheduler\PaymentDueDateController@store')->middleware('localization');

Route::prefix('sap')->group(function () {
    Route::get('/sales-office', 'SAP\SalesOfficeController@store')->middleware('localization');
    Route::get('/sales-group', 'SAP\SalesGroupController@store')->middleware('localization');
    Route::get('/sales-unit', 'SAP\SalesUnitController@store')->middleware('localization');
    Route::get('/sales-division', 'SAP\SalesDivisionController@store')->middleware('localization');
    Route::get('/plant-master', 'SAP\PlantController@store')->middleware('localization');
    Route::get('/plant-assign', 'SAP\PlantController@storeAssign')->middleware('localization');
    Route::get('/distrib-channel', 'SAP\DistribChannelController@store')->middleware('localization');
    Route::get('/sales-area-master', 'SAP\SalesAreaController@store')->middleware('localization');
    Route::get('/sales-area-assign', 'SAP\SalesAreaAssigController@store')->middleware('localization');
    Route::get('/customer-master', 'SAP\CustomerController@store')->middleware('localization');
    Route::get('/customer-address', 'SAP\CustomerController@storeAddress')->middleware('localization');
    Route::get('/customer-contact', 'SAP\CustomerController@storeContact')->middleware('localization');
    Route::get('/customer-payment', 'SAP\CustomerController@storePayment')->middleware('localization');
    Route::get('/customer-salesorg', 'SAP\CustomerController@storeCustSalesOrgAssg')->middleware('localization');
    Route::get('/customer-partner-function', 'SAP\CustomerController@storePartnerFunction')->middleware('localization');
    Route::get('/customer-sales-area', 'SAP\CustomerController@storeSalesArea')->middleware('localization');
    Route::get('/material', 'SAP\MaterialListController@store')->middleware('localization');
    Route::get('/sales-order/{id}', 'SAP\SalesOrderController@store')->middleware('localization');
    Route::get("/check-do-from-so", 'SAP\CheckDoFromSoController@verified');
    Route::get("/check-status-so", 'SAP\CheckCompleteSOController@index');
    Route::get("/sap-to-wcm", "SAP\SAPToWcmTestingController@index");
    Route::post("/sap-to-wcm/add", "SAP\SAPToWcmTestingController@store");
});

Route::post("check-plafon/{bankName}", "PaymentGatewayController@reverse")
    ->middleware(["jwt.authz"]);

Route::post("invoice/{bankName}", "PaymentGatewayController@reverseInvoice")
    ->middleware(["jwt.authz"]);



try {
    $permissions = Permission::with('hasRoutes')->get()->toArray();
    foreach ($permissions as $permission) {
        $perm = 'permissionz:' . $permission['name'];

        foreach ($permission['has_routes'] as $has_permission) {
            $dtMiddlewares = [];

            $dtUrl    = $has_permission['url'];
            $dtMethod = $has_permission['method'];
            $dtPath   = $has_permission['path'];
            if ($has_permission['middleware']) {
                $middlewares = explode(',', $has_permission['middleware']);
                foreach ($middlewares as $i => $iv) {
                    $dtMiddlewares[] = $iv;
                }
                $dtMiddlewares[] = $perm;
            }
            if (strpos($dtMethod,"OPTION") !== false) {
                Route::options($dtUrl, $dtPath);
            } else {
                Route::{strtolower($dtMethod)}($dtUrl, $dtPath)->middleware($dtMiddlewares);
            }

        }
    }
} catch (PDOException $e) {

}