<html>
<head>
    <title>Sales Order</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;            
        }
        .garis{
            border-collapse: collapse;            
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;            
        }
        .table-strip table{
            border-collapse: collapse;            
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{            
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
        #footer { position: fixed; left: 0px; right: 0px; bottom: 5px; font-size: 8pt;}
    </style>
</head>
<body>
    <footer style="width: 100%;" id="footer">
        print by : {{$user}} || printed at : {{$date}}
    </footer>
    <div style="position: absolute;"><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(180)->errorCorrection('H')->generate($url)) !!}" alt="BST" /> </div>
    <br><br><br><br><br>
    <p style="text-align:center;font-size:24px;text-transform:underline;font-weight:bold;"><u>SURAT JALAN</u></p>     
    <p style="color:grey;text-align:center;;font-size: 18px;">No. <?=$header->number?></p>
    <p style="color:grey;text-align:center">Tanggal <?= strtoupper($header->created_at)?></p>

    <div style="margin-top:10px">
        <table>
            <tr><td style="">Nama Distributor</td><td>: <b>{{$header->customer_name}}</b> </td></tr>
            <tr><td style="">Kota</td><td>: <b>{{$header->sales_group_name}}</b></td></tr>
        </table>
        <br>
        <p style=""> &nbsp; &nbsp; &nbsp; Bersama ini kami mengirimkan barang kepada pengecer sebagai berikut :</p> 
        <br>
    </div>
    <div style="margin-top:10px">
        <table class="garis" width="100%"  style="font-size: 12px;" >
            <tr>
                <td rowspan="2">No</td>
                <td rowspan="2">Sales Order</td>
                <td rowspan="2">No PKP</td>
                <td rowspan="2">Kecamatan</td>
                <td rowspan="2">Nama Pengecer</td>
                <td rowspan="2">Kode Pengecer</td>
                <td align="center">Urea</td>
                <td align="center">SP-36</td>
                <td align="center">ZA</td>
                <td align="center">NPK</td>
                <td align="center">NPK Kakao</td>
                <td align="center">Organik Granul</td>
                <td align="center">POC</td>
            </tr>
            <tr>
                <td colspan="6" align="center">TON</td>
                <td align="center">Liter</td>
            </tr>
            @foreach ($data as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item['so_number']}}</td>
                <td>{{$item['number']}}</td>
                <td>{{$item['sales_unit_name']}}</td>
                <td>{{$item['retail_name']}}</td>
                <td>{{$item['retail_code']}}</td>
                <td align="center">{{$item['P01'] !=null ? number_format((float)$item['P01'],3,',','.'):""}}</td>
                <td align="center">{{$item['P05'] !=null ? number_format((float)$item['P05'],3,',','.'):""}}</td>
                <td align="center">{{$item['P04'] !=null ? number_format((float)$item['P04'],3,',','.'):""}}</td>
                <td align="center">{{$item['P02'] !=null ? number_format((float)$item['P02'],3,',','.'):""}}</td>
                <td align="center">{{$item['P54'] !=null ? number_format((float)$item['P54'],3,',','.'):""}}</td>
                <td align="center">{{$item['P03'] !=null ? number_format((float)$item['P03'],3,',','.'):""}}</td>
                <td align="center">{{$item['P51'] !=null ? number_format((float)$item['P51'],3,',','.'):""}}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <table width="100%">
        <div style="margin-top:10px">
            <br>
            <p> &nbsp; &nbsp; &nbsp; Demikian surat jalan ini kami buat dengan sebenar benarnya dan diberikan kepada pengecer yang bersangkutan untuk dipergunakan sebagai mana mastinya. :</p> 
            <br>
            <br>
            <table style="text-align:center;width:100%;">
                <tr style="font-weight:600;">
                    <td width='50%'></td>
                    <td width='50%'>{{$header->sales_group_name}}, <?= strtoupper($header->created_at)?></td>                
                </tr>
                <tr>
                    <td style=""></td>
                    <td style="">Distributor <br> <br> </td>                
                </tr>
                <tr>
                    <td style=""><p style="margin-top:100px;"></p></td>
                    <td style=""><p style="margin-top:100px;"><b>{{$header->customer_name}}</b></p></td>                
                </tr>            
            </table>
        </div>
    </table>
    
  
</body>
</html>