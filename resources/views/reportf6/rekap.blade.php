<style type="text/css">
    body {
        font-family:sans-serif;
    }
    table.bordered, table {
        margin: 3px;
        table-layout: fixed;
        font-size: 12px;
    }
    table.bordered thead {
        background-
    }
    table.bordered th {
        border: 1px solid black;
        padding: 10px
    }
    table.bordered td {
        border: 1px solid black;
        padding: 10px;
    }
    td.no-top-border {
        border-top: none !important;
        border-bottom: none !important;
        padding: 10px;
    }
    . {
        
    }
</style>

<body>
    <div style="position: relative; background-color: transparent;"><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(224)->errorCorrection('H')->generate($header['url'])) !!}" alt="BST" />
    </div>
    <table width="100%" style="margin-top: -100px;">
        <thead>
            <tr>
                <td width="75%">&nbsp;</td>
                <td width="25%" style="font-size: 14px">Kepada Yth.</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 14px">{{ $details['header']['sales_org_name'] }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 14px">Di Tempat</td>
            </tr>
            <tr>
                <th colspan="22" align="center" style="font-size: 16px">{{ $details['header']['number'] }}</th>
            </tr>
            <tr>
                <th colspan="22" align="center" style="font-size: 16px">REKAPITULASI LAPORAN BULANAN PENGECER</th>
            </tr>
        </thead>
    </table>
    <table border="0" align="left" width="100%">
        <tbody>
            <tr>
                <th align="left" width="8%">PROVINSI</th>
                <th align="left" width="80%">:&nbsp;{{$details['header']['sales_office_name'] }}</th>
                <th rowspan="3" style="border: 1px solid" align="center">REKAP LAMP VI</th>
            </tr>
            <tr>
                <th align="left">DISTRIBUTOR</th>
                <th align="left" width="80%">:&nbsp;{{ $details['header']['customer_name'] }}</th>
            </tr>
            <tr>
                <th align="left">PERIODE</th>
                <th align="left" width="80%">:&nbsp;{{ $header["periode"] }}</th>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td align="right"><i>( Dalam Satuan Ton )</i></td>
            </tr>
        </tbody>
    </table>

    <table width="100%" class="bordered" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr style="background-color: rgb(242,242,242);">
                <th rowspan="2" width="2%" align="center">NO</th>
                <th rowspan="2" width="18%" align="center">KECAMATAN/PENGECER</th>
                <th colspan="7" width="20%" align="center">PERSEDIAAN AWAL</th>
                <th colspan="7" width="20%" align="center">PENEBUSAN</th>
                <th colspan="7" width="20%" align="center">PENYALURAN</th>
                <th colspan="7" width="20%" align="center">PERSEDIAAN AKHIR</th>
            </tr>
            <tr style="background-color: rgb(242,242,242);font-weight: bold; border:1px solid #000000 !important;">
                <td  width="4%" align="center" >UREA</td>
                <td  width="4%" align="center" >NPK</td>
                <td  width="6%" align="center" >ORGANIK</td>
                <td  width="4%" align="center" >ZA</td>
                <td  width="4%" align="center" >SP-36</td>
                <td  width="4%" align="center" >POC</td>
                <td  width="4%" align="center" >KAKAO</td>
                <td  width="4%" align="center" >UREA</td>
                <td  width="4%" align="center" >NPK</td>
                <td  width="6%" align="center" >ORGANIK</td>
                <td  width="4%" align="center" >ZA</td>
                <td  width="4%" align="center" >SP-36</td>
                <td  width="4%" align="center" >POC</td>
                <td  width="4%" align="center" >KAKAO</td>
                <td  width="4%" align="center" >UREA</td>
                <td  width="4%" align="center" >NPK</td>
                <td  width="6%" align="center" >ORGANIK</td>
                <td  width="4%" align="center" >ZA</td>
                <td  width="4%" align="center" >SP-36</td>
                <td  width="4%" align="center" >POC</td>
                <td  width="4%" align="center" >KAKAO</td>
                <td  width="4%" align="center" >UREA</td>
                <td  width="4%" align="center" >NPK</td>
                <td  width="6%" align="center" >ORGANIK</td>
                <td  width="4%" align="center" >ZA</td>
                <td  width="4%" align="center" >SP-36</td>
                <td  width="4%" align="center" >POC</td>
                <td  width="4%" align="center"  style="border: 1px solid black;">KAKAO</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="30">{{ $details['header']['sales_group_name']}}</td>
            </tr>
            @foreach ( $details['data'] as $key => $kecamatan)
                <tr>
                    <td>{{ ($key+1) }}</td>
                    <td>{{ $kecamatan['name'] }}</td>
                    <td colspan="7"></td>
                    <td colspan="7"></td>
                    <td colspan="7"></td>
                    <td colspan="7"></td>
                </tr>
                @foreach($kecamatan['data'] as $keyretail => $retail)
                    <tr>
                        <td class="no-top-border"></td>
                        <td class="">{{ $retail['name'] }}</td>
                        <td align="center" class=""> @if($retail['data']["awal"][0]['qty'] > 0) {{  number_format( $retail['data']["awal"][0]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][1]['qty'] > 0) {{  number_format( $retail['data']["awal"][1]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][2]['qty'] > 0) {{  number_format( $retail['data']["awal"][2]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][3]['qty'] > 0) {{  number_format( $retail['data']["awal"][3]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][4]['qty'] > 0) {{  number_format( $retail['data']["awal"][4]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][5]['qty'] > 0) {{  number_format( $retail['data']["awal"][5]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["awal"][6]['qty'] > 0) {{  number_format( $retail['data']["awal"][6]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][0]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][0]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][1]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][1]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][2]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][2]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][3]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][3]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][4]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][4]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][5]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][5]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penebusan"][6]['qty'] > 0) {{  number_format( $retail['data']["penebusan"][6]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][0]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][0]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][1]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][1]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][2]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][2]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][3]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][3]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][4]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][4]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][5]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][5]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["penyaluran"][6]['qty'] > 0) {{  number_format( $retail['data']["penyaluran"][6]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][0]['qty'] > 0) {{  number_format( $retail['data']["akhir"][0]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][1]['qty'] > 0) {{  number_format( $retail['data']["akhir"][1]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][2]['qty'] > 0) {{  number_format( $retail['data']["akhir"][2]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][3]['qty'] > 0) {{  number_format( $retail['data']["akhir"][3]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][4]['qty'] > 0) {{  number_format( $retail['data']["akhir"][4]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][5]['qty'] > 0) {{  number_format( $retail['data']["akhir"][5]['qty'],3,',','.') }} @endif</td>
                        <td align="center" class=""> @if($retail['data']["akhir"][6]['qty'] > 0) {{  number_format( $retail['data']["akhir"][6]['qty'],3,',','.') }} @endif</td>
                    </tr>
                @endforeach

                <tr>
                    <td colspan="2" style="font-weight: bold;">Jumlah Kecamatan {{ $kecamatan['name'] }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][0]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][1]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][2]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][3]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][4]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][5]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["awal"][6]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][0]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][1]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][2]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][3]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][4]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][5]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penebusan"][6]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][0]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][1]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][2]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][3]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][4]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][5]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["penyaluran"][6]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][0]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][1]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][2]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][3]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][4]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][5]['qty'],3,',','.') }}</td>
                    <td align="center">{{  number_format(   $kecamatan['total']["akhir"][6]['qty'],3,',','.') }}</td>
                </tr>
            @endforeach
        </tbody>

    </table>
    
    <table width="100%" class="bordered" border="0" cellspacing="0" cellpadding="0" style="padding-top: -14;">
            <tr>
                <th height="2" style="border: 0px solid;" width="2%" </th>
                <th height="2" style="border: 0px solid;" width="18%" </th>
                <th height="2" style="border: 0px solid;" colspan="5" width="20%" </th>
                <th height="2" style="border: 0px solid;" colspan="5" width="20%" </th>
                <th height="2" style="border: 0px solid;" colspan="5" width="20%" </th>
                <th height="2" style="border: 0px solid;" colspan="5" width="20%" </th>
            </tr>
            <tr style="border: 1px solid black;">
            <td colspan="2" style="font-weight: bold;">Jumlah {{ $details['header']['customer_name'] }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][0]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][1]['qty'],3,',','.') }}</td>
                <td width="6%" align="center">{{  number_format( $details['total']["awal"][2]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][3]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][4]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][5]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["awal"][6]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][0]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][1]['qty'],3,',','.') }}</td>
                <td width="6%" align="center">{{  number_format( $details['total']["penebusan"][2]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][3]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][4]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][5]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penebusan"][6]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][0]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][1]['qty'],3,',','.') }}</td>
                <td width="6%" align="center">{{  number_format( $details['total']["penyaluran"][2]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][3]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][4]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][5]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["penyaluran"][6]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][0]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][1]['qty'],3,',','.') }}</td>
                <td width="6%" align="center">{{  number_format( $details['total']["akhir"][2]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][3]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][4]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][5]['qty'],3,',','.') }}</td>
                <td width="4%" align="center">{{  number_format( $details['total']["akhir"][6]['qty'],3,',','.') }}</td>
            </tr>
            <tr>
                <td colspan="23"></td>
                <td colspan="7" align="center">
                    {{$details['header']['sales_group_name']}}, {{ $header['date_ttd'] }}
                    <br>
                    <br>
                    {{ $details['header']['customer_name'] }}
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ...........................................
                    <br>
                    <br>
                </td>
            </tr>
        
            
    </table>

</body>
