

<html>
<head>
    <title>BAST</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;
        }
        .garis{
            border-collapse: collapse;
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;
        }
        .table-strip table{
            border-collapse: collapse;
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
        td.no-top-border {
            border-top: none !important;
            border-bottom: none !important;
            border-right: none !important;
            padding: 10px;
        }

        td.no-top-border-remove-buttom {
            border-bottom: none !important;
        }
    </style>
</head>
<body >
     <div style="position: absolute;"><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(225)->errorCorrection('H')->generate($url)) !!}" alt="BST" /> </div>
     <div> <br> </div>

        <p style="font-weight:700;text-align:center;margin-top:100px;">BERITA ACARA</p>
        <p style="font-weight:700;text-align:center;">HASIL VERIFIKASI DAN VALIDASI PUPUK BERSUBSIDI TIM KECAMATAN</p>
        <p style="font-weight:700;text-align:center;">PROVINSI {{ strtoupper($data['header']['sales_office_name'])}}</p>
        <p style="font-weight:700;text-align:center;">KABUPATEN {{ strtoupper($data['header']['sales_group_name'])}}</p>
        <p style="font-weight:700;text-align:center;">PERIODE BULAN {{strtoupper($data['header']['month'])}} {{$data['header']['year']}}</p>

        <div style="margin-top:20px;font-weight: bold;">
            <p>Pada hari __________ tanggal _____________ bertempat di __________________ telah dilakukan verifikasi dan validasi penyaluran pupuk bersubsidi dengan hasil sebagai berikut : </p>
            <p>PRODUSEN   : {{strtoupper($data['header']['sales_org_name'])}} </p>
        </div>

    <div >
        <p style="text-align:right;margin-right:10px;font-weight: bold;font-size: 10px;padding-bottom: 5px;"><i>(DALAM SATUAN TON)</i></p>
        <table class="garis" style="text-align:center;width:100%;;font-size:7pt;">
            <thead>
                <tr style="background-color: rgb(242,242,242)">
                    <th style="width: 40px;" rowspan="3">NO</th>
                    <th style="width: 200px;" rowspan="2" colspan="2">WILAYAH/TIM VARIFIKASI DAN VALIDASI</th>
                    <th colspan="21">PENYALURAN</th>
                    <th style="border-bottom: none !important;"></th>
                </tr>
                <tr style="background-color: rgb(242,242,242)">
                    <th colspan={{count($data['produks'])}}>DATA DISTRIBUTOR</th>
                    <th colspan={{count($data['produks'])}}>KOREKSI</th>
                    <th colspan={{count($data['produks'])}}>HASIL VERIFIKASI DAN VALIDASI</th>
                    <th style="border-top: none !important; border-bottom: none !important;"> ALASAN KOREKSI</th>
                </tr>
                <tr style="background-color: rgb(242,242,242)">
                    <th>KECAMATAN</th>
                    <th>NAMA PENGECER</th>
                    @for ($i = 0; $i < 3; $i++)
                        @foreach ($data['produks'] as $item)
                            <th style="width: 50px;" class="garis">
                               @if($item['name']=="ORGANIK CAIR")
                                   POC
                               @elseif($item['name']=="NPK KAKAO")
                                   KAKAO  
                               @else
                                   {{$item['name']}}
                               @endif
                            </th>
                        @endforeach
                    @endfor
                    <th style="border-top: none !important;"></th>
                </tr>
            </thead>
            <tbody>
            @php
                $loops=1;
            @endphp
            @foreach ($data['data'] as $dataItem)
                <tr style="background-color: rgb(242,242,242)">
                    <td>{{$loops}}</td>
                    <td colspan="2" style=" font-weight: bold;text-align: left;">{{$dataItem['name']}}</td>
                    <td colspan={{count($data['produks'])}}></td>
                    <td colspan={{count($data['produks'])}}></td>
                    <td colspan={{count($data['produks'])}}></td>
                    <td></td>
                </tr>

                {{-- Loop Items Retail In Kecamatan --}}
                @php
                    $loopsttd=0;
                    $dataRetail=count($dataItem['data'] );
                @endphp
                @foreach ($dataItem['data'] as $item)
                    <tr style="">
                        <td class="no-top-border"></td>
                        <!-- <td class="no-top-border"></td> -->
                        @if ($dataRetail==1)
                            <td class="no-top-border"} style="height: 80px;">
                                Tim Verifikasi <br>
                                Kec. {{$dataItem['name']}}
                                <br>
                                <br>
                                <br>
                                ______________
                            </td>
                        @elseif($dataRetail==2)
                            @if($loopsttd==0)
                            <td class="no-top-border">
                                Tim Verifikasi <br>
                                Kec. {{$dataItem['name']}}
                                <br>
                            </td>
                            @else
                            <td class="no-top-border">
                                <br>
                                ______________
                            </td>
                            @endif
                        
                        @elseif($dataRetail==3)
                            @if($loopsttd==0)
                            <td class="no-top-border">
                                Tim Verifikasi <br>
                                Kec. {{$dataItem['name']}}
                                <br>
                            </td>
                            @elseif($loopsttd==1)
                            <td class="no-top-border">
                                <br>
                            </td>
                            @else
                              <td class="no-top-border">
                                ______________
                              </td>
                            @endif
                        @elseif($dataRetail>=4)
                            @if($loopsttd==0)
                            <td class="no-top-border">
                                Tim Verifikasi <br>
                                Kec. {{$dataItem['name']}}
                                
                            </td>
                            @elseif($loopsttd==1)
                            <td class="no-top-border">
                                <br>
                            </td>
                            @elseif($loopsttd==2)
                              <td class="no-top-border">
                                <br>
                                ______________
                              </td>
                            @else
                                <td class="no-top-border"></td>
                            @endif
                        @endif
                        <td>{{$item['name']}}</td>

                        @foreach ($item['data']['penyaluran'] as $itemTotal)
                            <th>
                                @if ($itemTotal['qty']!=0)
                                    {{number_format((float)$itemTotal['qty'],3,',','.')}}
                                @endif
                            </th>
                        @endforeach

                        @for ($i = 0; $i < 2; $i++)
                            @foreach ($data['produks'] as $item)
                                <th></th>
                            @endforeach
                        @endfor
                        <td></td>
                    </tr>



                    @php
                        $loopsttd+=1;
                    @endphp
                @endforeach
                {{-- END Loop Items Retail In Kecamatan --}}

                <tr style="height: 40px;">
                    <td colspan="3" style="text-align:left;font-weight: bold;">Sub Jumlah Hasil varifikasi Dokumen</td>
                    @foreach ($dataItem['total']['penyaluran'] as $itemTotal)
                        <th>
                            @if ($itemTotal['qty']!=0)
                                {{ number_format((float)$itemTotal['qty'],3,',','.')}}
                            @endif
                        </th>
                    @endforeach
                    @for ($i = 0; $i < 2; $i++)
                        @foreach ($data['produks'] as $item)
                            <th></th>
                        @endforeach
                    @endfor
                    <td></td>
                </tr>
                <tr style="height: 40px;">
                    <td colspan={{(count($data['produks'])*2)+3}} style="text-align:left; font-weight: bold;"> Hasil Varifikasi dan Validasi Lapangan</td>
                    @foreach ($dataItem['total']['penyaluran'] as $itemTotal)
                        <th></th>
                    @endforeach
                    <td></td>
                </tr>
                <tr style="height: 40px;">
                    <td colspan={{(count($data['produks'])*2)+3}} style="text-align:left;font-weight: bold;"> Total Hasil Verifikasi dan Validasi </td>
                    @foreach ($dataItem['total']['penyaluran'] as $itemTotal)
                        <th></th>
                    @endforeach
                    <td></td>
                </tr>

            @php
                $loops+=1;
            @endphp
            @endforeach
            <tr style="">
                <td colspan="3" style=" font-weight: bold;text-align: left;">JUMLAH {{ strtoupper($data['header']['sales_group_name'])}} </td>
                @foreach ($data['total']['penyaluran'] as $itempenyaluran)
                    <td style="font-weight: bold;">
                            @if($itempenyaluran['qty']!=0)
                            {{ number_format((float)$itempenyaluran['qty'],3,',','.') }}
                            @else
                            0
                            @endif

                    </td>
                @endforeach

                @for ($i = 0; $i < 2; $i++)
                    @foreach ($data['produks'] as $item)
                        <td></td>
                    @endforeach
                @endfor
                <td></td>
            </tr>
            <tr style="">
                <td colspan="{{(count($data['produks'])*3) + 3}}" style="text-align: left;">
                    <p>Catatan :</p>
                    <p>1. Penyaluran adalah penyaluran pupuk bersubsidi dari Kios pengecer ke Kelompok tani/Petani dalam RDKK</p>
                    <p>2. Kolom koreksi diisi dengan hal-hal sebagai berikut :</p>
                        <p style="padding-left: 7px;"> <&nbsp> <&nbsp> <&nbsp> - Perbedaan antara data distributor dengan bukti yang ada, seperti log book, bukti penjualan, dan catatan lainnya </p>

                    <p>3. Bila terdapat koreksi akibat hasil verifikasi dan validasi lapangan agar dilampirkan Berita Acara Hasil Verifikasi dan validasi</p>
                    <br>
                    <br>
                    <br>
                </td>
                <td style="text-align: center;">
                    <br>
                    <br>
                    {{ strtoupper($data['header']['customer_name']) }}
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                   

                    ______________

                    <br>
                    <br>
                </td>
            </tr>
            </tbody>

        </table>
    </div>
</body>
</html>
