<html>
<head>
    <title>DOWNLOAD</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;
        }
        .garis{
            border-collapse: collapse;
            width:100%;
        }
        .page_break { page-break-after: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;
        }
        .table-strip table{
            border-collapse: collapse;
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
    </style>
</head>
<body>
    <?php
        $count=0;
        $max=$data['jumlah_page'] ;
    ?>

    @foreach ($data['data'] as $item)
         @foreach ($item['data'] as $detail)
            <?php $count+=1; ?>
            <p style="font-weight:100;text-align:left;font-size: 10pt;" >{{$count}}.&nbsp;LAMPIRAN VI</p>
            <p style="font-weight:100;text-align:left;font-size: 10pt;"> PERATURAN MENTERI PERDAGANGAN REPUBLIK INDONESIA</p>
            <p style="text-align:left;padding:0;margin:0;font-weight:100;;font-size: 10pt;">Nomor   : 15/M-DAG/PER/4/2013 </p>
            <p style="text-align:left;padding:0;margin:0;font-weight:100;;font-size: 10pt;">Tanggal : 1 NOVEMBER 2013 </p>

            <p style="font-weight:100;text-align:left;padding-left:70%;margin:0;;font-size: 10pt;">Kepada Yth :</p>
            <p style="text-align:left;padding-left:70%;margin:0;font-weight:100;;font-size: 10pt;">Distributor   : {{ strtoupper($data['header']['customer_name']) }} </p>
            <p style="text-align:left;padding-left:70%;margin:0;font-weight:100;;font-size: 10pt;">di {{$data['header']['sales_group_name']}} </p>

            <div style="margin-top:40px">
                <p style="font-size: 10pt;">LAPORAN BULANAN PENGECER : {{ strtoupper ($detail['name'])}} </p>
                <p style="font-size: 10pt;">PERIODE BULAN  <b style="color: black;">{{  strtoupper ($data['header']['month'])  }}</b> TAHUN  <b style="color: black;">{{  $data['header']['year']  }}</b> </p>
                <p style="font-size: 10pt;"> KABUPATEN  : <b style="color: black;">{{ strtoupper($data['header']['sales_group_name'])  }} </b> , KECAMATAN : <b style="color:black;">{{ strtoupper($item['name']) }} </b></p>
                <br>
                <p style="text-align:left;padding-left:80%;font-size: 10pt;">Dalam (Ton)</p>
                <table class="garis" style="text-align:center;width:100%;;font-size: 10pt;">
                    <tr style="background-color: rgb(242,242,242);">
                        <th>JENIS PUPUK</th>
                        <th>PERSEDIAAN AWAL</th>
                        <th>PENEBUSAN</th>
                        <th>PENYALURAN</th>
                        <th>PERSEDIAAN AKHIR</th>
                    </tr>
                    @php
                         $loops=0;$total_awal=0;$total_penebusan=0;$total_penyaluran=0;$total_akhir=0;
                    @endphp
                    @foreach ($data['produks'] as $product)
                       
                        <tr style="">
                            <td>
                                {{$product['name']}}
                            </td>
                            <td>
                                @if($detail['data']['awal'][$loops]['qty']!=0)
                                {{number_format((float)$detail['data']['awal'][$loops]['qty'],3,',','.')}}
                                @endif
                            </td>
                            <td>
                                @if($detail['data']['penebusan'][$loops]['qty']!=0)
                                {{number_format((float)$detail['data']['penebusan'][$loops]['qty'],3,',','.')}}
                                @endif
                            </td>
                            <td>
                                @if($detail['data']['penyaluran'][$loops]['qty']!=0)
                                {{number_format((float)$detail['data']['penyaluran'][$loops]['qty'],3,',','.')}}
                                @endif
                            </td>
                            <td>
                                @if($detail['data']['akhir'][$loops]['qty']!=0)
                                {{number_format((float)$detail['data']['akhir'][$loops]['qty'],3,',','.')}}
                                @endif
                            </td>
                        </tr>
                        @php
                            $total_awal+=$detail['data']['awal'][$loops]['qty']; 
                            $total_penebusan+=$detail['data']['penebusan'][$loops]['qty'];
                            $total_penyaluran+=$detail['data']['penyaluran'][$loops]['qty']; 
                            $total_akhir+=$detail['data']['akhir'][$loops]['qty']; 
                            $loops++;
                        @endphp
                    @endforeach
                    <tr style="background-color: rgb(242,242,242);">
                        <th>Total</th>
                        <th> @if($total_awal!=0) {{number_format((float) $total_awal,3,',','.')}} @endif </th>
                        <th> @if($total_penebusan != 0) {{number_format((float) $total_penebusan,3,',','.')}} @endif </th>
                        <th> @if($total_penyaluran != 0) {{number_format((float) $total_penyaluran,3,',','.')}} @endif </th>
                        <th> @if($total_akhir != 0) {{number_format((float) $total_akhir,3,',','.')}} @endif </th>
                    </tr>

                </table>
            </div>
            <div style="margin-top:30px;">
                <table style="text-align:center;width:100%;;font-size: 10pt;">
                    <tr style="font-weight:100;">
                        <td width='50%'></td>
                        <td width='50%'>{{strtoupper($data['header']['sales_group_name'])}}, .... {{ strtoupper ($data['header']['month'])}}  {{$data['header']['year']}}</td>
                    </tr>
                     <tr style="font-weight:100;">
                        <td width='50%'></td>
                        <td width='50%'>{{ strtoupper ($detail['name'])}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:100;"></td>
                        <td style="font-weight:100;"></td>
                    </tr>
                    <tr>
                        <td><p style="margin-top:100px;"></p></td>
                        <td><p style="margin-top:100px;"></p></td>
                    </tr>
                    <tr>
                        <td><p style="font-size:14px;"></p></td>
                        <td><p style="font-size:14px;"><br><br>(........................)</p></td>
                    </tr>
                </table>
            </div>
            <br>
            <p style="padding-left:3px;font-size: 10pt;">Tembusan</p>
            <p style="padding-left:5px;font-size: 10pt;">1. Kepala Dinas Perindag {{strtoupper($data['header']['sales_group_name'])}}</p>
            <p style="padding-left:5px;font-size: 10pt;">2. Kepala Dinas Pertanian {{strtoupper($data['header']['sales_group_name'])}}</p>
            <p style="padding-left:5px;font-size: 10pt;">3. Komisi Pengawas Pupuk dan Pestisida {{strtoupper($data['header']['sales_group_name'])}}</p>

            @if ($count<($max))
                <div class="page_break">
                </div>
            @endif

         @endforeach
    @endforeach
</body>
</html>
