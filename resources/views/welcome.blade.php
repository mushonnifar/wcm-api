<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta charset="utf-8">
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Webcommerce is activity of buying or selling of products on online services or over the Internet. ">
    <meta name="keywords" content="pupuk indonesia">
    <meta name="author" content="aeeeeeng">
    <title>WCM - PIHC</title>
    <link rel="icon" href="/images/logo/favicon.ico" sizes="16x16">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="/css/template.css">
    <link rel="stylesheet" href="/css/style.css">
    <style media="screen"></style>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
    <!-- fixed-top-->
    <div id="app"></div>
    <script src="/core/libraries/jquery.min.js" charset="utf-8"></script>
    <script src="/js/app.js" charset="utf-8"></script>
    <script src="/js/template.js" charset="utf-8"></script>
    <script src="/core/app-menu.js" charset="utf-8"></script>
    <script src="/core/app.js" charset="utf-8"></script>
    <script src="/core/app-additional.js" charset="utf-8"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-182014346-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-182014346-1');
    </script>
  </body>
</html>
