<html>
<head>
    <title>BAST</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;            
        }
        .garis{
            border-collapse: collapse;            
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;            
        }
        .table-strip table{
            border-collapse: collapse;            
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{            
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
    </style>
</head>
<body>
    <?php
        $count=0;
        $max=count($data);
    ?>
   
    @foreach ($data as $item)
        <?php $count+=1; ?>
        <p style="text-align:right;padding:0;margin:0;font-weight:bold;font-size: 16px;">Nomor: {{$item['number']}} </p>    
        <p style="text-align:right;padding:0;margin:0;font-weight:bold;font-size: 16px;">Nomor SO: {{$item['number_so']}}</p>
        <p style="font-weight:bold;text-align:center;font-size: 18px;padding-top: 15px;"> BERITA ACARA SERAH TERIMA BARANG PUPUK BERSUBSIDI</p>
        <div style="margin-top:40px;">
            <p>Pada Hari {{$item['day']}} Tanggal {{$item['date']}}, Bulan {{$item['month']}}, Tahun {{$item['year']}}. Telah dilaksanakan serah terima bukti Pupuk Bersubsidi dari Pihak Pertama kepada Pihak Kedua dalam keadaan baik dan sesuai dengan rincian sbb:</p>        
        </div>
        <div style="margin-top:10px">    
            <table class="garis" width=100>
                <tr style="font-weight:bold;font-size:14px;text-align:center;background-color: rgb(242,242,242)">
                    <td> JENIS BARANG</td>
                    <td> JUMLAH BARANG Ton</td>
                    <td> NAMA PENGECER</td>
                    <td> ALAMAT PENGECER</td>
                </tr>  
                @foreach ($item['data'] as $v)
                    <tr >
                        <td>
                            {{$v['product_name']}}
                        </td>
                        <td> {{$v['qty']}}</td>
                        <td> {{$v['retail_name']}} </td>
                        <td> {{$v['address']}}</td>
                    </tr>
                @endforeach          
                                        
            </table>
        </div>
        <div style="margin-top:30px;">    
            <table style="text-align:center;width:100%;">
                <tr style="font-weight:600;">
                    <td width='50%'>Yang Menyerahkan,</td>
                    <td width='50%'>Yang Menerima</td>                
                </tr>
                <tr>
                    <td style="">Pihak Pertama</td>
                    <td style="">Pihak Kedua</td>                
                </tr>
                <tr>
                    <td style=""><p style="margin-top:100px;">(.......................)</p></td>
                    <td style=""><p style="margin-top:100px;">{{$item['owner']}}</p></td>                
                </tr>            
            </table>
        </div>
        <div style="margin-top:30px;">        
            <ol>
                <li>Untuk {{$sales_org_name}}</li>
                <li>Untuk Distributor</li>
                <li>Untuk Pengecer</li>
            </ol>
        </div>

        @if ($count<$max)
             <div class="page_break">
             </div>
        @endif
       
    @endforeach
    
</body>
</html>