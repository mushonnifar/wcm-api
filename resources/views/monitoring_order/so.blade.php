<html>
<head>
    <title>Sales Order</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;            
        }
        .garis{
            border-collapse: collapse;            
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;            
        }
        .table-strip table{
            border-collapse: collapse;            
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{            
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
    </style>
</head>
<body>
    <p style="text-align:center;font-size:24px;text-transform:underline;font-weight:bold;"><u>SALES ORDER</u></p>     
    <p style="color:grey;text-align:center;;font-size: 18px;">No. <?=$data[0]->so_number?></p>
    <p style="color:grey;text-align:center">Tanggal <?= strtoupper($data[0]->order_date)?></p>
    <div style="margin-top:55px">
        <p style="color: grey;">Kepada Yth.</p>
        <p style="font-weight:bold"><?=$data[0]->customer_name;?></p>
        <p style="color: grey;"><?=$data[0]->address.' DESA '.$data[0]->village_name.' KEC. '.$data[0]->sales_unit_name.' '.$data[0]->sales_group_name;?></p>
        <p style="color: grey;">Telp. <?=$data[0]->tlp_no.', Fax '.$data[0]->fax_no;?></p>
    </div>
    <div style="margin-top:50px;color: grey;">
        <p>Dengan hormat, <br/> 
        Bersama ini disampaikan rincian pembelian sebagai berikut:</p>
    </div>
    <div style="margin-top:20px">    
        <table class="garis" width=100>
            <tr style="font-weight:bold">
                <td> PRODUK</td>
                <td> KUANTITAS</td>
                <td> INCOTERM</td>
                <td> PLANT</td>
            </tr>
            <?php foreach($product as $key => $value){?>
                <tr>
                    <td style="color: grey;"> <?=$value->product_name;?></td>
                    <td style="color: grey;text-align:right;"> <?= number_format((float)$value->product_qty, 2, '.', '');?> MT</td>
                    <td style="color: grey;"> <?=$value->delivery_name.' - '.$value->sales_group_name_incoterm;?></td>                
                    <td style="color: grey;"> <?=$value->plant_code.' - '.$value->sales_group_name_plant.' - '.$value->plant_name;?></td>
                </tr>            
            <?php }?>
        </table>
    </div>
    <div style="margin-top:30px; color: grey;">    
        <table>
            <tr>
                <td>Alat Angkut</td>
                <td>:</td>
                <td><?=$data[0]->angkutan;?></td>
            </tr>
            <tr>
                <td>Tujuan</td>
                <td>:</td>
                <td><?=$data[0]->tujuan_kab.', '.$data[0]->tujuan_prov;?></td>
            </tr>
            <tr>
                <td>ETA</td>
                <td>:</td>
                <td></td>
            </tr>
            <tr>
                <td>Sektor</td>
                <td>:</td>
                <td>Subsidi</td>                
            </tr>
            <tr>
                <td>No. Kontrak</td>
                <td>:</td>
                <td><?=$data[0]->spjb_no;?></td>
            </tr>
            <tr>
                <td>No. Order</td>
                <td>:</td>
                <td>{{ $data[0]->number }}</td>
            </tr>
        </table>
    </div>
    <div style="margin-top:30px; color: grey;">        
        <p>Produk tersebut harap diambil di plant yang telah ditentukan paling lambat <b><?=$data[0]->good_redemption_due_date;?></b><br/><br>
        Demikian disampaikan, atas perhatiannya diucapkan terima kasih.</p>
    </div>
    <div class="page_break">
        <p style="text-decoration:underline;font-weight:bold;font-size: 24px;">Lampiran Sales Order <?=$data[0]->so_number?></p>
        <?php 
        $x='A'; 
        foreach($lampiran as $key =>$value){?>
        <h3 class="bold"><?=$x++?>. Produk: <?=$value['name']?></h3>
            <?php foreach($lampiran[$key]['data'] as $keyKab => $valueKab){?>
            <h3 class="bold"><?=$valueKab['name'].' - '.$valueKab['prov']?></h3>
            <div class="table-strip">
                <table>
                    <tr class="bold">
                        <td>No.</td>
                        <td>Kecamatan</td>
                        <td>Pengecer</td>
                        <td>Kuantum</td>
                    </tr>                            
                    <?php 
                    $no=1;
                    foreach($lampiran[$key]['data'][$keyKab]['data'] as $keyKec => $valueKec){ 
                        $kec=count($lampiran[$key]['data'][$keyKab]['data'][$keyKec]['data'])-1; 
                        foreach($lampiran[$key]['data'][$keyKab]['data'][$keyKec]['data'] as $keyRet => $valueRet){?>  
                            <tr style="color: grey;">
                                <td><?=$no++?></td>
                                <td><?=$valueKec['name']?></td>
                                <td><?=$valueRet['name']?></td>
                                <td style="text-align: right;"><?= number_format((float)$valueRet['qty'], 2, ',','.');?> MT</td>
                            </tr>
                            <?php if($kec == $keyRet){?>
                                    <tr class="bold">
                                        <td colspan=3>Total Kecamatan <?=$valueKec['name']?></td>
                                        <td style="text-align: right;"><?=number_format((float)$valueKec['total'],2,',','.');?> MT</td>
                                    </tr>
                                <?php }                                                                        
                        }
                    }?>                    
                    <tr class="bold">
                        <td colspan=3>Total <?=$valueKab['name'];?></td>
                        <td style="text-align: right;"><?=number_format((float)$valueKab['total'],2,',','.');?> MT</td>
                    </tr>
                </table>
            </div>
            <?php }?>
        <?php }?>
    </div>
</body>
</html>