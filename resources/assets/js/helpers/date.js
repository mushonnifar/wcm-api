import Vue from 'vue';
let dateHelperMethods = {
  convertDate : (date) => {
    function monthName(month){
      switch (month) {
        case '01': return 'Jan'; break;
        case '02': return 'Feb'; break;
        case '03': return 'Mar'; break;
        case '04': return 'Apr'; break;
        case '05': return 'Mei'; break;
        case '06': return 'Jun'; break;
        case '07': return 'Jul'; break;
        case '08': return 'Ags'; break;
        case '09': return 'Sep'; break;
        case '10': return 'Okt'; break;
        case '11': return 'Nov'; break;
        case '12': return 'Des'; break;
        default: return 'Undefined Month';
      }
    }
    if(date === '' || date === null || date === ' ' || date === '0000-00-00' || date === '00-00-0000') {
      return '';
    }
    let newDate = date.split(' ')
    let onlyDate = newDate[0]
    let onlyTime = (newDate[1] != undefined) ? ', '+newDate[1] : ''
    let splitDate = onlyDate.split('-')
    let getMonth = monthName(splitDate[1])
    onlyDate = splitDate[0]+'-'+getMonth+'-'+splitDate[2]
    return onlyDate+onlyTime;
  }
}

export const dateHelper = Vue.mixin({
  methods: dateHelperMethods
})
