export default {
  language: "id",
  global : {
    form: {
      labelAdd: "Entri Baru",
      labelEdit: "Ubah",
      labelDelete: "Hapus",
      labelUpdate: "Perbarui",
      labelSave: "Simpan",
      labelUpload: "Upload Data",
      labelCancel: "Batal",
      labelShowData: "Lihat Data",
    },
    language : {
        icon : "flag-icon flag-icon-id"
    },
    filter: {
      all: "semua",
      search: "cari",
      reset: "reset",
      modal: {
        date: {
          titleOne: 'Rentang Waktu',
          titleTwo: 'Waktu Tertentu'
        },
        value:{
          titleOne: 'Rentang Nilai',
          titleTwo: 'Nilai Tertentu'
        },
        form: {
          labelFrom: 'Dari',
          labelTo: 'Sampai',
          labelSearch: 'Cari',
          labelClose: 'Tutup'
        }
      }
    }
  },
  header : {
    dropdownItemProfile : 'Profil',
    dropdownItemLogout : 'Keluar',
  },
  loginPage: {
    title: "M A S U K",
    usernameLabel: "Nama Pengguna",
    passwordLabel: "Kata Kunci",
    rememberLabel: "Ingatkan Saya",
    forgetLabel: "Lupa Password ? ",
    loginButton: "Masuk"
  },
  distributorMasterPage: {
    breadcrumbFirst: "Master Data",
    breadcrumbSec: "Distributor",
    cardtTitle: "Master Distributor",
    download: "Unduh",
    table: {
      one: "Kode",
      two: "Nama",
      three: "Nama Pemilik",
      four: "Status",
      five: "Alamat",
      six: "Provinsi",
      seven: "Kota / Kabupaten",
      eight: "Kecamatan",
      nine: "No. Telp. Kantor",
      ten: "No. Fax. Kantor"
    }
  },
  userAllMasterPage: {
    breadcrumbFirst: "Master Data",
    breadcrumbSec: "Master Semua Pengguna",
    cardtTitle: "Master Semua Pengguna",
    download: "Unduh",
    labelAdd: "Tambah Pengguna",
    table: {
      one: "Nama",
      two: "Nama Pengguna",
      three: "Email",
      four: "Tanggal Dibuat",
      five: "Tanggal Di ubah",
    }
  },
  userAllFormPage: {
    breadcrumbFirst: "Master Data",
    breadcrumbSec: "Master Semua Pengguna",
    breadcrumbTrd: "Form Pengguna",
    cardtTitle: "Form Pengguna",
    password: {
      titleLabel: "Ubah Kata Kunci",
      input:{
        one: "Kata Kunci Lama",
        two: "Kata Kunci Baru",
        three: "Ulangi Kata Kunci Baru"
      },
      buttonLabelChange: "Ubah",
      buttonLabelClose: "Tutup",
    },
    form:{
      titleAdd: "Tambah Pengguna",
      titleEdit: "Ubah Pengguna",
      one: "Nama",
      two: "Nama Pengguna",
      three: "Email",
      four: "Kata Kunci",
      five: "Ulangi Kata Kunci",
      six: "Bagian",
      labelSave: "Simpan",
      labelDraft: "Draft",
      labelCancel: "Kembali",
      changePasswordLabel: "Ganti Kata Kunci"
    }
  },
  roleMasterPage:{
    cardtTitle:"Master Role",
    breadcrumbNames:{
      first:"Master Role",
      second:"Add Form Roles"
    },
    table:{
      one:"Nama",
      two:"Jabatan",
      three:"Action",
    },
    addButton:"Tambah Role",
    filterPlaceName:"filter Name",
    saveBotton:"Simpan",
    closeButton:"Batal"
  },
  menuRoutePage:{
    cardtTitle:"Master Route",
    breadcrumbFirst: "Route",
    table:{
      one:"Route",
      action:"Action"
    },
    addButton:"Tambah Route"
  },
  menuMasterPage: {
    cardtTitle: "Master Menu",
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master Menu",
    labelAdd: "Tambah Menu",
    table: {
      one: "Nama (EN)",
      two: "Nama (ID)",
      three: "URI",
      four: "NO Urutan",
      five: "LOGO",
      six: "Anak dari (EN)",
      seven: "Anak dari (ID)",
    }
  },
  menuFormPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master Menu",
    breadcrumbTrd: "Form Menu",
    cardtTitle: "Form Menu",
    form:{
      titleAdd: "Tambah Menu ",
      titleEdit: "Ubah Menu ",
      one: "Nama Inggris",
      two: "Nama Indonesia",
      three: "URL",
      four: "Nomor Urut",
      five: "LOGO",
      six: "Anak dari",
      seven: "Orang tua dari nama menu indonesia",
      labelSave: "Save",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  farmerGroupPage: {
    pageTitle: "KELOMPOK TANI",
    breadcrumbFirst: "KELOMPOK TANI",
    breadcrumbSec: "Data Master",
    breadcrumbTrd: "Master",
    cardtTitle: "Form Menu",
    download : "Unduh",
    download: "Download",
    uploadData: "Upload Data",
    addButton: "Entry Data",
    downloadTemplate: "Download Template",
    downloadWilayah: "Download Kode Wilayah",
    table: {
      titleAdd: "Add Menu ",
      titleEdit: "Edit Menu ",
      one: "Produsen",
      two: "Kode",
      three: "Nama",
      four: "Kode Pengecer",
      five: "Nama Pengecer",
      six: "Status",
      seven: "Alamat",
      eight: "Provinsi",
      nine: "Kota / Kabupaten",
      ten: "Kecamatan",
      eleven: "Kelurahan / Desa",
      twelve: "Subsektor",
      thirteen: "Komoditas",
      fourteen: "Total Anggota",
      fiveteen: "Luas Lahan",
      sixteen: "Di Buat Pada",
      seventeen: "Di Perbarui pada",
      labelSave: "",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  subtitutionPage: {
    pageTitle: "SUBTITUTION",
    breadcrumbFirst: "MASTER",
    breadcrumbSec: "SUBTITUTION",
    breadcrumbTrd: "Master",
    cardtTitle: "Form Menu",
    download: "Download",
    titleAdd: "Tambah Material",
    titleEdit: "Edit Menu ",
    saveBotton: "Simpan",
    closeButton: "Batal",
    table: {
      one: "Produsen",
      two: "Valid Awal",
      three: "Valid Akhir",
      four: "Gudang",
      five: "Material",
      six: "Nama",
      seven: "Material Subtitution",
      eight: "UOM",
      nine: "Alasan",
      ten: "Status",
      eleven: "Village",
      twelve: "Sub Sector",
      thirteen: "Commodity",
      fourteen: "Total Member",
      fiveteen: "Land Area",
      sixteen: "Create At",
      seventeen: "Update At",
      labelSave: "",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  salesAreaPage: {
    breadcrumbFirst: "Sales Area",
    breadcrumbSec: "Area",
    cardtTitle: "Master Sales Area",
    download: "Unduh",
    table: {
      one: "Kode Distributor",
      two: "Distributor",
      three: "Kode Produsen",
      four: "Produsen",
      five: "Kode Saluran Distribusi",
      six: "Saluran Distribusi",
      seven: "Kode Divisi",
      eight: "Divisi",
      nine: "Term Of Payment",
      ten: "TOP DP",
      eleven: "Tax Classification",
      twelve: "PPH 22",
      third: "Tipe Pembayaran",
      fourteen : "Status"
    }
  },
  privilegeMenu: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Hak Akses Menu",
    cardtTitle: " Pengelola Hak Akses Menu",
    table: {
      one: "Nama Role/Bagian",
    }
  },
  petaniMasterPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Petani",
    cardtTitle: "Master Petani",
    download: "Unduh",
    title: "Petani",
    table: {
      one: "Produsen",
      two: "Kode",
      three: "Nama Lengkap",
      four: "Status",
      five: "Alamat",
      six: "Provinsi",
      seven: "Kota / Kabupaten",
      eight: "Kecamatan",
      nine: "Kelurahan / Desa",
      ten: "No. Telp.",
      eleven: "Nomor Telepon Genggam",
      twelve: "Luas Lahan",
      thirteen: "Komoditas",
      fourteen: "KTP",
      fiveteen: "Dibuat Pada",
      sixteen: "Diperbarui Pada",
    }
  },
}
