import Vue from 'vue'
import { AclInstaller, AclCreate, AclRule } from 'vue-acl'
import router from './router'
import EncryptionFunction from '../helpers/EncryptionFunction';

Vue.use(AclInstaller)

var role = (localStorage.getItem("access-control") != undefined) ? localStorage.getItem("access-control") : '';
var initial = EncryptionFunction.toText(EncryptionFunction.decrypt(role));
// console.log(initial);

export default new AclCreate({
  initial: initial,
  notfound: '/error',
  router,
  acceptLocalRules: true,
  globalRules: {
    isPublic: new AclRule('*').or('distributor').or('anper').generate(),
    isAnper: new AclRule('anper').generate(),
    isDistributor: new AclRule('distributor').or('anper').generate()
  }
})
