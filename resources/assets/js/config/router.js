import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from "../components/Login.vue"
import Forget from "../components/Forget.vue"
import ExampleVue from "../components/ExampleVue.vue"
import NewPassword from "../components/NewPassword.vue"

// master
import DistributorMaster from '../components/master/distributor/DistributorMaster.vue';
import DetailDistributor from '../components/master/distributor/DetailDistributor.vue';
import DistributorProfile from '../components/master/distributor/detail/DistributorProfile.vue';
import DistributorSalesArea from '../components/master/distributor/detail/DistributorSalesArea.vue';
import PetaniMaster from '../components/master/petani/PetaniMaster.vue';
import PetaniTable from '../components/master/petani/PetaniTable.vue';
import FarmergroupMaster from '../components/master/farmergroup/FarmergroupMaster.vue';
import KelompokTani from '../components/master/kelompok-tani/KelompokTani.vue';
import KelompokTaniComponent from '../components/master/kelompok-tani/KelompokTaniComponent.vue';
import SalesareaMaster from '../components/master/sales/SalesareaMaster.vue';
import SalesareaComponent from '../components/master/sales/SalesareaComponent.vue';
import FarmergroupComponent from '../components/master/farmergroup/FarmergroupComponent.vue';
import RetailMaster from '../components/master/retail/RetailMaster.vue';
import ComponentRelasi from '../components/master/relasi-distributor-pengecer/ComponentRelasi.vue';
import PricingMaster from '../components/master/pricing/PricingMaster.vue';
import SubtitutionMaster from '../components/master/subtitusi/SubtitutionMaster.vue';
import MasterRelasi from '../components/master/relasi-distributor-pengecer/MasterRelasi.vue';
import FormRelasi from '../components/master/relasi-distributor-pengecer/form-relasi/FormRelasi.vue';
import WarehouseMaster from '../components/master/warehouse/WarehouseMaster.vue';
import WarehouseComponent from '../components/master/warehouse/WarehouseComponent.vue';
import BankDfComponent from '../components/master/bank-df/BankDfComponent.vue';
import MasterSupplyPoint from '../components/master/supply-point/SupplyPointMaster.vue';
import BankAccount from '../components/master/bank-account/BankAccount.vue';
import LogOrder from '../components/master/log-order/LogOrder.vue';
import BatasAmbil from '../components/master/batas-ambil/BatasAmbil.vue';
import LogHost from '../components/master/log-host-to-host/LogHost.vue';
import PeriodeTanam from '../components/master/periode-tanam/PeriodeTanam.vue';
import LintasAlur from '../components/master/lintas-alur/LintasAlur.vue';
// master CR
import Desa from '../components/master/desa/DesaMaster.vue';
import MatrixRdkk from '../components/master/matrix-rdkk/MatrixRdkkMaster.vue';


// alokasi
// import Perbup from '../components/alokasi/perbup/Perbup.vue';
// import PerbupDetail from '../components/alokasi/perbup/detail/DetailPerbub.vue';
// import Pergub from '../components/alokasi/pergub/Pergub.vue';
// import PergubNo from '../components/alokasi/pergub/detail/PergubNo.vue';
// import Permentan from '../components/alokasi/permentan/Permentan.vue';
// import PermentanDetail from '../components/alokasi/permentan/detail/PermentanDetail.vue';
// import SpjbAsal from '../components/alokasi/spjb/asal/SpjbAsal.vue';
// import DetailSpjbAsal from '../components/alokasi/spjb/asal/detail/DetailSpjbAsal.vue';
// import SpjbOp from '../components/alokasi/spjb/operasional/SpjbOp.vue';
// import DetailSpjbOp from '../components/alokasi/spjb/operasional/detail/DetailSpjbOp.vue';
import RdkkMaster from '../components/alokasi/rdkk/RdkkMaster.vue';
import RdkkDetail from '../components/alokasi/rdkk/detail/RdkkForm.vue';
import RdkkApproval from '../components/alokasi/rdkk/approval/Header.vue';
// alokasi CR
import AlokasiProvinsi from '../components/alokasi/alokasi-provinsi/Permentan.vue';
import AlokasiProvinsiDetail from '../components/alokasi/alokasi-provinsi/detail/PermentanDetail.vue';
import AlokasiProvinsiEdit from '../components/alokasi/alokasi-provinsi/detail/edit-table/Header.vue';
import SpjbAsalCR from '../components/alokasi/spjb/asal/SpjbAsal.vue';
import DetailSpjbAsalCR from '../components/alokasi/spjb/asal/detail/DetailSpjbAsal.vue';
import EditSpjbAsalCR from '../components/alokasi/spjb/asal/detail/edit-table/Header.vue';
import SpjbOpCR from '../components/alokasi/spjb/operasional/SpjbOp.vue';
import DetailSpjbOpCR from '../components/alokasi/spjb/operasional/detail/DetailSpjbOp.vue';
import EditSpjbOprCR from '../components/alokasi/spjb/operasional/detail/edit-table/Header.vue';
import AlokasiKecamatan from '../components/alokasi/alokasi-kecamatan/Perbup.vue';
import AlokasiKecamatanDetail from '../components/alokasi/alokasi-kecamatan/detail/DetailPerbub.vue';
import AlokasiKecamatanEdit from '../components/alokasi/alokasi-kecamatan/detail/edit-table/Header.vue';
import AlokasiKabupaten from '../components/alokasi/alokasi-kabupaten/Pergub.vue';
import AlokasiKabupatenNo from '../components/alokasi/alokasi-kabupaten/detail/PergubNo.vue';
import AlokasiKabupatenEdit from '../components/alokasi/alokasi-kabupaten/detail/edit-table/Header.vue';


// transaksi
import Permintaan from '../components/transaksi/order/Permintaan.vue';
import PenebusanDetail from '../components/transaksi/order/PermintaanDetail.vue';
import PermintaanDetail from '../components/transaksi/permintaan-penebusan/PermintaanDetail.vue';
import Penebusan from '../components/transaksi/order/step-1/Penebusan.vue';
import ItemPenebusan from '../components/transaksi/order/step-2/ItemPenebusan.vue';
import CheckoutOrder from '../components/transaksi/order/step-3/CheckoutOrder.vue';
import MonitoringOrder from '../components/transaksi/monitoring-order/MonitoringOrder.vue'
import MonitoringDo from '../components/transaksi/monitoring-do/MonitoringDo.vue'
import MonitoringDf from '../components/transaksi/monitoring-df/MonitoringDf.vue'
import ManualApproved from '../components/transaksi/manual-approved/ManualApproved.vue'
import ManualApprovedSetting from '../components/transaksi/manual-approved/setting/ManualApprovedSetting.vue'

// laporan
import PenyaluranDo from '../components/laporan/penyaluran-do/PenyaluranDo.vue';
import DetailPenyaluranDo from '../components/laporan/penyaluran-do/detail/DetailPenyaluranDo.vue';
import ListDetailDo from '../components/laporan/penyaluran-do/list-detail-do/ListDetailDo.vue';
import ItemPenyaluran from '../components/laporan/item-penyaluran/ItemPenyaluran.vue';
import LaporanBulananDistributor from '../components/laporan/laporan-bulanan-distributor/LaporanBulananDistributor.vue';
import DetailLaporanBulananDistributor from '../components/laporan/laporan-bulanan-distributor/detail/DetailLaporanBulananDistributor.vue';
import InitialStokf5 from '../components/laporan/initial-stok-f5/InitialStokf5.vue';
import InitialStokf6 from '../components/laporan/initial-stok-f6/InitialStokf6.vue';
import ItemInitialF6 from '../components/laporan/initial-stok-f6/item-inital-f6/ItemInitialF6.vue';
import DetailInitialStokF6 from '../components/laporan/initial-stok-f6/detail/DetailInitialStokF6.vue';
import PenyaluranInitialStokf5 from '../components/laporan/initial-stok-f5/detail-f5/PenyaluranInitialStokf5.vue';
import DetailInitialStokf5 from '../components/laporan/initial-stok-f5/detail/DetailInitialStokf5.vue';
import LaporanBulananPengecer from '../components/laporan/laporan-bulanan-pengecer/LaporanBulananPengecer.vue';
import DetailLaporanBulananPengecer from '../components/laporan/laporan-bulanan-pengecer/detail/DetailLaporanBulananPengecer.vue';
import OutstandingSo from '../components/laporan/outstanding-so/OutstandingSo.vue';
import HeaderLaporanSo from '../components/laporan/header-laporan-so/HeaderLaporanSo.vue';
import LevelLaporanSoIndex from '../components/laporan/level-laporan-so/LevelLaporanSoIndex';
import SuratJalan from '../components/laporan/surat-jalan/SuratJalan.vue';
import DetailSuratJalan from '../components/laporan/surat-jalan/detail/DetailSuratJalan.vue';

// config
import UserMasterAll from '../components/master/user/UserMasterAll.vue';
import FormUser from '../components/master/user/FormUser.vue';
import FormUserDistributor from '../components/master/user/FormUserDistributor.vue';
import FormUserPenggunaBaru from '../components/master/user/FormUserPenggunaBaru.vue';
import MasterRole from '../components/master/role_permissions/MasterRole.vue';
import MasterPermissions from '../components/master/role_permissions/MasterPermissions.vue';
import MenuMaster from '../components/master/menu/MenuMaster.vue';
import MasterRoutes from '../components/master/route/MasterRoutes.vue';
import MenuForm from '../components/master/menu/MenuForm.vue';
import ListPrivilegeMenu from '../components/master/privilege/menu/ListPrivilegeMenu.vue';
import ListPrivilegeRouter from '../components/master/privilege/router/ListPrivilegeRouter.vue';
import PengecerProdusen from '../components/master/pengecer-produsen/PengecerProdusen.vue'


//sync
import Sync from '../components/sync/Sync.vue';

// user guide
import UserGuide from '../components/user-guide/UserGuide.vue';

// profile
import Profile from '../components/profile/Profile';


Vue.use(VueRouter)

export default new VueRouter({
  linkActiveClass: "active",
  linkExactActiveClass: "exact-active",
  routes: [
    {
      path: '/',
      redirect: {
        name: "login"
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        auth: false,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/forget-password",
      name: "forget_password",
      component: Forget,
      meta: {
        auth: false,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/new-password/:id",
      name: "new_password",
      component: NewPassword,
      meta: {
        auth: false,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/home",
      name: "home",
      component: ExampleVue,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-distributor",
      name: "master_distributor",
      component: DistributorMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-distributor/detail/:id",
      component: DetailDistributor,
      children: [
        {
          path: '',
          name: "detail_distributor",
          component: DistributorProfile,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'sales-area',
          name: "detail_distributor_sales_area",
          component: SalesareaComponent,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'retail',
          name: "detail_distributor_retail",
          component: ComponentRelasi,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'warehouse',
          name: "detail_distributor_warehouse",
          component: WarehouseComponent,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'bank-df',
          name: "detail_distributor_bank_df",
          component: BankDfComponent,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'detailfarmergroup',
          name: "detail_farmergroup_component",
          component: KelompokTaniComponent,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'detailfarmer',
          name: "detail_farmer_component",
          component: PetaniTable,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        }
      ],
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-pengecer",
      name: "master_retail",
      component: RetailMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: '/:access/retail-distributor-relation',
      name: "retail_distributor_relation",
      component: MasterRelasi,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: '/:access/retail-distributor-relation/form',
      name: "form_retail_distributor_relation",
      component: FormRelasi,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: '/:access/retail-distributor-relation/form/:id',
      name: "form_retail_distributor_relation_customer",
      component: FormRelasi,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-pricing",
      name: "master_pricing",
      component: PricingMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-warehouse",
      name: "master_warehouse",
      component: WarehouseMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all",
      name: "master_user_all",
      component: UserMasterAll,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form",
      name: "user_form",
      component: FormUser,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form-new-register",
      name: "user_form_new_register",
      component: FormUserPenggunaBaru,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form-new-register/:id",
      name: "user_form_new_register_edit",
      component: FormUserPenggunaBaru,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form-distributor",
      name: "user_form_distributor",
      component: FormUserDistributor,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form-distributor/:id",
      name: "user_form_distributor_edit",
      component: FormUserDistributor,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-user-all/user-form/:id",
      name: "user_form_edit",
      component: FormUser,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-roles",
      name: "master_roles",
      component: MasterRole,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-permissions",
      name: "master_permissions",
      component: MasterPermissions,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-menu",
      name: "master_menu",
      component: MenuMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-menu/menu-form",
      name: "menu_form",
      component: MenuForm,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-menu/menu-form/:id",
      name: "menu_form_edit",
      component: MenuForm,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-route",
      name: "master_routes",
      component: MasterRoutes,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/management-menu-privilege",
      name: "privilege_menu",
      component: ListPrivilegeMenu,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/management-router-privilege",
      name: "privilege_router",
      component: ListPrivilegeRouter,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/pengecer-produsen",
      name: "pengecer_produsen",
      component: PengecerProdusen,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/sales-area-master",
      name: "sales_area",
      component: SalesareaMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/farmer-group-master",
      name: "farmergroup_master",
      component: KelompokTani,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-petani",
      name: "master_petani",
      component: PetaniMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-subtitution",
      name: "master_subtitution",
      component: SubtitutionMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-supply-point",
      name: "supply_point",
      component: MasterSupplyPoint,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/master-supply-point",
      name: "data_permentan",
      component: MasterSupplyPoint,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/rdkk",
      name: "rdkk",
      component: RdkkMaster,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/rdkk/detail/:id",
      name: "rdkk_detail",
      component: RdkkDetail,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/rdkk/approval",
      name: "rdkk_approval",
      component: RdkkApproval,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/order",
      name: "rencana_penebusan",
      component: Permintaan,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/order/detail/null/step-1",
      name: "penebusan_baru",
      component: PermintaanDetail,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/order/detail/:id",
      component: PenebusanDetail,
      children: [
        {
          path: '',
          name: "permintaan_penebusan",
          component: Penebusan,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'step-2',
          name: "item_penebusan",
          component: ItemPenebusan,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'step-3',
          name: "checkout_penebusan",
          component: CheckoutOrder,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
        {
          path: 'step-3/change-plant',
          name: "checkout_penebusan_change_plant",
          component: CheckoutOrder,
          meta: {
            auth: true,
            progress: {
              func: [
                { call: 'color', modifier: 'temp', argument: '#fff' },
                { call: 'fail', modifier: 'temp', argument: '#fff' },
                { call: 'location', modifier: 'temp', argument: 'top' },
                { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
              ]
            }
          }
        },
      ],
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/monitoring-order",
      name: "monitoring_order",
      component: MonitoringOrder,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/monitoring-do",
      name: "monitoring_do",
      component: MonitoringDo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/monitoring-df",
      name: "monitoring_df",
      component: MonitoringDf,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/penyaluran-do",
      name: "penyaluran_do",
      component: PenyaluranDo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/penyaluran-do/detail/:uuid",
      name: "penyaluran_do_detail",
      component: DetailPenyaluranDo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/list-penyaluran-do/detail/:uuid/:number_order",
      name: "list_penyaluran_do_detail",
      component: ListDetailDo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/item-penyaluran/",
      name: "item_penyaluran",
      component: ItemPenyaluran,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/laporan-bulanan-distributor/",
      name: "laporan_bulanan_distributor",
      component: LaporanBulananDistributor,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/laporan-bulanan-distributor/detail/:uuid",
      name: "detail_laporan_bulanan_distributor",
      component: DetailLaporanBulananDistributor,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/manual-approved/",
      name: "manual_approved",
      component: ManualApproved,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/transaksi/manual-approved/setting/",
      name: "manual_approved_setting",
      component: ManualApprovedSetting,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f5/",
      name: "initial_stok_f5",
      component: InitialStokf5,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f5/detail-f5/:id",
      name: "penyaluran_initial_stok_f5",
      component: PenyaluranInitialStokf5,
      meta: {
        auth: true,
        title: 'Initial Stok F5',
        type: 'F5',
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f5/detail/:id",
      name: "detail_initial_stok_f5",
      component: DetailInitialStokf5,
      meta: {
        auth: true,
        title: 'Initial Stok F5',
        type: 'F5',
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f6/detail-f6/:id",
      name: "penyaluran_initial_stok_f6",
      component: PenyaluranInitialStokf5,
      meta: {
        auth: true,
        title: 'Initial Stok F6',
        type: 'F6',
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f6/",
      name: "initial_stok_f6",
      component: InitialStokf6,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f6/item/:uuid",
      name: "item_initial_stok_f6",
      component: ItemInitialF6,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/initial-stok-f6/detail/:uuid",
      name: "detail_initial_stok_f6",
      component: DetailInitialStokF6,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/laporan-bulanan-pengecer/",
      name: "rekapitulasi_laporan_bulanan_pengecer",
      component: LaporanBulananPengecer,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/laporan-bulanan-pengecer/detail/:uuid",
      name: "detail_laporan_bulanan_pengecer",
      component: DetailLaporanBulananPengecer,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/sync",
      name: "sync",
      component: Sync,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/user-guide",
      name: "user_guide",
      component: UserGuide,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/bank-account",
      name: "bank_account",
      component: BankAccount,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/log-order",
      name: "log_order",
      component: LogOrder,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/batas-ambil",
      name: "batas_ambil",
      component: BatasAmbil,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/log-host-to-host",
      name: "log_host_to_host",
      component: LogHost,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/periode-tanam",
      name: "periode_tanam",
      component: PeriodeTanam,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/desa",
      name: "desa",
      component: Desa,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/matrix-rdkk",
      name: "matrix_rdkk",
      component: MatrixRdkk,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/outstanding-so/",
      name: "outstanding_so",
      component: OutstandingSo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/header-laporan-so/",
      name: "header_laporan_so",
      component: HeaderLaporanSo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/level-laporan-so/",
      name: "lavel_laporan_so",
      component: LevelLaporanSoIndex,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/surat-jalan/",
      name: "surat_jalan",
      component: SuratJalan,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/laporan/surat-jalan/detail/:uuid",
      name: "surat_jalan_detail",
      component: DetailSuratJalan,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/user-profile",
      name: "user_profile",
      component: Profile,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    // change request CR
    {
      path: "/:access/alokasi/alokasi-provinsi",
      name: "alokasi_provinsi",
      component: AlokasiProvinsi,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-provinsi/detail/:uuid/:year",
      name: "alokasi_provinsi_detail",
      component: AlokasiProvinsiDetail,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-provinsi/detail/edit-table/:uuid/:year",
      name: "alokasi_provinsi_edit",
      component: AlokasiProvinsiEdit,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/asal",
      name: "spjb_asal",
      component: SpjbAsalCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/asal/detail/:uuid/:id",
      name: "detail_spjb_asal",
      component: DetailSpjbAsalCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/asal/detail/edit-table/:uuid/:id",
      name: "edit_spjb_asal",
      component: EditSpjbAsalCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/operasional",
      name: "spjb_operasional",
      component: SpjbOpCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/operasional/detail/:uuid/:id",
      name: "detail_spjb_operasional",
      component: DetailSpjbOpCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/spjb/operasional/detail/edit-table/:uuid/:id",
      name: "edit_spjb_operasional",
      component: EditSpjbOprCR,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kecamatan",
      name: "alokasi_kecamatan",
      component: AlokasiKecamatan,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kecamatan/detail/:uuid/:year",
      name: "alokasi_kecamatan_detail",
      component: AlokasiKecamatanDetail,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kecamatan/detail/edit-table/:uuid/:year",
      name: "alokasi_kecamatan_edit",
      component: AlokasiKecamatanEdit,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kabupaten",
      name: "alokasi_kabupaten",
      component: AlokasiKabupaten,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kabupaten/:id",
      name: "alokasi_kabupaten_detail",
      component: AlokasiKabupatenNo,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/alokasi/alokasi-kabupaten/detail/edit-table/:uuid",
      name: "alokasi_kabupaten_edit",
      component: AlokasiKabupatenEdit,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    {
      path: "/:access/lintas-alur",
      name: "lintas_alur",
      component: LintasAlur,
      meta: {
        auth: true,
        progress: {
          func: [
            { call: 'color', modifier: 'temp', argument: '#fff' },
            { call: 'fail', modifier: 'temp', argument: '#fff' },
            { call: 'location', modifier: 'temp', argument: 'top' },
            { call: 'transition', modifier: 'temp', argument: { speed: '5s', opacity: '0.9s', termination: 400 } }
          ]
        }
      }
    },
    // change request CR
    {
      path: "*",
      name: "0",
      component: {
        template: '<center><H1 style="text-align=center">Page Not Found</h1></center>'
      },
      meta: {}
    }
  ]
})
