// library
require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueProgressBar from 'vue-progressbar';


import  router from './config/router';
import App from './components/main/App.vue';
import './config/lang/ml';
//helper
import { languageHelper } from './helpers/language';
//environment
import { wcmBaseUrl } from './config/environment';
//encrypt
import { encryptHelper } from './helpers/encryption';
//date
import { dateHelper } from './helpers/date';
// resetFilter
import { filterHelper } from './helpers/resetFilter';


const options = {
  color: '#fff',
  failedColor: '#fff',
  thickness: '8px',
  transition: {
    speed: '5s',
    opacity: '0.9s',
    termination: 300
  },
  autoRevert: true,
  location: 'left',
  inverse: false
}

var lang = (localStorage.getItem('app-lang') == undefined) ? 'id' : localStorage.getItem('app-lang');
localStorage.setItem('app-lang', lang);

Vue.use(VueProgressBar, options);
Vue.use(VueAxios, axios);
Vue.router = router;

const baseUrl = wcmBaseUrl();
axios.defaults.baseURL = baseUrl;
axios.defaults.headers.common['Language'] = lang;
axios.defaults.timeout = 1200000;

Vue.use(require('@websanova/vue-auth'),{
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  loginData: {url: baseUrl+'/login', method: 'POST'},
	logoutData: {url: baseUrl+'/logout', method: 'POST', redirect: '/', makeRequest: true, success: ()=>{  
    // localStorage.clear() 
    localStorage.removeItem('access-control');
  }},
	refreshData: {url: baseUrl+'/user/refresh', method: 'GET', enabled: false},
	fetchData: {url: baseUrl+'/user/fetch', method: 'GET', enabled: true,  authType: 'bearer'},
  notFoundRedirect: { path: '/home' },
});
languageHelper;
encryptHelper;
dateHelper;
filterHelper;
App.router = Vue.router;
App.router = Vue.router;
const app = new Vue({
  data: { loadingWayang: false },
  router,
  render: h => h(App)
}).$mount('#app');

router.beforeEach((to, from, next) => {
  app.loadingWayang = true
	next()
})

router.afterEach((to, from, next) => {
  setTimeout(() => app.loadingWayang = false, 500) // timeout for demo purposes
})
